/*
 * MultiParticleClusterDataCall.h
 *
 * Modification of MultiParticleDataCall.h by Julia B�hnke
 * Copyright (C) 2009 by Universitaet Stuttgart (VISUS). 
 * Alle Rechte vorbehalten.
 */

#ifndef MEGAMOLVPC_MULTIPARTICLECLUSTERDATACALL_H_INCLUDED
#define MEGAMOLVPC_MULTIPARTICLECLUSTERDATACALL_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/moldyn/AbstractParticleDataCall.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"
#include "mmcore/factories/CallAutoDescription.h"
#include "vislib/assert.h"
#include "vislib/Map.h"
#include "vislib/Array.h"
#include "vector"


namespace megamol {
namespace vis_particle_clustering {

	/**
     * Class holding all data of a single particle type
     *
     */
    class ClusteredSphericalParticles {
    public:
        /** Ctor */
        ClusteredSphericalParticles(void);

        /**
         * Copy ctor
         *
         * @param src The object to clone from
         */
        ClusteredSphericalParticles(const ClusteredSphericalParticles& src);

        /** Dtor */
        ~ClusteredSphericalParticles(void);

        /**
         * Answer the colour data type
         *
         * @return The colour data type
         */
		inline core::moldyn::SimpleSphericalParticles::ColourDataType GetColourDataType(void) const {
            return this->colDataType;
        }

        /**
         * Answer the colour data pointer
         *
         * @return The colour data pointer
         */
        inline const void * GetColourData(void) const {
            return this->colPtr;
        }

        /**
         * Answer the colour data stride
         *
         * @return The colour data stride
         */
        inline unsigned int GetColourDataStride(void) const {
            return this->colStride;
        }

        /**
         * Answer the number of stored objects
         *
         * @return The number of stored objects
         */
        inline UINT64 GetCount(void) const {
            return this->count;
        }

        /**
         * Answer the global colour
         *
         * @return The global colour as a pointer to four unsigned bytes
         *         storing the RGBA colour components
         */
        inline const unsigned char * GetGlobalColour(void) const {
            return this->col;
        }

        /**
         * Answer the global radius
         *
         * @return The global radius
         */
        inline float GetGlobalRadius(void) const {
            return this->radius;
        }

        /**
         * Answer the global particle type
         *
         * @return the global type
         */
        inline unsigned int GetGlobalType(void) const {
            return this->particleType;
        }

        /**
         * Answer the maximum colour index value to be mapped
         *
         * @return The maximum colour index value to be mapped
         */
        inline float GetMaxColourIndexValue(void) const {
            return this->maxColI;
        }

        /**
         * Answer the minimum colour index value to be mapped
         *
         * @return The minimum colour index value to be mapped
         */
        inline float GetMinColourIndexValue(void) const {
            return this->minColI;
        }

        /**
         * Answer the vertex data type
         *
         * @return The vertex data type
         */
        inline core::moldyn::SimpleSphericalParticles::VertexDataType GetVertexDataType(void) const {
            return this->vertDataType;
        }

        /**
         * Answer the vertex data pointer
         *
         * @return The vertex data pointer
         */
        inline const void * GetVertexData(void) const {
            return this->vertPtr;
        }

        /**
         * Answer the vertex data stride
         *
         * @return The vertex data stride
         */
        inline unsigned int GetVertexDataStride(void) const {
            return this->vertStride;
        }

		/**
         * Answer the cluster data pointer
         *
         * @return The cluster data pointer
         */
        inline const void * GetClusterData(void) const {
            return this->clusterPtr;
        }

        /**
         * Answer the vertex data stride
         *
         * @return The vertex data stride
         */
        inline unsigned int GetClusterDataStride(void) const {
            return this->clusterStride;
        }

		/**
         * Answer the number of clusters
         *
         * @return The number of stored objects
         */
        inline unsigned int GetNumberClusters(void) const {
            return this->numberClusters;
        }

        /**
         * Sets the colour data
         *
         * @param t The type of the colour data
         * @param p The pointer to the colour data (must not be NULL if t
         *          is not 'COLDATA_NONE'
         * @param s The stride of the colour data
         */
        void SetColourData(core::moldyn::SimpleSphericalParticles::ColourDataType t, const void *p,
                unsigned int s = 0) {
        //    ASSERT((p != NULL) || (t == COLDATA_NONE));
            this->colDataType = t;
            this->colPtr = p;
            this->colStride = s;
        }

        /**
         * Sets the colour map index values
         *
         * @param minVal The minimum colour index value to be mapped
         * @param maxVal The maximum colour index value to be mapped
         */
        void SetColourMapIndexValues(float minVal, float maxVal) {
            this->maxColI = maxVal;
            this->minColI = minVal;
        }

        /**
         * Sets the number of objects stored and resets all data pointers!
         *
         * @param cnt The number of stored objects
         */
        void SetCount(UINT64 cnt) {
            this->colDataType = core::moldyn::SimpleSphericalParticles::ColourDataType::COLDATA_NONE;
            this->colPtr = NULL; // DO NOT DELETE
            this->vertDataType = core::moldyn::SimpleSphericalParticles::VertexDataType::VERTDATA_NONE;
            this->vertPtr = NULL; // DO NOT DELETE

            this->count = cnt;
        }

        /**
         * Sets the global colour data
         *
         * @param r The red colour component
         * @param g The green colour component
         * @param b The blue colour component
         * @param a The opacity alpha
         */
        void SetGlobalColour(unsigned int r, unsigned int g,
                unsigned int b, unsigned int a = 255) {
            this->col[0] = r;
            this->col[1] = g;
            this->col[2] = b;
            this->col[3] = a;
        }

        /**
         * Sets the global radius
         *
         * @param r The global radius
         */
        void SetGlobalRadius(float r) {
            this->radius = r;
        }

        /**
         * Sets the global particle type
         *
         * @param t The global type
         */
        void SetGlobalType(unsigned int t) {
            this->particleType = t;
        }

        /**
         * Sets the vertex data
         *
         * @param t The type of the vertex data
         * @param p The pointer to the vertex data (must not be NULL if t
         *          is not 'VERTDATA_NONE'
         * @param s The stride of the vertex data
         */
        void SetVertexData(core::moldyn::SimpleSphericalParticles::VertexDataType t, const void *p,
                unsigned int s = 0) {
            ASSERT(this->disabledNullChecks || (p != NULL) || (t == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE));
            this->vertDataType = t;
            this->vertPtr = p;
            this->vertStride = s;
        }

		/**
         * Sets the number of clusters stored and resets the cluster data pointer!
         *
         * @param cnt The number of clusters
         */
        void SetNumberClusters(unsigned int cnt) {
			this->clusterPtr = NULL; // DO NOT DELETE

			this->numberClusters = cnt;
        }

		/**
         * Sets the cluster data
         *
         * @param p The pointer to the cluster data
         * @param s The stride of the cluster data
         */
        void SetClusterData(const void *p, unsigned int s = 0) {
            ASSERT(this->disabledNullChecks || (p != NULL));
			this->clusterPtr = p;
            this->clusterStride = s;
        }

        /**
         * Assignment operator
         *
         * @param rhs The right hand side operand
         *
         * @return A reference to 'this'
         */
        ClusteredSphericalParticles& operator=(const ClusteredSphericalParticles& rhs);

        /**
         * Test for equality
         *
         * @param rhs The right hand side operand
         *
         * @return 'true' if 'this' and 'rhs' are equal.
         */
        bool operator==(const ClusteredSphericalParticles& rhs) const;

		/**
         * Disable NULL-checks in case we have an OpenGL-VAO
         * @param disable flag to disable/enable the checks
         */
		void disableNullChecksForVAOs(bool disable = true)
		{
			disabledNullChecks = disable;
		}
		
		/**
		* Defines wether we transport VAOs instead of real data
		* @param vao flag to disable/enable the checks
		*/
		void SetIsVAO(bool vao)
		{
			this->isVAO = vao;
		}

		/**
		* Disable NULL-checks in case we have an OpenGL-VAO
		* @param disable flag to disable/enable the checks
		*/
		bool IsVAO()
		{
			return this->isVAO;
		}

		/**
		* Sets the VertexArrayObject, VertexBuffer and ColorBuffer used
		*/
		void SetVAOs(unsigned int vao, unsigned int vb, unsigned int cb)
		{
			this->glVAO = vao;
			this->glVB = vb;
			this->glCB = cb;
		}

		/**
		* Gets the VertexArrayObject, VertexBuffer and ColorBuffer used
		*/
		void GetVAOs(unsigned int &vao, unsigned int &vb, unsigned int &cb)
		{
			vao = this->glVAO;
			vb = this->glVB;
			cb = this->glCB;
		}

    private:

        /** The global colour */
        unsigned char col[4];

        /** The colour data type */
        core::moldyn::SimpleSphericalParticles::ColourDataType colDataType;

        /** The colour data pointer */
        const void *colPtr;

        /** The colour data stride */
        unsigned int colStride;

        /** The number of objects stored */
        UINT64 count;

        /** The maximum colour index value to be mapped */
        float maxColI;

        /** The minimum colour index value to be mapped */
        float minColI;

        /** The global radius */
        float radius;

        /** The global type of particles in the list */
        unsigned int particleType;

        /** The vertex data type */
        core::moldyn::SimpleSphericalParticles::VertexDataType vertDataType;

        /** The vertex data pointer */
        const void *vertPtr;

        /** The vertex data stride */
        unsigned int vertStride;
		
		/** disable NULL-checks if used with OpenGL-VAO */
		bool disabledNullChecks;

		/** do we use a VertexArrayObject? */
		bool isVAO;

		/** Vertex Array Object to transport */
		unsigned int glVAO;
		/** Vertex Buffer to transport */
		unsigned int glVB;
		/** Color Buffer to transport */
		unsigned int glCB;

		/** The cluster data pointer */
        const void *clusterPtr;

		/** The cluster data stride */
        unsigned int clusterStride;

		/** The number of clusters */
        unsigned int numberClusters;
    };

} /* end namespace vis_particle_clustering */

namespace core{
namespace moldyn {
	template class AbstractParticleDataCall<vis_particle_clustering::ClusteredSphericalParticles>;
} /* end namespace moldyn */
} /* end namespace core */

namespace vis_particle_clustering {

    /**
     * Call for multi-stream particle data.
     */
	class MultiParticleClusterDataCall: public core::moldyn::AbstractParticleDataCall<ClusteredSphericalParticles> {
    public:
        /** typedef for legacy name */
        typedef ClusteredSphericalParticles Particles;

        /**
         * Answer the name of the objects of this description.
         *
         * @return The name of the objects of this description.
         */
        static const char *ClassName(void) {
            return "MultiParticleClusterDataCall";
        }

        /** Ctor. */
        MultiParticleClusterDataCall(void);

        /** Dtor. */
        virtual ~MultiParticleClusterDataCall(void);

        /**
         * Assignment operator.
         * Makes a deep copy of all members. While for data these are only
         * pointers, the pointer to the unlocker object is also copied.
         *
         * @param rhs The right hand side operand
         *
         * @return A reference to this
         */
        MultiParticleClusterDataCall& operator=(const MultiParticleClusterDataCall& rhs);

		/** Set the ID of first frame */
		void SetFirstFrame(int firstFrame) {
			this->firstFrame = firstFrame;
        }

		/** Set the ID of last frame */
		void SetLastFrame(int lastFrame) {
			this->lastFrame = lastFrame;
        }

		/** Set the step between frames */
		void SetFrameStep(int frameStep) {
			this->frameStep = frameStep;
        }

		/**
         * Answer the ID of the first frame
         *
         * @return The ID of the first frame
         */
		inline int GetFirstFrame() {
			return this->firstFrame;
        }

		/**
         * Answer the ID of the last frame
         *
         * @return The ID of the last frame
         */
		inline int GetLastFrame() {
			return this->lastFrame;
        }

		/**
         * Answer the step between frames
         *
         * @return The step between frames
         */
		inline int GetFrameStep() {
			return this->frameStep;
        }

	private:
		/** The ID of the first frame of a trajectory (or -1 for particle clustering) */
		int firstFrame;
		
		/** The ID of the last frame of a trajectory (or -1 for particle clustering) */
		int lastFrame;

		/** The step between frames of a trajectory (or -1 for particle clustering) */
		int frameStep;

    };


    /** Description class typedef */
    typedef core::factories::CallAutoDescription<MultiParticleClusterDataCall>
        MultiParticleClusterDataCallDescription;



} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLCORE_MULTIPARTICLECLUSTERDATACALL_H_INCLUDED */