/*
 * MultiParticleDataCallExtended.h
 *
 * Modification of MultiParticleDataCall.h by Julia B�hnke
 * Copyright (C) 2009 by Universitaet Stuttgart (VISUS). 
 * Alle Rechte vorbehalten.
 */

#ifndef MEGAMOLVPC_MULTIPARTICLEDATACALLEXTENDED_H_INCLUDED
#define MEGAMOLVPC_MULTIPARTICLEDATACALLEXTENDED_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/moldyn/AbstractParticleDataCall.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"
#include "mmcore/factories/CallAutoDescription.h"
#include "vislib/assert.h"
#include "vislib/Map.h"
#include "vislib/Array.h"
#include "vector"


namespace megamol {
namespace vis_particle_clustering {

    /**
     * Class holding all data of a single particle type
     *
     */
    class SphericalParticles {
    public:
        /** Ctor */
        SphericalParticles(void);

        /**
         * Copy ctor
         *
         * @param src The object to clone from
         */
        SphericalParticles(const SphericalParticles& src);

        /** Dtor */
        ~SphericalParticles(void);

        /**
         * Answer the colour data type
         *
         * @return The colour data type
         */
        inline core::moldyn::SimpleSphericalParticles::ColourDataType GetColourDataType(void) const {
            return this->colDataType;
        }

        /**
         * Answer the colour data pointer
         *
         * @return The colour data pointer
         */
        inline const void * GetColourData(void) const {
            return this->colPtr;
        }

        /**
         * Answer the colour data stride
         *
         * @return The colour data stride
         */
        inline unsigned int GetColourDataStride(void) const {
            return this->colStride;
        }

        /**
         * Answer the number of stored objects
         *
         * @return The number of stored objects
         */
        inline UINT64 GetCount(void) const {
            return this->count;
        }

        /**
         * Answer the global colour
         *
         * @return The global colour as a pointer to four unsigned bytes
         *         storing the RGBA colour components
         */
        inline const unsigned char * GetGlobalColour(void) const {
            return this->col;
        }

        /**
         * Answer the global radius
         *
         * @return The global radius
         */
        inline float GetGlobalRadius(void) const {
            return this->radius;
        }

		/**
         * Answer the global rho
         *
         * @return The global rho
         */
        inline float GetGlobalRho(void) const {
            return this->rho;
        }

		/**
         * Answer the global time
         *
         * @return The global time
         */
        inline float GetGlobalTime(void) const {
            return this->time;
        }		

        /**
         * Answer the global particle type
         *
         * @return the global type
         */
        inline unsigned int GetGlobalType(void) const {
            return this->particleType;
        }

        /**
         * Answer the maximum colour index value to be mapped
         *
         * @return The maximum colour index value to be mapped
         */
        inline float GetMaxColourIndexValue(void) const {
            return this->maxColI;
        }

        /**
         * Answer the minimum colour index value to be mapped
         *
         * @return The minimum colour index value to be mapped
         */
        inline float GetMinColourIndexValue(void) const {
            return this->minColI;
        }

        /**
         * Answer the vertex data type
         *
         * @return The vertex data type
         */
        inline core::moldyn::SimpleSphericalParticles::VertexDataType GetVertexDataType(void) const {
            return this->vertDataType;
        }

        /**
         * Answer the vertex data pointer
         *
         * @return The vertex data pointer
         */
        inline const void * GetVertexData(void) const {
            return this->vertPtr;
        }

        /**
         * Answer the vertex data stride
         *
         * @return The vertex data stride
         */
        inline unsigned int GetVertexDataStride(void) const {
            return this->vertStride;
        }

		/**
         * Answer the extraMember data pointer (uc, vc, wc, o1, o2, o3) + 6 extra
         *
         * @return The extraMember data pointer
         */
        inline const void * GetExtraMemberData(void) const {
            return this->extraMemberPtr;
        }

        /**
         * Answer the extraMember data stride
         *
         * @return The extraMember data stride
         */
        inline unsigned int GetExtraMemberDataStride(void) const {
            return this->extraMemberStride;
        }

		/**
         * Answer the minimum velocities
         *
         * @return The minimum velocities
         */
		const std::vector<float>& GetMinVelocity() {
			return this->minVelocity;
		}

		/**
         * Answer the maximum velocities
         *
         * @return The maximum velocities
         */
		const std::vector<float>& GetMaxVelocity() {
			return this->maxVelocity;
		}

		/**
         * Answer the minimum length of velocity vector
         *
         * @return The minimum length of velocity vector
         */
		const float GetMinVelocityLength() {
			return this->minVelocityLength;
		}

		/**
         * Answer the maximum length of velocity vector
         *
         * @return The maximum length of velocity vector
         */
		const float GetMaxVelocityLength() {
			return this->maxVelocityLength;
		}

		/**
         * Answer the minimum rotation angle
         *
         * @return The minimum rotation angle
         */
		const float GetMinRotationAngle() {
			return this->minRotationAngle;
		}

		/**
         * Answer the maximum rotation angle
         *
         * @return The maximum rotation angle
         */
		const float GetMaxRotationAngle() {
			return this->maxRotationAngle;
		}

        /**
         * Sets the colour data
         *
         * @param t The type of the colour data
         * @param p The pointer to the colour data (must not be NULL if t
         *          is not 'COLDATA_NONE'
         * @param s The stride of the colour data
         */
        void SetColourData(core::moldyn::SimpleSphericalParticles::ColourDataType t, const void *p,
                unsigned int s = 0) {
        //    ASSERT((p != NULL) || (t == COLDATA_NONE));
            this->colDataType = t;
            this->colPtr = p;
            this->colStride = s;
        }

        /**
         * Sets the colour map index values
         *
         * @param minVal The minimum colour index value to be mapped
         * @param maxVal The maximum colour index value to be mapped
         */
        void SetColourMapIndexValues(float minVal, float maxVal) {
            this->maxColI = maxVal;
            this->minColI = minVal;
        }

        /**
         * Sets the number of objects stored and resets all data pointers!
         *
         * @param cnt The number of stored objects
         */
        void SetCount(UINT64 cnt) {
            this->colDataType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE;
            this->colPtr = NULL; // DO NOT DELETE
            this->vertDataType = core::moldyn::SimpleSphericalParticles::VERTDATA_NONE;
            this->vertPtr = NULL; // DO NOT DELETE

            this->count = cnt;
        }

        /**
         * Sets the global colour data
         *
         * @param r The red colour component
         * @param g The green colour component
         * @param b The blue colour component
         * @param a The opacity alpha
         */
        void SetGlobalColour(unsigned int r, unsigned int g,
                unsigned int b, unsigned int a = 255) {
            this->col[0] = r;
            this->col[1] = g;
            this->col[2] = b;
            this->col[3] = a;
        }

        /**
         * Sets the global radius
         *
         * @param r The global radius
         */
        void SetGlobalRadius(float r) {
            this->radius = r;
        }

		/**
         * Sets the global rho
         *
         * @param rho The global rho
         */
        void SetGlobalRho(float rho) {
            this->rho = rho;
        }

		/**
         * Sets the globaltime
         *
         * @param time The global time
         */
        void SetGlobalTime(float time) {
            this->time = time;
        }


        /**
         * Sets the global particle type
         *
         * @param t The global type
         */
        void SetGlobalType(unsigned int t) {
            this->particleType = t;
        }

        /**
         * Sets the vertex data
         *
         * @param t The type of the vertex data
         * @param p The pointer to the vertex data (must not be NULL if t
         *          is not 'VERTDATA_NONE'
         * @param s The stride of the vertex data
         */
        void SetVertexData(core::moldyn::SimpleSphericalParticles::VertexDataType t, const void *p,
                unsigned int s = 0) {
            ASSERT(this->disabledNullChecks || (p != NULL) || (t == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE));
            this->vertDataType = t;
            this->vertPtr = p;
            this->vertStride = s;
        }

		/**
         * Sets the extraMember data
         *
         * @param t The type of the extraMember data
         * @param p The pointer to the extraMember data
         * @param s The stride of the extraMember data
         */
        void SetExtraMemberData(const void *p,
                unsigned int s = 0) {
            ASSERT(this->disabledNullChecks || (p != NULL));
            this->extraMemberPtr = p;
            this->extraMemberStride = s;
        }

		/**
         * Sets the minimum velocities
         *
         * @param minVelocity The minimum velocities
         */
		void SetMinVelocity(std::vector<float> minVelocity) {
			this->minVelocity = minVelocity;
		}

		/**
         * Sets the maximum velocities
         *
         * @param maxVelocity The maximum velocities
         */
		void SetMaxVelocity(std::vector<float> maxVelocity) {
			this->maxVelocity = maxVelocity;
		}

		/**
         * Sets the minimum length of velocity vector
         *
         * @param minVelocityLength The minimum length of velocity vector
         */
		void SetMinVelocityLength(float minVelocityLength) {
			this->minVelocityLength = minVelocityLength;
		}

		/**
         * Sets the maximum length of velocity vector
         *
         * @param maxVelocityLength The maximum length of velocity vector
         */
		void SetMaxVelocityLength(float maxVelocityLength) {
			this->maxVelocityLength = maxVelocityLength;
		}

		/**
         * Sets the minimum rotation angle
         *
         * @param minAngle The minimum rotation angle
         */
		void SetMinRotationAngle(float minAngle) {
			this->minRotationAngle = minAngle;
		}

		/**
         * Sets the maximum rotation angle
         *
         * @param maxAngle The maximum rotation angle
         */
		void SetMaxRotationAngle(float maxAngle) {
			this->maxRotationAngle = maxAngle;
		}

        /**
         * Assignment operator
         *
         * @param rhs The right hand side operand
         *
         * @return A reference to 'this'
         */
        SphericalParticles& operator=(const SphericalParticles& rhs);

        /**
         * Test for equality
         *
         * @param rhs The right hand side operand
         *
         * @return 'true' if 'this' and 'rhs' are equal.
         */
        bool operator==(const SphericalParticles& rhs) const;

		/**
         * Disable NULL-checks in case we have an OpenGL-VAO
         * @param disable flag to disable/enable the checks
         */
		void disableNullChecksForVAOs(bool disable = true)
		{
			disabledNullChecks = disable;
		}
		
		/**
		* Defines wether we transport VAOs instead of real data
		* @param vao flag to disable/enable the checks
		*/
		void SetIsVAO(bool vao)
		{
			this->isVAO = vao;
		}

		/**
		* Disable NULL-checks in case we have an OpenGL-VAO
		* @param disable flag to disable/enable the checks
		*/
		bool IsVAO()
		{
			return this->isVAO;
		}

		/**
		* Sets the VertexArrayObject, VertexBuffer and ColorBuffer used
		*/
		void SetVAOs(unsigned int vao, unsigned int vb, unsigned int cb)
		{
			this->glVAO = vao;
			this->glVB = vb;
			this->glCB = cb;
		}

		/**
		* Gets the VertexArrayObject, VertexBuffer and ColorBuffer used
		*/
		void GetVAOs(unsigned int &vao, unsigned int &vb, unsigned int &cb)
		{
			vao = this->glVAO;
			vb = this->glVB;
			cb = this->glCB;
		}

    private:

        /** The global colour */
        unsigned char col[4];

        /** The colour data type */
        core::moldyn::SimpleSphericalParticles::ColourDataType colDataType;

        /** The colour data pointer */
        const void *colPtr;

        /** The colour data stride */
        unsigned int colStride;

        /** The number of objects stored */
        UINT64 count;

        /** The maximum colour index value to be mapped */
        float maxColI;

        /** The minimum colour index value to be mapped */
        float minColI;

        /** The global radius */
        float radius;

		/** The global rho */
        float rho;

		/** The global time */
        float time;

        /** The global type of particles in the list */
        unsigned int particleType;

        /** The vertex data type */
        core::moldyn::SimpleSphericalParticles::VertexDataType vertDataType;

        /** The vertex data pointer */
        const void *vertPtr;

        /** The vertex data stride */
        unsigned int vertStride;

		/** The extraMember data pointer */
        const void *extraMemberPtr;

        /** The data stride for extraMember*/
        unsigned int extraMemberStride;

		/** The minimum velocities */
		std::vector<float> minVelocity;

		/** The maximum velocities */
		std::vector<float> maxVelocity;

		/** The minimum length of velocity vector */
		float minVelocityLength;

		/** The maximum length of velocity vector */
		float maxVelocityLength;

		/** The minimum of the rotation angle */
		float minRotationAngle;

		/** The maximum of the rotation angle */
		float maxRotationAngle;
		
		/** disable NULL-checks if used with OpenGL-VAO */
		bool disabledNullChecks;

		/** do we use a VertexArrayObject? */
		bool isVAO;

		/** Vertex Array Object to transport */
		unsigned int glVAO;
		/** Vertex Buffer to transport */
		unsigned int glVB;
		/** Color Buffer to transport */
		unsigned int glCB;
    };

} /* end namespace vis_particle_clustering */

namespace core{
namespace moldyn {
	template class AbstractParticleDataCall<vis_particle_clustering::SphericalParticles>;
} /* end namespace moldyn */
} /* end namespace core */

namespace vis_particle_clustering {

    /**
     * Call for multi-stream particle data.
     */
    class MultiParticleDataCallExtended
        : public core::moldyn::AbstractParticleDataCall<SphericalParticles> {
    public:
        /** typedef for legacy name */
        typedef SphericalParticles Particles;

        /**
         * Answer the name of the objects of this description.
         *
         * @return The name of the objects of this description.
         */
        static const char *ClassName(void) {
            return "MultiParticleDataCallExtended";
        }

        /** Ctor. */
        MultiParticleDataCallExtended(void);

        /** Dtor. */
        virtual ~MultiParticleDataCallExtended(void);

        /**
         * Assignment operator.
         * Makes a deep copy of all members. While for data these are only
         * pointers, the pointer to the unlocker object is also copied.
         *
         * @param rhs The right hand side operand
         *
         * @return A reference to this
         */
        MultiParticleDataCallExtended& operator=(const MultiParticleDataCallExtended& rhs);

		/**
         * Sets the minimum velocities
         *
         * @param minVelocity The minimum velocities
         */
		void SetMinVelocity(std::vector<float> minVelocity) {
			this->minVelocity = minVelocity;
		}

		/**
         * Sets the maximum velocities
         *
         * @param maxVelocity The maximum velocities
         */
		void SetMaxVelocity(std::vector<float> maxVelocity) {
			this->maxVelocity = maxVelocity;
		}

		/**
         * Sets the minimum length of velocity vector
         *
         * @param minVelocityLength The minimum length of velocity vector
         */
		void SetMinVelocityLength(float minVelocityLength) {
			this->minVelocityLength = minVelocityLength;
		}

		/**
         * Sets the maximum length of velocity vector
         *
         * @param maxVelocityLength The maximum length of velocity vector
         */
		void SetMaxVelocityLength(float maxVelocityLength) {
			this->maxVelocityLength = maxVelocityLength;
		}

		/**
         * Sets the minimum rotation angle
         *
         * @param minAngle The minimum rotation angle
         */
		void SetMinRotationAngle(float minAngle) {
			this->minRotationAngle = minAngle;
		}

		/**
         * Sets the maximum rotation angle
         *
         * @param maxAngle The maximum rotation angle
         */
		void SetMaxRotationAngle(float maxAngle) {
			this->maxRotationAngle = maxAngle;
		}

		/**
         * Answer the minimum velocities
         *
         * @return The minimum velocities
         */
		const std::vector<float>& GetMinVelocity() {
			return this->minVelocity;
		}

		/**
         * Answer the maximum velocities
         *
         * @return The maximum velocities
         */
		const std::vector<float>& GetMaxVelocity() {
			return this->maxVelocity;
		}

		/**
         * Answer the minimum length of velocity vector
         *
         * @return The minimum length of velocity vector
         */
		const float GetMinVelocityLength() {
			return this->minVelocityLength;
		}

		/**
         * Answer the maximum length of velocity vector
         *
         * @return The maximum length of velocity vector
         */
		const float GetMaxVelocityLength() {
			return this->maxVelocityLength;
		}

		/**
         * Answer the minimum rotation angle
         *
         * @return The minimum rotation angle
         */
		const float GetMinRotationAngle() {
			return this->minRotationAngle;
		}

		/**
         * Answer the maximum rotation angle
         *
         * @return The maximum rotation angle
         */
		const float GetMaxRotationAngle() {
			return this->maxRotationAngle;
		}

	private:
		/** The minimum velocities */
		std::vector<float> minVelocity;

		/** The maximum velocities */
		std::vector<float> maxVelocity;

		/** The minimum length of velocity vector */
		float minVelocityLength;

		/** The maximum length of velocity vector */
		float maxVelocityLength;

		/** The minimum of the rotation angle */
		float minRotationAngle;

		/** The maximum of the rotation angle */
		float maxRotationAngle;

    };


    /** Description class typedef */
    typedef core::factories::CallAutoDescription<MultiParticleDataCallExtended>
        MultiParticleDataCallExtendedDescription;



} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLCORE_MULTIPARTICLEDATACALLEXTENDED_H_INCLUDED */