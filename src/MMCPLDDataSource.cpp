/*
 * MMPLDDataSource.cpp
 *
 * Modification of MMPLDDataSource.cpp by Julia B�hnke
 * Copyright (C) 2010 by VISUS (Universitaet Stuttgart)
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "MMCPLDDataSource.h"
#include "mmcore/param/FilePathParam.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/CoreInstance.h"
#include "vislib/sys/Log.h"
#include "vislib/sys/MemmappedFile.h"
#include "vislib/String.h"
#include "vislib/sys/SystemInformation.h"

using namespace megamol;

/* defines for the frame cache size */
// minimum number of frames in the cache (2 for interpolation; 1 for loading)
#define CACHE_SIZE_MIN 3
// maximum number of frames in the cache (just a nice number)
#define CACHE_SIZE_MAX 1000
// factor multiplied to the frame size for estimating the overhead to the pure data.
#define CACHE_FRAME_FACTOR 1.15f

/*****************************************************************************/

/*
 * vis_particle_clustering::MMCPLDDataSource::Frame::Frame
 */
vis_particle_clustering::MMCPLDDataSource::Frame::Frame(core::view::AnimDataModule& owner)
        : core::view::AnimDataModule::Frame(owner), dat() {
    // intentionally empty
}


/*
 * vis_particle_clustering::MMPLDDataSource::Frame::~Frame
 */
vis_particle_clustering::MMCPLDDataSource::Frame::~Frame() {
    this->dat.EnforceSize(0);
}


/*
 * vis_particle_clustering::MMCPLDDataSource::Frame::LoadFrame
 */
bool vis_particle_clustering::MMCPLDDataSource::Frame::LoadFrame(vislib::sys::File *file, unsigned int idx, UINT64 size, unsigned int version) {
    this->frame = idx;
    this->fileVersion = version;
    this->dat.EnforceSize(static_cast<SIZE_T>(size));
    return (file->Read(this->dat, size) == size);
}


/*
 * vis_particle_clustering::MMPLDDataSource::Frame::SetData
 */
void vis_particle_clustering::MMCPLDDataSource::Frame::SetData(core::moldyn::MultiParticleDataCall& call) {
    if (this->dat.IsEmpty()) {
        call.SetParticleListCount(0);
        return;
    }

    SIZE_T p = sizeof(UINT32);
    UINT32 plc = *this->dat.As<UINT32>();
    call.SetParticleListCount(plc);
    for (UINT32 i = 0; i < plc; i++) {
        core::moldyn::MultiParticleDataCall::Particles &pts = call.AccessParticles(i);

        UINT8 vrtType = *this->dat.AsAt<UINT8>(p); p += 1;
        UINT8 colType = *this->dat.AsAt<UINT8>(p); p += 1;
        core::moldyn::MultiParticleDataCall::Particles::VertexDataType vrtDatType;
        core::moldyn::MultiParticleDataCall::Particles::ColourDataType colDatType;
        SIZE_T vrtSize = 0;
        SIZE_T colSize = 0;

        switch (vrtType) {
            case 0: vrtSize = 0; vrtDatType = core::moldyn::MultiParticleDataCall::Particles::VERTDATA_NONE; break;
            case 1: vrtSize = 12; vrtDatType = core::moldyn::MultiParticleDataCall::Particles::VERTDATA_FLOAT_XYZ; break;
            case 2: vrtSize = 16; vrtDatType = core::moldyn::MultiParticleDataCall::Particles::VERTDATA_FLOAT_XYZR; break;
            case 3: vrtSize = 6; vrtDatType = core::moldyn::MultiParticleDataCall::Particles::VERTDATA_SHORT_XYZ; break;
            default: vrtSize = 0; vrtDatType = core::moldyn::MultiParticleDataCall::Particles::VERTDATA_NONE; break;
        }
        if (vrtType != 0) {
            switch (colType) {
                case 0: colSize = 0; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_NONE; break;
                case 1: colSize = 3; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_UINT8_RGB; break;
                case 2: colSize = 4; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_UINT8_RGBA; break;
                case 3: colSize = 4; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_I; break;
                case 4: colSize = 12; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_RGB; break;
                case 5: colSize = 16; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_RGBA; break;
                default: colSize = 0; colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_NONE; break;
            }
        } else {
            colDatType = core::moldyn::MultiParticleDataCall::Particles::COLDATA_NONE;
            colSize = 0;
        }

		SIZE_T extraSize = 4;	// file stores extra unsigned int for clusterIdx
		unsigned int stride = static_cast<unsigned int>(vrtSize + colSize + extraSize);	

        if ((vrtType == 1) || (vrtType == 3)) {
            pts.SetGlobalRadius(*this->dat.AsAt<float>(p)); p += 4;
        } else {
            pts.SetGlobalRadius(0.05f);
        }
		if (colType == 0) {
            pts.SetGlobalColour(*this->dat.AsAt<UINT8>(p),
                *this->dat.AsAt<UINT8>(p + 1),
                *this->dat.AsAt<UINT8>(p + 2));
            p += 4;
        } else {
            pts.SetGlobalColour(192, 192, 192);
            if (colType == 3) {
                pts.SetColourMapIndexValues(
                    *this->dat.AsAt<float>(p),
                    *this->dat.AsAt<float>(p + 4));
                p += 8;
            } else {
                pts.SetColourMapIndexValues(0.0f, 1.0f);
            }
        }

        pts.SetCount(*this->dat.AsAt<UINT64>(p)); p += 8;
		p += 4; // ClusterCount

        pts.SetVertexData(vrtDatType, this->dat.At(p), stride);
        pts.SetColourData(colDatType, this->dat.At(p + vrtSize), stride);

        p += stride * pts.GetCount();
    }
}
	/*
 * vis_particle_clustering::MMPLDDataSource::Frame::SetData
 */
void vis_particle_clustering::MMCPLDDataSource::Frame::SetData(MultiParticleClusterDataCall& call) {
    if (this->dat.IsEmpty()) {
        call.SetParticleListCount(0);
        return;
    }

    SIZE_T p = sizeof(UINT32);
    UINT32 plc = *this->dat.As<UINT32>();
    call.SetParticleListCount(plc);
    for (UINT32 i = 0; i < plc; i++) {
        MultiParticleClusterDataCall::Particles &pts = call.AccessParticles(i);

        UINT8 vrtType = *this->dat.AsAt<UINT8>(p); p += 1;
        UINT8 colType = *this->dat.AsAt<UINT8>(p); p += 1;
		core::moldyn::SimpleSphericalParticles::VertexDataType vrtDatType;
        core::moldyn::SimpleSphericalParticles::ColourDataType colDatType;
        SIZE_T vrtSize = 0;
        SIZE_T colSize = 0;

        switch (vrtType) {
            case 0: vrtSize = 0; vrtDatType = core::moldyn::SimpleSphericalParticles::VERTDATA_NONE; break;
            case 1: vrtSize = 12; vrtDatType = core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ; break;
            case 2: vrtSize = 16; vrtDatType = core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZR; break;
            case 3: vrtSize = 6; vrtDatType = core::moldyn::SimpleSphericalParticles::VERTDATA_SHORT_XYZ; break;
            default: vrtSize = 0; vrtDatType = core::moldyn::SimpleSphericalParticles::VERTDATA_NONE; break;
        }
        if (vrtType != 0) {
            switch (colType) {
                case 0: colSize = 0; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE; break;
                case 1: colSize = 3; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_UINT8_RGB; break;
                case 2: colSize = 4; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_UINT8_RGBA; break;
                case 3: colSize = 4; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_I; break;
                case 4: colSize = 12; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_RGB; break;
                case 5: colSize = 16; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_RGBA; break;
                default: colSize = 0; colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE; break;
            }
        } else {
            colDatType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE;
            colSize = 0;
        }

		SIZE_T extraSize = 4;	// file stores extra unsigned int for clusterIdx
		unsigned int stride = static_cast<unsigned int>(vrtSize + colSize + extraSize);	

        if ((vrtType == 1) || (vrtType == 3)) {
            pts.SetGlobalRadius(*this->dat.AsAt<float>(p)); p += 4;
		} else {
            pts.SetGlobalRadius(0.05f);
        }
		if (colType == 0) {
            pts.SetGlobalColour(*this->dat.AsAt<UINT8>(p),
                *this->dat.AsAt<UINT8>(p + 1),
                *this->dat.AsAt<UINT8>(p + 2));
            p += 4;
        } else {
            pts.SetGlobalColour(192, 192, 192);
            if (colType == 3) {
                pts.SetColourMapIndexValues(
                    *this->dat.AsAt<float>(p),
                    *this->dat.AsAt<float>(p + 4));
                p += 8;
            } else {
                pts.SetColourMapIndexValues(0.0f, 1.0f);
            }
        }

        pts.SetCount(*this->dat.AsAt<UINT64>(p)); p += 8;
		pts.SetNumberClusters(*this->dat.AsAt<unsigned int>(p)); p += 4;

        pts.SetVertexData(vrtDatType, this->dat.At(p), stride);
        pts.SetColourData(colDatType, this->dat.At(p + vrtSize), stride);
		pts.SetClusterData(this->dat.At(p + vrtSize + colSize), stride);
		
        p += stride * pts.GetCount();
    }
}

/*****************************************************************************/


/*
 * vis_particle_clustering::MMCPLDDataSource::MMCPLDDataSource
 */
vis_particle_clustering::MMCPLDDataSource::MMCPLDDataSource(void) : core::view::AnimDataModule(),
        filename("filename", "The path to the MMCPLD file to load."),
        limitMemorySlot("limitMemory", "Limits the memory cache size"),
        limitMemorySizeSlot("limitMemorySize", "Specifies the size limit (in MegaBytes) of the memory cache"),
        getData("getdata", "Slot to request data from this data source."),
        file(NULL), frameIdx(NULL), bbox(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f),
        clipbox(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f), data_hash(0) {

    this->filename.SetParameter(new core::param::FilePathParam(""));
    this->filename.SetUpdateCallback(&MMCPLDDataSource::filenameChanged);
    this->MakeSlotAvailable(&this->filename);

    this->limitMemorySlot << new core::param::BoolParam(
#if defined(_WIN64) || defined(LIN64)
        false
#else
        true
#endif
        );
    this->MakeSlotAvailable(&this->limitMemorySlot);

    this->limitMemorySizeSlot << new core::param::IntParam(2 * 1024, 1);
    this->MakeSlotAvailable(&this->limitMemorySizeSlot);

    this->getData.SetCallback("MultiParticleDataCall", "GetData", &MMCPLDDataSource::getDataCallback);
    this->getData.SetCallback("MultiParticleDataCall", "GetExtent", &MMCPLDDataSource::getExtentCallback);
	this->getData.SetCallback("MultiParticleClusterDataCall", "GetData", &MMCPLDDataSource::getDataCallback);
    this->getData.SetCallback("MultiParticleClusterDataCall", "GetExtent", &MMCPLDDataSource::getExtentCallback);
    this->MakeSlotAvailable(&this->getData);

    this->setFrameCount(1);
    this->initFrameCache(1);
}

/*
 * vis_particle_clustering::MMCPLDDataSource::~MMCPLDDataSource
 */
vis_particle_clustering::MMCPLDDataSource::~MMCPLDDataSource(void)  {
	this->Release();
}

/*
 * vis_particle_clustering::MMCPLDDataSource::constructFrame
 */
core::view::AnimDataModule::Frame* vis_particle_clustering::MMCPLDDataSource::constructFrame(void) const {
    Frame *f = new Frame(*const_cast<vis_particle_clustering::MMCPLDDataSource*>(this));
    return f;
}


/*
 * vis_particle_clustering::MMCPLDDataSource::create
 */
bool vis_particle_clustering::MMCPLDDataSource::create(void) {
    return true;
}


/*
 * vis_particle_clustering::MMCPLDDataSource::loadFrame
 */
void vis_particle_clustering::MMCPLDDataSource::loadFrame(core::view::AnimDataModule::Frame *frame,
        unsigned int idx) {
    using vislib::sys::Log;
    Frame *f = dynamic_cast<Frame*>(frame);
    if (f == NULL) return;
    if (this->file == NULL) {
        f->Clear();
        return;
    }
    //printf("Requesting frame %u of %u frames\n", idx, this->FrameCount());
    //printf("Requesting frame %u of %u frames\n", idx, this->FrameCount());
    //Log::DefaultLog.WriteMsg(Log::LEVEL_INFO, "Requesting frame %u of %u frames\n", idx, this->FrameCount());
    ASSERT(idx < this->FrameCount());
    this->file->Seek(this->frameIdx[idx]);
    if (!f->LoadFrame(this->file, idx, this->frameIdx[idx + 1] - this->frameIdx[idx], this->fileVersion)) {
        // failed
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Unable to read frame %d from MMCPLD file\n", idx);
    }
}

/*
 * vis_particle_clustering::MMCPLDDataSource::release
 */
void vis_particle_clustering::MMCPLDDataSource::release(void) {
    this->resetFrameCache();
    if (this->file != NULL) {
        vislib::sys::File *f = this->file;
        this->file = NULL;
        f->Close();
        delete f;
    }
    ARY_SAFE_DELETE(this->frameIdx);
}

/*
 * vis_particle_clustering::MMCPLDDataSource::filenameChanged
 */
bool vis_particle_clustering::MMCPLDDataSource::filenameChanged(core::param::ParamSlot& slot) {
    using vislib::sys::Log;
    using vislib::sys::File;
    this->resetFrameCache();
    this->bbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f);
    this->clipbox = this->bbox;
    this->data_hash++;

    if (this->file == NULL) {
        this->file = new vislib::sys::MemmappedFile();
    } else {
        this->file->Close();
    }
    ASSERT(this->filename.Param<core::param::FilePathParam>() != NULL);

    if (!this->file->Open(this->filename.Param<core::param::FilePathParam>()->Value(), File::READ_ONLY, File::SHARE_READ, File::OPEN_ONLY)) {
        this->GetCoreInstance()->Log().WriteMsg(Log::LEVEL_ERROR, "Unable to open MMCPLD-File \"%s\".", vislib::StringA(
            this->filename.Param<core::param::FilePathParam>()->Value()).PeekBuffer());

        SAFE_DELETE(this->file);
        this->setFrameCount(1);
        this->initFrameCache(1);

        return true;
    }

#define _ERROR_OUT(MSG) Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, MSG); \
        SAFE_DELETE(this->file); \
        this->setFrameCount(1); \
        this->initFrameCache(1); \
        this->bbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f); \
        this->clipbox = this->bbox; \
        return true;
#define _ASSERT_READFILE(BUFFER, BUFFERSIZE) if (this->file->Read((BUFFER), (BUFFERSIZE)) != (BUFFERSIZE)) { \
        _ERROR_OUT("Unable to read MMCPLD file header"); \
    }

    char magicid[7];
    _ASSERT_READFILE(magicid, 7);
    if (::memcmp(magicid, "MMCPLD", 7) != 0) {
        _ERROR_OUT("MMCPLD file header id wrong");
    }
    unsigned short ver;
    _ASSERT_READFILE(&ver, 2);
    if (ver != 100 && ver != 101) {
        _ERROR_OUT("MMCPLD file header version wrong");
    }
    this->fileVersion = ver;

	UINT32 frmCnt = 0;
    _ASSERT_READFILE(&frmCnt, 4);
    if (frmCnt == 0) {
        _ERROR_OUT("MMCPLD file does not contain any frame information");
    }

	int firstF;
	_ASSERT_READFILE(&firstF, 4);
	this->firstFrame = firstF;
	int lastF;
	_ASSERT_READFILE(&lastF, 4);
	this->lastFrame = lastF;
	int frameS;
	_ASSERT_READFILE(&frameS, 4);
	this->frameStep = frameS;
  

    float box[6];
    _ASSERT_READFILE(box, 4 * 6);
    this->bbox.Set(box[0], box[1], box[2], box[3], box[4], box[5]);
    _ASSERT_READFILE(box, 4 * 6);
    this->clipbox.Set(box[0], box[1], box[2], box[3], box[4], box[5]);

    delete[] this->frameIdx;
    this->frameIdx = new UINT64[frmCnt + 1];
    _ASSERT_READFILE(this->frameIdx, 8 * (frmCnt + 1));
    double size = 0.0;
    for (UINT32 i = 0; i < frmCnt; i++) {
        size += static_cast<double>(this->frameIdx[i + 1] - this->frameIdx[i]);
    }
    size /= static_cast<double>(frmCnt);
    size *= CACHE_FRAME_FACTOR;

    UINT64 mem = vislib::sys::SystemInformation::AvailableMemorySize();
    if (this->limitMemorySlot.Param<core::param::BoolParam>()->Value()) {
        mem = vislib::math::Min(mem, 
            (UINT64)(this->limitMemorySizeSlot.Param<core::param::IntParam>()->Value())
            * (UINT64)(1024u * 1024u));
    }
    unsigned int cacheSize = static_cast<unsigned int>(mem / size);

    if (cacheSize > CACHE_SIZE_MAX) {
        cacheSize = CACHE_SIZE_MAX;
    }
    if (cacheSize < CACHE_SIZE_MIN) {
        vislib::StringA msg;
        msg.Format("Frame cache size forced to %i. Calculated size was %u.\n",
            CACHE_SIZE_MIN, cacheSize);
        this->GetCoreInstance()->Log().WriteMsg(vislib::sys::Log::LEVEL_WARN, msg);
        cacheSize = CACHE_SIZE_MIN;
    } else {
        vislib::StringA msg;
        msg.Format("Frame cache size set to %i.\n", cacheSize);
        this->GetCoreInstance()->Log().WriteMsg(vislib::sys::Log::LEVEL_INFO, msg);
    }

    this->setFrameCount(frmCnt);
    this->initFrameCache(cacheSize);

#undef _ASSERT_READFILE
#undef _ERROR_OUT

    return true;
}

/*
 * vis_particle_clustering::MMPLDDataSource::getDataCallback
 */
bool vis_particle_clustering::MMCPLDDataSource::getDataCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *mpdc = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
	vis_particle_clustering::MultiParticleClusterDataCall *mpdcx = dynamic_cast<vis_particle_clustering::MultiParticleClusterDataCall*>(&caller);
    if ((mpdc == NULL) && (mpdcx == NULL)) return false;

    Frame *f = NULL;
    if (mpdc != NULL) {
        f = dynamic_cast<Frame *>(this->requestLockedFrame(mpdc->FrameID()));
        if (f == NULL) return false;
        mpdc->SetUnlocker(new MPDCUnlocker(*f));
        mpdc->SetFrameID(f->FrameNumber());
        mpdc->SetDataHash(this->data_hash);
        f->SetData(*mpdc);
    } else if (mpdcx != NULL) {
		f = dynamic_cast<Frame *>(this->requestLockedFrame(mpdcx->FrameID()));
        if (f == NULL) return false;
        mpdcx->SetUnlocker(new MPDCCUnlocker(*f));
        mpdcx->SetFrameID(f->FrameNumber());
        mpdcx->SetDataHash(this->data_hash);
        f->SetData(*mpdcx);
	}

    return true;
}


/*
 * vis_particle_clustering::MMPLDDataSource::getExtentCallback
 */
bool vis_particle_clustering::MMCPLDDataSource::getExtentCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *mpdc = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
	vis_particle_clustering::MultiParticleClusterDataCall *mpdcx = dynamic_cast<vis_particle_clustering::MultiParticleClusterDataCall*>(&caller);
    if ((mpdc == NULL) && (mpdcx == NULL)) return false;

    if (mpdc != NULL) {
        mpdc->SetFrameCount(this->FrameCount());
        mpdc->AccessBoundingBoxes().Clear();
        mpdc->AccessBoundingBoxes().SetObjectSpaceBBox(this->bbox);
        mpdc->AccessBoundingBoxes().SetObjectSpaceClipBox(this->clipbox);
        mpdc->SetDataHash(this->data_hash);
        return true;
    } else if (mpdcx != NULL) {
		mpdcx->SetFrameCount(this->FrameCount());
		mpdcx->SetFirstFrame(firstFrame);
		mpdcx->SetLastFrame(lastFrame);
		mpdcx->SetFrameStep(frameStep);
        mpdcx->AccessBoundingBoxes().Clear();
        mpdcx->AccessBoundingBoxes().SetObjectSpaceBBox(this->bbox);
        mpdcx->AccessBoundingBoxes().SetObjectSpaceClipBox(this->clipbox);
        mpdcx->SetDataHash(this->data_hash);
        return true;
	}

    return false;
}
