/*
 * MultiParticleClusterDataCall.cpp
 *
 * Modification of MultiParticleDataCall.cpp by Julia B�hnke
 * Copyright (C) 2009 by Universitaet Stuttgart (VISUS). 
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "MultiParticleClusterDataCall.h"
//#include "vislib/memutils.h"

using namespace megamol;

/****************************************************************************/


/*
 * vis_particle_clustering::ClusteredSphericalParticles::SphericalParticles
 */
vis_particle_clustering::ClusteredSphericalParticles::ClusteredSphericalParticles(void)
        : colDataType(core::moldyn::SimpleSphericalParticles::COLDATA_NONE), colPtr(NULL), colStride(0), count(0),
		maxColI(1.0f), minColI(0.0f), radius(0.5f), particleType(0),
		vertDataType(core::moldyn::SimpleSphericalParticles::VERTDATA_NONE), vertPtr(NULL), vertStride(0), 
		disabledNullChecks(false) {
    this->col[0] = 255;
    this->col[1] = 0;
    this->col[2] = 0;
    this->col[3] = 255;
}


/*
 * vis_particle_custering::ClusteredSphericalParticles::ClusteredSphericalParticles
 */
vis_particle_clustering::ClusteredSphericalParticles::ClusteredSphericalParticles(
        const vis_particle_clustering::ClusteredSphericalParticles& src) {
    *this = src;
}


/*
 * vis_particle_custering::ClusteredSphericalParticles::~ClusteredSphericalParticles
 */
vis_particle_clustering::ClusteredSphericalParticles::~ClusteredSphericalParticles(void) {
    this->colDataType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE;
    this->colPtr = NULL; // DO NOT DELETE
    this->count = 0;
    this->vertDataType = core::moldyn::SimpleSphericalParticles::VERTDATA_NONE;
    this->vertPtr = NULL; // DO NOT DELETE
	this->clusterPtr = NULL; // DO NOT DELETE
}


/*
 * vis_particle_custering::ClusteredSphericalParticles::operator=
 */
vis_particle_clustering::ClusteredSphericalParticles&
vis_particle_clustering::ClusteredSphericalParticles::operator=(
        const vis_particle_clustering::ClusteredSphericalParticles& rhs) {
    this->col[0] = rhs.col[0];
    this->col[1] = rhs.col[1];
    this->col[2] = rhs.col[2];
    this->col[3] = rhs.col[3];
    this->colDataType = rhs.colDataType;
    this->colPtr = rhs.colPtr;
    this->colStride = rhs.colStride;
    this->count = rhs.count;
    this->maxColI = rhs.maxColI;
    this->minColI = rhs.minColI;
    this->radius = rhs.radius;
    this->particleType = rhs.particleType;
    this->vertDataType = rhs.vertDataType;
    this->vertPtr = rhs.vertPtr;
    this->vertStride = rhs.vertStride;
	this->disabledNullChecks = rhs.disabledNullChecks;
	this->clusterPtr = rhs.clusterPtr;
	this->clusterStride = rhs.clusterStride;
	this->numberClusters = rhs.numberClusters;
    return *this;
}


/*
 * vis_particle_custering::ClusteredSphericalParticles::operator==
 */
bool vis_particle_clustering::ClusteredSphericalParticles::operator==(
        const vis_particle_clustering::ClusteredSphericalParticles& rhs) const {
    return ((this->col[0] == rhs.col[0])
        && (this->col[1] == rhs.col[1])
        && (this->col[2] == rhs.col[2])
        && (this->col[3] == rhs.col[3])
        && (this->colDataType == rhs.colDataType)
        && (this->colPtr == rhs.colPtr)
        && (this->colStride == rhs.colStride)
        && (this->count == rhs.count)
        && (this->maxColI == rhs.maxColI)
        && (this->minColI == rhs.minColI)
        && (this->radius == rhs.radius)
        && (this->vertDataType == rhs.vertDataType)
        && (this->vertPtr == rhs.vertPtr)
        && (this->vertStride == rhs.vertStride)
		&& (this->clusterPtr == rhs.clusterPtr)
		&& (this->clusterStride == rhs.clusterStride)
		&& (this->numberClusters == rhs.numberClusters));
}

/****************************************************************************/


/*
 * vis_particle_clustering::MultiParticleClusterDataCall::MultiParticleClusterDataCall
 */
vis_particle_clustering::MultiParticleClusterDataCall::MultiParticleClusterDataCall(void)
        : core::moldyn::AbstractParticleDataCall<ClusteredSphericalParticles>(),
		firstFrame(-1), lastFrame(-1), frameStep(-1) {
    // Intentionally empty
}


/*
 * vis_particle_clustering::MultiParticleClusterDataCall::~MultiParticleClusterDataCall
 */
vis_particle_clustering::MultiParticleClusterDataCall::~MultiParticleClusterDataCall(void) {
    // Intentionally empty
}


/*
 * vis_particle_clustering::MultiParticleClusterDataCall::operator=
 */
vis_particle_clustering::MultiParticleClusterDataCall& vis_particle_clustering::MultiParticleClusterDataCall::operator=(
        const vis_particle_clustering::MultiParticleClusterDataCall& rhs) {
			core::moldyn::AbstractParticleDataCall<ClusteredSphericalParticles>::operator =(rhs);
			return *this;
}
