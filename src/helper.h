/*
 * helper.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_HELPER_FUNCTIONS
#define MEGAMOLVPC_HELPER_FUNCTIONS

#include "MultiParticleDataCallExtended.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/Vector.h"
#include "vislib/math/Quaternion.h"
#include "vislib/sys/Log.h"
#include "vislib/sys/Thread.h"
#include <vector>

#ifdef _MSC_VER
#pragma push_macro("max")
#undef max
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

static bool getFrame(megamol::vis_particle_clustering::MultiParticleDataCallExtended *call, unsigned int frameID) {
	unsigned int missCnt = 0;
	do {
        call->SetFrameID(frameID, true);
        if (!(*call)(0)) {
            vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Cannot get data frame %u. Abort.\n", frameID);
            return false;
        }
        if (call->FrameID() != frameID) {
            if (((missCnt + 1) % 10) == 0) {
                vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN,
                    "Frame %u returned on request for frame %u\n", call->FrameID(), frameID);
            }
            missCnt++;
            vislib::sys::Thread::Sleep(missCnt * 100);
        }
    } while(call->FrameID() != frameID && missCnt < 3);	// missCnt < 3 to prevent endless loop
	
	return true;
}

template <typename T>
static int sign(T signedNumber) { // 1:positive, -1:negative, 0:null
	if (signedNumber > static_cast<T>(0)) {
		return 1;
	}
	if (signedNumber < static_cast<T>(0)) {
		return -1;
	}
	return 0;
}

static float getDeltaMax(const vislib::math::Point<float,3>& minValues, const vislib::math::Point<float,3>& maxValues) {
	std::vector<float> deltaValues(3);
	float deltaValueMax = std::numeric_limits<float>::min();
	for (int c = 0; c<3; ++c) {
		deltaValues[c] = maxValues[c] - minValues[c];
		if (deltaValues[c] > deltaValueMax) {
			deltaValueMax = deltaValues[c];
		}
	}
	return deltaValueMax;
}

static vislib::math::Vector<float,4> getNormalizedPosition(const UINT8 *data, const vislib::math::Point<float,3>& minValues, const vislib::math::Point<float,3>& maxValues) {
	float deltaValueMax = getDeltaMax(minValues, maxValues);
	vislib::math::Vector<float,4> normalizedFeature((reinterpret_cast<const float*>(data)[0] - minValues[0]) / deltaValueMax,
													(reinterpret_cast<const float*>(data)[1] - minValues[1]) / deltaValueMax,
													(reinterpret_cast<const float*>(data)[2] - minValues[2]) / deltaValueMax, 1.0f);
	return normalizedFeature;
}

static vislib::math::Vector<float,4> getNormalizedFeature(const UINT8 *data, const vislib::math::Point<float,3>& minValues, const vislib::math::Point<float,3>& maxValues) {
	std::vector<float> deltaValues(3);
	for (int c = 0; c<3; ++c) {
		deltaValues[c] = maxValues[c] - minValues[c];
	}
	vislib::math::Vector<float,4> normalizedFeature((reinterpret_cast<const float*>(data)[0] - minValues[0]) / deltaValues[0],
													(reinterpret_cast<const float*>(data)[1] - minValues[1]) / deltaValues[1],
													(reinterpret_cast<const float*>(data)[2] - minValues[2]) / deltaValues[2], 1.0f);
	return normalizedFeature;
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#pragma pop_macro("max")
#endif /* _MSC_VER */

template <typename T, unsigned int D>
static vislib::math::Matrix<T,D,vislib::math::MatrixLayout::ROW_MAJOR> dyad(const vislib::math::Vector<T,D>& p, const vislib::math::Vector<T,D>& q) {
	vislib::math::Matrix<T,D,vislib::math::MatrixLayout::ROW_MAJOR> dyadPQ;
	for (unsigned int row = 0; row < D; ++row) {
		for (unsigned int col = 0; col < D; ++col) {
			dyadPQ(row, col) = p[row]*q[col];
		}
	}
	return dyadPQ;
}

// q * p^T
template <typename T>
static vislib::math::Matrix<T,4,vislib::math::MatrixLayout::ROW_MAJOR> dyad(const vislib::math::Quaternion<T>& p, const vislib::math::Quaternion<T>& q) {
	return dyad(vislib::math::Vector<T,4>(p.GetX(), p.GetY(), p.GetZ(), p.GetW()), vislib::math::Vector<T,4>(q.GetX(), q.GetY(), q.GetZ(), q.GetW()));
}

// return the index of the largest eigenvector
template<class T, unsigned int D>
static unsigned int maxEigenvector(vislib::math::Vector<T, D> *evec, T *eval, unsigned int cnt) {
	T max = eval[0];
	unsigned int idx = 0;
	for (unsigned int i=0; i<cnt; ++i) {
		if (eval[i] > max) {
			max = eval[i];
			idx = i;
		}
	}
	return idx;
}

template <typename T>
static T dot(const vislib::math::Quaternion<T>& a, const vislib::math::Quaternion<T>& b) {
	return a.GetX()*b.GetX() + a.GetY()*b.GetY() + a.GetZ()*b.GetZ() + a.GetW()*b.GetW();
}

template <typename T, unsigned int D>
static vislib::math::Vector<T,D-1> wClip(const vislib::math::Vector<T,D>& p) {
	vislib::math::Vector<T,D-1> result;
	for (unsigned int i=0; i<D-1; ++i) {
		result[i] = p[i] / p[D-1];
	}
	return result;
}

#endif /* MEGAMOLVPC_HELPER_FUNCTIONS */