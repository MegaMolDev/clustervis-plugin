/*
 * MMCPLDWriter.cpp
 *
 * Modification of MMPLDWriter.cpp by Julia B�hnke
 * Copyright (C) 2010 by VISUS (Universitaet Stuttgart)
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "MMCPLDWriter.h"
#include "mmcore/BoundingBoxes.h"
#include "mmcore/param/FilePathParam.h"
#include "vislib/sys/Log.h"
#include "vislib/sys/MemmappedFile.h"
#include "vislib/String.h"
#include "vislib/sys/Thread.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"

using namespace megamol;

/*
 * vis_particle_clustering::MMCPLDWriter::MMCPLDWriter
 */
vis_particle_clustering::MMCPLDWriter::MMCPLDWriter(void) : core::AbstractDataWriter(),
        filenameSlot("filename", "The path to the MMCPLD file to be written"),
        dataSlot("data", "The slot requesting the data to be written") {

    this->filenameSlot << new core::param::FilePathParam("");
    this->MakeSlotAvailable(&this->filenameSlot);

    this->dataSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
    this->MakeSlotAvailable(&this->dataSlot);
}


/*
 * vis_particle_clustering::MMCPLDWriter::~MMCPLDWriter
 */
vis_particle_clustering::MMCPLDWriter::~MMCPLDWriter(void) {
    this->Release();
}


/*
 * vis_particle_clustering::MMCPLDWriter::create
 */
bool vis_particle_clustering::MMCPLDWriter::create(void) {
    return true;
}


/*
 * vis_particle_clustering::MMCPLDWriter::release
 */
void vis_particle_clustering::MMCPLDWriter::release(void) {
}


/*
 * vis_particle_clustering::MMCPLDWriter::run
 */
bool vis_particle_clustering::MMCPLDWriter::run(void) {
    using vislib::sys::Log;
    vislib::TString filename(this->filenameSlot.Param<core::param::FilePathParam>()->Value());
    if (filename.IsEmpty()) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR,
            "No file name specified. Abort.");
        return false;
    }

    MultiParticleClusterDataCall *mpcdc = this->dataSlot.CallAs<MultiParticleClusterDataCall>();
    if (mpcdc == NULL) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR,
            "No data source connected. Abort.");
        return false;
    }

    if (vislib::sys::File::Exists(filename)) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_WARN,
            "File %s already exists and will be overwritten.",
            vislib::StringA(filename).PeekBuffer());
    }

    vislib::math::Cuboid<float> bbox;
    vislib::math::Cuboid<float> cbox;
    UINT32 frameCnt = 1;
	int firstFrame = -1;
	int lastFrame = -1;
	int frameStep = -1;
    if (!(*mpcdc)(1)) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_WARN, "Unable to query data extents.");
        bbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f);
        cbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f);
    } else {
        if (mpcdc->AccessBoundingBoxes().IsObjectSpaceBBoxValid()
                || mpcdc->AccessBoundingBoxes().IsObjectSpaceClipBoxValid()) {
            if (mpcdc->AccessBoundingBoxes().IsObjectSpaceBBoxValid()) {
                bbox = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox();
            } else {
                bbox = mpcdc->AccessBoundingBoxes().ObjectSpaceClipBox();
            }
            if (mpcdc->AccessBoundingBoxes().IsObjectSpaceClipBoxValid()) {
                cbox = mpcdc->AccessBoundingBoxes().ObjectSpaceClipBox();
            } else {
                cbox = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox();
            }
        } else {
            Log::DefaultLog.WriteMsg(Log::LEVEL_WARN, "Object space bounding boxes not valid. Using defaults");
            bbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f);
            cbox.Set(-1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f);
        }
        frameCnt = mpcdc->FrameCount();
        if (frameCnt == 0) {
            Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR,
                "Data source counts zero frames. Abort.");
            return false;
        }
    }

	unsigned int missCnt = 0;
    do {
        mpcdc->SetFrameID(0, true);
        if (!(*mpcdc)(0)) {
            Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Cannot get data frame %u. Abort.\n", 0);
            return false;
        }
        if (mpcdc->FrameID() != 0) {
            if (((missCnt + 1) % 10) == 0) {
                Log::DefaultLog.WriteMsg(Log::LEVEL_WARN,
                    "Frame %u returned on request for frame %u\n", mpcdc->FrameID(), 0);
            }
            missCnt++;
            vislib::sys::Thread::Sleep(missCnt * 100);
        }
    } while(mpcdc->FrameID() != 0);

	firstFrame = mpcdc->GetFirstFrame();
	lastFrame = mpcdc->GetLastFrame();
	frameStep = mpcdc->GetFrameStep();

    // DEBUG
//    frameCnt = 10;
    // END DEBUG

    vislib::sys::MemmappedFile file;
    if (!file.Open(filename, vislib::sys::File::WRITE_ONLY, vislib::sys::File::SHARE_EXCLUSIVE, vislib::sys::File::CREATE_OVERWRITE)) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR,
            "Unable to create output file \"%s\". Abort.",
            vislib::StringA(filename).PeekBuffer());
        return false;
    }

#define ASSERT_WRITEOUT(A, S) if (file.Write((A), (S)) != (S)) { \
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Write error %d", __LINE__); \
        file.Close(); \
        return false; \
    }

	unsigned int startFrame = 0;
	unsigned int endFrame = frameCnt - 1;
	unsigned int step = 1;

	if (firstFrame >= 0 && lastFrame > firstFrame && frameStep > 0 && lastFrame - firstFrame >= frameStep) {
		frameCnt = (lastFrame + 1 - firstFrame) / frameStep;
		startFrame = firstFrame;
		endFrame = lastFrame;
		step = frameStep;
	}

    vislib::StringA magicID("MMCPLD");
    ASSERT_WRITEOUT(magicID.PeekBuffer(), 7);
    UINT16 version = 0;
    ASSERT_WRITEOUT(&version, 2);
    ASSERT_WRITEOUT(&frameCnt, 4);
	ASSERT_WRITEOUT(&firstFrame, 4);
	ASSERT_WRITEOUT(&lastFrame, 4);
	ASSERT_WRITEOUT(&frameStep, 4);
    ASSERT_WRITEOUT(bbox.PeekBounds(), 6 * 4);
    ASSERT_WRITEOUT(cbox.PeekBounds(), 6 * 4);

    UINT64 seekTable = static_cast<UINT64>(file.Tell());
    UINT64 frameOffset = 0;
    for (UINT32 i = 0; i <= frameCnt; i++) {
        ASSERT_WRITEOUT(&frameOffset, 8);
    }

    for (UINT32 i = startFrame; i <= endFrame; i += step) {
        frameOffset = static_cast<UINT64>(file.Tell());
        file.Seek(seekTable + i * 8);
        ASSERT_WRITEOUT(&frameOffset, 8);
        file.Seek(frameOffset);

        Log::DefaultLog.WriteMsg(Log::LEVEL_INFO, "Started writing data frame %u\n", i);

        unsigned int missCnt = 0;
        do {
            mpcdc->SetFrameID(i, true);
            if (!(*mpcdc)(0)) {
                Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Cannot get data frame %u. Abort.\n", i);
                file.Close();
                return false;
            }
            if (mpcdc->FrameID() != i) {
                if (((missCnt + 1) % 10) == 0) {
                    Log::DefaultLog.WriteMsg(Log::LEVEL_WARN,
                        "Frame %u returned on request for frame %u\n", mpcdc->FrameID(), i);
                }
                missCnt++;
                vislib::sys::Thread::Sleep(missCnt * 100);
            }
        } while(mpcdc->FrameID() != i);

        if (!this->writeFrame(file, *mpcdc)) {
            mpcdc->Unlock();
            Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Cannot write data frame %u. Abort.\n", i);
            file.Close();
            return false;
        }
        mpcdc->Unlock();
    }

    frameOffset = static_cast<UINT64>(file.Tell());
    file.Seek(seekTable + frameCnt * 8);
    ASSERT_WRITEOUT(&frameOffset, 8);

    file.Seek(7); // set correct version to show that file is complete
    version = 100;

    ASSERT_WRITEOUT(&version, 2);

    file.Seek(frameOffset);

    Log::DefaultLog.WriteMsg(Log::LEVEL_INFO, "Completed writing data\n");
    file.Close();

    return true;
}


/*
 * vis_particle_clustering::MMCPLDWriter::getCapabilities
 */
bool vis_particle_clustering::MMCPLDWriter::getCapabilities(core::DataWriterCtrlCall& call) {
    call.SetAbortable(false);
    return true;
}


/*
 * vis_particle_clustering::MMCPLDWriter::writeFrame
 */
bool vis_particle_clustering::MMCPLDWriter::writeFrame(vislib::sys::File& file, MultiParticleClusterDataCall& data) {
    using vislib::sys::Log;
    UINT32 listCnt = data.GetParticleListCount();
    ASSERT_WRITEOUT(&listCnt, 4);

    for (UINT32 li = 0; li < listCnt; li++) {
        MultiParticleClusterDataCall::Particles &points = data.AccessParticles(li);
        UINT8 vt = 0, ct = 0;
        unsigned int vs = 0, vo = 0, cs = 0, co = 0;
		unsigned int clo = 4; // cluster offset (= stride)
        switch(points.GetVertexDataType()) {
			case core::moldyn::MultiParticleDataCall::Particles::VERTDATA_NONE: vt = 0; vs = 0; break;
            case core::moldyn::MultiParticleDataCall::Particles::VERTDATA_FLOAT_XYZ: vt = 1; vs = 12; break;
            case core::moldyn::MultiParticleDataCall::Particles::VERTDATA_FLOAT_XYZR: vt = 2; vs = 16; break;
            case core::moldyn::MultiParticleDataCall::Particles::VERTDATA_SHORT_XYZ: vt = 3; vs = 6; break;
            default: vt = 0; vs = 0; break;
        }
        if (vt != 0) {
            switch(points.GetColourDataType()) {
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_NONE: ct = 0; cs = 0; break;
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_UINT8_RGB: ct = 1; cs = 3; break;
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_UINT8_RGBA: ct = 2; cs = 4; break;
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_I: ct = 3; cs = 4; break;
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_RGB: ct = 4; cs = 12; break;
                case core::moldyn::MultiParticleDataCall::Particles::COLDATA_FLOAT_RGBA: ct = 5; cs = 16; break;
                default: ct = 0; cs = 0; break;
            }
        } else {
            ct = 0;
        }
        ASSERT_WRITEOUT(&vt, 1);
        ASSERT_WRITEOUT(&ct, 1);

        if (points.GetVertexDataStride() > vs) {
            vo = points.GetVertexDataStride();
        } else {
            vo = vs;
        }
        if (points.GetColourDataStride() > cs) {
            co = points.GetColourDataStride();
        } else {
            co = cs;
        }
		if (points.GetClusterDataStride() > 4) {
            clo = points.GetClusterDataStride();
        }

        if ((vt == 1) || (vt == 3)) {
            float f = points.GetGlobalRadius();
            ASSERT_WRITEOUT(&f, 4);
        }
        if (ct == 0) {
            const unsigned char *col = points.GetGlobalColour();
            ASSERT_WRITEOUT(col, 4);
        } else if (ct == 3) {
            float f = points.GetMinColourIndexValue();
            ASSERT_WRITEOUT(&f, 4);
            f = points.GetMaxColourIndexValue();
            ASSERT_WRITEOUT(&f, 4);
        }

        UINT64 cnt = points.GetCount();
        if (vt == 0) cnt = 0;
        ASSERT_WRITEOUT(&cnt, 8);
		unsigned int numberClusters = points.GetNumberClusters();
		ASSERT_WRITEOUT(&numberClusters, 4);
        if (vt == 0) continue;
        const unsigned char *vp = static_cast<const unsigned char *>(points.GetVertexData());
        const unsigned char *cp = static_cast<const unsigned char *>(points.GetColourData());
		const unsigned char *clp = static_cast<const unsigned char *>(points.GetClusterData());	// cluster pointer
        for (UINT64 i = 0; i < cnt; i++) {
            ASSERT_WRITEOUT(vp, vs);
            vp += vo;
            if (ct != 0) {
                ASSERT_WRITEOUT(cp, cs);
                cp += co;
            }
			ASSERT_WRITEOUT(clp, clo);
			clp += clo;
        }
    }

    return true;
}

#undef ASSERT_WRITEOUT
