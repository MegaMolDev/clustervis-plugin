/*
 * vis_particle_clustering.cpp
 * Copyright (C) 2009-2015 by MegaMol Team
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "vis_particle_clustering/vis_particle_clustering.h"

#include "mmcore/api/MegaMolCore.std.h"
#include "mmcore/utility/plugins/Plugin200Instance.h"
#include "mmcore/versioninfo.h"
#include "vislib/vislibversion.h"

#include "MMXPLDDataSource.h"
#include "MMCPLDDataSource.h"
#include "MMCPLDWriter.h"
#include "clustering/KMeansParticleClusterer.h"
#include "clustering/MeanshiftParticleClusterer.h"
#include "clustering/KMeansTrajectoryClusterer.h"
#include "clustering/MeanshiftTrajectoryClusterer.h"
#include "visualization/ClusterColourMapper.h"
#include "visualization/FeatureColourMapper.h"
#include "visualization/SimpleHairBallRenderer.h"
#include "visualization/HairBallRenderer.h"
#include "visualization/HairBallCylinderRenderer.h"
#include "visualization/MetaballRenderer.h"
#include "visualization/TrajectoryRenderer.h"
#include "visualization/CycleTransferFunction.h"
#include "visualization/ClusterTransferFunctionRenderer.h"
#include "visualization/FeatureTransferFunctionRenderer.h"
#include "visualization/SharedVisualizationParameters.h"
#include "visualization/CallVisParams.h"
#include "visualization/CallFeatureParams.h"
#include "MultiParticleDataCallExtended.h"
#include "MultiParticleClusterDataCall.h"
#include "visualization/LinearTransferFunction2.h"


/* anonymous namespace hides this type from any other object files */
namespace {
    /** Implementing the instance class of this plugin */
    class plugin_instance : public ::megamol::core::utility::plugins::Plugin200Instance {
    public:
        /** ctor */
        plugin_instance(void)
            : ::megamol::core::utility::plugins::Plugin200Instance(

                /* machine-readable plugin assembly name */
                "vis_particle_clustering", 

                /* human-readable plugin description */
                "Plugin for clustering an visualization of particle data") {

            // here we could perform addition initialization
        };
        /** Dtor */
        virtual ~plugin_instance(void) {
            // here we could perform addition de-initialization
        }
        /** Registers modules and calls */
        virtual void registerClasses(void) {

            // Register modules
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::MMXPLDDataSource>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::MMCPLDDataSource>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::MMCPLDWriter>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::clustering::KMeansParticleClusterer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::clustering::MeanshiftParticleClusterer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::clustering::KMeansTrajectoryClusterer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::clustering::MeanshiftTrajectoryClusterer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::ClusterColourMapper>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::FeatureColourMapper>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::SimpleHairBallRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::HairBallRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::HairBallCylinderRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::TrajectoryRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::CycleTransferFunction>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::ClusterTransferFunctionRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::FeatureTransferFunctionRenderer>();
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::SharedVisualizationParameters>();
#ifdef WITH_CUDA
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::MetaballRenderer>();
#endif
            this->module_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::LinearTransferFunction>();

            // Register calls
            this->call_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::MultiParticleDataCallExtended>();
            this->call_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::MultiParticleClusterDataCall>();
            this->call_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::CallVisParams>();
            this->call_descriptions.RegisterAutoDescription<megamol::vis_particle_clustering::visualization::CallFeatureParams>();

        }
        MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_plugininstance_connectStatics
    };
}


/*
 * mmplgPluginAPIVersion
 */
VIS_PARTICLE_CLUSTERING_API int mmplgPluginAPIVersion(void) {
    MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgPluginAPIVersion
}


/*
 * mmplgGetPluginCompatibilityInfo
 */
VIS_PARTICLE_CLUSTERING_API
::megamol::core::utility::plugins::PluginCompatibilityInfo *
mmplgGetPluginCompatibilityInfo(
        ::megamol::core::utility::plugins::ErrorCallback onError) {
    // compatibility information with core and vislib
    using ::megamol::core::utility::plugins::PluginCompatibilityInfo;
    using ::megamol::core::utility::plugins::LibraryVersionInfo;

    PluginCompatibilityInfo *ci = new PluginCompatibilityInfo;
    ci->libs_cnt = 2;
    ci->libs = new LibraryVersionInfo[2];

    SetLibraryVersionInfo(ci->libs[0], "MegaMolCore",
        MEGAMOL_CORE_MAJOR_VER, MEGAMOL_CORE_MINOR_VER, MEGAMOL_CORE_MAJOR_REV, MEGAMOL_CORE_MINOR_REV, 0
#if defined(DEBUG) || defined(_DEBUG)
        | MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DEBUG_BUILD
#endif
#if defined(MEGAMOL_CORE_DIRTY) && (MEGAMOL_CORE_DIRTY != 0)
        | MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DIRTY_BUILD
#endif
        );

    SetLibraryVersionInfo(ci->libs[1], "vislib",
        VISLIB_VERSION_MAJOR, VISLIB_VERSION_MINOR, VISLIB_VERSION_REVISION, VISLIB_VERSION_BUILD, 0
#if defined(DEBUG) || defined(_DEBUG)
        | MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DEBUG_BUILD
#endif
#if defined(VISLIB_DIRTY_BUILD) && (VISLIB_DIRTY_BUILD != 0)
        | MEGAMOLCORE_PLUGIN200UTIL_FLAGS_DIRTY_BUILD
#endif
        );
    //
    // If you want to test additional compatibilties, add the corresponding versions here
    //

    return ci;
}


/*
 * mmplgReleasePluginCompatibilityInfo
 */
VIS_PARTICLE_CLUSTERING_API
void mmplgReleasePluginCompatibilityInfo(
        ::megamol::core::utility::plugins::PluginCompatibilityInfo* ci) {
    // release compatiblity data on the correct heap
    MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgReleasePluginCompatibilityInfo(ci)
}


/*
 * mmplgGetPluginInstance
 */
VIS_PARTICLE_CLUSTERING_API
::megamol::core::utility::plugins::AbstractPluginInstance*
mmplgGetPluginInstance(
        ::megamol::core::utility::plugins::ErrorCallback onError) {
    MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgGetPluginInstance(plugin_instance, onError)
}


/*
 * mmplgReleasePluginInstance
 */
VIS_PARTICLE_CLUSTERING_API
void mmplgReleasePluginInstance(
        ::megamol::core::utility::plugins::AbstractPluginInstance* pi) {
    MEGAMOLCORE_PLUGIN200UTIL_IMPLEMENT_mmplgReleasePluginInstance(pi)
}
