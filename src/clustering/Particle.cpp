/*
 * Particle.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "Particle.h"
#include "helper.h"

using namespace megamol::vis_particle_clustering;

clustering::Particle::Particle(vislib::math::Vector<float,4> position, 
							   vislib::math::Vector<float,4> velocity,
							   float rotationAngle,
							   vislib::math::Quaternion<float> rotation) : position(position), 
								   velocity(velocity),
								   rotationAngle(rotationAngle),
								   rotation(rotation) {
	covRotations = dyad(rotation, rotation);
}

clustering::Particle::~Particle(void) {
}

clustering::Particle& clustering::Particle::AddSample(const Particle &rhs) {
	this->position += rhs.GetPosition();
	this->velocity += rhs.GetVelocity();
	this->rotationAngle += rhs.GetRotationAngle();
	this->covRotations += rhs.GetCovRotations();

	return *this;
}

bool clustering::Particle::IsValid() const {
	return !(this->position.GetW() == 0 || this->velocity.GetW() == 0);
}

// Assumption:: Particle is valid
clustering::Particle& clustering::Particle::ComputeMean() {
	float numberAccumulatedParticles = this->position.GetW();
	this->position = vislib::math::Vector<float,4>(this->position.GetX() / this->position.GetW(),
													this->position.GetY() / this->position.GetW(),
													this->position.GetZ() / this->position.GetW(), 1.0f);
	this->velocity = vislib::math::Vector<float,4>(this->velocity.GetX() / this->velocity.GetW(),
													this->velocity.GetY() / this->velocity.GetW(),
													this->velocity.GetZ() / this->velocity.GetW(), 1.0f);
	this->rotationAngle /= numberAccumulatedParticles;
	
	// Average Quaternion => largest EV of Sum(qi * qi^T) = meanQuaternions[i]
	float eigenValues[4];
	vislib::math::Vector<float,4> eigenVectors[4];
	(this->covRotations).FindEigenvalues(eigenValues, eigenVectors, 4);
	unsigned int idx = maxEigenvector(eigenVectors, eigenValues, 4);
	this->rotation = vislib::math::Quaternion<float>(	eigenVectors[0].PeekComponents()[idx], 
														eigenVectors[1].PeekComponents()[idx], 
														eigenVectors[2].PeekComponents()[idx], 
														eigenVectors[3].PeekComponents()[idx]); // strange representation of eigenvectors - doesn't use i-th vector but instead i-th component of each vector
		
	this->rotation.Normalise(); // just to be sure...

	this->covRotations = dyad(this->rotation, this->rotation);

	return *this;
}

std::vector< float >& clustering::Particle::GetData(const clustering::ParticleWeight& particleWeight, bool usePosition) const {
	std::vector<float> data(4 + 3*usePosition);

	vislib::math::Vector<float,3> velocityWeight = particleWeight.GetVelocityWeight();
	data[0] = this->velocity.GetX() * velocityWeight.GetX();
	data[1] = this->velocity.GetY() * velocityWeight.GetY();
	data[2] = this->velocity.GetZ() * velocityWeight.GetZ();

	data[3] = this->rotationAngle * particleWeight.GetRotationWeight();

	if (usePosition) {
		vislib::math::Vector<float,3> positionWeight = particleWeight.GetPositionWeight();
		data[4] = this->position.GetX() * positionWeight.GetX();
		data[5] = this->position.GetY() * positionWeight.GetY();
		data[6] = this->position.GetZ() * positionWeight.GetZ();
	}

	return data;
//	return std::move(data);
}