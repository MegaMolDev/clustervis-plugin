/*
 * MeanshiftParticleClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "MeanshiftParticleClusterer.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/Vector3fParam.h"
#include "mmcore/param/ButtonParam.h"

using namespace megamol::vis_particle_clustering;

clustering::MeanshiftParticleClusterer::MeanshiftParticleClusterer(void) : 
		kSlot("K", "K for use of Meanshift (LSH-structure)"), 
		lSlot("L", "L for use of Meanshift (LSH-structure)"),
		numberNeighboursSlot("n", "Number of neighbours for use of Meanshift (LSH-structure)"),
		kOffsetSlot("KOffset", "Offset to K for FindKL"),
		kJumpSlot("KJump", "Jump of K for FindKL"),
		lMaxSlot("LMax", "LMax for FindKL"),
		findKLSlot("Find K, L", "Find optimal K,L for use of Meanshift (LSH-structure)") {
	
	this->kSlot.SetParameter(new core::param::IntParam(10,1,70));
	this->MakeSlotAvailable(&this->kSlot);

	this->lSlot.SetParameter(new core::param::IntParam(10,1,500));
	this->MakeSlotAvailable(&this->lSlot);

	this->numberNeighboursSlot.SetParameter(new core::param::IntParam(100,1,8000));
	this->MakeSlotAvailable(&this->numberNeighboursSlot);

	this->kOffsetSlot.SetParameter(new core::param::IntParam(5,1,35));
	this->MakeSlotAvailable(&this->kOffsetSlot);

	this->kJumpSlot.SetParameter(new core::param::IntParam(2,1,10));
	this->MakeSlotAvailable(&this->kJumpSlot);

	this->lMaxSlot.SetParameter(new core::param::IntParam(50,1,500));
	this->MakeSlotAvailable(&this->lMaxSlot);

	this->findKLSlot.SetParameter(new core::param::ButtonParam());
	this->MakeSlotAvailable(&this->findKLSlot);
	
	initMeanshift();
}


clustering::MeanshiftParticleClusterer::~MeanshiftParticleClusterer(void) {
	this->Release();
}

bool clustering::MeanshiftParticleClusterer::anySlotDirty() {
	bool dirty= false;
	if (this->kSlot.IsDirty() || this->lSlot.IsDirty()) {
		this->kSlot.ResetDirty();
		this->lSlot.ResetDirty();
		dirty = true;
		initMeanshift();
	}

	if (this->findKLSlot.IsDirty()) {
		this->findKLSlot.ResetDirty();
		dirty = true;
		findKLMeanshift();
	}

	return dirty;
}

void clustering::MeanshiftParticleClusterer::initMeanshift() {
	int K = this->kSlot.Param<core::param::IntParam>()->Value();
	int L = this->lSlot.Param<core::param::IntParam>()->Value();
	int N = this->numberNeighboursSlot.Param<core::param::IntParam>()->Value();
	meanshift.Init(K, L, N);
}

void clustering::MeanshiftParticleClusterer::findKLMeanshift() {
	int K = this->kSlot.Param<core::param::IntParam>()->Value();
	int L = this->lSlot.Param<core::param::IntParam>()->Value();
	int KOffset = this->kOffsetSlot.Param<core::param::IntParam>()->Value();
	int KJump = this->kJumpSlot.Param<core::param::IntParam>()->Value();
	int LMax = this->lMaxSlot.Param<core::param::IntParam>()->Value();

	int KMin = K - KOffset;	// TODO: Testen ob Grenzen f�r K eingehalten werden
	int KMax = K + KOffset;
	meanshift.FindKL(KMin, KMax, KJump, LMax, 0.05f);

	this->kSlot.Param<core::param::IntParam>()->SetValue(meanshift.GetK(), false);
	this->lSlot.Param<core::param::IntParam>()->SetValue(meanshift.GetL(), false);
}

// Assumptions: only one particle list with core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ particles
void clustering::MeanshiftParticleClusterer::cluster(MultiParticleDataCallExtended *dat, bool recluster) {
    UINT64 all_cnt = dat->AccessParticles(0).GetCount();
    if (dat->GetParticleListCount() != 1 || dat->AccessParticles(0).GetVertexDataType() != core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ) {
		return;
	}

	this->getClusterData().EnforceSize(all_cnt * sizeof(unsigned int));
	unsigned int *f = this->getClusterData().As<unsigned int>();
	
	std::vector<Particle> particles = buildParticleVector(dat);

	ParticleWeight particleWeight(this->getPositionWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getVelocityWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getRotationWeightSlot().Param<core::param::FloatParam>()->Value());
	
	meanshift.SetData(particles, particleWeight, true);
	meanshift.Run();
	setNumberClusters(meanshift.GetClusters(f));
}