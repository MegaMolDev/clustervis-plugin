/*
 * AbstractParticleClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "AbstractParticleClusterer.h"
#include "helper.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <ctime>
#include <chrono>

using namespace megamol::vis_particle_clustering;

clustering::AbstractParticleClusterer::AbstractParticleClusterer(void) {
}

clustering::AbstractParticleClusterer::~AbstractParticleClusterer(void) {
	this->Release();
}

bool clustering::AbstractParticleClusterer::outCallChanged(MultiParticleDataCallExtended *outCall) {
	if ((this->getLastFrameId() != outCall->FrameID()) || 
		(this->getLastHash() != outCall->DataHash()) || 
		(outCall->DataHash() == 0)) {
        this->setLastFrameId(outCall->FrameID());
        this->setLastHash(outCall->DataHash());
		return true;
    }
	return false;
}

std::vector<clustering::Particle> clustering::AbstractParticleClusterer::buildParticleVector(MultiParticleDataCallExtended *call) {
	std::vector<Particle> result;

	MultiParticleDataCallExtended::Particles pl = call->AccessParticles(0);
	UINT64 particleCount = pl.GetCount();
		
	unsigned int vert_stride = pl.GetVertexDataStride();
	const unsigned char *vert = static_cast<const unsigned char*>(pl.GetVertexData());
	if (vert_stride < 12) vert_stride = 12;

	unsigned int extraMemberStride = pl.GetExtraMemberDataStride();
	const unsigned char *extraMember = static_cast<const unsigned char*>(pl.GetExtraMemberData());

	vislib::math::Cuboid<float> bbox_temp = call->AccessBoundingBoxes().ObjectSpaceBBox();
	vislib::math::Point<float,3> minPos = bbox_temp.GetLeftBottomBack();
	vislib::math::Point<float,3> maxPos = bbox_temp.GetRightTopFront();
	vislib::math::Point<float,3> minVel(pl.GetMinVelocity().data());
	vislib::math::Point<float,3> maxVel(pl.GetMaxVelocity().data());
	float minAngle = pl.GetMinRotationAngle();
	float maxAngle = pl.GetMaxRotationAngle();
	float deltaAngle = maxAngle - minAngle;

	for (unsigned int particleIndex = 0; particleIndex < particleCount; ++particleIndex, vert += vert_stride, extraMember += extraMemberStride) {
		vislib::math::Vector<float,4> currentPosition(getNormalizedPosition(vert, minPos, maxPos));
		vislib::math::Vector<float,4> currentVelocity(getNormalizedFeature(extraMember, minVel, maxVel));
		vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extraMember)[3],
															reinterpret_cast<const float*>(extraMember)[4],
															reinterpret_cast<const float*>(extraMember)[5]);
		float currentRotationAngle = currentRotationAxis.Normalise();
		vislib::math::Quaternion<float> currentRotation(currentRotationAngle, currentRotationAxis);
		currentRotation.Normalise(); // just to be sure
		result.push_back(Particle(currentPosition, currentVelocity, (currentRotationAngle - minAngle) / deltaAngle, currentRotation));
	}

	return std::move(result);
}

#ifdef TIME_MEASUREMENT
void clustering::AbstractParticleClusterer::performanceTest(MultiParticleDataCallExtended *dat) {
	std::string fileName = "perfomanceTest";
	fileName.append(this->Name()).append(".csv");
	std::ofstream file(fileName, std::fstream::out);

	if (!file.good()) {
		return;
	}

	file << "frameId;time in milliseconds;number of clusters\n";
	
	unsigned int startFrameId = 0;
	unsigned int endFrameId = 450;
	unsigned int step = 10;

	unsigned int frameIdTmp = dat->FrameID();

	std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
	for (unsigned int currentFrame = startFrameId; currentFrame <= endFrameId; currentFrame += step) {
		if (!getFrame(dat, currentFrame)) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Failed to load frame -> abort performance test\n");
			return;
		}
		std::chrono::time_point<std::chrono::system_clock> currentStart, currentEnd;
		currentStart = std::chrono::system_clock::now();
		this->cluster(dat, true);
		currentEnd = std::chrono::system_clock::now();
		int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(currentEnd-currentStart).count();
		file << currentFrame << ";" << currentElapsedMilliseconds << ";" << this->getNumberClusters() << "\n";
	}
	end = std::chrono::system_clock::now();
	int elapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

	file << ";" << elapsedMilliseconds << ";\n";

	getFrame(dat, frameIdTmp);
}
#endif