/*
 * TrajectoryDistance.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "TrajectoryDistance.h"
#include "vislib/sys/Log.h"

using namespace megamol::vis_particle_clustering;

 // takes ownership of *particleMetric
clustering::TrajectoryDistance::TrajectoryDistance(const Metric<Particle> *particleMetric) : particleMetric(particleMetric) {
}

clustering::TrajectoryDistance::~TrajectoryDistance(void) {
	delete particleMetric;
}

float clustering::TrajectoryDistance::DistanceBetween(const Trajectory &lhs, const Trajectory &rhs, const bool usePosition) const {
	const std::vector<Particle> &lhsTrajectoryParticles = lhs.GetTrajectoryParticles();
	const std::vector<Particle> &rhsTrajectoryParticles = rhs.GetTrajectoryParticles();
	
	if (lhsTrajectoryParticles.size() != rhsTrajectoryParticles.size()) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Cannot compute distance between trajectories of different length\n");
		return (std::numeric_limits<float>::max)();
	}

	float distance = 0.0f;
	unsigned int length = lhsTrajectoryParticles.size();
	for (unsigned int l = 0; l < length; ++l) {
		distance += this->particleMetric->DistanceBetween(lhsTrajectoryParticles[l], rhsTrajectoryParticles[l], usePosition);
	}
	distance /= static_cast<float>(length);
	return distance;
}