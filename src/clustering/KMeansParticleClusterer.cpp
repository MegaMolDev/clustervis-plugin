/*
 * KMeansParticleClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "KMeansParticleClusterer.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/Vector3fParam.h"
#include "mmcore/param/EnumParam.h"
#include "ParticleDistance.h"
#include "ParticleEuclideanDistance.h"
#include <memory>

using namespace megamol::vis_particle_clustering;

clustering::KMeansParticleClusterer::KMeansParticleClusterer(void) : 
		numberClusterSlot("numbercluster", "The number of clusters"),
		numberIterationsSlot("numberiterations", "The number of iterations for kmeans"),
		metricSlot("metric", "The metric used to calculate the distance between particles") {
	
	unsigned int numberClusters = 10;
	unsigned int numberIterations = 20;

	this->numberClusterSlot.SetParameter(new core::param::IntParam(static_cast<int>(numberClusters),1,100));
    this->MakeSlotAvailable(&this->numberClusterSlot);

	this->numberIterationsSlot.SetParameter(new core::param::IntParam(static_cast<int>(numberIterations),1,100));
    this->MakeSlotAvailable(&this->numberIterationsSlot);

	core::param::EnumParam* usedMetric = new core::param::EnumParam(PARTICLEDISTANCE);
	usedMetric->SetTypePair(PARTICLEDISTANCE, "Particle distance");
	usedMetric->SetTypePair(PARTICLEEUCLIDEANDISTANCE, "Particle euclidean distance");
	this->metricSlot.SetParameter(usedMetric);
	this->MakeSlotAvailable(&this->metricSlot);

	kMeans.SetNumberClusters(numberClusters);
	setNumberClusters(numberClusters);

	kMeans.SetNumberIterations(numberIterations);
}


clustering::KMeansParticleClusterer::~KMeansParticleClusterer(void) {
	this->Release();
}

bool clustering::KMeansParticleClusterer::anySlotDirty() {
	bool dirty = false;
	
	if (this->numberClusterSlot.IsDirty()) {
		this->numberClusterSlot.ResetDirty();
		unsigned int numberClusters = static_cast<unsigned int>(this->numberClusterSlot.Param<core::param::IntParam>()->Value());
		kMeans.SetNumberClusters(numberClusters);
		setNumberClusters(numberClusters);
		dirty = true;
	}
	if (this->numberIterationsSlot.IsDirty()) {
		this->numberIterationsSlot.ResetDirty();
		kMeans.SetNumberIterations(this->numberIterationsSlot.Param<core::param::IntParam>()->Value());
		dirty = true;
	}
	if (this->metricSlot.IsDirty()) {
		this->metricSlot.ResetDirty();
		this->kMeans.Reset();
		dirty = true;
	}
	return dirty;
}

// Assumptions: only one particle list with core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ particles
void clustering::KMeansParticleClusterer::cluster(MultiParticleDataCallExtended *dat, bool recluster) {
    if (recluster) {
		this->kMeans.Reset();
	}
	
	std::vector<Particle> particles = buildParticleVector(dat);

	ParticleWeight particleWeight(this->getPositionWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getVelocityWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getRotationWeightSlot().Param<core::param::FloatParam>()->Value());
	
	std::unique_ptr<Metric<Particle> > particleMetricPtr;
	switch(this->metricSlot.Param<core::param::EnumParam>()->Value()) {
		case PARTICLEDISTANCE:
			particleMetricPtr.reset(new ParticleDistance(particleWeight));
			break;
		case PARTICLEEUCLIDEANDISTANCE: 
			particleMetricPtr.reset(new ParticleEuclideanDistance(particleWeight));
			break;
		default:
			return;
	}
	if (particleMetricPtr == nullptr) {
		return;
	}

	kMeans.SetData(&particles, true, *particleMetricPtr);

	kMeans.Run(*particleMetricPtr);
	
	// set clusterdata
	UINT64 all_cnt = dat->AccessParticles(0).GetCount();
    if (dat->GetParticleListCount() != 1 || dat->AccessParticles(0).GetVertexDataType() != core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ) {
		return;
	}
	this->getClusterData().EnforceSize(all_cnt * sizeof(unsigned int));
	unsigned int *f = this->getClusterData().As<unsigned int>();
	kMeans.GetClusters(f);
}