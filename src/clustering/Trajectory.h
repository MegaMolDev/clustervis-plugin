/*
 * Trajectory.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_TRAJECTORY_H_INCLUDED
#define MEGAMOLVPC_TRAJECTORY_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "Particle.h"
#include <vector>

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Trajectory class stores all particles of one trajectory
     */
	class Trajectory	{
	public:
		/** Ctor. */
		Trajectory(unsigned int length = 0);

		/** Ctor. */
		Trajectory(const vislib::math::Point<float,3> &minPos, const vislib::math::Point<float,3>& maxPos, unsigned int length = 0);
		
		/** Dtor. */
		virtual ~Trajectory(void);

		/** Add another trajectory - particles will be accumulated (for mean computation) */
		Trajectory& AddSample(Trajectory const& rhs);

		/** Compute mean of accumulated trajectories */
		Trajectory& ComputeMean();

		/** Add particle to trajectory with computation of difference in position */
		Trajectory& AddTimestepGradientPosition(const Particle& particle);

		/** Add particle to trajectory without any additional computation */
		Trajectory& AddTimestep(const Particle& particle);
	
		/** 
		 * Provide trajectory data (for computation of distances)
		 *
		 * @param usePosition Specify if position data of the particles should be provided
		 * @return vector of trajectory data
		 */
		std::vector<float> GetData(const ParticleWeight &particleWeight, bool usePositionGradient) const;
		
		/** Answer if particle is valid (trajectory contains particles) */
		bool IsValid() const;

		/** Answer number of particles in trajectory */
		inline unsigned int GetSize() const {
			return this->trajectoryParticles.size();
		}

		/** Provide access to particles of trajectory */
		inline const std::vector<Particle>& GetTrajectoryParticles() const {
			return this->trajectoryParticles;
		}

	private:
		/** Compute gradient of current and last position */
		vislib::math::Vector<float,4> computePositionGradient(vislib::math::Vector<float,4> &currentPosition) const;
		
		/** Compute gradient of current and last position with "anti-jump" */
		vislib::math::Vector<float,4> getDifferenceInPosition(vislib::math::Vector<float,4> &currentPosition);
		
		/** Set bounding box of particle positions  - used for "anti-jump" for difference in position */
		void setParticleBoundingbox(const vislib::math::Point<float,3> &minPos, const vislib::math::Point<float,3>& maxPos);
	
		/** Particles of trajectory */
		std::vector<Particle> trajectoryParticles;
	
		/** Original position of last particle in trajectory */
		vislib::math::Vector<float,4> lastPos;
		
		/** Offset in x-direction - used for "anti-jump" for difference in position */
		int offsetX;

		/** Offset in z-direction - used for "anti-jump" for difference in position */
		int offsetZ;

		/** X-width - used for "anti-jump" for difference in position */
		float normalizedDeltaX;

		/** Z-width - used for "anti-jump" for difference in position */
		float normalizedDeltaZ;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_TRAJECTORY_H_INCLUDED */