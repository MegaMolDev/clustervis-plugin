/*
 * AbstractTrajectoryClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "AbstractTrajectoryClusterer.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/BoolParam.h"
#include "helper.h"
#include <algorithm>
#include <fstream>
#include <chrono>

using namespace megamol::vis_particle_clustering;

clustering::AbstractTrajectoryClusterer::AbstractTrajectoryClusterer(void) : 
		firstFrameSlot("firstFrame", "The number of the first frame"),
        lastFrameSlot("lastFrame", "The number of the last frame"),
        frameStepSlot("frameStep", "The step length between two frames"),
		usePositionGradientSlot("usePositionGradient", "Determines the use of the position gradient for clustering"),
		useCompressedTrajectoriesSlot("useCompressedTrajectories", "Determines if trajectories are compressed using PAA") {
	this->firstFrameSlot.SetParameter(new core::param::IntParam(0, 0));
    this->MakeSlotAvailable(&this->firstFrameSlot);

    this->lastFrameSlot.SetParameter(new core::param::IntParam(1000000, 0));
    this->MakeSlotAvailable(&this->lastFrameSlot);

    this->frameStepSlot.SetParameter(new core::param::IntParam(1, 1));
    this->MakeSlotAvailable(&this->frameStepSlot);

	this->usePositionGradientSlot.SetParameter(new core::param::BoolParam(false));
	this->MakeSlotAvailable(&this->usePositionGradientSlot);

	this->useCompressedTrajectoriesSlot.SetParameter(new core::param::BoolParam(false));
	this->MakeSlotAvailable(&this->useCompressedTrajectoriesSlot);
}


clustering::AbstractTrajectoryClusterer::~AbstractTrajectoryClusterer(void) {
	this->Release();
}

bool clustering::AbstractTrajectoryClusterer::outCallChanged(MultiParticleDataCallExtended *outCall) {
	bool dirty = false;
	
	// same effect as if outcall changed -> if slot dirty -> cluster
	if (this->firstFrameSlot.IsDirty() ||
		this->lastFrameSlot.IsDirty() ||
		this->frameStepSlot.IsDirty()) {
		this->firstFrameSlot.ResetDirty();
		this->lastFrameSlot.ResetDirty();
		this->frameStepSlot.ResetDirty();
		dirty = true;
	}

	if (this->usePositionGradientSlot.IsDirty()) {
		this->usePositionGradientSlot.ResetDirty();
		dirty = true;
	}

	if (this->useCompressedTrajectoriesSlot.IsDirty()) {
		this->useCompressedTrajectoriesSlot.ResetDirty();
		dirty = true;
	}

	// outcall changed
	if ((this->getLastHash() != outCall->DataHash()) || 
		(outCall->DataHash() == 0)) {
        this->setLastHash(outCall->DataHash());
		dirty = true;
    }

	return dirty;
}

// compressed trajectories using PAA
std::vector<clustering::Trajectory> clustering::AbstractTrajectoryClusterer::compressTrajectoryVector(const std::vector<Trajectory>& trajectories, const unsigned int factor) const {
	std::cout << "Compressing trajectory vector .........." << std::flush;
	std::cout << "\rCompressing trajectory vector " << std::flush;
	std::vector<Trajectory> compressedTrajectories;
	unsigned int trajectoryCount = trajectories.size();

	compressedTrajectories.reserve(trajectoryCount);

	unsigned int modul = (trajectoryCount - 1) / 10;
	if (modul == 0) {
		modul = 1;
	}
	for (unsigned int trajectoryIndex = 0; trajectoryIndex < trajectoryCount; ++trajectoryIndex) {
		if((trajectoryIndex + 1) % modul == 0) {
			std::cout << "x" << std::flush;
		}
		const Trajectory &currentTrajectory = trajectories[trajectoryIndex];
		const std::vector<Particle> currentTrajectoryParticles = currentTrajectory.GetTrajectoryParticles();
		Trajectory currentTrajectoryCompressed;

		unsigned int currentTrajectoryLength = currentTrajectory.GetSize();

		if (currentTrajectoryLength < factor) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "Factor for compression of trajectories too large - using uncompressed trajectories");
			return trajectories;
		}
		
		Particle compressedParticles;
		for (unsigned int currentTrajectoryParticleIndex = 0; currentTrajectoryParticleIndex < currentTrajectoryLength; ++currentTrajectoryParticleIndex) {
			if (currentTrajectoryParticleIndex > 0 && currentTrajectoryParticleIndex % factor == 0) {
				currentTrajectoryCompressed.AddTimestep(compressedParticles.ComputeMean());
				compressedParticles = Particle();
			}
			compressedParticles.AddSample(currentTrajectoryParticles[currentTrajectoryParticleIndex]);
		}
		currentTrajectoryCompressed.AddTimestep(compressedParticles.ComputeMean()); // add last segment (can be shorter than the others)
		compressedTrajectories.push_back(currentTrajectoryCompressed);
	}

	std::cout << " done" << std::endl;

	return std::move(compressedTrajectories);
}

std::vector<clustering::Trajectory> clustering::AbstractTrajectoryClusterer::buildTrajectoryVector(MultiParticleDataCallExtended *call) {
	vislib::math::Cuboid<float> bbox_temp = call->AccessBoundingBoxes().ObjectSpaceBBox();
	vislib::math::Point<float,3> minPos = bbox_temp.GetLeftBottomBack();
	vislib::math::Point<float,3> maxPos = bbox_temp.GetRightTopFront();
	vislib::math::Point<float,3> minVel(call->GetMinVelocity().data());
	vislib::math::Point<float,3> maxVel(call->GetMaxVelocity().data());
	float minAngle = call->GetMinRotationAngle();
	float maxAngle = call->GetMaxRotationAngle();
	float deltaAngle = maxAngle - minAngle;

	int minF = this->firstFrameSlot.Param<core::param::IntParam>()->Value();
    int maxF = this->lastFrameSlot.Param<core::param::IntParam>()->Value();
    int lenF = this->frameStepSlot.Param<core::param::IntParam>()->Value();

	maxF = std::min<unsigned int>(maxF, call->FrameCount());
	this->lastFrameSlot.Param<core::param::IntParam>()->SetValue(maxF, false);

	setFirstFrame(minF);
	setLastFrame(maxF);
	setFrameStep(lenF);

	std::cout << minF << ", " << maxF  << ", " << lenF << std::endl;

	unsigned int numberTimesteps = (maxF - minF) / lenF + 1;
	if (numberTimesteps < 2) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN,
                        "Only %u timesteps used for trajectory clustering\n", numberTimesteps);
	}

	std::vector<Trajectory> result;
	unsigned int trajectoryCount = call->AccessParticles(0).GetCount();
	result.resize(trajectoryCount, Trajectory(minPos, maxPos, numberTimesteps));

	unsigned int frameIdTmp = call->FrameID();

	std::cout << "Building trajectory vector .........." << std::flush;
	std::cout << "\rBuilding trajectory vector " << std::flush;
	
	unsigned int modul = (numberTimesteps - 1) / 10;
	if (modul == 0) {
		modul = 1;
	}

	unsigned int cnt = 0;
	for (int timestep = minF; timestep <= maxF; timestep += lenF, ++cnt) {
		if((cnt + 1) % modul == 0) {
			std::cout << "x" << std::flush;
		}
		// load data for timestep
		if (!getFrame(call, timestep)) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Could not load timestep -> you'll get an invalid trajectory\n");
			return std::vector<Trajectory>();
		}
		
		MultiParticleDataCallExtended::Particles pl = call->AccessParticles(0);
		UINT64 particleCount = pl.GetCount();
		
		unsigned int vert_stride = pl.GetVertexDataStride();
		const unsigned char *vert = static_cast<const unsigned char*>(pl.GetVertexData());
		if (vert_stride < 12) vert_stride = 12;

		unsigned int extraMemberStride = pl.GetExtraMemberDataStride();
		const unsigned char *extraMember = static_cast<const unsigned char*>(pl.GetExtraMemberData());

		for (unsigned int particleIndex = 0; particleIndex < particleCount; ++particleIndex) {
			vislib::math::Vector<float,4> currentPosition(getNormalizedPosition(vert, minPos, maxPos));
			vislib::math::Vector<float,4> currentVelocity(getNormalizedFeature(extraMember, minVel, maxVel));
			vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extraMember)[3],
																reinterpret_cast<const float*>(extraMember)[4],
																reinterpret_cast<const float*>(extraMember)[5]);
			float currentRotationAngle = currentRotationAxis.Normalise();
			vislib::math::Quaternion<float> currentRotation(currentRotationAngle, currentRotationAxis);
			currentRotation.Normalise(); // just to be sure
			result[particleIndex].AddTimestepGradientPosition(Particle(currentPosition, currentVelocity, (currentRotationAngle - minAngle) / deltaAngle, currentRotation));
			if (!result[particleIndex].IsValid()) {
				vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "error: invalid trajectory\n");
				// try "reset" call to frameID it had before
				getFrame(call, frameIdTmp);
				return std::vector<Trajectory>();
			}
			vert += vert_stride;
			extraMember += extraMemberStride;
		}
	}

	// try "reset" call to frameID it had before
	getFrame(call, frameIdTmp);

	std::cout << " done" << std::endl;

	return std::move(result);
}

#ifdef TIME_MEASUREMENT
void clustering::AbstractTrajectoryClusterer::performanceTest(MultiParticleDataCallExtended *dat) {
	std::string fileName = "perfomanceTest";
	fileName.append(this->Name()).append(".csv");
	std::ofstream file(fileName, std::fstream::out);

	if (!file.good()) {
		return;
	}

	file << "number of timesteps;time in milliseconds;number of clusters\n";
	
	int minF = this->firstFrameSlot.Param<core::param::IntParam>()->Value();
    int maxF = this->lastFrameSlot.Param<core::param::IntParam>()->Value();
    int lenF = this->frameStepSlot.Param<core::param::IntParam>()->Value();

	unsigned int startFrameId = 0;
	unsigned int frameStep = 1;
	unsigned int minNumberTimesteps = 5;
	unsigned int maxNumberTimesteps = 25;
	unsigned int timestepStep = 5;

	for (unsigned int currentNumberTimesteps = minNumberTimesteps; currentNumberTimesteps <= maxNumberTimesteps; currentNumberTimesteps += timestepStep) {
		unsigned int endFrameId = startFrameId + currentNumberTimesteps * frameStep - 1;
		
		if (endFrameId >= dat->FrameCount()) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Not enough frames\n");
			break;
		}

		this->firstFrameSlot.Param<core::param::IntParam>()->SetValue(startFrameId, false);
		this->frameStepSlot.Param<core::param::IntParam>()->SetValue(frameStep, false);
		this->lastFrameSlot.Param<core::param::IntParam>()->SetValue(endFrameId, false);
		
		std::chrono::time_point<std::chrono::system_clock> currentStart, currentEnd;
		currentStart = std::chrono::system_clock::now();
		this->cluster(dat, true);
		currentEnd = std::chrono::system_clock::now();
		int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(currentEnd-currentStart).count();
		file << currentNumberTimesteps << ";" << currentElapsedMilliseconds << ";" << this->getNumberClusters() << "\n";
	}

	this->firstFrameSlot.Param<core::param::IntParam>()->SetValue(minF, false);
	this->frameStepSlot.Param<core::param::IntParam>()->SetValue(lenF, false);
	maxF = std::min<unsigned int>(maxF, dat->FrameCount());
	this->lastFrameSlot.Param<core::param::IntParam>()->SetValue(maxF, false);
}
#endif