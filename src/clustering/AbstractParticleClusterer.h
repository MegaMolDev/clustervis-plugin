/*
 * AbstractParticleClusterer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_ABSTRACTPARTICLECLUSTERER_H_INCLUDED
#define MEGAMOLVPC_ABSTRACTPARTICLECLUSTERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */


#include "AbstractClusterer.h"
#include "Particle.h"
#include "helper.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Abstract base class for all ParticleClusterer
     */
	class AbstractParticleClusterer : public AbstractClusterer {
	public:
		/** Ctor. */
		AbstractParticleClusterer(void);

		/** Dtor. */
		virtual ~AbstractParticleClusterer(void);

		/** 
		 * Construct a vector of all particle of current timestep
		 *
		 * @return Vector of all particles
		 */
		std::vector<Particle> buildParticleVector(MultiParticleDataCallExtended *call);

	private:
		/** Cluster particles */
		virtual void cluster(MultiParticleDataCallExtended *dat, bool recluster) = 0;

		/** Change in data? Need to know if data needs to be reclustered */
		bool outCallChanged(MultiParticleDataCallExtended *outCall);
	
	#ifdef TIME_MEASUREMENT
		/** Measurement of ellapsed time for clustering */
		void performanceTest(MultiParticleDataCallExtended *dat);
	#endif
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_ABSTRACTPARTICLECLUSTERER_H_INCLUDED */