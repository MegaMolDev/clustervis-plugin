/*
 * Particle.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_PARTICLE_H_INCLUDED
#define MEGAMOLVPC_PARTICLE_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "vislib/math/Quaternion.h"
#include "vislib/math/Matrix.h"
#include "ParticleWeight.h"
#include <vector>

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Particle class stores all features of one particle
     */
	class Particle	{
	public:
		/** Ctor. */
		Particle(vislib::math::Vector<float,4> position = vislib::math::Vector<float,4>(), 
				vislib::math::Vector<float,4> velocity = vislib::math::Vector<float,4>(), 
				float rotationAngle = 0.0f,
				vislib::math::Quaternion<float> rotation = vislib::math::Quaternion<float>(0,0,0,0));
		
		/** Dtor. */
		virtual ~Particle(void);

		/** Add another Particle - features will be accumulated (for mean computation) */
		Particle& AddSample(const Particle &rhs);
		
		/** Compute mean of accumulated particles */
		Particle& ComputeMean();

		//Particle& SubtractMean(const Particle &mean);

		/** 
		 * Provide particle data (for computation of distances)
		 *
		 * @param usePosition Specify if position data should be provided
		 * @return vector of particle data
		 */
		std::vector<float> &GetData(const ParticleWeight &particleWeight, bool usePosition) const;
		
		/** Answer if particle is valid (its features are valid) */
		bool IsValid() const;

		/** Answer number of particles - always one (only needed for trajectories) */
		inline unsigned int GetSize() const {
			return 1;
		}

		inline const vislib::math::Vector<float,4>& GetPosition() const {
			return this->position;
		}
		inline const vislib::math::Vector<float,4>& GetVelocity() const {
			return this->velocity;
		}
		inline const float GetRotationAngle() const {
			return this->rotationAngle;
		}
		inline const vislib::math::Quaternion<float>& GetRotation() const {
			return this->rotation;
		}
		inline const vislib::math::Matrix<float,4, vislib::math::MatrixLayout::ROW_MAJOR>& GetCovRotations() const {
			return this->covRotations;
		}

		inline void SetPosition(vislib::math::Vector<float,4> position) {
			this->position = position;
		}
		inline void SetVelocity(vislib::math::Vector<float,4> velocity) {
			this->velocity = velocity;
		}
		inline void SetRotationAngle(float rotationAngle) {
			this->rotationAngle = rotationAngle;
		}

	private:
		/** feature-data: position - 4. component is the number of accumulated particles (for mean computation) */
		vislib::math::Vector<float,4> position;
		
		/** feature-data: velocity - 4. component is the number of accumulated particles (for mean computation) */
		vislib::math::Vector<float,4> velocity;

		/** feature-data: rotation */
		vislib::math::Quaternion<float> rotation;

		/** Covarianzmatrix for computation of mean rotation */
		vislib::math::Matrix<float,4, vislib::math::MatrixLayout::ROW_MAJOR> covRotations;
		
		/** feature-data: rotation angle */
		float rotationAngle;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_PARTICLE_H_INCLUDED */