/*
 * Trajectory.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "Trajectory.h"
#include "vislib/sys/Log.h"
#include "helper.h"

using namespace megamol::vis_particle_clustering;

clustering::Trajectory::Trajectory(unsigned int length) : offsetX(0), offsetZ(0), normalizedDeltaX(1.0f), normalizedDeltaZ(1.0f) {
	if (length > 0) {
		try {
			trajectoryParticles.reserve(length);
		} catch (const std::length_error& le) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "length exceeds max_length\n");
		} catch (std::bad_alloc& ba) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "Could not reserve enough memory\n");
		}
	}
}

clustering::Trajectory::Trajectory(const vislib::math::Point<float,3> &minPos, const vislib::math::Point<float,3>& maxPos, unsigned int length) : offsetX(0), offsetZ(0) {
	setParticleBoundingbox(minPos, maxPos);
	if (length > 0) {
		try {
			trajectoryParticles.reserve(length);
		} catch (const std::length_error& le) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "length exceeds max_length\n");
		} catch (std::bad_alloc& ba) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "Could not reserve enough memory\n");
		}
	}
}

clustering::Trajectory::~Trajectory(void) {
}

clustering::Trajectory& clustering::Trajectory::AddSample(Trajectory const& rhs) {
	unsigned int length = this->GetSize();

	const std::vector<Particle>& rhsTrajectoryParticles = rhs.GetTrajectoryParticles();
	if (length == 0) { // init Trajectory with rhs
		this->trajectoryParticles = std::vector<Particle>(rhsTrajectoryParticles);
		return *this;
	}

	ASSERT(length == rhs.GetSize() && "Length lhs != length rhs"); 
	for (unsigned int l = 0; l < length; ++l) {
		this->trajectoryParticles[l].AddSample(rhsTrajectoryParticles[l]);
	}

	return *this;
}

bool clustering::Trajectory::IsValid() const {
	return !(this->trajectoryParticles.size() == 0);
}

clustering::Trajectory& clustering::Trajectory::ComputeMean() {
	unsigned int length = this->GetSize();

	for (unsigned int l = 0; l < length; ++l) {
		this->trajectoryParticles[l].ComputeMean();
	}

	return *this;
}

void clustering::Trajectory::setParticleBoundingbox(const vislib::math::Point<float,3> &minPos, const vislib::math::Point<float,3>& maxPos) {
	float deltaMax = getDeltaMax(minPos, maxPos);
	this->normalizedDeltaX = (maxPos[0] - minPos[0]) / deltaMax;
	this->normalizedDeltaZ = (maxPos[2] - minPos[2]) / deltaMax;
}

vislib::math::Vector<float,4> clustering::Trajectory::computePositionGradient(vislib::math::Vector<float,4> &currentPosition) const {
	currentPosition.SetX(currentPosition.GetX() + offsetX * normalizedDeltaX);
	currentPosition.SetZ(currentPosition.GetZ() + offsetZ * normalizedDeltaZ);
	return currentPosition - lastPos; // TODO: /2? - sollte ja eigentlich egal sein, wenn ich es konsequent bei allen weglasse
}

vislib::math::Vector<float,4> clustering::Trajectory::getDifferenceInPosition(vislib::math::Vector<float,4> &currentPosition) {
	vislib::math::Vector<float,4> diffPos = computePositionGradient(currentPosition);
		
	if (diffPos.GetX() > normalizedDeltaX/2.0f || 
		-diffPos.GetX() > normalizedDeltaX/2.0f) { // jump in streamwise direction
		offsetX += sign(diffPos.GetX());
		diffPos = computePositionGradient(currentPosition);
	}
	if (diffPos.GetZ() > normalizedDeltaZ/2.0f ||
		-diffPos.GetZ() > normalizedDeltaZ/2.0f) { // jump in spanwise direction
		offsetZ += sign(diffPos.GetZ());
		diffPos = computePositionGradient(currentPosition);
	}

	return diffPos;
}

clustering::Trajectory& clustering::Trajectory::AddTimestepGradientPosition(const clustering::Particle& particle) {
	// use difference in position instead of position for trajectory clustering
	// first timestep: difference in position = position (since postemp = (0,0,0,0)) -> but gradient should be zero
	vislib::math::Vector<float,4> diffPos;
	vislib::math::Vector<float,4> currentPosition = particle.GetPosition();

	unsigned int length = this->GetSize();

	if (length > 0) {
		diffPos = getDifferenceInPosition(currentPosition);
	}
	diffPos.SetW(1.0f); // counter

	lastPos = currentPosition;

//	particle.SetPosition(diffPos);
	try {
		trajectoryParticles.push_back(particle);
        trajectoryParticles.back().SetPosition(diffPos);
	} catch (std::bad_alloc& ba) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "not enough memory -> you'll get an invalid trajectory\n");
		trajectoryParticles.clear();
	}

	return *this;
}

clustering::Trajectory& clustering::Trajectory::AddTimestep(const Particle &particle) {
	try {
		trajectoryParticles.push_back(particle);
	} catch (std::bad_alloc& ba) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "not enough memory -> you'll get an invalid trajectory\n");
		trajectoryParticles.clear();
	}

	return *this;
}

std::vector<float> clustering::Trajectory::GetData(const ParticleWeight &particleWeight, bool usePositionGradient) const {
	std::vector<float> data;
	unsigned int length = this->GetSize();

	for (unsigned int l = 0; l < length; ++l) {
		std::vector<float>& particleData = this->trajectoryParticles[l].GetData(particleWeight, usePositionGradient);
		unsigned int particleLength = particleData.size();
		for (unsigned int pl = 0; pl < particleLength; ++pl){
			data.push_back(particleData[pl]);
		}
	}

	return data;
}