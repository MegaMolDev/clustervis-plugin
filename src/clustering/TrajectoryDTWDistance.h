/*
 * TrajectoryDTWDistance.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_TRAJECTORYDTWDISTANCE_H_INCLUDED
#define MEGAMOLVPC_TRAJECTORYDTWDISTANCE_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "Trajectory.h"
#include "Metric.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Class for computation of DTW distance between trajectories
     */
	class TrajectoryDTWDistance : public Metric<Trajectory>	{
	public:
		/** Ctor. */
		TrajectoryDTWDistance(const Metric<Particle> *particleMetric);
		
		/** Dtor. */
		virtual ~TrajectoryDTWDistance(void);

		/** Computation of distance between trajectories */
		float DistanceBetween(const Trajectory &lhs, const Trajectory &rhs, const bool usePosition) const;
		
		/** Answer name (for logging) */
		inline const std::string GetName() const {
			return std::string("Trajectory DTW Distance - ").append(particleMetric->GetName());
		};

	private:
		/** Particle metric used for distance computation between the particles of the trajectory */
		const Metric<Particle> *particleMetric;
	};


} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_TRAJECTORYDTWDISTANCE_H_INCLUDED */