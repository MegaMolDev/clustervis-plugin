/*
 * Meanshift.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_MEANSHIFT_H_INCLUDED
#define MEGAMOLVPC_MEANSHIFT_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "fams.h"
#include "vislib/RawStorage.h"
#include <string>
#include <vector>
#include "MultiParticleDataCallExtended.h"
#include "ParticleWeight.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Template class for meanshift clustering of Samples (Particle or Trajectory)
     */
	template <class Sample>
	class Meanshift {
	public:
		/** Ctor. */
		Meanshift(void);

		/** Dtor. */
		virtual ~Meanshift(void);

		/** 
		  * Init meanshift
		  * K,L are the LSH parameters. When they are both zero the LSH data
		  * structure is not built and the linear algorithm is run.
		  * jump and percent are two ways to choose from which points to start the mean shift procedure. 
		  * The default is to perform the procedure on every point. 
		  * Obviously, each run of the program will yield a different set. Only one of these options can be chosen.
		  * The width*d (d the dimension of the data) is the distance under the L1 norm used as the fixed bandwidth.
		  *
		  * @param k_neigh is the number of neighbors used in the construction of the pilot density.
		  * @param jump means once every jump a point is selected. if jump is 5 then points 1,6,11,16... are chosen. 
		  * @param percent means that percent/100*n (n the number of points) are chosen at random with replacement. 
		  * @param width enables the user to run the fixed bandwidth mean shift procedure.
		  */
		void Init(int K, int L, int k_neigh, int jump = 1, double percent = 0.0, float width = -1.0f);
	
		/** Set data and weights */
		void SetData(const std::vector<Sample> &data, const ParticleWeight &particleWeight, const bool usePosition);
		
		/** 
		  * the program computes automatically the optimal K and L.
		  * the optimal K is searched between Kmin and K (the first parameter) with step Kjump
		  * the optimal L is searched between 1 and L (second parameter)
		  * epsilon represents the allowed error (in experiments epsilon = 0.05)
		  */
		void FindKL(int Kmin, int Kmax, int Kjump, int Lmax, float epsilon);
		
		/** Run meanshift */
		void Run();

		/** 
		  * Return clustering
		  *
		  * @return number of clusters
		  */
		unsigned int GetClusters(unsigned int *clusterData);

		int const GetK() const {
			 return this->K;
		}

		int const GetL() const {
			 return this->L;
		}

	private:
		/** Normalize and set points */
		void setFamsPoints();

		/** Search for nearest mode to get index of cluster */
		int findNearestMode(std::vector<std::vector<float> > &modes, std::vector<float> &testMode);

		/** fams implementation */
		FAMS fams;

		int K;
		int L;
		int k_neigh;
		int jump;
		double percent;
		float width;

		/** Name of txt file in which the pilot densities are stored */
		const std::string pilot_fn;

		/** Data points before normalization */
		std::vector<std::vector<float> > pttemp;
	};

	template <class Sample>
	clustering::Meanshift<Sample>::Meanshift(void) : fams(0), jump(1), percent(0.0), width(-1.0f), pilot_fn("pilot.txt") {
	}

	template <class Sample>
	clustering::Meanshift<Sample>::~Meanshift(void) {
		std::remove(pilot_fn.c_str());
	}

	template <class Sample>
	void clustering::Meanshift<Sample>::Init(int K, int L, int k_neigh, int jump, double percent, float width) {
		std::remove(pilot_fn.c_str()); // remove bandwidth file if it exists
	
		this->K = K;
		this->L = L;
		this->k_neigh = k_neigh;
		this->jump = jump;
		this->percent = percent;
		this->width = width;
	
		fams.noLSH_ = (K<=0) || (L <= 0);

		if (jump<1) jump=1;
		if ((percent<0) || (percent>1)) percent = 0;
	}

	template <class Sample>
	void clustering::Meanshift<Sample>::FindKL(int Kmin, int Kmax, int Kjump, int Lmax, float epsilon) {
		std::remove(pilot_fn.c_str());

		fams.FindKL(Kmin, Kmax, Kjump, Lmax, this->k_neigh, this->width, epsilon, this->K, this->L);

		std::cout << "K: " << K << ", L: " << L << std::endl;
	}

	template <class Sample>
	void clustering::Meanshift<Sample>::SetData(const std::vector<Sample> &data, const ParticleWeight &particleWeight, const bool usePosition) {
		fams.CleanPoints();
		fams.n_ = data.size(); 	//n_ bumber of points, d_ dimension of points
		fams.d_ = (4 + 3 * usePosition) * data[0].GetSize();
		std::cout << fams.n_ << ", " << fams.d_ << std::endl;

		pttemp.clear();
		pttemp.reserve(fams.n_);

		for (int index = 0; index < fams.n_; ++index) {
			pttemp.push_back(data[index].GetData(particleWeight, usePosition));
		}

		setFamsPoints();
	}

	template <class Sample>
	void clustering::Meanshift<Sample>::setFamsPoints() {
		int i,j;
		// allocate and convert to integer
		for (i=0, fams.minVal_=pttemp[0][0], fams.maxVal_=pttemp[0][0]; i<fams.n_; ++i) {
			for (j=0; j<fams.d_; ++j) {
				if (fams.minVal_>pttemp[i][j])
					fams.minVal_ = pttemp[i][j];
				else if (fams.maxVal_<pttemp[i][j])
					fams.maxVal_ = pttemp[i][j];
			}
		}

		fams.data_ = new unsigned short[fams.n_*fams.d_];
		fams.rr_ = new double[fams.d_];
		fams.hasPoints_ = 1;
		float deltaVal = fams.maxVal_-fams.minVal_;
		if (deltaVal == 0) deltaVal = 1;
		for (i=0; i<fams.n_; i++) {
			for (j=0; j<fams.d_; ++j) {
				fams.data_[i*fams.d_ + j] = (unsigned short) (65535.0*(pttemp[i][j]-fams.minVal_)/deltaVal);
			}
		}
		pttemp.clear();
		fams.dataSize_ = fams.d_*sizeof(unsigned short);
 
		fams.points_ = new fams_point[fams.n_];
		unsigned short* dtempp;
		for (i=0, dtempp=fams.data_; i<fams.n_; i++, dtempp+=fams.d_) {
			fams.points_[i].data_ = dtempp;
			fams.points_[i].usedFlag_ = 0;
		}
	}

	template <class Sample>
	void clustering::Meanshift<Sample>::Run() {
		char *pilot_fn_cstr = new char[pilot_fn.length() + 1];
		strcpy(pilot_fn_cstr, pilot_fn.c_str());
		// fams won't take const char* :(
		fams.RunFAMS(this->K, this->L, this->k_neigh, this->percent, this->jump, this->width, pilot_fn_cstr);
	
		delete [] pilot_fn_cstr;
	}

	#ifdef _MSC_VER
	#pragma push_macro("max")
	#undef max
	#endif /* _MSC_VER */

	template <class Sample>
	int clustering::Meanshift<Sample>::findNearestMode(std::vector<std::vector<float> > &modes, std::vector<float> &testMode) {
		int idx = 0;
		int clusterIdx = -1;
		float minDist = std::numeric_limits<float>::max();
		std::vector<std::vector<float> >::iterator it;
		for (it = modes.begin(); it < modes.end(); ++it, ++idx) {
			std::vector<float> currentMode = *it;
			float dist = 0.0f;
			for (unsigned int i = 0; i < currentMode.size(); ++i) {
				dist += std::pow(currentMode[i] - testMode[i], 2);
			}
			if (dist < minDist) {
				minDist = dist;
				clusterIdx = idx;
			}
		}
		return clusterIdx;
	}

	#ifdef _MSC_VER
	#pragma pop_macro("max")
	#endif /* _MSC_VER */

	template <class Sample>
	unsigned int clustering::Meanshift<Sample>::GetClusters(unsigned int *clusterData) {
		if (fams.nsel_ < 1)
		   return -1;
    
		std::vector<std::vector<float> > modes;
		std::vector<float> modesTmp;

		std::vector<std::vector<float> > prunedModes;
		int i,j,idx;
		idx = 0;
	
		for (i=0; i<fams.npm_; i++) {
			std::vector<float> mode(fams.d_);
		   for (j=0; j<fams.d_; j++) {
			  mode[j] = fams.prunedmodes_[idx++] * (fams.maxVal_ - fams.minVal_) / 65535.0f + fams.minVal_;
		   }
		   prunedModes.push_back(mode);
		}

		idx = 0;
		for (i=0; i<fams.nsel_; i++) {
			std::vector<float> mode(fams.d_);
			for (j=0; j<fams.d_; j++) {
				mode[j] = fams.modes_[idx++] * (fams.maxVal_ - fams.minVal_) / 65535.0f + fams.minVal_;
				modesTmp.push_back(mode[j]);
			}
			int clusterId = findNearestMode(prunedModes, mode);
			clusterData[i] = static_cast<unsigned int>(clusterId);
		}

		return prunedModes.size();
	}

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_MEANSHIFT_H_INCLUDED */