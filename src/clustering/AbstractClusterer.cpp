/*
 * AbstractClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "AbstractClusterer.h"
#include "vislib/sys/Thread.h"
#include "vislib/sys/Log.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/Vector3fParam.h"
#include "mmcore/param/ButtonParam.h"
// #include "helper.h"
#include <algorithm>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

clustering::AbstractClusterer::AbstractClusterer(void) : Module(),
		putDataSlot("putdata", "Connects from the data consumer"),
		getDataSlot("getdata", "Connects to the data source"),
		positionWeightSlot("position weight", "The weight of position for error metric"),
		velocityWeightSlot("velocity weight", "The weight of velocity position for error metric"),
		rotationWeightSlot("rotation weight", "The weight of rotation for error metric"),
		reclusterSlot("recluster", "Recluster the data."),
	#ifdef TIME_MEASUREMENT
		performanceTestSlot("performancetest", "Performance Test."),
	#endif
		numberClusters(0), firstFrame(-1), lastFrame(-1), frameStep(-1),
		lastHash(0), newHash(0), clusterData() {

	this->putDataSlot.SetCallback(MultiParticleClusterDataCall::ClassName(), "GetData", &AbstractClusterer::getDataCallback);
    this->putDataSlot.SetCallback(MultiParticleClusterDataCall::ClassName(), "GetExtent", &AbstractClusterer::getExtentCallback);
    this->MakeSlotAvailable(&this->putDataSlot);

    this->getDataSlot.SetCompatibleCall<MultiParticleDataCallExtendedDescription>();
    this->MakeSlotAvailable(&this->getDataSlot);

	this->positionWeightSlot.SetParameter(new core::param::Vector3fParam(	vislib::math::Vector<float,3>(), 
																			vislib::math::Vector<float,3>(), 
																			vislib::math::Vector<float,3>(5.0f, 5.0f, 5.0f)));
    this->MakeSlotAvailable(&this->positionWeightSlot);

	this->velocityWeightSlot.SetParameter(new core::param::Vector3fParam(	vislib::math::Vector<float,3>(1.0f, 0.3f, 1.0f), 
																			vislib::math::Vector<float,3>(), 
																			vislib::math::Vector<float,3>(5.0f, 5.0f, 5.0f)));
    this->MakeSlotAvailable(&this->velocityWeightSlot);

	this->rotationWeightSlot.SetParameter(new core::param::FloatParam(1.0f, 0.0f, 5.0f));
    this->MakeSlotAvailable(&this->rotationWeightSlot);

	this->reclusterSlot.SetParameter(new core::param::ButtonParam());
	this->MakeSlotAvailable(&this->reclusterSlot);

#ifdef TIME_MEASUREMENT
	this->performanceTestSlot.SetParameter(new core::param::ButtonParam());
	this->MakeSlotAvailable(&this->performanceTestSlot);
#endif
}


clustering::AbstractClusterer::~AbstractClusterer(void) {
	this->Release();
}

bool clustering::AbstractClusterer::create(void) {
    // intentionally empty
    return true;
}


void clustering::AbstractClusterer::release(void) {
    // intentionally empty
}

bool clustering::AbstractClusterer::getDataCallback(core::Call& caller) {
    MultiParticleClusterDataCall *inCall = dynamic_cast<MultiParticleClusterDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleDataCallExtended *outCall = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
    if (outCall == nullptr) return false;

    outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall

    if ((*outCall)(0)) { // GetDataCallback for outCall
        bool updateData = false;
		
		if (outCallChanged(outCall)) {
            updateData = true;
        }
	
		if (anySlotDirty()) {
			updateData = true;
        }

		if (this->positionWeightSlot.IsDirty() || 
			this->velocityWeightSlot.IsDirty() || 
			this->rotationWeightSlot.IsDirty()) {
			this->positionWeightSlot.ResetDirty();
			this->velocityWeightSlot.ResetDirty();
			this->rotationWeightSlot.ResetDirty();
			updateData = true;
		}

		if (this->reclusterSlot.IsDirty()) {
			updateData = true;
		}

		inCall->SetFrameID(outCall->FrameID());

	#ifdef TIME_MEASUREMENT
		if (performanceTestSlot.IsDirty()) {
			this->performanceTest(outCall);
			this->performanceTestSlot.ResetDirty();
			updateData = true;
		}
	#endif

		if (updateData)	{
 			++newHash;
			if (newHash == lastHash) {
				++newHash; // make sure that change is noticed by following modules
			}
			this->cluster(outCall, this->reclusterSlot.IsDirty());
			this->reclusterSlot.ResetDirty();
		}

		inCall->SetDataHash(newHash);
		inCall->SetFirstFrame(firstFrame);
		inCall->SetLastFrame(lastFrame);
		inCall->SetFrameStep(frameStep);
		
        size_t cnt = 0;
        unsigned int plc = outCall->GetParticleListCount();
		inCall->SetParticleListCount(plc);
        for (unsigned int pli = 0; pli < plc; pli++) {
            ClusteredSphericalParticles &p = inCall->AccessParticles(pli);
			SphericalParticles &po = outCall->AccessParticles(pli);
            p.SetGlobalRadius(po.GetGlobalRadius());
			p.SetGlobalColour(po.GetGlobalColour()[0],po.GetGlobalColour()[1],po.GetGlobalColour()[2]);
            p.SetCount(po.GetCount());
			p.SetVertexData(po.GetVertexDataType(), po.GetVertexData(), po.GetVertexDataStride());
			p.SetColourData(po.GetColourDataType(), po.GetColourData(), po.GetColourDataStride());
			p.SetNumberClusters(numberClusters);
			p.SetClusterData(this->clusterData.At(cnt * sizeof(unsigned int)), sizeof(unsigned int));
			cnt += static_cast<size_t>(p.GetCount());
        }

		inCall->SetUnlocker(new Unlocker(outCall->GetUnlocker()), false);
        outCall->SetUnlocker(nullptr, false);

        return true;
    }

    return false;
}

bool clustering::AbstractClusterer::getExtentCallback(core::Call& caller) {
    MultiParticleClusterDataCall *inCall = dynamic_cast<MultiParticleClusterDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleDataCallExtended *outCall = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
    if (outCall == nullptr) return false;

	outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall
	
    if ((*outCall)(1)) { // GetExtentCallback for outCall
		outCall->SetUnlocker(nullptr, false);

        inCall->SetFrameCount(outCall->FrameCount());
		inCall->AccessBoundingBoxes().Clear();
		inCall->AccessBoundingBoxes().SetObjectSpaceBBox(outCall->AccessBoundingBoxes().ObjectSpaceBBox());
		inCall->AccessBoundingBoxes().SetObjectSpaceClipBox(outCall->AccessBoundingBoxes().ObjectSpaceClipBox());
		inCall->SetDataHash(outCall->DataHash());

		return true;
	}

    return false;
}