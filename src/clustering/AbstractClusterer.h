/*
 * AbstractClusterer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_ABSTRACTCLUSTERER_H_INCLUDED
#define MEGAMOLVPC_ABSTRACTCLUSTERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Module.h"
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/ParamSlot.h"
#include "MultiParticleDataCallExtended.h"
#include "MultiParticleClusterDataCall.h"
#include "vislib/RawStorage.h"
#include "vislib/memutils.h"

//#define TIME_MEASUREMENT

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Abstract base class for all Clusterer
     */
	class AbstractClusterer : public core::Module{
	public:
		/** Ctor. */
		AbstractClusterer(void);

		/** Dtor. */
		virtual ~AbstractClusterer(void);

	protected:
		inline void setNumberClusters(unsigned int numberClusters) {
			this->numberClusters = numberClusters;
		}
		inline void setFirstFrame(int firstFrame) {
			this->firstFrame = firstFrame;
		}
		inline void setLastFrame(int lastFrame) {
			this->lastFrame = lastFrame;
		}
		inline void setFrameStep(unsigned int frameStep) {
			this->frameStep = frameStep;
		}

		inline unsigned int getNumberClusters() const {
			return this->numberClusters;
		}
		inline vislib::RawStorage& getClusterData() {
			return this->clusterData;
		}
		inline core::param::ParamSlot& getPositionWeightSlot() {
			return this->positionWeightSlot;
		}
		inline core::param::ParamSlot& getVelocityWeightSlot() {
			return this->velocityWeightSlot;
		}
		inline core::param::ParamSlot& getRotationWeightSlot() {
			return this->rotationWeightSlot;
		}

		inline unsigned int getLastFrameId() const {
			return this->lastFrameId;
		}
		inline size_t getLastHash() const {
			return this->lastHash;
		}
		inline void setLastFrameId(unsigned int lastFrameId) {
			this->lastFrameId = lastFrameId;
		}
		inline void setLastHash(size_t lastHash) {
			this->lastHash = lastHash;
		}

	private:
		class Unlocker : public MultiParticleDataCallExtended::Unlocker {
        public:
            Unlocker(MultiParticleDataCallExtended::Unlocker *inner) :
                    MultiParticleDataCallExtended::Unlocker(), inner(inner) {
                // intentionally empty
            }
            virtual ~Unlocker(void) {
                this->Unlock();
            }
            virtual void Unlock(void) {
                if (this->inner != nullptr) {
                    this->inner->Unlock();
                    SAFE_DELETE(this->inner);
                }
            }
        private:
            MultiParticleDataCallExtended::Unlocker *inner;
        };

		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
		virtual bool create(void);

		/**
         * Implementation of 'Release'.
         */
		virtual void release(void);

		/**
         * Gets the data from the source.
         *
         * @param caller The calling call.
         *
         * @return 'true' on success, 'false' on failure.
         */
		bool getDataCallback(core::Call& caller);

		/**
         * Gets the data from the source.
         *
         * @param caller The calling call.
         *
         * @return 'true' on success, 'false' on failure.
         */
		bool getExtentCallback(core::Call& caller);

		/** Clustering of data */
		virtual void cluster(MultiParticleDataCallExtended *dat, bool recluster) = 0;
		
		/** Answer if parameter changed -> reclustering needed */
		virtual bool anySlotDirty() = 0;  // should reset the isDirty Flag
		
		/** Answer if data changed -> reclustering needed */
		virtual bool outCallChanged(MultiParticleDataCallExtended *outCall) = 0;
	
	#ifdef TIME_MEASUREMENT
		/** Measurement of ellapsed time for clustering */
		virtual void performanceTest(MultiParticleDataCallExtended *dat) = 0;
	#endif
		/** The call for the output data */
		core::CalleeSlot putDataSlot;

		/** The call for the input data */
        core::CallerSlot getDataSlot;

		/** Weights of position (x, y, z) */
		core::param::ParamSlot positionWeightSlot;

		/** Weights of velocity (u, v, w) */
		core::param::ParamSlot velocityWeightSlot;

		/** Weight of rotation r */
		core::param::ParamSlot rotationWeightSlot;

		/** Manually force reclustering */
		core::param::ParamSlot reclusterSlot;

	#ifdef TIME_MEASUREMENT
		/** Measurement of ellapsed time for clustering */
		core::param::ParamSlot performanceTestSlot;
	#endif
		/** Number of clusters */
		unsigned int numberClusters;

		/** First frame of trajectory, -1 for ParticleClustering */
		int firstFrame;

		/** Last frame of trajectory, -1 for ParticleClustering */
		int lastFrame;

		/** Frame Step of trajectory, -1 for ParticleClustering */
		int frameStep;

		/** ID of last frame */
		unsigned int lastFrameId;

		/** Last data hash */
        size_t lastHash;

		/** New data hash for call (a little bit dirty)
		 * (every renderer needs to know that the clustering changed -> change dataHash)
		 */
		size_t newHash;

		/** Result of clustering */
        vislib::RawStorage clusterData;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_ABSTRACTCLUSTERER_H_INCLUDED */