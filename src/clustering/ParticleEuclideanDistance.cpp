/*
 * ParticleEuclideanDistance.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "ParticleEuclideanDistance.h"
#include "helper.h"

using namespace megamol::vis_particle_clustering;

clustering::ParticleEuclideanDistance::ParticleEuclideanDistance(const ParticleWeight &particleWeight) : particleWeight(particleWeight) {
}

clustering::ParticleEuclideanDistance::~ParticleEuclideanDistance(void) {
}

float clustering::ParticleEuclideanDistance::DistanceBetween(const Particle &lhs, const Particle &rhs, const bool usePosition) const {
	vislib::math::Vector<float,3> positionDifference = (lhs.GetPosition() - rhs.GetPosition());
	positionDifference *= particleWeight.GetPositionWeight();
	
	vislib::math::Vector<float,3> velocityDifference = (lhs.GetVelocity() - rhs.GetVelocity());
	velocityDifference *= particleWeight.GetVelocityWeight();

	float distance = positionDifference.SquareLength() * usePosition +
					velocityDifference.SquareLength() +
					std::pow(lhs.GetRotationAngle() - rhs.GetRotationAngle(), 2) * std::pow(particleWeight.GetRotationWeight(), 2);

	return sqrtf(distance);
}