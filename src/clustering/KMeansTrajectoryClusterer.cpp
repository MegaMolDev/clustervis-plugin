/*
 * KMeansTrajectoryClusterer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "KMeansTrajectoryClusterer.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/Vector3fParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/BoolParam.h"
#include "TrajectoryDistance.h"
#include "TrajectoryDTWDistance.h"
#include "ParticleDistance.h"
#include "ParticleEuclideanDistance.h"

using namespace megamol::vis_particle_clustering;

clustering::KMeansTrajectoryClusterer::KMeansTrajectoryClusterer(void) : 
		numberClusterSlot("numbercluster", "The number of clusters"),
		numberIterationsSlot("numberiterations", "The number of iterations for kmeans"),
		metricSlot("metric", "The metric used to calculate the distance between trajectories") {
	
	unsigned int numberClusters = 10;
	unsigned int numberIterations = 20;

	this->numberClusterSlot.SetParameter(new core::param::IntParam(static_cast<int>(numberClusters),1,100));
    this->MakeSlotAvailable(&this->numberClusterSlot);

	this->numberIterationsSlot.SetParameter(new core::param::IntParam(static_cast<int>(numberIterations),1,100));
    this->MakeSlotAvailable(&this->numberIterationsSlot);

	core::param::EnumParam* usedMetric = new core::param::EnumParam(TRAJECTORYDTWPARTICLEEUCLIDEANDISTANCE);
	usedMetric->SetTypePair(TRAJECTORYPARTICLEDISTANCE, "Trajectory - Particle distance");
	usedMetric->SetTypePair(TRAJECTORYPARTICLEEUCLIDEANDISTANCE, "Trajectory - Particle euclidean distance");
	usedMetric->SetTypePair(TRAJECTORYDTWPARTICLEDISTANCE, "Trajectory DTW - Particle distance");
	usedMetric->SetTypePair(TRAJECTORYDTWPARTICLEEUCLIDEANDISTANCE, "Trajectory DTW - Particle euclidean distance");
	this->metricSlot.SetParameter(usedMetric);
	this->MakeSlotAvailable(&this->metricSlot);

	kMeans.SetNumberClusters(numberClusters);
	setNumberClusters(numberClusters);

	kMeans.SetNumberIterations(numberIterations);
}


clustering::KMeansTrajectoryClusterer::~KMeansTrajectoryClusterer(void) {
	this->Release();
}

bool clustering::KMeansTrajectoryClusterer::anySlotDirty() {
	bool dirty = false;
	
	if (this->numberClusterSlot.IsDirty()) {
		this->numberClusterSlot.ResetDirty();
		unsigned int numberClusters = static_cast<unsigned int>(this->numberClusterSlot.Param<core::param::IntParam>()->Value());
		kMeans.SetNumberClusters(numberClusters);
		setNumberClusters(numberClusters);
		dirty = true;
	}
	if (this->numberIterationsSlot.IsDirty()) {
		this->numberIterationsSlot.ResetDirty();
		kMeans.SetNumberIterations(this->numberIterationsSlot.Param<core::param::IntParam>()->Value());
		dirty = true;
	}
	if (this->metricSlot.IsDirty()) {
		this->metricSlot.ResetDirty();
		this->kMeans.Reset();
		dirty = true;
	}
	return dirty;
}

// Assumptions: only one particle list with core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ particles
void clustering::KMeansTrajectoryClusterer::cluster(MultiParticleDataCallExtended *dat, bool recluster) {
	ASSERT(dat->GetParticleListCount() == 1);

	UINT64 all_cnt = dat->AccessParticles(0).GetCount();
    if (dat->GetParticleListCount() != 1 || dat->AccessParticles(0).GetVertexDataType() != core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ) {
		return;
	}
	
	this->getClusterData().EnforceSize(all_cnt * sizeof(unsigned int));
	unsigned int *f = this->getClusterData().As<unsigned int>();
	
	if (recluster) {
		this->kMeans.Reset();
	}

    std::vector<Trajectory> trajectories = buildTrajectoryVector(dat);
	if (trajectories.size() == 0) {
		this->getClusterData().ZeroAll();
		setNumberClusters(1);
		return;
	}

	bool useCompressedTrajectories = this->getUseCompressedTrajectoriesSlot().Param<core::param::BoolParam>()->Value();
	if (useCompressedTrajectories) {
		trajectories = compressTrajectoryVector(trajectories);
	}

	ParticleWeight particleWeight(this->getPositionWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getVelocityWeightSlot().Param<core::param::Vector3fParam>()->Value(),
								this->getRotationWeightSlot().Param<core::param::FloatParam>()->Value());

	std::unique_ptr<Metric<Trajectory> > trajectoryMetricPtr;
	switch(this->metricSlot.Param<core::param::EnumParam>()->Value()) {
		case TRAJECTORYPARTICLEDISTANCE:
			trajectoryMetricPtr.reset(new TrajectoryDistance(new ParticleDistance(particleWeight)));
			break;
		case TRAJECTORYPARTICLEEUCLIDEANDISTANCE:
			trajectoryMetricPtr.reset(new TrajectoryDistance(new ParticleEuclideanDistance(particleWeight)));
			break;
		case TRAJECTORYDTWPARTICLEDISTANCE:
			trajectoryMetricPtr.reset(new TrajectoryDTWDistance(new ParticleDistance(particleWeight)));
			break;
		case TRAJECTORYDTWPARTICLEEUCLIDEANDISTANCE:
			trajectoryMetricPtr.reset(new TrajectoryDTWDistance(new ParticleEuclideanDistance(particleWeight)));
			break;
		default:
			return;
	}
	if (trajectoryMetricPtr == nullptr) {
		return;
	}

	bool usePositionGradient = this->getUseGradientPositionSlot().Param<core::param::BoolParam>()->Value();
	kMeans.SetData(&trajectories, usePositionGradient, *trajectoryMetricPtr); // TODO: hier ist die metric eigentlich �berfl�ssig
	
	kMeans.Run(*trajectoryMetricPtr);
	
	kMeans.GetClusters(f);
}