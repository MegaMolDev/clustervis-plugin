/*
 * Metric.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_METRIC_H_INCLUDED
#define MEGAMOLVPC_METRIC_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Base Class for all metrics
     */
	template <class Sample>
	class Metric 	{
	public:
		/** Ctor. */
		Metric(void) {};

		/** Dtor. */
		virtual ~Metric(void) {};

		/** Computation of distance */
		virtual float DistanceBetween(const Sample &lhs, const Sample &rhs, const bool usePosition) const = 0;
		
		/** Answer name (for logging) */
		virtual const std::string GetName() const = 0;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_METRIC_H_INCLUDED */
