/*
 * KMeans.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_KMEANS_H_INCLUDED
#define MEGAMOLVPC_KMEANS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "Metric.h"
#include "vislib/sys/Log.h"
#include <vector>
#include <algorithm>

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Template class for k-means clustering of Samples (Particle or Trajectory)
     */
	template <class Sample>
	class KMeans {
	public:
		/** Ctor. */
		KMeans<Sample>(void): numberIterations(20) {};

		/** Dtor. */
		virtual ~KMeans<Sample>(void) {};

		/** Set data and metric */
		void SetData(std::vector<Sample> *data, const bool usePosition, const Metric<Sample> &metric); // init of means (or update)
		
		/** Set k */
		inline void SetNumberClusters(unsigned int numberClusters) {
			this->numberClusters = numberClusters;
		}

		/** Set number of iterations */
		inline void SetNumberIterations(unsigned int numberIterations) {
			this->numberIterations = numberIterations;
		}

		/** Run k-means */
		bool Run(const Metric<Sample> &metric);

		/** Return clustering */
		void GetClusters(unsigned int *clusterData);

		/** Reset k-means (clears means -> next SetData will init new means) */
		inline void Reset() {
			this->means.clear();
		}

	private:
		/** Init means */
		void initMeans();

		/** Update means with new data but old clusters */
		void updateMeans(const Metric<Sample> &metric);
		
		/** Data */
		std::vector<Sample> *data; // so I don't have to copy vector

		/** Means */
		std::vector<Sample> means;

		/** k */
		unsigned int numberClusters;

		/** Number of iterations */
		unsigned int numberIterations;

		/** Number of datapoints (particles or trajectories) */
		unsigned int numberDataPoints;

		/** Result of clustering (cluster[idx] = cluster index of data[idx]) */
		std::vector<unsigned int> cluster;

		/** Use position information for clustering? */
		bool usePosition;
	};

	template <class Sample>
	void clustering::KMeans<Sample>::SetData(std::vector<Sample> *data, const bool usePosition, const Metric<Sample> &metric) {
		this->data = data;
		this->numberDataPoints = data->size();
		this->usePosition = usePosition;

		if (means.empty() || means.size() != numberClusters) {
			initMeans();
		} else {
			updateMeans(metric);
		}
	}

	// assumption: numberclusters < data.size()
	template <class Sample>
	void clustering::KMeans<Sample>::initMeans() {
		means.clear();
		cluster.clear();
		cluster.resize(numberDataPoints);

		std::vector<int> usedIndices;

		std::srand(static_cast<unsigned int>(std::time(0))); // use current time as seed for random generator
		for (unsigned int i=0; i<numberClusters; ++i) {
			int currIdx = std::rand() % numberDataPoints;
			while (std::find(usedIndices.begin(), usedIndices.end(), currIdx) != usedIndices.end()) { // same datapoint already used
				currIdx = std::rand() % numberDataPoints;
			}
			means.push_back((*this->data)[currIdx]);
			usedIndices.push_back(currIdx);
		}
	}

	#ifdef _MSC_VER
	#pragma push_macro("max")
	#undef max
	#endif /* _MSC_VER */

	template <class Sample>
	void clustering::KMeans<Sample>::updateMeans(const Metric<Sample> &metric) {
		means.clear();
		means.resize(numberClusters, Sample());

		for (unsigned int dataPointIndex = 0; dataPointIndex < numberDataPoints; ++dataPointIndex) {
			unsigned int particleCluster = cluster[dataPointIndex];
			if (particleCluster >= numberClusters) { // particle in previous step not existent -> crossed first frame
				continue;	// TODO: lieber alles neu initialisieren? kann eigentlich weg
			}
			means[particleCluster].AddSample((*this->data)[dataPointIndex]);
		}
		for (unsigned int i = 0; i < numberClusters; ++i) {
			if (means[i].IsValid()) {
				means[i].ComputeMean();
			} else { // doesn't happen (?)
				vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "updateMeans : cluster vanished - new random mean");
				std::srand(static_cast<unsigned int>(std::time(0))); // use current time as seed for random generator
				int currIdx = std::rand() % numberDataPoints;
				means[i] = (*this->data)[currIdx];
			}
		}
		
		// compute nearest Point
		std::vector<Sample> newMeans(numberClusters);
		std::vector<float> minDists(numberClusters, std::numeric_limits<float>::max());
		
		for (unsigned int dataPointIndex = 0; dataPointIndex < numberDataPoints; ++dataPointIndex) {
			unsigned int clusterIndex = cluster[dataPointIndex];
			if (clusterIndex >= numberClusters) { // particle in previous step not existent -> crossed first frame
				continue;	// TODO: lieber alles neu initialisieren? kann eigentlich weg
			}
			Sample particle((*this->data)[dataPointIndex]);
			float distance = metric.DistanceBetween(particle, this->means[clusterIndex], usePosition);
			if (distance < minDists[clusterIndex]) {
				minDists[clusterIndex] = distance;
				newMeans[clusterIndex] = particle;
			}
		}
		this->means = newMeans;
	}

	template <class Sample>
	bool clustering::KMeans<Sample>::Run(const Metric<Sample> &metric) {
		std::cout << "Run KMeans - Metric: " << metric.GetName().c_str() << std::endl;
		std::vector<Sample> accumulatedSamples;
		for (unsigned int iter=0; iter<numberIterations; ++iter) {
			std::cout << "\rKMeans iteration: " << iter << "/" << this->numberIterations << " .........." << std::flush;
			std::cout << "\rKMeans iteration: " << iter << "/" << this->numberIterations << " " << std::flush;
			accumulatedSamples.clear();
			accumulatedSamples.resize(numberClusters, Sample());

			unsigned int modul = (numberDataPoints - 1) / 10;
			if (modul == 0) {
				modul = 1;
			}

			// TODO: parallelize
			for (unsigned int dataPointIndex = 0; dataPointIndex < numberDataPoints; ++dataPointIndex) {
				if ((dataPointIndex + 1) % modul == 0) {
					std::cout << "x" << std::flush;
				}
				if (numberClusters > 1) {
					float minDistance = std::numeric_limits<float>::max();
					unsigned int clusterId = static_cast<unsigned int>(-1);
					for (unsigned int clusterIndex = 0; clusterIndex < numberClusters; ++clusterIndex) {
						float distance = metric.DistanceBetween((*this->data)[dataPointIndex], this->means[clusterIndex], usePosition);
						if (distance < minDistance) {
							minDistance = distance;
							clusterId = clusterIndex;
						}
					}

					if (clusterId >= numberClusters) {
						std::cout << "not supposed to happen!" << std::endl;
						return false;
					}

					accumulatedSamples[clusterId].AddSample((*this->data)[dataPointIndex]);

					cluster[dataPointIndex] = clusterId;
				} else {
					accumulatedSamples[0].AddSample((*this->data)[dataPointIndex]);
					cluster[dataPointIndex] = 0;
				}
			}
			for (unsigned int i = 0; i < numberClusters; ++i) {
				if (accumulatedSamples[i].IsValid()) {
					accumulatedSamples[i].ComputeMean();
				} else {
					vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_WARN, "Run: cluster vanished - new random mean");
					std::srand(static_cast<unsigned int>(std::time(0))); // use current time as seed for random generator
					int currIdx = std::rand() % numberDataPoints;
					accumulatedSamples[i] = (*this->data)[currIdx];
				}
			}
			this->means = accumulatedSamples;
		}
		std::cout << " done" << std::endl;
		return true;
	}

	#ifdef _MSC_VER
	#pragma pop_macro("max")
	#endif /* _MSC_VER */

	template <class Sample>
	void clustering::KMeans<Sample>::GetClusters(unsigned int *clusterData) {
		for (unsigned int dataPointIndex = 0; dataPointIndex < numberDataPoints; ++dataPointIndex) {
			clusterData[dataPointIndex] = cluster[dataPointIndex];
		}
	}

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_KMEANS_H_INCLUDED */
