/*
 * KMeansParticleClusterer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_KMEANSPARTICLECLUSTERER_H_INCLUDED
#define MEGAMOLVPC_KMEANSPARTICLECLUSTERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "AbstractParticleClusterer.h"
#include "KMeans.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Class for clustering particle data using the k-means algorithm
     */
	class KMeansParticleClusterer : public AbstractParticleClusterer {
	public:
		/**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
		static const char *ClassName(void) {
			return "KMeansParticleClusterer";
		}

		/**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
		static const char *Description(void) {
			return "Compute particle clusters.";
		}

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
		static bool IsAvailable(void) {
			return true;
		}

		/** Ctor. */
		KMeansParticleClusterer(void);
		
		/** Dtor. */
		virtual ~KMeansParticleClusterer(void);

	private:
		/** Answer if parameter changed -> reclustering needed */
		bool anySlotDirty(); // should reset the isDirty Flag

		/** Cluster particles */
		void cluster(MultiParticleDataCallExtended *dat, bool recluster);

		enum metricType {PARTICLEDISTANCE, PARTICLEEUCLIDEANDISTANCE};

		/** k-means */
		KMeans<Particle> kMeans;

		/** Specify number of k */
		core::param::ParamSlot numberClusterSlot;

		/** Specify number of iterations */
		core::param::ParamSlot numberIterationsSlot;

		/** Specify used metric */
		core::param::ParamSlot metricSlot;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_KMEANSPARTICLECLUSTERER_H_INCLUDED */