/*
 * ParticleDistance.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_PARTICLEDISTANCE_H_INCLUDED
#define MEGAMOLVPC_PARTICLEDISTANCE_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "Particle.h"
#include "ParticleWeight.h"
#include "Metric.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Class for computation of (own) Particle-Distance
     */
	class ParticleDistance : public Metric<Particle>	{
	public:
		/** Ctor. */
		ParticleDistance(const ParticleWeight &particleWeight);

		/** Dtor. */
		virtual ~ParticleDistance(void);

		/** Computation of distance between particles */
		float DistanceBetween(const Particle &lhs, const Particle &rhs, const bool usePosition) const;

		/** Set weights of features for distance computation */
		inline void SetParticleWeight(const ParticleWeight &particleWeight) {
			this->particleWeight = particleWeight;
		}

		/** Answer name (for logging) */
		inline const std::string GetName() const {
			return "Particle Distance";
		};

	private:
		/** Weights of particle features */
		ParticleWeight particleWeight;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_PARTICLEDISTANCE_H_INCLUDED */