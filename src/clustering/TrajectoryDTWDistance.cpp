/*
 * TrajectoryDTWDistance.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "TrajectoryDTWDistance.h"
#include "vislib/sys/Log.h"
#include <algorithm>

using namespace megamol::vis_particle_clustering;

 // takes ownership of *particleMetric
clustering::TrajectoryDTWDistance::TrajectoryDTWDistance(const Metric<Particle> *particleMetric) : particleMetric(particleMetric) {
}

clustering::TrajectoryDTWDistance::~TrajectoryDTWDistance(void) {
	delete particleMetric;
}

#ifdef _MSC_VER
#pragma push_macro("max")
#undef max
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

float clustering::TrajectoryDTWDistance::DistanceBetween(const Trajectory &lhs, const Trajectory &rhs, const bool usePosition) const {
	const std::vector<Particle> &lhsTrajectoryParticles = lhs.GetTrajectoryParticles();
	const std::vector<Particle> &rhsTrajectoryParticles = rhs.GetTrajectoryParticles();
	
	int n = static_cast<int>(lhs.GetSize());
	int m = static_cast<int>(rhs.GetSize());

	if (n == 0 || m == 0) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Cannot compute distance if trajectory length is 0\n");
		return std::numeric_limits<float>::max();
	}

	int w = std::ceil(std::max(n,m) / 10.0f); // window size: 10% of trajectory length	
	w = std::max(w, std::abs(n-m));

	std::vector<std::vector<float> > DTW;
	DTW.resize(n+1, std::vector<float>(m+1, std::numeric_limits<float>::max()));
	DTW[0][0] = 0.0f;

	for (int i = 1; i <= n; ++i) {
		for (int j = std::max(1, i-w); j <= std::min(m, i+w); ++j) {
			float cost = this->particleMetric->DistanceBetween(lhsTrajectoryParticles[i-1], rhsTrajectoryParticles[j-1], usePosition);
			DTW[i][j] = cost + std::min(std::min(DTW[i-1][j], DTW[i][j-1]), DTW[i-1][j-1]);
		}
	}

	return DTW[n][m];
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#pragma pop_macro("max")
#endif /* _MSC_VER */