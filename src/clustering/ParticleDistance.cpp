/*
 * ParticleDistance.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "ParticleDistance.h"
#include "helper.h"

using namespace megamol::vis_particle_clustering;

clustering::ParticleDistance::ParticleDistance(const ParticleWeight &particleWeight) : particleWeight(particleWeight) {
}

clustering::ParticleDistance::~ParticleDistance(void) {
}

float clustering::ParticleDistance::DistanceBetween(const Particle &lhs, const Particle &rhs, const bool usePosition) const {
	vislib::math::Vector<float,3> positionDifference(lhs.GetPosition() - rhs.GetPosition());
	positionDifference *= particleWeight.GetPositionWeight();
	
	vislib::math::Vector<float,3> velocityDifference(lhs.GetVelocity() - rhs.GetVelocity());
	velocityDifference *= particleWeight.GetVelocityWeight();
	
	return (positionDifference).Length() * usePosition +
			(velocityDifference).Length() +
			(1.0f - std::abs(dot(lhs.GetRotation(), rhs.GetRotation()))) * particleWeight.GetRotationWeight();
}