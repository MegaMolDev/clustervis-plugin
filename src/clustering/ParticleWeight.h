/*
 * ParticleWeight.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_PARTICLEWEIGHT_H_INCLUDED
#define MEGAMOLVPC_PARTICLEWEIGHT_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Class containing the weights of particle features
     */
	class ParticleWeight 	{
	public:
		/** Ctor. */
		ParticleWeight(	vislib::math::Vector<float,3> positionWeight = vislib::math::Vector<float,3>(1.0f, 1.0f, 1.0f), 
						vislib::math::Vector<float,3> velocityWeight = vislib::math::Vector<float,3>(1.0f, 1.0f, 1.0f), 
						float rotationWeight = 1.0f) :	positionWeight(positionWeight), velocityWeight(velocityWeight), rotationWeight(rotationWeight) {};
		/** Dtor. */
		virtual ~ParticleWeight(void) {};
		
		inline void SetWeights(vislib::math::Vector<float,3> positionWeight, vislib::math::Vector<float,3> velocityWeight, float rotationWeight) {
			this->positionWeight = positionWeight;
			this->velocityWeight = velocityWeight;
			this->rotationWeight = rotationWeight;
		}
		
		inline const vislib::math::Vector<float,3> GetPositionWeight() const {
			return this->positionWeight;
		}
		inline const vislib::math::Vector<float,3> GetVelocityWeight() const {
			return this->velocityWeight;
		}
		inline const float GetRotationWeight() const {
			return this->rotationWeight;
		}

	private:
		/** Weights for the position */
		vislib::math::Vector<float,3> positionWeight;

		/** Weights for the velocity */
		vislib::math::Vector<float,3> velocityWeight;

		/** Weight for the rotation */
		float rotationWeight;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_PARTICLEWEIGHT_H_INCLUDED */
