/*
 * MeanshiftTrajectoryClusterer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_MEANSHIFTTRAJECTORYCLUSTERER_H_INCLUDED
#define MEGAMOLVPC_MEANSHIFTTRAJECTORYCLUSTERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "AbstractTrajectoryClusterer.h"
#include "Meanshift.h"

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Class for clustering trajectory data using the meanshift algorithm
     */
	class MeanshiftTrajectoryClusterer : public AbstractTrajectoryClusterer {
	public:
		/**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
		static const char *ClassName(void) {
			return "MeanshiftTrajectoryClusterer";
		}

		/**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
		static const char *Description(void) {
			return "Compute trajectory clusters.";
		}

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
		static bool IsAvailable(void) {
			return true;
		}

		/** Ctor. */
		MeanshiftTrajectoryClusterer(void);

		/** Dtor. */
		virtual ~MeanshiftTrajectoryClusterer(void);

	private:
		/** Answer if parameter changed -> reclustering needed */
		bool anySlotDirty(); // should reset the isDirty Flag

		/** Cluster particles */
		void cluster(MultiParticleDataCallExtended *dat, bool recluster);

		/** Init Meanshift */
		void initMeanshift();

		/** Find optimal K, L for meanshift procedure */
		void findKLMeanshift();

		/** Specify K */
		core::param::ParamSlot kSlot;

		/** Specify L */
		core::param::ParamSlot lSlot;

		/** Specify number of neighbours used for construction of density */
		core::param::ParamSlot numberNeighboursSlot;

		/** Specify offset for K for search of optimal K: searching from k - kOffset ti k + kOffset */
		core::param::ParamSlot kOffsetSlot;

		/** Specify jump for search of optimal K */
		core::param::ParamSlot kJumpSlot;

		/** Specify max L for search of optimal L */
		core::param::ParamSlot lMaxSlot;

		/** Search optimal K and L */
		core::param::ParamSlot findKLSlot;

		/** Meanshift */
		Meanshift<Trajectory> meanshift;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_MEANSHIFTTRAJECTORYCLUSTERER_H_INCLUDED */