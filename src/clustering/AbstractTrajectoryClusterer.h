/*
 * AbstractTrajectoryClusterer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_ABSTRACTTRAJECTORYCLUSTERER_H_INCLUDED
#define MEGAMOLVPC_ABSTRACTTRAJECTORYCLUSTERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "AbstractClusterer.h"
#include "Trajectory.h"
#include <vector>

namespace megamol {
namespace vis_particle_clustering {
namespace clustering {

	/**
     * Abstract base class for all TrajectoryClusterer
     */
	class AbstractTrajectoryClusterer : public AbstractClusterer {
	public:
		/** Ctor. */
		AbstractTrajectoryClusterer(void);

		/** Dtor. */
		virtual ~AbstractTrajectoryClusterer(void);

		/** 
		 * Construct all trajectories for the specified time span
		 *
		 * @return Vector of trajectories
		 */
		std::vector<Trajectory> buildTrajectoryVector(MultiParticleDataCallExtended *call);
		
		/** 
		 * Compression of trajectory vector
		 *
		 * @return Vector of compressed trajectories
		 */
		std::vector<Trajectory> compressTrajectoryVector(const std::vector<Trajectory>& trajectories, const unsigned int factor = 10) const;
	
	protected:
		inline const core::param::ParamSlot& getUseGradientPositionSlot() const {
			return this->usePositionGradientSlot;
		}
		inline const core::param::ParamSlot& getUseCompressedTrajectoriesSlot() const {
			return this->useCompressedTrajectoriesSlot;
		}
	
	private:
		/** Cluster trajectories */
		virtual void cluster(MultiParticleDataCallExtended *dat, bool recluster) = 0;
		
		/** Change in data? Need to know if data needs to be reclustered */
		bool outCallChanged(MultiParticleDataCallExtended *outCall);
	
	#ifdef TIME_MEASUREMENT
		/** Measurement of ellapsed time for clustering */
		void performanceTest(MultiParticleDataCallExtended *dat);
	#endif

		/** The number of the first frame */
        core::param::ParamSlot firstFrameSlot;

        /** The number of the last frame */
        core::param::ParamSlot lastFrameSlot;

        /** The step length between two frames */
        core::param::ParamSlot frameStepSlot;

		/** Should the position gradient be used? */
		core::param::ParamSlot usePositionGradientSlot;

		/** Use compressed or original trajectories? */
		core::param::ParamSlot useCompressedTrajectoriesSlot;
	};

} /* end namespace clustering */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_ABSTRACTTRAJECTORYCLUSTERER_H_INCLUDED */