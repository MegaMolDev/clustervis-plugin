/*
 * FeatureColourMapper.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_FEATURECOLOURMAPPER_H_INCLUDED
#define MEGAMOLVPC_FEATURECOLOURMAPPER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Module.h"
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/ParamSlot.h"
#include "MultiParticleDataCallExtended.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"
#include "vislib/RawStorage.h"
#include "vislib/memutils.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	class FeatureColourMapper : public core::Module {
	public:
		static const char *ClassName(void) {
			return "FeatureColourMapper";
		}
		static const char *Description(void) {
			return "Map feature to colour.";
		}
		static bool IsAvailable(void) {
			return true;
		}
		FeatureColourMapper(void);
		virtual ~FeatureColourMapper(void);

	private:
		class Unlocker : public core::moldyn::MultiParticleDataCall::Unlocker {
        public:
            Unlocker(core::moldyn::MultiParticleDataCall::Unlocker *inner) :
                    core::moldyn::MultiParticleDataCall::Unlocker(), inner(inner) {
                // intentionally empty
            }
            virtual ~Unlocker(void) {
                this->Unlock();
            }
            virtual void Unlock(void) {
                if (this->inner != nullptr) {
                    this->inner->Unlock();
                    SAFE_DELETE(this->inner);
                }
            }
        private:
            core::moldyn::MultiParticleDataCall::Unlocker *inner;
        };

		core::param::ParamSlot useGlobalMaximaSlot;
		core::param::ParamSlot centerTransferZeroSlot;
		core::param::ParamSlot whichFeatureSlot;
		enum featureType {POSITION, VELOCITY, VELOCITYDIR, VELOCITYX, VELOCITYY, VELOCITYZ, ROTATIONANGLE, ROTATIONAXIS, ROTATIONAXISX, ROTATIONAXISY, ROTATIONAXISZ};

		virtual bool create(void);
		virtual void release(void);
		bool getDataCallback(core::Call& caller);
		bool getExtentCallback(core::Call& caller);
		bool setFeatureParams(megamol::core::Call& call);
	
		void colourParticles(MultiParticleDataCallExtended *dat);

		core::CalleeSlot putDataSlot;
        core::CallerSlot getDataSlot;
		core::CalleeSlot featureParamsSlot;

		unsigned int lastFrame;
        size_t lastHash;
        vislib::RawStorage colData;

		float minValue;
		float maxValue;
	};

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_FEATURECOLOURMAPPER_H_INCLUDED */
