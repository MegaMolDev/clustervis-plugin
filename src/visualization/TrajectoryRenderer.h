/*
 * TrajectoryRenderer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_TRAJECTORYRENDERER_H_INCLUDED
#define MEGAMOLVPC_TRAJECTORYRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/view/Renderer2DModule.h"
#include "mmcore/Call.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/ParamSlot.h"
#include "MultiParticleDataCallExtended.h"
#include "MultiParticleClusterDataCall.h"
#include "vislib/graphics/gl/GLSLShader.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/**
     * Renderer for trajectories
     */
	class TrajectoryRenderer : public core::view::Renderer2DModule {
	public:
		/**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void) {
            return "TrajectoryRenderer";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void) {
            return "Renderer for trajectories.";
        }

		/**
		 * Answers whether this module is available on the current system.
		 *
		 * @return 'true' if the module is available, 'false' otherwise.
		 */
		static bool IsAvailable(void) {
			return true;
		}

		/** Ctor */
		TrajectoryRenderer(void);

		/** Dtor */
		~TrajectoryRenderer(void);
	
	protected:

		/**
		 * Implementation of 'Create'.
		 *
		 * @return 'true' on success, 'false' otherwise.
		 */
		virtual bool create(void);
		
		/**
		 * Implementation of 'Release'.
		 */
		virtual void release(void);

        /**
         * Callback for mouse events (move, press, and release)
         *
         * @param x The x coordinate of the mouse in world space
         * @param y The y coordinate of the mouse in world space
         * @param flags The mouse flags
         */
        virtual bool MouseEvent(float x, float y, core::view::MouseFlags flags);

		/**
         * The get extents callback. The module should set the members of
         * 'call' to tell the caller the extents of its data (bounding boxes
         * and times).
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetExtents(core::view::CallRender2D& call);

		/**
		* The Open GL Render callback.
		*
		* @param call The calling call.
		* @return The return value of the function.
		*/
        virtual bool Render(core::view::CallRender2D& call);
	
	private:
		/** Build trajectories */
		bool buildTrajectoryVector(MultiParticleDataCallExtended *call, MultiParticleClusterDataCall *mpcdc, const unsigned int numberClusters, const uint8_t * clusterPtr, const unsigned int clusterStride);
		
		/** Render one trajectory */
		void renderTrajectory(GLint featurePos, unsigned int currentClusterIdx, unsigned int trajectoryIndex, unsigned int numberTimesteps);
		
		/** Render one mean trajectory */
		void renderMeanTrajectory(GLint featurePos, unsigned int currentClusterIdx, unsigned int numberClusters, unsigned int numberTimesteps);
		
		/** Render labeling */
		void renderLabels(unsigned int numberTimesteps, int firstFrameNumber, int lastFrameNumber);

		enum featureType {POSITIONX, POSITIONY, POSITIONZ,
						VELOCITYX, VELOCITYY, VELOCITYZ,
						ROTATIONANGLE};

		enum drawType {MEAN, TRAJECTORY, BOTH};

		/** The call for data */
        core::CallerSlot getDataSlot;

		/** The call for cluster data */
		core::CallerSlot getClusterDataSlot;

		/** The call for Transfer function */
        core::CallerSlot getTFSlot;

		/** The call for shared visualization parameters */
		core::CallerSlot sharedVisParamsSlot;

		/** The width of trajectory lines */
		core::param::ParamSlot lineWidthSlot;

		/** The opacity of trajectory lines */
		core::param::ParamSlot opacitySlot;

		/** Pick feature you want visualized */
		core::param::ParamSlot featureSlot;

		/** Pick what you want to see: trajectories, mean trajectories or both? */
		core::param::ParamSlot drawSlot;

		/** Scaling of labels */
		core::param::ParamSlot scaleSlot;

		/** Specify percentage of trajectories to be rendered */
		core::param::ParamSlot percentTrajectoriesSlot;
		
		/** A simple black-to-white transfer function texture as fallback */
        unsigned int greyTF;

		/** The trajectory shader */
		vislib::graphics::gl::GLSLShader trajectoryShader;

		// helper for rendering of trajectories - values for all features
		std::vector<std::vector<std::vector<float> > > trajectoryPositions;
		std::vector<std::vector<std::vector<float> > > trajectoryVelocities;
		std::vector<std::vector<std::vector<float> > > trajectoryRotationAngles;
		std::vector<std::vector<float> > meanTrajectoryPositions;
		std::vector<std::vector<float> > meanTrajectoryVelocities;
		std::vector<std::vector<float> > meanTrajectoryRotationAngles;
		
		size_t lastHash;
		size_t lastClusterHash;

		float scaling; // Helper to scale y-Axis
		// Helper for y-Axis (label)
		float minY;
		float maxY;
	};

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_TRAJECTORYRENDERER_H_INCLUDED */