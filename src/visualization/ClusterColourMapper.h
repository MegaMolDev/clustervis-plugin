/*
 * ClusterColourMapper.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_CLUSTERCOLOURMAPPER_H_INCLUDED
#define MEGAMOLVPC_CLUSTERCOLOURMAPPER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Module.h"
#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/CallerSlot.h"
#include "CallVisParams.h"
#include "MultiParticleClusterDataCall.h"
#include "mmcore/moldyn/MultiParticleDataCall.h"
#include "vislib/RawStorage.h"
#include "vislib/memutils.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/**
     * Class for colouring particle data according to their cluster index
     */
	class ClusterColourMapper : public core::Module {
	public:
		/**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
		static const char *ClassName(void) {
			return "ClusterColourMapper";
		}

		/**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
		static const char *Description(void) {
			return "Map particle clusters to colour.";
		}

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
		static bool IsAvailable(void) {
			return true;
		}

		/** Ctor */
		ClusterColourMapper(void);

		/** Dtor */
		virtual ~ClusterColourMapper(void);

	private:
		class Unlocker : public core::moldyn::MultiParticleDataCall::Unlocker {
        public:
            Unlocker(core::moldyn::MultiParticleDataCall::Unlocker *inner) :
                    core::moldyn::MultiParticleDataCall::Unlocker(), inner(inner) {
                // intentionally empty
            }
            virtual ~Unlocker(void) {
                this->Unlock();
            }
            virtual void Unlock(void) {
                if (this->inner != nullptr) {
                    this->inner->Unlock();
                    SAFE_DELETE(this->inner);
                }
            }
        private:
            core::moldyn::MultiParticleDataCall::Unlocker *inner;
        };

		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
		virtual bool create(void);

		/**
         * Implementation of 'Release'.
         */
		virtual void release(void);

		/**
         * Gets the data from the source.
         *
         * @param caller The calling call.
         *
         * @return 'true' on success, 'false' on failure.
         */
		bool getDataCallback(core::Call& caller);

		/**
         * Gets the data from the source.
         *
         * @param caller The calling call.
         *
         * @return 'true' on success, 'false' on failure.
         */
		bool getExtentCallback(core::Call& caller);
	
		/** Colouring of data */
		void colourParticles(MultiParticleClusterDataCall *dat);

		/** The call for the output data */
		core::CalleeSlot putDataSlot;

		/** The call for the input data */
        core::CallerSlot getDataSlot;

		/** The call for the shared visualization parameters */
		core::CallerSlot sharedVisParamsSlot;

		/** ID of last frame */
		unsigned int lastFrame;

		/** Last data hash */
        size_t lastHash;

		/** Last ID of "drawOnly" cluster */
		int lastClusterId;

		/** Last value for cycleTF */
		bool lastCycleTF;

		/** Colour data of particles */
        vislib::RawStorage colData;

		/** Position data of particles */
		vislib::RawStorage posData;

		/** Number of particles in cluster  */
		unsigned int numberOfParticlesInCluster;
	};

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_CLUSTERCOLOURMAPPER_H_INCLUDED */
