/*
 * ClusterTransferFunctionRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "ClusterTransferFunctionRenderer.h"
#include "CallVisParams.h"
#include "RenderHelper.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/view/CallRender3D.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/StringParam.h"
#include "vislib/assert.h"
#include "vislib/math/Vector.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/graphics/gl/SimpleFont.h"
#include "mmcore/utility/ColourParser.h"
#include <memory>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::ClusterTransferFunctionRenderer::ClusterTransferFunctionRenderer
 */
visualization::ClusterTransferFunctionRenderer::ClusterTransferFunctionRenderer(void) : Renderer3DModule(),
        getDataSlot("getdata", "Connects to the data source"),
        tfSlot("gettransferfunction", "Connects to the transfer function module"),
		sharedVisParamsSlot("sharedvisparams", "Connects to the shared visualization parameters"),
		fontColourSlot("fontcolour", "The colour of the font"),
		drawClusterNumbersSlot("drawclusternumbers", "Draw Cluster-Numbers?"),
		greyTF(0) {
    this->getDataSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
    this->MakeSlotAvailable(&this->getDataSlot);

	this->tfSlot.SetCompatibleCall<core::view::CallGetTransferFunctionDescription>();
    this->MakeSlotAvailable(&this->tfSlot);

	this->sharedVisParamsSlot.SetCompatibleCall<CallVisParamsDescription>();
    this->MakeSlotAvailable(&this->sharedVisParamsSlot);

	this->fontColourSlot.SetParameter(new core::param::StringParam("black"));
	this->MakeSlotAvailable(&this->fontColourSlot);

	this->drawClusterNumbersSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&this->drawClusterNumbersSlot);
}

/*
 * visualization::ClusterTransferFunctionRenderer::~ClusterTransferFunctionRenderer
 */
visualization::ClusterTransferFunctionRenderer::~ClusterTransferFunctionRenderer(void) {
	this->Release();
}

/*
 * visualization::ClusterTransferFunctionRenderer::GetExtents
 */
bool visualization::ClusterTransferFunctionRenderer::GetExtents(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    MultiParticleClusterDataCall *c2 = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    if ((c2 != NULL) && ((*c2)(1))) {
        cr->SetTimeFramesCount(c2->FrameCount());
        cr->AccessBoundingBoxes() = c2->AccessBoundingBoxes();

        float scaling = cr->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
        if (scaling > 0.0000001) {
            scaling = 10.0f / scaling;
        } else {
            scaling = 1.0f;
        }
        cr->AccessBoundingBoxes().MakeScaledWorld(scaling);

    } else {
        cr->SetTimeFramesCount(1);
        cr->AccessBoundingBoxes().Clear();
    }

    return true;
}

/*
 * visualization::ClusterTransferFunctionRenderer::GetCapabilities
 */
bool visualization::ClusterTransferFunctionRenderer::GetCapabilities(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    cr->SetCapabilities(
        core::view::CallRender3D::CAP_RENDER
        | core::view::CallRender3D::CAP_LIGHTING
        | core::view::CallRender3D::CAP_ANIMATION
        );

    return true;
}


/*
 * visualization::ClusterTransferFunctionRenderer::getData
 */
vis_particle_clustering::MultiParticleClusterDataCall* visualization::ClusterTransferFunctionRenderer::getData(unsigned int t, float& outScaling) {
    MultiParticleClusterDataCall *c2 = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    outScaling = 1.0f;
    if (c2 != NULL) {
        c2->SetFrameID(t);
        if (!(*c2)(1)) return NULL;

        // calculate scaling
        outScaling = c2->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
        if (outScaling > 0.0000001) {
            outScaling = 10.0f / outScaling;
        } else {
            outScaling = 1.0f;
        }

        c2->SetFrameID(t);
        if (!(*c2)(0)) return NULL;

        return c2;
    } else {
        return NULL;
    }
}

/*
 * visualization::ClusterTransferFunctionRenderer::create
 */
bool visualization::ClusterTransferFunctionRenderer::create(void) {
	ASSERT(IsAvailable());
	CreateShader(this->instance(), this->transferFunctionShader, "transferfunction");

    glEnable(GL_TEXTURE_1D);
    glGenTextures(1, &this->greyTF);
    unsigned char tex[6] = {
        0, 0, 0,  255, 255, 255
    };
    glBindTexture(GL_TEXTURE_1D, this->greyTF);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, tex);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glBindTexture(GL_TEXTURE_1D, 0);

    glDisable(GL_TEXTURE_1D);

    return true;
}


/*
 * visualization::ClusterTransferFunctionRenderer::release
 */
void visualization::ClusterTransferFunctionRenderer::release(void) {
	this->transferFunctionShader.Release();
    ::glDeleteTextures(1, &this->greyTF);
}

/*
 * visualization::ClusterTransferFunctionRenderer::Render
 */
bool visualization::ClusterTransferFunctionRenderer::Render(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    float scaling = 1.0f;
	MultiParticleClusterDataCall *mpcdc = this->getData(static_cast<unsigned int>(cr->Time()), scaling);
    if (mpcdc == NULL) return false;

	unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	bool cycleTransferFunction = true;

	CallVisParams *vp = this->sharedVisParamsSlot.CallAs<CallVisParams>();
	if (vp != nullptr) {
		vp->SetVisParams(VisualizationParams(numberClusters));
		if (!(*vp)(CallVisParams::CallForGetVisParams) || !(*vp)(CallVisParams::CallForSetVisParams)) {
			return false;
		}
		cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
	}
	
	// calc positions for the (coloured) quads/quadstrip which is used to render the transferfunction
	unsigned int numberColours = 13U - static_cast<unsigned int>(cycleTransferFunction);
	float ratio = 12.0f / static_cast<float>(numberColours - 1); // both strips should have the same length
	
	// transferfunction
	std::vector<float> transferFuntionPositions(52);
	std::vector<float> colourIds(26);
	for (unsigned int colourId = 0; colourId < 13; ++colourId) {
		colourIds[2 * colourId] = colourId;
		transferFuntionPositions[4 * colourId] = colourId;
		transferFuntionPositions[4 * colourId + 1] = 1;

		colourIds[2 * colourId + 1] = colourId;
		transferFuntionPositions[4 * colourId + 2] = colourId;
		transferFuntionPositions[4 * colourId + 3] = 0;
	}

	// cluster quads
	std::vector<float> clusterTransferPositions(8 * numberClusters);
	std::vector<float> clusterIds(4 * numberClusters);
	for (unsigned int clusterId = 0; clusterId < numberClusters; ++clusterId) {
		clusterIds[4 * clusterId] = clusterId;
		clusterTransferPositions[8 * clusterId] = clusterId / static_cast<float>(numberClusters) * (numberColours - 1) * ratio;
		clusterTransferPositions[8 * clusterId + 1] = 2;

		clusterIds[4 * clusterId + 1] = clusterId;
		clusterTransferPositions[8 * clusterId + 2] = clusterId / static_cast<float>(numberClusters) * (numberColours - 1) * ratio;
		clusterTransferPositions[8 * clusterId + 3] = 1;

		clusterIds[4 * clusterId + 2] = clusterId;
		clusterTransferPositions[8 * clusterId + 4] = (clusterId + 1) / static_cast<float>(numberClusters) * (numberColours - 1) * ratio;
		clusterTransferPositions[8 * clusterId + 5] = 1;

		clusterIds[4 * clusterId + 3] = clusterId;
		clusterTransferPositions[8 * clusterId + 6] = (clusterId + 1) / static_cast<float>(numberClusters) * (numberColours - 1) * ratio;
		clusterTransferPositions[8 * clusterId + 7] = 2;
	}

	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	float viewportStuff[4];
    ::glGetFloatv(GL_VIEWPORT, viewportStuff);
    if (viewportStuff[2] < 1.0f) viewportStuff[2] = 1.0f;
    if (viewportStuff[3] < 1.0f) viewportStuff[3] = 1.0f;

	// positioning in lower left corner
	glPushMatrix();
	glLoadIdentity();
	glScalef(20.0f, 20.0f, 1.0f);
	glTranslatef(1.0f, 1.0f, 0.0f);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, viewportStuff[2], 0.0, viewportStuff[3], -1.0, 1.0);

    viewportStuff[2] = 2.0f / viewportStuff[2];
    viewportStuff[3] = 2.0f / viewportStuff[3];

	this->transferFunctionShader.Enable();
	core::view::CallGetTransferFunction *cgtf = this->tfSlot.CallAs<core::view::CallGetTransferFunction>();
	SetMVPMatTextureParams(this->transferFunctionShader, mpcdc, cgtf, this->greyTF);

	GLint vertexPos = glGetAttribLocation(this->transferFunctionShader, "vertex");
	GLint cial = glGetAttribLocationARB(this->transferFunctionShader, "colIdx");

    glEnableVertexAttribArrayARB(cial);
	glEnableVertexAttribArray(vertexPos);

	// render cluster colours
	glUniform1f(this->transferFunctionShader.ParameterLocation("cycleTransferFunction"), 0.0f);
	glUniform1f(this->transferFunctionShader.ParameterLocation("numberCluster"), 13.0f); // should be numberColours (reused shader)

	glVertexAttribPointerARB(cial, 1, GL_FLOAT, GL_FALSE, 0, colourIds.data());
	glVertexAttribPointer(vertexPos, 2, GL_FLOAT, GL_FALSE, 0, transferFuntionPositions.data());

	glDrawArrays(GL_QUAD_STRIP, 0, static_cast<GLsizei>(colourIds.size()));
	
	// render Transferfunction
	glUniform1f(this->transferFunctionShader.ParameterLocation("cycleTransferFunction"), static_cast<float>(cycleTransferFunction));
	glUniform1f(this->transferFunctionShader.ParameterLocation("numberCluster"), static_cast<float>(numberClusters));

	glVertexAttribPointerARB(cial, 1, GL_FLOAT, GL_FALSE, 0, clusterIds.data());
	glVertexAttribPointer(vertexPos, 2, GL_FLOAT, GL_FALSE, 0, clusterTransferPositions.data());

	glDrawArrays(GL_QUADS, 0, static_cast<GLsizei>(clusterIds.size()));

	glDisableVertexAttribArray(vertexPos);
	glDisableVertexAttribArrayARB(cial);
	glDisable(GL_TEXTURE_1D);
    
	this->transferFunctionShader.Disable();

	renderLabels(numberClusters, numberColours, ratio);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
    
	mpcdc->Unlock();

    return true;
}

void visualization::ClusterTransferFunctionRenderer::renderLabels(unsigned int numberClusters, unsigned int numberColours, float ratio) {
glDisable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	float r, g, b;
	core::utility::ColourParser::FromString(this->fontColourSlot.Param<core::param::StringParam>()->Value(), r, g, b);

	glColor3f(r, g, b);

	vislib::graphics::gl::SimpleFont f;
	float fontsize = 0.75f;
	float padding = 0.3f;

	if (f.Initialise()) {
		vislib::StringA tmpStr("Transferfunction");
		f.DrawString(static_cast<float>(numberColours - 1) * ratio + padding, 0.5f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_LEFT_MIDDLE);
		
		tmpStr.Clear();
		tmpStr.Append("Cluster colours");
		f.DrawString(static_cast<float>(numberColours - 1) * ratio + padding, 1.5f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_LEFT_MIDDLE);
		
		fontsize = 0.5f;
		float offset = 0.5f / static_cast<float>(numberClusters) * (numberColours - 1);
		if (this->drawClusterNumbersSlot.Param<core::param::BoolParam>()->Value()) {
			glColor3f(1.0f, 1.0f, 1.0f);
			for (unsigned int clusterIdx = 0; clusterIdx < numberClusters; ++clusterIdx) {
				tmpStr.Clear();
				tmpStr.Format("%i", clusterIdx);
				f.DrawString((clusterIdx + 1) / static_cast<float>(numberClusters) * (numberColours - 1) * ratio - offset, 1.5f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_MIDDLE);
			}
		}
	}
}