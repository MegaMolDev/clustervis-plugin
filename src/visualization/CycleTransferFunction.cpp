/*
 * CycleTransferFunction.cpp
 *
 * Author: Julia B�hnke
 * Modificated requestTF-Method of LinearTransferFunction.cpp
 */

#include "stdafx.h"
#include <cmath>
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "CycleTransferFunction.h"
#ifdef _WIN32
#include <windows.h>
#endif
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/Vector3fParam.h"
#include "vislib/Array.h"
#include "vislib/assert.h"
#include "vislib/math/Vector.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::CycleTransferFunction::CycleTransferFunction
 */
visualization::CycleTransferFunction::CycleTransferFunction(void) : Module(),
        getTFSlot("gettransferfunction", "Provides the transfer function"),
        texSizeSlot("texsize", "The size of the texture to generate"),
		colourSpaceSlot("colourspace", "which colour space is used"),
		colourCircleCenterSlot("colourcirclecenter", "Center of the Colourcircle"),
		colourCircleRadiusSlot("colourcircleradius", "Radius of the Colourcircle"),
		colourCircleAngleSlot("colourcircleangle", "Start angle for building of the Colourcircle (degree)"),
        texID(0), texSize(1),
        texFormat(core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGB) {

    core::view::CallGetTransferFunctionDescription cgtfd;
    this->getTFSlot.SetCallback(cgtfd.ClassName(), cgtfd.FunctionName(0),
        &CycleTransferFunction::requestTF);
    this->MakeSlotAvailable(&this->getTFSlot);

    this->texSizeSlot << new core::param::IntParam(128, 2, 1024);
    this->MakeSlotAvailable(&this->texSizeSlot);

	core::param::EnumParam *colourSpaces = new core::param::EnumParam(LUV);
	colourSpaces->SetTypePair(LUV, "Luv");
	colourSpaces->SetTypePair(LAB, "Lab");
	this->colourSpaceSlot.SetParameter(colourSpaces);
    this->MakeSlotAvailable(&this->colourSpaceSlot);

	this->colourCircleCenterSlot.SetParameter(new core::param::Vector3fParam(vislib::math::Vector<float,3>(67.1f, 21.1f, 11.6f), 
																		vislib::math::Vector<float,3>(0.0f, -134.0f, -140.0f), 
																		vislib::math::Vector<float,3>(100.0f, 224.0f, 128.0f)));
	this->MakeSlotAvailable(&this->colourCircleCenterSlot);

	this->colourCircleRadiusSlot.SetParameter(new core::param::FloatParam(110.4f, 1.0f, 128.0f));
	this->MakeSlotAvailable(&this->colourCircleRadiusSlot);

	this->colourCircleAngleSlot.SetParameter(new core::param::FloatParam(14.0f, 0.0f, 360.0f));
	this->MakeSlotAvailable(&this->colourCircleAngleSlot);
}


/*
 * visualization::CycleTransferFunction::~CycleTransferFunction
 */
visualization::CycleTransferFunction::~CycleTransferFunction(void) {
    this->Release();
}


/*
 * visualization::CycleTransferFunction::create
 */
bool visualization::CycleTransferFunction::create(void) {
    // intentionally empty
    return true;
}


/*
 * visualization::CycleTransferFunction::release
 */
void visualization::CycleTransferFunction::release(void) {
    ::glDeleteTextures(1, &this->texID);
    this->texID = 0;
}

std::vector<colourspace::LUVSpace> visualization::CycleTransferFunction::buildColourCirle(const colourspace::LUVSpace& center, const float radius, const unsigned int numberColours, float alphaRad) {
	std::vector<colourspace::LUVSpace> colourCircle(numberColours);

	// L constant
	float deltaAlphaRad = (2.0f * vislib::math::PI_DOUBLE) / static_cast<float>(numberColours);

	for (unsigned int colourIdx = 0; colourIdx < numberColours; ++colourIdx) {
		colourCircle[colourIdx].SetL(center.GetL());
		colourCircle[colourIdx].SetU(center.GetU() + radius * std::cos(alphaRad));
		colourCircle[colourIdx].SetV(center.GetV() + radius * std::sin(alphaRad));

		alphaRad += deltaAlphaRad;
	}

	return std::move(colourCircle);
}

std::vector<colourspace::LABSpace> visualization::CycleTransferFunction::buildColourCirle(const colourspace::LABSpace& center, const float radius, const unsigned int numberColours, float alphaRad) {
	std::vector<colourspace::LABSpace> colourCircle(numberColours);

	// L 
	float deltaAlphaRad = (2.0f * vislib::math::PI_DOUBLE) / static_cast<float>(numberColours);

	for (unsigned int colourIdx = 0; colourIdx < numberColours; ++colourIdx) {
		colourCircle[colourIdx].SetL(center.GetL());
		colourCircle[colourIdx].SetA(center.GetA() + radius * std::cos(alphaRad));
		colourCircle[colourIdx].SetB(center.GetB() + radius * std::sin(alphaRad));

		alphaRad += deltaAlphaRad;
	}

	return std::move(colourCircle);
}

/*
 * visualization::CycleTransferFunction::requestTF
 */
bool visualization::CycleTransferFunction::requestTF(core::Call& call) {
    core::view::CallGetTransferFunction *cgtf = dynamic_cast<core::view::CallGetTransferFunction*>(&call);
    if (cgtf == NULL) return false;

	bool dirty = this->texSizeSlot.IsDirty() || 
				this->colourSpaceSlot.IsDirty() || 
				this->colourCircleCenterSlot.IsDirty() || 
				this->colourCircleRadiusSlot.IsDirty() || 
				this->colourCircleAngleSlot.IsDirty();
	
    if ((this->texID == 0) || dirty) {
        this->texSizeSlot.ResetDirty();
		this->colourSpaceSlot.ResetDirty();
		this->colourCircleCenterSlot.ResetDirty();
		this->colourCircleRadiusSlot.ResetDirty();
		this->colourCircleAngleSlot.ResetDirty();

        bool t1de = (glIsEnabled(GL_TEXTURE_1D) == GL_TRUE);
        if (!t1de) glEnable(GL_TEXTURE_1D);
        if (this->texID == 0) glGenTextures(1, &this->texID);

		const unsigned int numberColours = 12;

		vislib::math::Vector<float,3> center = this->colourCircleCenterSlot.Param<core::param::Vector3fParam>()->Value();
		float radius = this->colourCircleRadiusSlot.Param<core::param::FloatParam>()->Value();
		float startAngleDeg = this->colourCircleAngleSlot.Param<core::param::FloatParam>()->Value();

		std::vector<colourspace::RGBSpace> colourCircleRGB;
		switch (this->colourSpaceSlot.Param<core::param::EnumParam>()->Value()) {
			case LUV: {
				colourspace::LUVSpace colourCenterLUV(center.GetX(), center.GetY(),center.GetZ());
				std::vector<colourspace::LUVSpace> colourCircleLUV = buildColourCirle(colourCenterLUV, radius, numberColours, startAngleDeg * vislib::math::PI_DOUBLE / 180.0f);
				colourCircleRGB = convertColourCircleToRGB(colourCircleLUV);
				break;
			}
			case LAB: {
				colourspace::LABSpace colourCenterLAB(center.GetX(), center.GetY(),center.GetZ());
				std::vector<colourspace::LABSpace> colourCircleLAB = buildColourCirle(colourCenterLAB, radius, numberColours, startAngleDeg * vislib::math::PI_DOUBLE / 180.0f);
				colourCircleRGB = convertColourCircleToRGB(colourCircleLAB);
				break;
			}
		}

		vislib::Array<vislib::math::Vector<float, 5> > cols;
        vislib::math::Vector<float, 5> cx1, cx2;

		bool validAlpha = false;
		
		cx1[0] = colourCircleRGB[0].GetR() / 255.0f;
		cx1[1] = colourCircleRGB[0].GetG() / 255.0f;
		cx1[2] = colourCircleRGB[0].GetB() / 255.0f;
		cx1[3] = 1.0f;
		cx1[4] = 0.0f;
		cols.Add(cx1);

		for (unsigned int colourIdx = 1; colourIdx < numberColours; ++colourIdx) {
			cx1[0] = colourCircleRGB[colourIdx].GetR() / 255.0f;
			cx1[1] = colourCircleRGB[colourIdx].GetG() / 255.0f;
			cx1[2] = colourCircleRGB[colourIdx].GetB() / 255.0f;
			cx1[3] = 1.0f;
			cx1[4] = colourIdx / static_cast<float>(numberColours);
			cols.Add(cx1);
		}

		cx1[0] = colourCircleRGB[0].GetR() / 255.0f;
		cx1[1] = colourCircleRGB[0].GetG() / 255.0f;
		cx1[2] = colourCircleRGB[0].GetB() / 255.0f;
		cx1[3] = 1.0f;
		cx1[4] = 1.0f;
		cols.Add(cx1);

        this->texSize = this->texSizeSlot.Param<core::param::IntParam>()->Value();
        float *tex = new float[4 * this->texSize];
        int p1, p2;

        cx2 = cols[0];
        p2 = 0;
        for (SIZE_T i = 1; i < cols.Count(); i++) {
            cx1 = cx2;
            p1 = p2;
            cx2 = cols[i];
            ASSERT(cx2[4] <= 1.0f + vislib::math::FLOAT_EPSILON);
            p2 = static_cast<int>(cx2[4] * static_cast<float>(this->texSize - 1));
            ASSERT(p2 < static_cast<int>(this->texSize));
            ASSERT(p2 >= p1);

            for (int p = p1; p <= p2; p++) {
                float al = static_cast<float>(p - p1) / static_cast<float>(p2 - p1);
                float be = 1.0f - al;

                tex[p * 4] = cx1[0] * be + cx2[0] * al;
                tex[p * 4 + 1] = cx1[1] * be + cx2[1] * al;
                tex[p * 4 + 2] = cx1[2] * be + cx2[2] * al;
                tex[p * 4 + 3] = cx1[3] * be + cx2[3] * al;
            }
        }

        GLint otid = 0;
        glGetIntegerv(GL_TEXTURE_BINDING_1D, &otid);
        glBindTexture(GL_TEXTURE_1D, this->texID);

        glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, this->texSize, 0, GL_RGBA, GL_FLOAT, tex);

        delete[] tex;

        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);

        glBindTexture(GL_TEXTURE_1D, otid);

        if (!t1de) glDisable(GL_TEXTURE_1D);

        this->texFormat = validAlpha
            ? core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGBA
            : core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGB;

    }

    cgtf->SetTexture(this->texID, this->texSize, this->texFormat);

    return true;
}
