/*
 * RenderHelper.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_RENDERHELPER_FUNCTIONS
#define MEGAMOLVPC_RENDERHELPER_FUNCTIONS

#include "MultiParticleClusterDataCall.h"
#include "CallVisParams.h"
#include "mmcore/Module.h"
#include "mmcore/CoreInstance.h"
#include "vislib/graphics/gl/GLSLShader.h"
#include "vislib/sys/Log.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include <vector>

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/** Barycentric interpolation */
	static void getBaryCentricInterpolatedColour(float x, float y, float z, vislib::math::Vector<float,3> &colour) {
		vislib::math::Vector<float,3> xColour(1.0f, 0.0f, 0.0f);
		vislib::math::Vector<float,3> yColour(0.0f, 1.0f, 0.0f);
		vislib::math::Vector<float,3> zColour(0.0f, 0.0f, 1.0f);

		if (x < 0.0f) {
			xColour = vislib::math::Vector<float,3> (0.0f, 1.0f, 1.0f);
		}
		if (y < 0.0f) {
			yColour = vislib::math::Vector<float,3> (1.0f, 0.0f, 1.0f);
		}
		if (z < 0.0f) {
			zColour = vislib::math::Vector<float,3> (1.0f, 1.0f, 0.0f);
		}
		colour = x*x * xColour + y*y * yColour + z*z * zColour; // barycentric interpolation, x^2 + y^2 + z^2 = 1 of normalised vector
	}

	/** Create shader */
	static bool CreateShader(core::CoreInstance *coreInstance, vislib::graphics::gl::GLSLShader &shader, std::string shaderName) {
		vislib::graphics::gl::ShaderSource vert, frag;

		vislib::StringA vertName(shaderName.c_str());
		vertName.Append("::vertex");

		vislib::StringA fragName(shaderName.c_str());
		fragName.Append("::fragment");

		if (!coreInstance->ShaderSourceFactory().MakeShaderSource(vertName, vert)) {
			return false;
		}
		if (!coreInstance->ShaderSourceFactory().MakeShaderSource(fragName, frag)) {
			return false;
		}

		try {
			if (!shader.Create(vert.Code(), vert.Count(), frag.Code(), frag.Count())) {
				vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
					"Unable to compile sphere shader: Unknown error\n");
				return false;
			}

		} catch(vislib::graphics::gl::AbstractOpenGLShader::CompileException ce) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
				"Unable to compile sphere shader (@%s): %s\n", 
				vislib::graphics::gl::AbstractOpenGLShader::CompileException::CompileActionName(
				ce.FailedAction()) ,ce.GetMsgA());
			return false;
		} catch(vislib::Exception e) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
				"Unable to compile sphere shader: %s\n", e.GetMsgA());
			return false;
		} catch(...) {
			vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR,
				"Unable to compile sphere shader: Unknown exception\n");
			return false;
		}

		return true;
	}

	/** Pushback vislib vector to stl vector */
	static void stdVectorPushBackVislibVector(std::vector<float>& features, const vislib::math::Vector<float,3>& feature) {
		features.push_back(feature.GetX());
		features.push_back(feature.GetY());
		features.push_back(feature.GetZ());
	}

	/** Sets Modelview- and projection matrix, texture for transferfunction and inConsts1 */
	static void SetViewportLightParams(vislib::graphics::gl::GLSLShader &shader) {
		float viewportStuff[4];
		::glGetFloatv(GL_VIEWPORT, viewportStuff);
		if (viewportStuff[2] < 1.0f) viewportStuff[2] = 1.0f;
		if (viewportStuff[3] < 1.0f) viewportStuff[3] = 1.0f;
		viewportStuff[2] = 2.0f / viewportStuff[2];
		viewportStuff[3] = 2.0f / viewportStuff[3];
	
		// Get light position
		GLfloat lightPos[4];
		glGetLightfv(GL_LIGHT0, GL_POSITION, lightPos);

		glUniform4fv(shader.ParameterLocation("viewAttr"), 1, viewportStuff);
		glUniform4fv(shader.ParameterLocation("lightPos"), 1, lightPos);
	}

	/** sets Modelview- and projection matrix, texture for transferfunction and inConsts1 */
	static void SetMVPMatTextureParams(vislib::graphics::gl::GLSLShader &shader, MultiParticleClusterDataCall *mpcdc, core::view::CallGetTransferFunction *cgtf, unsigned int greyTF) {
		// Get GL_MODELVIEW matrix
		GLfloat modelMatrix_column[16];
		glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix_column);
		vislib::math::ShallowMatrix<GLfloat, 4, vislib::math::COLUMN_MAJOR> modelMatrix(&modelMatrix_column[0]);

		// Get GL_PROJECTION matrix
		GLfloat projMatrix_column[16];
		glGetFloatv(GL_PROJECTION_MATRIX, projMatrix_column);
		vislib::math::ShallowMatrix<GLfloat, 4, vislib::math::COLUMN_MAJOR> projMatrix(&projMatrix_column[0]);

		// Compute modelviewprojection matrix
		vislib::math::Matrix<GLfloat, 4, vislib::math::ROW_MAJOR> modelProjMatrix = projMatrix * modelMatrix;
	
		glUniformMatrix4fv(shader.ParameterLocation("modelview"), 1, false, modelMatrix_column);
		glUniformMatrix4fv(shader.ParameterLocation("proj"), 1, false, projMatrix_column);
	
		MultiParticleClusterDataCall::Particles &pl = mpcdc->AccessParticles(0);
		float minC = 0.0f, maxC = 0.0f;
		unsigned int colTabSize = 0;

		glEnable(GL_TEXTURE_1D);

		if ((cgtf != NULL) && ((*cgtf)())) {
			glBindTexture(GL_TEXTURE_1D, cgtf->OpenGLTexture());
			colTabSize = cgtf->TextureSize();
		} else {
			glBindTexture(GL_TEXTURE_1D, greyTF);
			colTabSize = 2;
		}

		glUniform1i(shader.ParameterLocation("colTab"), 0);
		minC = pl.GetMinColourIndexValue();
		maxC = pl.GetMaxColourIndexValue();
		glColor3ub(127, 127, 127);

		glUniform3f(shader.ParameterLocation("inConsts1"), minC, maxC, float(colTabSize));
	}

	/** Ask for shared visualization parameter: draw cluster of idx */
	static int GetDrawClusterOfIdx(CallVisParams *vp, unsigned int numberClusters) {
		if (vp != nullptr) {
			vp->SetVisParams(VisualizationParams(numberClusters));
			if (!(*vp)(CallVisParams::CallForGetVisParams)) {
				return false;
			}
			if (!(*vp)(CallVisParams::CallForSetVisParams)) {
				return false;
			}
			return vp->GetVisParams().drawOnlyClusterId;
		}
		return -1; // all cluster are rendered
	}

	/** Ask for shared visualization parameter: cycle transferfunction */
	static bool GetCycleTransferFunction(CallVisParams *vp, unsigned int numberClusters) {
		if (vp != nullptr) {
			vp->SetVisParams(VisualizationParams(numberClusters));
			if (!(*vp)(CallVisParams::CallForGetVisParams)) {
				return false;
			}
			if (!(*vp)(CallVisParams::CallForSetVisParams)) {
				return false;
			}
			return vp->GetVisParams().cycleTransferFunction;
		}
		return true;
	}

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_RENDERHELPER_FUNCTIONS */