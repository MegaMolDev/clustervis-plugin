/*
 * HairBallRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "HairBallRenderer.h"
#include "RenderHelper.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/view/CallRender3D.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/StringParam.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/assert.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include <memory>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::HairBallRenderer::HairBallRenderer
 */
visualization::HairBallRenderer::HairBallRenderer(void) : AbstractHairBallRenderer(),
		lineWidthSlot("linewidth", "The width of the hairball-lines") {
	this->lineWidthSlot.SetParameter(new core::param::FloatParam(5.0f, 0.1f));
	this->MakeSlotAvailable(&this->lineWidthSlot);
}

/*
 * visualization::HairBallRenderer::~HairBallRenderer
 */
visualization::HairBallRenderer::~HairBallRenderer(void) {
    this->Release();
}

/*
 * visualization::HairBallRenderer::createHairballShader
 */
bool visualization::HairBallRenderer::create(void) {
	ASSERT(IsAvailable());

    CreateShader(this->instance(), this->hairballShader, "hairball");

    return AbstractHairBallRenderer::create();
}

/*
 * visualization::HairBallRenderer::release
 */
void visualization::HairBallRenderer::release(void) {
	this->hairballShader.Release();
    AbstractHairBallRenderer::release();
}

bool visualization::HairBallRenderer::renderHairballNeighbours(core::view::CallRender3D *cr, MultiParticleClusterDataCall* mpcdc) {
	// already tested that that cr and mpcdc != NULL

	bool usedStoredData = true;
	if (dataChanged(mpcdc)) { // nicht ganz sauber, aber da in der Regel alles glatt geht wei� ich so ob gespeicherte Sachen genutzt wurden oder neuberechnet wurde
		findNearestNeighbours(mpcdc, result);
		usedStoredData = false;
	}

	unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	int drawClusterOfIdx = -1;
	bool cycleTransferFunction = true;

	CallVisParams *vp = this->getVisParamsSlot().CallAs<CallVisParams>();
	if (vp != nullptr) {
		vp->SetVisParams(VisualizationParams(numberClusters));
		if (!(*vp)(CallVisParams::CallForGetVisParams)) {
			return false;
		}
		if (!(*vp)(CallVisParams::CallForSetVisParams)) {
			return false;
		}
		drawClusterOfIdx = vp->GetVisParams().drawOnlyClusterId;
		cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
	}

	// init shader
	float cylinderRadius = 0.01f; // TODO: an Zoomstufe anpassen

	this->hairballShader.Enable();
	SetViewportLightParams(this->hairballShader);
	core::view::CallGetTransferFunction *cgtf = this->getTFSlot().CallAs<core::view::CallGetTransferFunction>();
	SetMVPMatTextureParams(this->hairballShader, mpcdc, cgtf, this->getGreyTF());
	glUniform1f(this->hairballShader.ParameterLocation("drawClusterOfIdx"), static_cast<float>(drawClusterOfIdx)); // draw only lines of one cluster
	glUniform1f(this->hairballShader.ParameterLocation("numberCluster"), static_cast<float>(numberClusters));
	glUniform1f(this->hairballShader.ParameterLocation("cycleTransferFunction"), static_cast<float>(cycleTransferFunction));
	glUniform1f(this->hairballShader.ParameterLocation("radius"), cylinderRadius);
	
	GLint vertexPos = glGetAttribLocation(this->hairballShader, "vertex");
	GLint directionPos = glGetAttribLocation(this->hairballShader, "direction");
    GLint clusterIDs = glGetAttribLocationARB(this->hairballShader, "colIdx");
	
	glEnableVertexAttribArray(vertexPos);
	glEnableVertexAttribArray(directionPos);
	glEnableVertexAttribArrayARB(clusterIDs);
    
	glVertexAttribPointerARB(clusterIDs, 1, GL_FLOAT, GL_FALSE, 0, result.clusterIds.data());
	glVertexAttribPointer(vertexPos, 3, GL_FLOAT, GL_FALSE, 0, result.positions.data());
	glVertexAttribPointer(directionPos, 3, GL_FLOAT, GL_FALSE, 0, result.directions.data());
	
	// draw hairball
	glLineWidth(this->lineWidthSlot.Param<core::param::FloatParam>()->Value());
	glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(result.positions.size()/3));

    glDisableVertexAttribArray(vertexPos);
	glDisableVertexAttribArray(directionPos);
    glDisableVertexAttribArrayARB(clusterIDs);
    glDisable(GL_TEXTURE_1D);
    
	this->hairballShader.Disable();

	return usedStoredData;
}
