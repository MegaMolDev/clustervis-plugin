/*
 * CycleTransferFunction.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_CYCLETRANSFERFUNCTION_H_INCLUDED
#define MEGAMOLVPC_CYCLETRANSFERFUNCTION_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Call.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/Module.h"
#include "mmcore/param/ParamSlot.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include <vector>
#include "colourspace/LUVSpace.h"
#include "colourspace/LABSpace.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {


    /**
     * Module defining a piecewise cycle transfer function based on the
     * interval [0..1]
     */
    class CycleTransferFunction : public core::Module {
    public:

        /**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void) {
            return "CycleTransferFunction";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void) {
            return "Module defining a piecewise cycle transfer function";
        }

        /**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
        static bool IsAvailable(void) {
            return true;
        }

        /** Ctor. */
        CycleTransferFunction(void);

        /** Dtor. */
        virtual ~CycleTransferFunction(void);

    private:
        /**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);

        /**
         * Implementation of 'Release'.
         */
        virtual void release(void);

        /**
         * Callback called when the transfer function is requested.
         *
         * @param call The calling call
         *
         * @return 'true' on success
         */
        bool requestTF(core::Call& call);

        /** The callee slot called on request of a transfer function */
        core::CalleeSlot getTFSlot;

        /** The slot defining the texture size to generate */
        core::param::ParamSlot texSizeSlot;
		
		enum colourSpace {LAB, LUV};
		core::param::ParamSlot colourSpaceSlot;
		core::param::ParamSlot colourCircleCenterSlot;
		core::param::ParamSlot colourCircleRadiusSlot;
		core::param::ParamSlot colourCircleAngleSlot;

        /** The OpenGL texture object id */
        unsigned int texID;

        /** The texture size in texel */
        unsigned int texSize;

        /** The texture format */
        core::view::CallGetTransferFunction::TextureFormat texFormat;

		/** Build a colour circle in CIE-Lab space */
		std::vector<colourspace::LABSpace> buildColourCirle(const colourspace::LABSpace& center, const float radius, const unsigned int numberColours, float alphaRad = 0.0f);
		
		/** Build a colour circle in CIE-LUV space */
		std::vector<colourspace::LUVSpace> buildColourCirle(const colourspace::LUVSpace& center, const float radius, const unsigned int numberColours, float alphaRad = 0.0f);

		/** Convert colours to RGB space */
		template <class Colour>
		std::vector<colourspace::RGBSpace> convertColourCircleToRGB(std::vector<Colour>& colourCircleLUV);
	};

	template <class Colour>
	std::vector<colourspace::RGBSpace> visualization::CycleTransferFunction::convertColourCircleToRGB(std::vector<Colour>& colourCircle) {
		unsigned int numberColours = colourCircle.size();

		std::vector<colourspace::RGBSpace> colourCircleRGB(numberColours);

		for (unsigned int colourIdx = 0; colourIdx < numberColours; ++colourIdx) {
			Colour currentColour = colourCircle[colourIdx];
			colourCircleRGB[colourIdx] = currentColour.toRGB();
		}

		return std::move(colourCircleRGB);
	}

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_CYCLETRANSFERFUNCTION_H_INCLUDED */
