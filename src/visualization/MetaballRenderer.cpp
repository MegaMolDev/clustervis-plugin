/*
 * MetaballRenderer.cpp
 *
 * Copyright (C) 2011 by Universitaet Stuttgart (VISUS).
 * Modification of Render-Method and deletion of unused parameters by Julia B�hnke
 * All rights reserved.
 */

#include "stdafx.h"

#ifdef WITH_CUDA

#define _USE_MATH_DEFINES 1

#include "MetaballRenderer.h"
#include "CallVisParams.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/utility/ShaderSourceFactory.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/ButtonParam.h"
#include "mmcore/param/IntParam.h"
#include "vislib/assert.h"
#include "vislib/String.h"
#include "vislib/math/Quaternion.h"
#include "vislib/OutOfRangeException.h"
#include "vislib/Trace.h"
#include "vislib/graphics/gl/ShaderSource.h"
#include "vislib/graphics/gl/AbstractOpenGLShader.h"
#include "vislib/sys/ASCIIFileBuffer.h"
#include "vislib/StringConverter.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include <GL/glu.h>
#include <omp.h>
#include <cfloat>
#include <iostream>
#include <fstream>
#include <chrono>

using namespace megamol;
using namespace megamol::core;
using namespace megamol::vis_particle_clustering;
using namespace megamol::core::moldyn;


/*
 * visualization::MetaballRenderer::MetaballRenderer (CTOR)
 */
visualization::MetaballRenderer::MetaballRenderer(void) : Renderer3DModuleDS(),
    dataCallerSlot("getData", "Connects the molecule rendering with molecule data storage"),
	tfSlot("gettransferfunction", "Connects to the transfer function module"),
	sharedVisParamsSlot("sharedvisparams", "Connects to the shared visualization parameters"),
    qualityParam("quicksurf::quality", "Quality" ),
    radscaleParam("quicksurf::radscale", "Radius scale" ),
    gridspacingParam("quicksurf::gridspacing", "Grid spacing" ),
    isovalParam("quicksurf::isoval", "Isovalue" ),
    twoSidedLightParam("twoSidedLight", "Turns two-sided lighting on and off" ),
    numParticles(0), currentSurfaceArea(0.0f), 
	callTime(0.0f),
    setCUDAGLDevice(true),
    getClipPlaneSlot("getclipplane", "Connects to a clipping plane module")
{
    this->dataCallerSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
    this->MakeSlotAvailable(&this->dataCallerSlot);

	this->tfSlot.SetCompatibleCall<core::view::CallGetTransferFunctionDescription>();
    this->MakeSlotAvailable(&this->tfSlot);

	this->sharedVisParamsSlot.SetCompatibleCall<CallVisParamsDescription>();
    this->MakeSlotAvailable(&this->sharedVisParamsSlot);

    this->qualityParam.SetParameter(new param::IntParam(1, 0, 4));
    this->MakeSlotAvailable(&this->qualityParam);

    this->radscaleParam.SetParameter(new param::FloatParam(1.0f, 0.0f));
    this->MakeSlotAvailable(&this->radscaleParam);

    this->gridspacingParam.SetParameter(new param::FloatParam(1.0f, 0.0f));
    this->MakeSlotAvailable(&this->gridspacingParam);

    this->isovalParam.SetParameter(new param::FloatParam(0.5f, 0.0f));
    this->MakeSlotAvailable(&this->isovalParam);

    this->twoSidedLightParam.SetParameter(new param::BoolParam(false));
    this->MakeSlotAvailable(&this->twoSidedLightParam);

    isovalue = 0.5f;

    numvoxels[0] = 128;
    numvoxels[1] = 128;
    numvoxels[2] = 128;

    origin[0] = 0.0f;
    origin[1] = 0.0f;
    origin[2] = 0.0f;

    xaxis[0] = 1.0f;
    xaxis[1] = 0.0f;
    xaxis[2] = 0.0f;

    yaxis[0] = 0.0f;
    yaxis[1] = 1.0f;
    yaxis[2] = 0.0f;

    zaxis[0] = 0.0f;
    zaxis[1] = 0.0f;
    zaxis[2] = 1.0f;

    cudaqsurf = 0;

    gpuvertexarray=0;
    gpunumverts=0;
    gv=gn=gc=NULL;
    gpunumfacets=0;
    gf=NULL;

    this->getClipPlaneSlot.SetCompatibleCall<core::view::CallClipPlaneDescription>();
    this->MakeSlotAvailable(&this->getClipPlaneSlot);
}


/*
 * visualization::MetaballRenderer::~MetaballRenderer (DTOR)
 */
visualization::MetaballRenderer::~MetaballRenderer(void)  {
    if (cudaqsurf) {
        CUDAQuickSurf *cqs = (CUDAQuickSurf *) cudaqsurf;
        delete cqs;
    }

    if (gv)
        free(gv);
    if (gn)
        free(gn);
    if (gc)
        free(gc);
    if (gf)
        free(gf);

    this->Release();
}


/*
 * visualization::MetaballRenderer::release
 */
void visualization::MetaballRenderer::release(void) {

}


/*
 * visualization::MetaballRenderer::create
 */
bool visualization::MetaballRenderer::create(void) {
	if(!isExtAvailable( "GL_ARB_vertex_program") || !ogl_IsVersionGEQ(2,0) )
        return false;

    if (!vislib::graphics::gl::GLSLShader::InitialiseExtensions() )
        return false;

    //cudaGLSetGLDevice(cudaUtilGetMaxGflopsDeviceId() );
    //printf("cudaGLSetGLDevice: %s\n", cudaGetErrorString( cudaGetLastError()));

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glEnable(GL_VERTEX_PROGRAM_TWO_SIDE);
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE_ARB);

    using namespace vislib::sys;
    using namespace vislib::graphics::gl;

    ShaderSource vertSrc;
    ShaderSource fragSrc;

    //////////////////////////////////////////////////////
    // load the shader files for the per pixel lighting //
    //////////////////////////////////////////////////////
    // vertex shader
    if (!this->GetCoreInstance()->ShaderSourceFactory().MakeShaderSource("quicksurf::perpixellightVertexClip", vertSrc)) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Unable to load vertex shader source for perpixellight shader");
        return false;
    }
    if (!this->GetCoreInstance()->ShaderSourceFactory().MakeShaderSource("quicksurf::perpixellightFragmentClip", fragSrc)) {
        Log::DefaultLog.WriteMsg(Log::LEVEL_ERROR, "Unable to load fragment shader source for perpixellight shader");
        return false;
    }
    this->lightShader.Create(vertSrc.Code(), vertSrc.Count(), fragSrc.Code(), fragSrc.Count());

    return true;
}


/*
 * visualization::MetaballRenderer::GetCapabilities
 */
bool visualization::MetaballRenderer::GetCapabilities(Call& call) {
    view::AbstractCallRender3D *cr3d = dynamic_cast<view::AbstractCallRender3D *>(&call);
    if (cr3d == NULL) return false;

    cr3d->SetCapabilities(view::AbstractCallRender3D::CAP_RENDER
        | view::AbstractCallRender3D::CAP_LIGHTING
        | view::AbstractCallRender3D::CAP_ANIMATION );

    return true;
}


/*
 * visualization::MetaballRenderer::GetExtents
 */
bool visualization::MetaballRenderer::GetExtents(Call& call) {
    view::AbstractCallRender3D *cr3d = dynamic_cast<view::AbstractCallRender3D *>(&call);
    if(cr3d == NULL ) return false;

    MultiParticleClusterDataCall *mpcdc = this->dataCallerSlot.CallAs<MultiParticleClusterDataCall>();
    if(mpcdc == NULL ) return false;
    if (!(*mpcdc)(1)) return false;

    float scale;
    if(!vislib::math::IsEqual(mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge(), 0.0f) ) {
        scale = 2.0f / mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
    } else {
        scale = 1.0f;
    }

    cr3d->AccessBoundingBoxes() = mpcdc->AccessBoundingBoxes();
    cr3d->AccessBoundingBoxes().MakeScaledWorld(scale);
    cr3d->SetTimeFramesCount(mpcdc->FrameCount());

    return true;
}


/**********************************************************************
 * 'render'-functions
 **********************************************************************/

/*
 * visualization::MetaballRenderer::Render
 */
bool visualization::MetaballRenderer::Render(Call& call) {

    static int counter = 0;

#ifdef TIME_MEASUREMENT
    static std::chrono::time_point<std::chrono::system_clock> start;
    std::chrono::time_point<std::chrono::system_clock> currentStart;

    if (counter == 0) {
        start = std::chrono::system_clock::now();
    }

    currentStart = std::chrono::system_clock::now();
#endif

    // cast the call to Render3D
    view::AbstractCallRender3D *cr3d = dynamic_cast<view::AbstractCallRender3D *>(&call);
    if(cr3d == NULL ) return false;

    if(setCUDAGLDevice ) {
#ifdef _WIN32
        if( cr3d->IsGpuAffinity() ) {
            HGPUNV gpuId = cr3d->GpuAffinity<HGPUNV>();
            int devId;
            cudaWGLGetDevice( &devId, gpuId);
            cudaGLSetGLDevice( devId);
        } else {
            cudaGLSetGLDevice( cudaUtilGetMaxGflopsDeviceId());
        }
#else
        cudaGLSetGLDevice( cudaUtilGetMaxGflopsDeviceId());
#endif
        printf( "cudaGLSetGLDevice: %s\n", cudaGetErrorString( cudaGetLastError()));
        setCUDAGLDevice = false;
    }

    // get camera information
    this->cameraInfo = cr3d->GetCameraParameters();

    callTime = cr3d->Time();
    //if (callTime < 1.0f) callTime = 1.0f; // causes conflict with other renderers callTime = 0

    // get pointer to MultiParticleDataCall
    MultiParticleClusterDataCall *mpcdc = this->dataCallerSlot.CallAs<MultiParticleClusterDataCall>();
    if(mpcdc == NULL) return false;

    // set frame ID and call data
    mpcdc->SetFrameID(static_cast<int>(callTime));
	if (!(*mpcdc)(0)) return false;

	float globalRadius = mpcdc->AccessParticles(0).GetGlobalRadius();
	
	if ((this->lastFrame != mpcdc->FrameID()) || (this->lastHash != mpcdc->DataHash()) || (mpcdc->DataHash() == 0)) {
		this->lastFrame = mpcdc->FrameID();
		this->lastHash = mpcdc->DataHash();
		updateParticlePositions(mpcdc);
	#ifdef TIME_MEASUREMENT
		counter = 0;
        std::chrono::time_point<std::chrono::system_clock> currentEnd = std::chrono::system_clock::now();
        int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(currentEnd-currentStart).count();
        std::cout << "ellapsed time: " << currentElapsedMilliseconds << std::endl;

        std::string fileName = "HairballRenderer.csv";
        std::ofstream file(fileName, std::fstream::out | std::fstream::app);

        if (!file.good()) {
            return false;
        }

        file << "time in milliseconds;number of clusters;usedStoredData\n";
        file << currentElapsedMilliseconds << ";" << mpcdc->AccessParticles(0).GetNumberClusters() << ";false" << "\n";
	#endif
	} 
#ifdef TIME_MEASUREMENT
	else {
        ++counter;
    }
#endif

    glPushMatrix();
    // compute scale factor and scale world
    float scale;
    if(!vislib::math::IsEqual( mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge(), 0.0f) ) {
        scale = 2.0f / mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
    } else {
        scale = 1.0f;
    }
    glScalef(scale, scale, scale);

    // ---------- render ----------

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    float spec[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50.0f);
    glEnable(GL_COLOR_MATERIAL);

    glDisable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    float clipDat[4];
    float clipCol[4];
    this->getClipData(clipDat, clipCol);

    // calculate surface
    if(!cudaqsurf ) {
        cudaqsurf = new CUDAQuickSurf();
    }
    this->lightShader.Enable();
    glUniform1i(this->lightShader.ParameterLocation("twoSidedLight"), this->twoSidedLightParam.Param<param::BoolParam>()->Value());
    glUniform4fv(this->lightShader.ParameterLocation("clipDat"), 1, clipDat);
    glUniform4fv(this->lightShader.ParameterLocation("clipCol"), 1, clipCol);

	const unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	int drawClusterOfIdx = -1;
	bool cycleTransferFunction = true;

	CallVisParams *vp = this->sharedVisParamsSlot.CallAs<CallVisParams>();
	if (vp != nullptr) {
		vp->SetVisParams(VisualizationParams(numberClusters));
		if (!(*vp)(CallVisParams::CallForGetVisParams) || !(*vp)(CallVisParams::CallForSetVisParams)) {
			return false;
		}
		drawClusterOfIdx = vp->GetVisParams().drawOnlyClusterId;
		cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
	}
	
	if (drawClusterOfIdx < 0 || drawClusterOfIdx >= numberClusters) {
		for (unsigned int currentClusterIdx = 0; currentClusterIdx < numberClusters; ++currentClusterIdx) {
			numParticles = clusterPositions[currentClusterIdx].size() / 4;
			this->calcSurf(mpcdc, globalRadius, currentClusterIdx, cycleTransferFunction,
				this->qualityParam.Param<param::IntParam>()->Value(),
				this->radscaleParam.Param<param::FloatParam>()->Value(),
				this->gridspacingParam.Param<param::FloatParam>()->Value(),
				this->isovalParam.Param<param::FloatParam>()->Value());
		}
	} else {
		numParticles = clusterPositions[drawClusterOfIdx].size() / 4;
		this->calcSurf(mpcdc, globalRadius, drawClusterOfIdx, cycleTransferFunction,
			this->qualityParam.Param<param::IntParam>()->Value(),
			this->radscaleParam.Param<param::FloatParam>()->Value(),
			this->gridspacingParam.Param<param::FloatParam>()->Value(),
			this->isovalParam.Param<param::FloatParam>()->Value());
	}
    this->lightShader.Disable();

    glPopMatrix();

    // unlock the current frame
    mpcdc->Unlock();

#ifdef TIME_MEASUREMENT
    if (counter >= 100) {
        std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
        int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "ellapsed time: " << currentElapsedMilliseconds << std::endl;

        std::string fileName = "HairballRenderer.csv";
        std::ofstream file(fileName, std::fstream::out | std::fstream::app);

        if (!file.good()) {
            return false;
        }

        //file << "time in milliseconds;number of clusters\n";
        file << currentElapsedMilliseconds / counter << ";" << mpcdc->AccessParticles(0).GetNumberClusters() << ";true" << "\n";
        counter = 0;
    }
#endif

    return true;
}

#ifdef _MSC_VER
#pragma push_macro("max")
#undef max
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

bool visualization::MetaballRenderer::updateParticlePositions(MultiParticleClusterDataCall* mpcdc) {
	MultiParticleClusterDataCall::Particles &parts = mpcdc->AccessParticles(0);

	if (parts.GetVertexDataType() != megamol::core::moldyn::MultiParticleDataCall::Particles::VERTDATA_FLOAT_XYZ) {
		return false;
	}

	unsigned int numberClusters = parts.GetNumberClusters();

	std::vector<float> initValuesBBox(6);
	for (unsigned int idx = 0; idx < 3; ++idx) {
		initValuesBBox[idx] = std::numeric_limits<float>::max();
		initValuesBBox[idx + 3] = std::numeric_limits<float>::min();
	}

	clusterPositions.clear();
	clusterPositions.resize(numberClusters);
	clusterBoundingBoxes.clear();
	clusterBoundingBoxes.resize(numberClusters, initValuesBBox);

	const float *pos = static_cast<const float*>(parts.GetVertexData());
    unsigned int posStride = parts.GetVertexDataStride();
    float globRad = parts.GetGlobalRadius();
    if (posStride < 12) posStride = 12;

	const unsigned int *cluster = static_cast<const unsigned int*>(parts.GetClusterData());
	unsigned int clusterStride = parts.GetClusterDataStride();

    for (UINT64 j = 0; j < parts.GetCount(); j++) {
        unsigned int currentClusterId = cluster[0];
		clusterPositions[currentClusterId].push_back(pos[0]);
		clusterPositions[currentClusterId].push_back(pos[1]);
		clusterPositions[currentClusterId].push_back(pos[2]);
		clusterPositions[currentClusterId].push_back(globRad);

		// update bounding box for cluster
		for (unsigned int d = 0; d < 3; ++d) {
			// update min for each dimension
			if (pos[d] < clusterBoundingBoxes[currentClusterId][d]) {
				clusterBoundingBoxes[currentClusterId][d] = pos[d];
			}
			// update max for each dimension
			if (pos[d] > clusterBoundingBoxes[currentClusterId][d + 3]) {
				clusterBoundingBoxes[currentClusterId][d + 3] = pos[d];
			}
		}

		pos = reinterpret_cast<const float*>(reinterpret_cast<const char*>(pos) + posStride);
        cluster = reinterpret_cast<const unsigned int*>(reinterpret_cast<const char*>(cluster) + clusterStride);
    }

	/*for (unsigned int clusterId = 0; clusterId < numberClusters; ++clusterId) {
		std::cout << "Boundingbox cluster " << clusterId + 1 << ": " << clusterBoundingBoxes[clusterId][0];
		for (unsigned int d = 1; d < 6; ++d) {
			std::cout << ", " << clusterBoundingBoxes[clusterId][d];
		}
		std::cout << std::endl;
	}*/

	return true;
}

int visualization::MetaballRenderer::calcSurf(MultiParticleClusterDataCall *mpcdc, float globalRadius, float clusterIdx, bool useCycleTransferFunction,
                         int quality, float radscale, float gridspacing,
                         float isoval) {
    // initialize class variables
    isovalue=isoval;

//#define PADDING
#define USE_BOUNDING_BOX
    // crude estimate of the grid padding we require to prevent the
    // resulting isosurface from being clipped
#ifdef PADDING
    float gridpadding = radscale * globalRadius * 1.5;
    float padrad = gridpadding;
    padrad = 0.4 * sqrt(4.0/3.0*M_PI*padrad*padrad*padrad);
    gridpadding = std::max(gridpadding, padrad);
    gridpadding *= 2.0f;
#else
    float gridpadding = 0.0f;
#endif

	// TODO: Bounding box der ausgew�hlten partikel nutzen (muss nur einmal beim positionen setzen berechnet werden)
    // kroneml
	float mincoord[3], maxcoord[3];
	std::vector<float> currentBBox(clusterBoundingBoxes[clusterIdx]);
    mincoord[0] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Left();
    mincoord[1] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Bottom();
    mincoord[2] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Back();
    maxcoord[0] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Right();
    maxcoord[1] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Top();
    maxcoord[2] = mpcdc->AccessBoundingBoxes().ObjectSpaceBBox().Front();

    mincoord[0] -= gridpadding;
    mincoord[1] -= gridpadding;
    mincoord[2] -= gridpadding;
    maxcoord[0] += gridpadding;
    maxcoord[1] += gridpadding;
    maxcoord[2] += gridpadding;

    // compute the real grid dimensions from the selected atoms
    xaxis[0] = maxcoord[0]-mincoord[0];
    yaxis[1] = maxcoord[1]-mincoord[1];
    zaxis[2] = maxcoord[2]-mincoord[2];
    numvoxels[0] = (int) ceil(xaxis[0] / gridspacing);
    numvoxels[1] = (int) ceil(yaxis[1] / gridspacing);
    numvoxels[2] = (int) ceil(zaxis[2] / gridspacing);

    // recalc the grid dimensions from rounded/padded voxel counts
    xaxis[0] = (numvoxels[0]-1) * gridspacing;
    yaxis[1] = (numvoxels[1]-1) * gridspacing;
    zaxis[2] = (numvoxels[2]-1) * gridspacing;
    maxcoord[0] = mincoord[0] + xaxis[0];
    maxcoord[1] = mincoord[1] + yaxis[1];
    maxcoord[2] = mincoord[2] + zaxis[2];

    // update grid spacing
    float bBoxDim[3] = {maxcoord[0] - mincoord[0], maxcoord[1] - mincoord[1], maxcoord[2] - mincoord[2]};
    float gridDim[3] = {
        ceilf(bBoxDim[0] / gridspacing) + 1,
        ceilf(bBoxDim[1] / gridspacing) + 1,
        ceilf(bBoxDim[2] / gridspacing) + 1};
    float3 gridSpacing3D;
    gridSpacing3D.x = bBoxDim[0] / (gridDim[0] - 1.0f);
    gridSpacing3D.y = bBoxDim[1] / (gridDim[1] - 1.0f);
    gridSpacing3D.z = bBoxDim[2] / (gridDim[2] - 1.0f);
    numvoxels[0] = static_cast<int>(ceilf(bBoxDim[0] / gridSpacing3D.x)) + 1;
    numvoxels[1] = static_cast<int>(ceilf(bBoxDim[1] / gridSpacing3D.y)) + 1;
    numvoxels[2] = static_cast<int>(ceilf(bBoxDim[2] / gridSpacing3D.z)) + 1;

    //vec_copy(origin, mincoord);
    origin[0] = mincoord[0];
    origin[1] = mincoord[1];
    origin[2] = mincoord[2];

    // build compacted list of colors
    float *colors = NULL;
	colors = (float *) malloc(numParticles * sizeof(float) * 4);

	unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	bool useGreyTransferFunction = true; // if there is a transfer function, this will be set to false later

	core::view::CallGetTransferFunction *cgtf = this->tfSlot.CallAs<core::view::CallGetTransferFunction>();
	if ((cgtf != NULL) && ((*cgtf)())) {
		unsigned int textureChannels = 0;
		GLint textureFormat = cgtf->OpenGLTextureFormat();
		switch(textureFormat) {
			case GL_RGB:
				textureChannels = 3;
				break;
			case GL_RGBA:
				textureChannels = 4;
				break;
			default: // unsupported type
				break;
		}
		if (textureChannels >= 3) { // else: greyTF
			useGreyTransferFunction = false;
			unsigned int textureSize = cgtf->TextureSize();
			std::vector<GLfloat> transferFunction(textureSize * textureChannels);

			glEnable(GL_TEXTURE_1D);
			glBindTexture(GL_TEXTURE_1D, cgtf->OpenGLTexture());
			glGetTexImage(GL_TEXTURE_1D, 0, textureFormat, GL_FLOAT, transferFunction.data());
			glDisable(GL_TEXTURE_1D);

			for (unsigned int idx = 0; idx < numParticles; idx++) {
				float denominator = static_cast<float>(numberClusters - (1 - useCycleTransferFunction));
				int p = clusterIdx / denominator * (textureSize - 1);
				// TODO: interpolieren zwischen 2 Pixelfarben n�tig?
				colors[4 * idx    ] = transferFunction[p * textureChannels];
				colors[4 * idx + 1] = transferFunction[p * textureChannels + 1];
				colors[4 * idx + 2] = transferFunction[p * textureChannels + 2];
				colors[4 * idx + 3] = 1.0f;
			}
		}
	}
	if (useGreyTransferFunction) {
		for (unsigned int idx = 0; idx < numParticles; idx++) {
			float denominator = static_cast<float>(numberClusters - 1);
			colors[4 * idx    ] = clusterIdx / denominator;
			colors[4 * idx + 1] = clusterIdx / denominator;
			colors[4 * idx + 2] = clusterIdx / denominator;
			colors[4 * idx + 3] = 1.0f;
		}
	}

    // set gaussian window size based on user-specified quality parameter
    float gausslim = 2.0f;
    switch (quality) {
		case 3: gausslim = 4.0f; break; // max quality

		case 2: gausslim = 3.0f; break; // high quality

		case 1: gausslim = 2.5f; break; // medium quality

		case 0:
		default: gausslim = 2.0f; // low quality
			break;
    }

	CUDAQuickSurf *cqs = (CUDAQuickSurf *) cudaqsurf;
    cqs->useGaussKernel = true;

	glPushMatrix();
	glTranslatef(gridpadding,gridpadding,gridpadding);
	
    // compute both density map and floating point color texture map
    int rc = cqs->calc_surf(numParticles, clusterPositions[clusterIdx].data(),
        &colors[0],
        true, origin, numvoxels, globalRadius,
        radscale, gridSpacing3D, isovalue, gausslim,
        gpunumverts, gv, gn, gc, gpunumfacets, gf);

	glPopMatrix();

    this->currentSurfaceArea = cqs->surfaceArea;

    if (rc == 0) {
        gpuvertexarray = 1;
        if (colors) free(colors);
        return 0;
    } else {
        gpuvertexarray = 0;
        if (colors) free(colors);
        return 1;
    }
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#pragma pop_macro("max")
#endif /* _MSC_VER */

/*
 * MetaballRenderer::getClipData
 */
void visualization::MetaballRenderer::getClipData(float *clipDat, float *clipCol) {
    view::CallClipPlane *ccp = this->getClipPlaneSlot.CallAs<view::CallClipPlane>();
    if ((ccp != NULL) && (*ccp)()) {
        clipDat[0] = ccp->GetPlane().Normal().X();
        clipDat[1] = ccp->GetPlane().Normal().Y();
        clipDat[2] = ccp->GetPlane().Normal().Z();
        vislib::math::Vector<float, 3> grr(ccp->GetPlane().Point().PeekCoordinates());
        clipDat[3] = grr.Dot(ccp->GetPlane().Normal());
        clipCol[0] = static_cast<float>(ccp->GetColour()[0]) / 255.0f;
        clipCol[1] = static_cast<float>(ccp->GetColour()[1]) / 255.0f;
        clipCol[2] = static_cast<float>(ccp->GetColour()[2]) / 255.0f;
        clipCol[3] = static_cast<float>(ccp->GetColour()[3]) / 255.0f;

    } else {
        clipDat[0] = clipDat[1] = clipDat[2] = clipDat[3] = 0.0f;
        clipCol[0] = clipCol[1] = clipCol[2] = 0.75f;
        clipCol[3] = 1.0f;
    }
}


#endif // WITH_CUDA
