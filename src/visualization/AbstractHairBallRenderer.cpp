/*
 * AbstractHairBallRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "AbstractHairBallRenderer.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallClipPlane.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/view/CallRender3D.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/BoolParam.h"
#include "vislib/assert.h"
#include "vislib/math/Vector.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include <memory>
#include <fstream>
#ifdef TIME_MEASUREMENT
#include <chrono>
#endif

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::AbstractHairBallRenderer::AbstractHairBallRenderer
 */
visualization::AbstractHairBallRenderer::AbstractHairBallRenderer(void) : Renderer3DModule(),
        getDataSlot("getdata", "Connects to the data source"),
        tfSlot("gettransferfunction", "Connects to the transfer function module"),
		sharedVisParamsSlot("sharedvisparams", "Connects to the shared visualization parameters"),
		numberNeighboursSlot("numberneighbours", "The number of neighbours to connect"),
		connectAllComponentsSlot("connectallcomponents", "Are all components to be connected?"),
	#ifdef CONNECT_TO_MEAN
		drawHairballMeansSlot("draw hairball means", "draw hairball means or connect nearest neighbours."),
		drawHairballMeansHeightSlot("draw hairball means - offset height", "."),
	#endif
		greyTF(0) {
    this->getDataSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
    this->MakeSlotAvailable(&this->getDataSlot);

	this->tfSlot.SetCompatibleCall<core::view::CallGetTransferFunctionDescription>();
    this->MakeSlotAvailable(&this->tfSlot);

	this->sharedVisParamsSlot.SetCompatibleCall<CallVisParamsDescription>();
    this->MakeSlotAvailable(&this->sharedVisParamsSlot);

	this->numberNeighboursSlot.SetParameter(new core::param::IntParam(3,0,100));
    this->MakeSlotAvailable(&this->numberNeighboursSlot);

	this->connectAllComponentsSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&this->connectAllComponentsSlot);

#ifdef CONNECT_TO_MEAN
	this->drawHairballMeansSlot.SetParameter(new core::param::BoolParam(false));
	this->MakeSlotAvailable(&this->drawHairballMeansSlot);

	this->drawHairballMeansHeightSlot.SetParameter(new core::param::FloatParam(0.0f, -5.0f, 5.0f));
	this->MakeSlotAvailable(&this->drawHairballMeansHeightSlot);
#endif
}

/*
 * visualization::AbstractHairBallRenderer::~AbstractHairBallRenderer
 */
visualization::AbstractHairBallRenderer::~AbstractHairBallRenderer(void) {
	this->Release();
}

/*
 * visualization::AbstractHairBallRenderer::GetExtents
 */
bool visualization::AbstractHairBallRenderer::GetExtents(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    MultiParticleClusterDataCall *c2 = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    if ((c2 != NULL) && ((*c2)(1))) {
        cr->SetTimeFramesCount(c2->FrameCount());
        cr->AccessBoundingBoxes() = c2->AccessBoundingBoxes();

        float scaling = cr->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
        if (scaling > 0.0000001) {
            scaling = 10.0f / scaling;
        } else {
            scaling = 1.0f;
        }
        cr->AccessBoundingBoxes().MakeScaledWorld(scaling);

    } else {
        cr->SetTimeFramesCount(1);
        cr->AccessBoundingBoxes().Clear();
    }

    return true;
}

/*
 * visualization::AbstractHairBallRenderer::GetCapabilities
 */
bool visualization::AbstractHairBallRenderer::GetCapabilities(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    cr->SetCapabilities(
        core::view::CallRender3D::CAP_RENDER
        | core::view::CallRender3D::CAP_LIGHTING
        | core::view::CallRender3D::CAP_ANIMATION
        );

    return true;
}


/*
 * visualization::AbstractHairBallRenderer::getData
 */
vis_particle_clustering::MultiParticleClusterDataCall* visualization::AbstractHairBallRenderer::getData(unsigned int t, float& outScaling) {
    MultiParticleClusterDataCall *c2 = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    outScaling = 1.0f;
    if (c2 != NULL) {
        c2->SetFrameID(t);
        if (!(*c2)(1)) return NULL;

        // calculate scaling
        outScaling = c2->AccessBoundingBoxes().ObjectSpaceBBox().LongestEdge();
        if (outScaling > 0.0000001) {
            outScaling = 10.0f / outScaling;
        } else {
            outScaling = 1.0f;
        }

        c2->SetFrameID(t);
        if (!(*c2)(0)) return NULL;

	#ifdef CONNECT_TO_MEAN
		if (this->drawHairballMeansSlot.Param<core::param::BoolParam>()->Value() || 
			((this->lastFrame != c2->FrameID()) || 
			(this->lastHash != c2->DataHash()) || 
			(c2->DataHash() == 0))) {
			this->lastFrame = c2->FrameID();
			this->lastHash = c2->DataHash();
       		// compute the mean of the positions of particles for each cluster
			MultiParticleClusterDataCall::Particles pl = c2->AccessParticles(0);
			unsigned int clusterStride = pl.GetClusterDataStride();
			const uint8_t *cluster = static_cast<const uint8_t*>(pl.GetClusterData());
			unsigned int vert_stride = pl.GetVertexDataStride();
			const uint8_t *vert = static_cast<const uint8_t*>(pl.GetVertexData());
			if (vert_stride < 12) vert_stride = 12;

			unsigned int numberClusters = pl.GetNumberClusters();

			meanPositions.clear();
			meanPositions.resize(numberClusters, std::vector<float>(3, 0.0f));
		
			meanCount.clear();
			meanCount.resize(numberClusters, 0.0f);

			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, vert += vert_stride, cluster += clusterStride) {
				vislib::math::Vector<float,3> currentPosition(	reinterpret_cast<const float*>(vert)[0],
																reinterpret_cast<const float*>(vert)[1],
																reinterpret_cast<const float*>(vert)[2]);
				unsigned int currentCluster = reinterpret_cast<const unsigned int*>(cluster)[0];
				for (int c=0; c<3; ++c) {
					meanPositions[currentCluster] [c] += currentPosition[c];
				}
				meanCount[currentCluster]++;
			}
			for (unsigned int idx = 0; idx < numberClusters; ++idx) {
				for (int c=0; c<3; ++c) {
					meanPositions[idx] [c] /= meanCount[idx];
				}
			}
		}
	#endif

        return c2;
    } else {
        return NULL;
    }
}

/*
 * visualization::AbstractHairBallRenderer::create
 */
bool visualization::AbstractHairBallRenderer::create(void) {
    glEnable(GL_TEXTURE_1D);
    glGenTextures(1, &this->greyTF);
    unsigned char tex[6] = {
        0, 0, 0,  255, 255, 255
    };
    glBindTexture(GL_TEXTURE_1D, this->greyTF);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, tex);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glBindTexture(GL_TEXTURE_1D, 0);

    glDisable(GL_TEXTURE_1D);

    return true;
}


/*
 * visualization::AbstractHairBallRenderer::release
 */
void visualization::AbstractHairBallRenderer::release(void) {
    ::glDeleteTextures(1, &this->greyTF);
}

#ifdef CONNECT_TO_MEAN
void visualization::AbstractHairBallRenderer::renderHairballMeans(MultiParticleClusterDataCall* mpcdc) {
	MultiParticleClusterDataCall::Particles &parts = mpcdc->AccessParticles(0);
	
	UINT64 particleCount = parts.GetCount();
	unsigned int vert_stride = parts.GetVertexDataStride();
	const uint8_t *vert = static_cast<const uint8_t*>(parts.GetVertexData());
	if (vert_stride < 12) vert_stride = 12;

	unsigned int clusterStride = parts.GetClusterDataStride();
	const uint8_t * cluster = static_cast<const uint8_t*>(parts.GetClusterData());

	unsigned int numberClusters = parts.GetNumberClusters();
	std::vector<float> colors(3 * numberClusters);

	core::view::CallGetTransferFunction *cgtf = this->tfSlot.CallAs<core::view::CallGetTransferFunction>();
	if ((cgtf != NULL) && ((*cgtf)())) {
		unsigned int textureChannels = 0;
		GLint textureFormat = cgtf->OpenGLTextureFormat();
		switch(textureFormat) {
			case GL_RGB:
				textureChannels = 3;
				break;
			case GL_RGBA:
				textureChannels = 4;
				break;
			default: // unsupported type
				break;
		}
		if (textureChannels >= 3) { // else: greyTF
			unsigned int textureSize = cgtf->TextureSize();
			std::vector<GLfloat> transferFunction(textureSize * textureChannels);

			glEnable(GL_TEXTURE_1D);
			glBindTexture(GL_TEXTURE_1D, cgtf->OpenGLTexture());
			glGetTexImage(GL_TEXTURE_1D, 0, textureFormat, GL_FLOAT, transferFunction.data());
			glDisable(GL_TEXTURE_1D);

			for (unsigned int idx = 0; idx < numberClusters; idx++) {
				float denominator = static_cast<float>(numberClusters);
				int p = idx / denominator * (textureSize - 1);
				colors[3 * idx    ] = transferFunction[p * textureChannels];
				colors[3 * idx + 1] = transferFunction[p * textureChannels + 1];
				colors[3 * idx + 2] = transferFunction[p * textureChannels + 2];
			}
		}
	} else {
		for (unsigned int idx = 0; idx < numberClusters; idx++) {
			colors[3 * idx    ] = (idx + 1) / static_cast<float>(numberClusters);
			colors[3 * idx + 1] = (idx + 1) / static_cast<float>(numberClusters);
			colors[3 * idx + 2] = (idx + 1) / static_cast<float>(numberClusters);
		}
	}

	glLineWidth(1.0f);

	glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin(GL_LINES);
		glMatrixMode(GL_MODELVIEW);
		for (uint64_t pi = 0; pi < particleCount; pi++, vert += vert_stride, cluster += clusterStride) {
			unsigned int currentCluster = reinterpret_cast<const unsigned int*>(cluster)[0];
			glColor4f(	colors[3 * currentCluster],
						colors[3 * currentCluster + 1],
						colors[3 * currentCluster + 2],
						1.0f);
			glVertex3fv(&reinterpret_cast<const float*>(vert)[0]);
			
			glVertex3f(	meanPositions[currentCluster][0],
						meanPositions[currentCluster][1] + this->drawHairballMeansHeightSlot.Param<core::param::FloatParam>()->Value(),
						meanPositions[currentCluster][2]);
		}
	glEnd();
	glDisable(GL_BLEND);
}
#endif

/*
 * visualization::AbstractHairBallRenderer::Render
 */
bool visualization::AbstractHairBallRenderer::Render(core::Call& call) {
#ifdef TIME_MEASUREMENT
	static int counter = 0;

	static std::chrono::time_point<std::chrono::system_clock> start;
	
	std::chrono::time_point<std::chrono::system_clock> currentStart;
	
	if (counter == 0) {
		start = std::chrono::system_clock::now();
	}

	currentStart = std::chrono::system_clock::now();
#endif
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    float scaling = 1.0f;
	
	MultiParticleClusterDataCall *mpcdc = this->getData(static_cast<unsigned int>(cr->Time()), scaling);
    if (mpcdc == nullptr) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Hairball: no data provided!\n");
		return false;
	}

	glDisable(GL_BLEND);
	glPushMatrix();
	glScalef(scaling, scaling, scaling);

#ifdef CONNECT_TO_MEAN
	bool rendererUsedStoredData = false;
	if (this->drawHairballMeansSlot.Param<core::param::BoolParam>()->Value()) { // only used to create figure
		renderHairballMeans(mpcdc);
	} else {
		rendererUsedStoredData = renderHairballNeighbours(call, mpcdc);
	}
#else
	bool rendererUsedStoredData = renderHairballNeighbours(cr, mpcdc);
#endif

#ifdef TIME_MEASUREMENT
	if (!rendererUsedStoredData) { 
		counter = 0;
		std::chrono::time_point<std::chrono::system_clock> currentEnd = std::chrono::system_clock::now();
		int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(currentEnd-currentStart).count();
		std::cout << "ellapsed time: " << currentElapsedMilliseconds << std::endl;

		std::string fileName = "HairballRenderer.csv";
		std::ofstream file(fileName, std::fstream::out | std::fstream::app);

		if (!file.good()) {
			return false;
		}

		file << "time in milliseconds;number of clusters;usedStoredData\n";
		file << currentElapsedMilliseconds << ";" << mpcdc->AccessParticles(0).GetNumberClusters() << ";false" << "\n";
	} else {
		++counter;
	}
#endif

	glPopMatrix();
    
	mpcdc->Unlock();

#ifdef TIME_MEASUREMENT
	if (counter >= 100) {
		std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
		int currentElapsedMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
		std::cout << "ellapsed time: " << currentElapsedMilliseconds << std::endl;

		std::string fileName = "HairballRenderer.csv";
		std::ofstream file(fileName, std::fstream::out | std::fstream::app);

		if (!file.good()) {
			return false;
		}

		//file << "time in milliseconds;number of clusters\n";
		file << currentElapsedMilliseconds / counter << ";" << mpcdc->AccessParticles(0).GetNumberClusters() << ";true" << "\n";
		counter = 0;
	}
#endif
    return true;
}

bool visualization::AbstractHairBallRenderer::dataChanged(MultiParticleClusterDataCall* mpcdc) {
	bool dirty = false;
	
	if ((this->lastFrame != mpcdc->FrameID()) || (this->lastHash != mpcdc->DataHash()) || (mpcdc->DataHash() == 0)) {
		this->lastFrame = mpcdc->FrameID();
		this->lastHash = mpcdc->DataHash();
		dirty = true;
	}

	if (this->connectAllComponentsSlot.IsDirty()) {
		this->connectAllComponentsSlot.ResetDirty();
		dirty = true;
	}

	if (this->numberNeighboursSlot.IsDirty()) {
		this->numberNeighboursSlot.ResetDirty();
		dirty = true;
	}
	
	return dirty;
}

visualization::AbstractHairBallRenderer::ClusterComponentEdge visualization::AbstractHairBallRenderer::findNearestNeighboursBetweenComponents(std::vector<std::vector<ANNpoint> >& componentDataPts, std::vector<std::unique_ptr<ANNkd_tree> >& componentKdTree, unsigned int component1Id, unsigned int component2Id) {
	unsigned int numberComponents = componentDataPts.size();
	
	unsigned int k = numberComponents;
	ANNidxArray indices = new ANNidx[k];
	ANNdistArray distances = new ANNdist[k];

	vislib::math::Vector<float,3> startVertex;
	vislib::math::Vector<float,3> endVertex;

	// search for nearest point to component2 in component1
	componentKdTree[component1Id]->annkPriSearch(componentDataPts[component1Id][component2Id], k, indices, distances);
	for (unsigned int index = 1; index < k; ++index) {
		ANNidx idx = indices[index];
		if (idx >= numberComponents || idx == component1Id) {
			startVertex = vislib::math::Vector<float,3>(static_cast<float>(componentDataPts[component1Id][idx][0]),
													static_cast<float>(componentDataPts[component1Id][idx][1]),
													static_cast<float>(componentDataPts[component1Id][idx][2]));
			break;
		}
	}

	// search for nearest point to component1 in component2
	componentKdTree[component2Id]->annkPriSearch(componentDataPts[component2Id][component1Id], k, indices, distances);
	for (unsigned int index = 1; index < k; ++index) {
		ANNidx idx = indices[index];
		if (idx >= numberComponents || idx == component2Id) {
			endVertex = vislib::math::Vector<float,3>(static_cast<float>(componentDataPts[component2Id][idx][0]),
													static_cast<float>(componentDataPts[component2Id][idx][1]),
													static_cast<float>(componentDataPts[component2Id][idx][2]));
			break;
		}
	}

	delete[] indices;
	delete[] distances;

	return ClusterComponentEdge(component1Id, component2Id, startVertex, endVertex);
}

// too slow - not used
/*
visualization::ClusterComponentEdge visualization::AbstractHairBallRenderer::findNearestNeighboursBetweenComponents(const std::vector<std::vector<ClusterIndex> >& component, std::vector<ClusterIndex>& representatives, unsigned int component1Id, unsigned int component2Id, std::vector<ANNpoint>& dataPts, std::unique_ptr<ANNkd_tree>& kdTree) {
	ClusterIndex r1 = representatives[component1Id];
	ClusterIndex r2 = representatives[component2Id];

	vislib::math::Vector<float,3> v1(static_cast<float>(dataPts[r1][0]),
									static_cast<float>(dataPts[r1][1]),
									static_cast<float>(dataPts[r1][2]));
	vislib::math::Vector<float,3> v2(static_cast<float>(dataPts[r2][0]),
									static_cast<float>(dataPts[r2][1]),
									static_cast<float>(dataPts[r2][2]));

	float distance = (v1 - v2).SquareLength();
	int k1 = 0;
	k1 = kdTree->annkFRSearch(dataPts[r2], distance, k1); // get number k og neighbour in radius
	ANNidxArray indices1 = new ANNidx[k1];
	ANNdistArray distances1 = new ANNdist[k1];
	kdTree->annkFRSearch(dataPts[r2], distance, k1, indices1, distances1);

	unsigned int r1Strich;
	vislib::math::Vector<float,3> v1Strich;

	for(int j=1; j < k1; ++j) {
		ANNidx idx = indices1[j];
		if (std::binary_search(component[component1Id].begin(), component[component1Id].end(), idx)) {
			r1Strich = idx;
			v1Strich = vislib::math::Vector<float,3>(static_cast<float>(dataPts[r1Strich][0]),
													static_cast<float>(dataPts[r1Strich][1]),
													static_cast<float>(dataPts[r1Strich][2]));
			break;
		}
	}

	delete[] indices1;
	delete[] distances1;

	// TODO: extra Function
	distance = (v1Strich - v2).SquareLength();
	int k2 = 0;
	k2 = kdTree->annkFRSearch(dataPts[r1Strich], distance, k2); // get number k og neighbour in radius
	ANNidxArray indices2 = new ANNidx[k2];
	ANNdistArray distances2 = new ANNdist[k2];
	kdTree->annkFRSearch(dataPts[r1Strich], distance, k2, indices2, distances2);

	unsigned int r2Strich;
	vislib::math::Vector<float,3> v2Strich;

	for(int j=1; j < k2; ++j) {
		ANNidx idx = indices2[j];
		if (std::binary_search(component[component2Id].begin(), component[component2Id].end(), idx)) {
			r2Strich = idx;
			v2Strich = vislib::math::Vector<float,3>(static_cast<float>(dataPts[r2Strich][0]),
													static_cast<float>(dataPts[r2Strich][1]),
													static_cast<float>(dataPts[r2Strich][2]));
			break;
		}
	}

	delete[] indices2;
	delete[] distances2;

	return ClusterComponentEdge(component1Id, component2Id, v1Strich, v2Strich);
}*/