/*
 * FeatureTransferFunctionRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "FeatureTransferFunctionRenderer.h"
#include "CallVisParams.h"
#include "RenderHelper.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/view/CallRender3D.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/StringParam.h"
#include "vislib/assert.h"
#include "vislib/math/Vector.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/graphics/gl/SimpleFont.h"
#include "mmcore/utility/ColourParser.h"
#include <GL/glu.h>
#include <memory>
#include <vector>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::FeatureTransferFunctionRenderer::FeatureTransferFunctionRenderer
 */
visualization::FeatureTransferFunctionRenderer::FeatureTransferFunctionRenderer(void) : Renderer3DModule(),
        getFeatureParamsSlot("getfeatureparams", "Connects to the feature params"),
        tfSlot("gettransferfunction", "Connects to the transfer function module"),
		fontColourSlot("fontcolour", "The colour of the font"),
		greyTF(0) {
    this->getFeatureParamsSlot.SetCompatibleCall<CallFeatureParamsDescription>();
    this->MakeSlotAvailable(&this->getFeatureParamsSlot);

	this->tfSlot.SetCompatibleCall<core::view::CallGetTransferFunctionDescription>();
    this->MakeSlotAvailable(&this->tfSlot);

	this->fontColourSlot.SetParameter(new core::param::StringParam("black"));
	this->MakeSlotAvailable(&this->fontColourSlot);
}

/*
 * visualization::FeatureTransferFunctionRenderer::~FeatureTransferFunctionRenderer
 */
visualization::FeatureTransferFunctionRenderer::~FeatureTransferFunctionRenderer(void) {
	this->Release();
}

/*
 * visualization::FeatureTransferFunctionRenderer::GetExtents
 */
bool visualization::FeatureTransferFunctionRenderer::GetExtents(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    cr->SetTimeFramesCount(1);
    cr->AccessBoundingBoxes().Clear();

    return true;
}

/*
 * visualization::FeatureTransferFunctionRenderer::GetCapabilities
 */
bool visualization::FeatureTransferFunctionRenderer::GetCapabilities(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    cr->SetCapabilities(
        core::view::CallRender3D::CAP_RENDER
        | core::view::CallRender3D::CAP_LIGHTING
        | core::view::CallRender3D::CAP_ANIMATION
        );

    return true;
}

/*
 * visualization::FeatureTransferFunctionRenderer::create
 */
bool visualization::FeatureTransferFunctionRenderer::create(void) {
	ASSERT(IsAvailable());
	CreateShader(this->instance(), this->transferFunctionShader, "transferfunction");

    glEnable(GL_TEXTURE_1D);
    glGenTextures(1, &this->greyTF);
    unsigned char tex[6] = {
        0, 0, 0,  255, 255, 255
    };
    glBindTexture(GL_TEXTURE_1D, this->greyTF);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, tex);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glBindTexture(GL_TEXTURE_1D, 0);

    glDisable(GL_TEXTURE_1D);

    return true;
}


/*
 * visualization::FeatureTransferFunctionRenderer::release
 */
void visualization::FeatureTransferFunctionRenderer::release(void) {
	this->transferFunctionShader.Release();
    ::glDeleteTextures(1, &this->greyTF);
}

/*
 * visualization::FeatureTransferFunctionRenderer::Render
 */
bool visualization::FeatureTransferFunctionRenderer::Render(core::Call& call) {
    core::view::CallRender3D *cr = dynamic_cast<core::view::CallRender3D*>(&call);
    if (cr == NULL) return false;

    float scaling = 1.0f;
	
	FeatureParams params;
	CallFeatureParams *fp = this->getFeatureParamsSlot.CallAs<CallFeatureParams>();
	if (fp != nullptr) {
		if (!(*fp)(CallFeatureParams::CallForFeatureParams)) {
			return false;
		}
		params = fp->GetFeatureParams();
	}
	
	if (params.transferFunctionNeeded) { // show transferfunction and min-, max-values
		renderTransferfunction(params);
	} else { // show colored axis
		renderAxisBall();
	}

    return true;
}

void visualization::FeatureTransferFunctionRenderer::renderTransferfunction(FeatureParams params) {
	unsigned int numberColours = 13U;
	float ratio = 12.0f / static_cast<float>(numberColours - 1);
	std::vector<float> transferFuntionPositions(52);
	std::vector<float> colourIds(26);

	for (unsigned int colourId = 0; colourId < 13; ++colourId) {
		colourIds[2 * colourId] = colourId;
		transferFuntionPositions[4 * colourId] = colourId;
		transferFuntionPositions[4 * colourId + 1] = 1;

		colourIds[2 * colourId + 1] = colourId;
		transferFuntionPositions[4 * colourId + 2] = colourId;
		transferFuntionPositions[4 * colourId + 3] = 0;
	}

	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	float viewportStuff[4];
	::glGetFloatv(GL_VIEWPORT, viewportStuff);
	if (viewportStuff[2] < 1.0f) viewportStuff[2] = 1.0f;
	if (viewportStuff[3] < 1.0f) viewportStuff[3] = 1.0f;

	// positioning lower left corner
	glPushMatrix();
	glLoadIdentity();
	glScalef(20.0f, 20.0f, 1.0f);
	glTranslatef(1.0f, 1.0f, 0.0f);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, viewportStuff[2], 0.0, viewportStuff[3], -1.0, 1.0);

	viewportStuff[2] = 2.0f / viewportStuff[2];
	viewportStuff[3] = 2.0f / viewportStuff[3];

	// Get GL_MODELVIEW matrix
	GLfloat modelMatrix_column[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, modelMatrix_column);
	vislib::math::ShallowMatrix<GLfloat, 4, vislib::math::COLUMN_MAJOR> modelMatrix(&modelMatrix_column[0]);

	// Get GL_PROJECTION matrix
	GLfloat projMatrix_column[16];
	glGetFloatv(GL_PROJECTION_MATRIX, projMatrix_column);
	vislib::math::ShallowMatrix<GLfloat, 4, vislib::math::COLUMN_MAJOR> projMatrix(&projMatrix_column[0]);

	// Compute modelviewprojection matrix
	vislib::math::Matrix<GLfloat, 4, vislib::math::ROW_MAJOR> modelProjMatrix = projMatrix * modelMatrix;

	this->transferFunctionShader.Enable();

	glUniformMatrix4fv(this->transferFunctionShader.ParameterLocation("modelview"), 1, false, modelMatrix_column);
	glUniformMatrix4fv(this->transferFunctionShader.ParameterLocation("proj"), 1, false, projMatrix_column);

	GLint vertexPos = glGetAttribLocation(this->transferFunctionShader, "vertex");
	unsigned int cial = glGetAttribLocationARB(this->transferFunctionShader, "colIdx");

	float minC = 0.0f, maxC = 0.0f;
	unsigned int colTabSize = 0;

	// cluster -> colour
	glEnableVertexAttribArrayARB(cial);

	glEnable(GL_TEXTURE_1D);

	core::view::CallGetTransferFunction *cgtf = this->tfSlot.CallAs<core::view::CallGetTransferFunction>();
	if ((cgtf != NULL) && ((*cgtf)())) {
		glBindTexture(GL_TEXTURE_1D, cgtf->OpenGLTexture());
		colTabSize = cgtf->TextureSize();
	} else {
		glBindTexture(GL_TEXTURE_1D, this->greyTF);
		colTabSize = 2;
	}

	glUniform1i(this->transferFunctionShader.ParameterLocation("colTab"), 0);
	minC = 0.0f;
	maxC = 1.0f;
	glColor3ub(127, 127, 127);

	glUniform3f(this->transferFunctionShader.ParameterLocation("inConsts1"), minC, maxC, float(colTabSize));

	glEnableVertexAttribArray(vertexPos);

	glUniform1f(this->transferFunctionShader.ParameterLocation("cycleTransferFunction"), 0.0f);
	glUniform1f(this->transferFunctionShader.ParameterLocation("numberCluster"), 13.0f);

	glVertexAttribPointerARB(cial, 1, GL_FLOAT, GL_FALSE, 0, colourIds.data());
	glVertexAttribPointer(vertexPos, 2, GL_FLOAT, GL_FALSE, 0, transferFuntionPositions.data());

	glDrawArrays(GL_QUAD_STRIP, 0, static_cast<GLsizei>(colourIds.size()));

	glDisableVertexAttribArray(vertexPos);
	glDisableVertexAttribArrayARB(cial);
	glDisable(GL_TEXTURE_1D);
    
	this->transferFunctionShader.Disable();

	// render labeling for transferfunction
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	float r, g, b;
	core::utility::ColourParser::FromString(this->fontColourSlot.Param<core::param::StringParam>()->Value(), r, g, b);

	glColor3f(r, g, b);

	vislib::graphics::gl::SimpleFont f;
	float fontsize = 0.75f;
	float padding = 0.3f;

	if (f.Initialise()) {
		vislib::StringA tmpStr;
		tmpStr.Format("%f", params.minValue);
		tmpStr.Prepend("min: ");
		f.DrawString(0.0, 2.5f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_LEFT_MIDDLE);
	
		tmpStr.Clear();
		tmpStr.Format("%f", params.maxValue);
		tmpStr.Prepend("max: ");
		f.DrawString(0.0f, 1.5f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_LEFT_MIDDLE);
	
		fontsize = 0.45f;

		tmpStr.Clear();
		tmpStr.Append("max");
		f.DrawString(static_cast<float>(numberColours - 1) * ratio, -padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);

		// render label for min-value
		if (params.centerTransferZero && params.minValue >= 0.0f) {
			tmpStr.Clear();
			tmpStr.Append("min");
			float pos = static_cast<float>(numberColours - 1) / 2.0f;
			f.DrawString(pos, -padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);
		} else {
			if (params.centerTransferZero) {
				tmpStr.Clear();
				tmpStr.Format("%i", 0);
				f.DrawString(static_cast<float>(numberColours - 1) / 2.0f * ratio, -padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);
			}
			tmpStr.Clear();
			tmpStr.Append("min");
			float pos = static_cast<float>(numberColours - 1) / 2.0f + 0.5 * static_cast<float>(numberColours - 1) * ratio * (params.minValue / params.maxValue);
			f.DrawString(0.0f, -padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);
		}
	}

	// marker for min/max colour
	glLineWidth(1.0f);
	float markerLength = 0.2f;
	glBegin(GL_LINES);
		glVertex2f(static_cast<float>(numberColours - 1) * ratio, -markerLength);
		glVertex2f(static_cast<float>(numberColours - 1) * ratio, 1.0f);

		if (params.centerTransferZero && params.minValue >= 0.0f) {
			float pos = static_cast<float>(numberColours - 1) / 2.0f;
			glVertex2f(pos, -markerLength);
			glVertex2f(pos, 1.0f);
		} else {
			if (params.centerTransferZero) {
				glVertex2f(static_cast<float>(numberColours - 1) / 2.0f * ratio, -markerLength);
				glVertex2f(static_cast<float>(numberColours - 1) / 2.0f * ratio, 1.0f);
			}

			glVertex2f(0.0f, -markerLength);
			glVertex2f(0.0f, 1.0f);
		}
	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable(GL_DEPTH_TEST);
}
		
void visualization::FeatureTransferFunctionRenderer::renderAxisBall() {
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);

	// Get GL_MODELVIEW matrix
	GLdouble modelMatrix_column[16];
	glGetDoublev(GL_MODELVIEW_MATRIX, modelMatrix_column);

	// Get GL_PROJECTION matrix
	GLdouble projMatrix_column[16];
	glGetDoublev(GL_PROJECTION_MATRIX, projMatrix_column);

	int vp[4];
	float viewportStuff[4];
	::glGetFloatv(GL_VIEWPORT, viewportStuff);
	if (viewportStuff[2] < 1.0f) viewportStuff[2] = 1.0f;
	if (viewportStuff[3] < 1.0f) viewportStuff[3] = 1.0f;

	// positioning of axis-ball (lower left corner)
	for (unsigned int i = 0; i < 4; i++) {
		vp[i] = static_cast<int>(viewportStuff[i]);
	}

	double wx, wy, wz, sx1, sy1, sz1, sx2, sy2, sz2;
	double size = vislib::math::Min(static_cast<double>(viewportStuff[2]), static_cast<double>(viewportStuff[3])) * 0.1f;
	wx = size;
	wy = size;
	wz = 0.5;
	gluUnProject(wx, wy, wz, modelMatrix_column, projMatrix_column, vp, &sx1, &sy1, &sz1);
	size *= 0.5;
	wx = size;
	wy = size;
	wz = 0.5;
	::gluUnProject(wx, wy, wz, modelMatrix_column, projMatrix_column, vp, &sx2, &sy2, &sz2);
	sx2 -= sx1;
	sy2 -= sy1;
	sz2 -= sz1;
	size = vislib::math::Sqrt(sx2 * sx2 + sy2 * sy2 + sz2 * sz2);

	glTranslated(sx1, sy1, sz1);
	glScaled(size, size, size);

	glLineWidth(1.5f);
	float padding = 0.5f;

	// actual drawing of axis-ball
	glBegin(GL_LINES);
		for (float x = -1.0f; x <= 1.0f; x += 1.0f) {
			for (float y = -1.0f; y <= 1.0f; y += 1.0f) {
				for (float z = -1.0f; z <= 1.0f; z += 1.0f) {
					vislib::math::Vector<float,3> currentAxis(x, y, z);
					if (currentAxis.Normalise() == 0.0f) {
						continue;
					}

					vislib::math::Vector<float,3> colour;
					getBaryCentricInterpolatedColour(currentAxis.GetX(), currentAxis.GetY(), currentAxis.GetZ(), colour);
					
					vislib::math::Vector<float,3> startPoint = currentAxis;
					for (int c = 0; c < 3; ++c) {
						startPoint.PeekComponents()[c] -= (1.0f - padding) * startPoint.PeekComponents()[c]; 
					}

					glColor3f(colour.GetX(), colour.GetY(), colour.GetZ());
					glVertex3fv(startPoint.PeekComponents());
					glVertex3fv(currentAxis.PeekComponents());
				}
			}
		}			
	glEnd();

	// central point
	glColor3f(0.0f, 0.0f, 0.0f);
	glPointSize(5.0f);
	glBegin(GL_POINTS);
		glVertex3f(0.0f, 0.0f, 0.0f);
	glEnd();

	glEnable(GL_DEPTH_TEST);
}