/*
 * CallVisParams.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "CallVisParams.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

const unsigned int visualization::CallVisParams::CallForGetVisParams = 0;
const unsigned int visualization::CallVisParams::CallForSetVisParams = 1;

visualization::CallVisParams::CallVisParams(void) {
}


visualization::CallVisParams::~CallVisParams(void) {
}
