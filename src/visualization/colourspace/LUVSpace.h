/*
 * LUVSpace.h
 *
 * Author: Julia B�hnke
 */

#ifndef LUVSPACE_H_INCLUDED
#define LUVSPACE_H_INCLUDED

#pragma once

#include "XYZSpace.h"

namespace colourspace {

	/**
	  * Class for colour description in CIE-LUV space
	  */
	class LUVSpace	{
	public:
		/** Ctor. */
		LUVSpace(float L = 0.0f, float U = 0.0f, float V = 0.0f);

		/** Dtor. */
		~LUVSpace(void);

		/** 
		  * Convert colour to XYZ space 
		  *
		  * @return The colour in XYZ space
		  */
		XYZSpace toXYZ();

		/** 
		  * Convert colour to RGB space 
		  *
		  * @return The colour in RGB space
		  */
		RGBSpace toRGB();

		inline const float GetL() const {
			return this->L;
		}
		inline const float GetU() const {
			return this->U;
		}
		inline const float GetV() const {
			return this->V;
		}

		inline void SetL(const float L) {
			this->L = L;
		}
		inline void SetU(const float U) {
			this->U = U;
		}
		inline void SetV(const float V) {
			this->V = V;
		}

	private:
		/** Value of L component (luminance) */
		float L;

		/** Value of U component */
		float U;

		/** Value of V component */
		float V;
	};
} /* end namespace colourspace */

#endif /* LUVSPACE_H_INCLUDED */