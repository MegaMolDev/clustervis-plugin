/*
 * RGBSpace.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "RGBSpace.h"
#include <algorithm>

using namespace colourspace;

#ifdef _MSC_VER
#pragma push_macro("max")
#undef max
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

RGBSpace::RGBSpace(float r, float g, float b) {
	r = std::max(0.0f, std::min(r, 1.0f));
	g = std::max(0.0f, std::min(g, 1.0f));
	b = std::max(0.0f, std::min(b, 1.0f));

	this->R = static_cast<unsigned char>(255.0f * r);
	this->G = static_cast<unsigned char>(255.0f * g);
	this->B = static_cast<unsigned char>(255.0f * b);
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#pragma pop_macro("max")
#endif /* _MSC_VER */

RGBSpace::~RGBSpace(void) {
}