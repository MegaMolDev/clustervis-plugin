/*
 * LABSpace.cpp
 *
 * Author: Julia B�hnke
 */
#include "stdafx.h"
#include "LABSpace.h"
#include <cmath>

using namespace colourspace;

LABSpace::LABSpace(float L, float a, float b) : L(L), a(a), b(b) {
}

LABSpace::~LABSpace(void) {
}

XYZSpace LABSpace::toXYZ() {
	float y = (this->L + 16.0) / 116.0;
	float x = this->a / 500.0 + y;
	float z = y - this->b / 200.0;

	XYZSpace white;
	float x3 = x * x * x;
	float z3 = z * z * z;

	float X = white.GetX() * (x3 > 0.008856f ? x3 : (x - 16.0 / 116.0) / 7.787);
	float Y = white.GetY() * (this->L > (903.3f * 0.008856f) ? std::pow(((this->L + 16.0) / 116.0), 3) : this->L / 903.3f);
	float Z = white.GetZ() * (z3 > 0.008856f ? z3 : (z - 16.0 / 116.0) / 7.787);

	return XYZSpace(X, Y, Z);
}

RGBSpace LABSpace::toRGB() {
	XYZSpace tmp = this->toXYZ();

	return tmp.toRGB();
}