/*
 * LABSpace.h
 *
 * Author: Julia B�hnke
 */

#ifndef LABSPACE_H_INCLUDED
#define LABSPACE_H_INCLUDED

#pragma once

#include "XYZSpace.h"

namespace colourspace {

	/**
	  * Class for colour description in CIE-Lab space
	  */
	class LABSpace	{
	public:
		/** Ctor. */
		LABSpace(float L = 0.0f, float a = 0.0f, float b = 0.0f);

		/** Dtor. */
		~LABSpace(void);

		/** 
		  * Convert colour to XYZ space 
		  *
		  * @return The colour in XYZ space
		  */
		XYZSpace toXYZ();

		/** 
		  * Convert colour to RGB space 
		  *
		  * @return The colour in RGB space
		  */
		RGBSpace toRGB();

		inline const float GetL() const {
			return this->L;
		}
		inline const float GetA() const {
			return this->a;
		}
		inline const float GetB() const {
			return this->b;
		}

		inline void SetL(const float L) {
			this->L = L;
		}
		inline void SetA(const float a) {
			this->a = a;
		}
		inline void SetB(const float b) {
			this->b = b;
		}

	private:
		/** Value of L component (luminance) */
		float L;

		/** Value of a component */
		float a;

		/** Value of b component */
		float b;
	};

} /* end namespace colourspace */

#endif /* LABSPACE_H_INCLUDED */