/*
 * XYZSpace.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "XYZSpace.h"
#include <cmath>

using namespace colourspace;

XYZSpace::XYZSpace(float X, float Y, float Z) : X(X), Y(Y), Z(Z) {
}

XYZSpace::~XYZSpace(void) {
}

RGBSpace XYZSpace::toRGB() {
		float x = X / 100.0f;
		float y = Y / 100.0f;
		float z = Z / 100.0f;

		float r = x * 3.2406f + y * -1.5372f + z * -0.4986f;
		float g = x * -0.9689f + y * 1.8758f + z * 0.0415f;
		float b = x * 0.0557f + y * -0.2040f + z * 1.0570f;

		r = r > 0.0031308 ? 1.055 * std::pow(r, 1 / 2.4) - 0.055 : 12.92 * r;
		g = g > 0.0031308 ? 1.055 * std::pow(g, 1 / 2.4) - 0.055 : 12.92 * g;
		b = b > 0.0031308 ? 1.055 * std::pow(b, 1 / 2.4) - 0.055 : 12.92 * b;

		return RGBSpace(r, g, b);
	}