/*
 * XYZSpace.h
 *
 * Author: Julia B�hnke
 */

#ifndef XYZSPACE_H_INCLUDED
#define XYZSPACE_H_INCLUDED

#pragma once

#include "RGBSpace.h"

namespace colourspace {

	/**
	  * Class for colour description in XYZ space
	  */
	class XYZSpace	{
	public:
		/** Ctor. */
		XYZSpace(float X = 95.047f, float Y = 100.0f, float Z = 108.883f); // default: white
		
		/** Dtor. */
		~XYZSpace(void);

		/** 
		  * Convert colour to RGB space 
		  *
		  * @return The colour in RGB space
		  */
		RGBSpace toRGB();

		inline const float GetX() const {
			return this->X;
		}
		inline const float GetY() const {
			return this->Y;
		}
		inline const float GetZ() const {
			return this->Z;
		}

		inline void SetX(const float X) {
			this->X = X;
		}
		inline void SetY(const float Y) {
			this->Y = Y;
		}
		inline void SetZ(const float Z) {
			this->Z = Z;
		}

	private:
		/** Value of X component */
		float X;

		/** Value of Y component */
		float Y;

		/** Value of Z component */
		float Z;
	};
} /* end namespace colourspace */

#endif /*XYZSPACE_H_INCLUDED */