/*
 * LUVSpace.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "LUVSpace.h"

using namespace colourspace;

LUVSpace::LUVSpace(float L, float U, float V) : L(L), U(U), V(V) {
}

LUVSpace::~LUVSpace(void) {
}

XYZSpace LUVSpace::toXYZ() {
	XYZSpace white;
	const double c = -1.0 / 3.0;
	float uPrime = (4.0 * white.GetX()) / (white.GetX() + 15.0 * white.GetY() + 3.0 * white.GetZ());
	float vPrime = (9.0 * white.GetY()) / (white.GetX() + 15.0 * white.GetY() + 3.0 * white.GetZ());
		 
	float a = (1.0 / 3.0) * ((52.0 * L) / (U + 13 * L * uPrime) - 1.0);
	float imteL_16_116 = (L + 16.0) / 116.0;
	float y = L > 903.3f * 0.008856f
	? imteL_16_116 * imteL_16_116 * imteL_16_116
	: L / 903.3f;
	float b1 = -5.0 * y;
	float d = y * ((39.0 * L) / (V + 13.0 * L * vPrime) - 5.0);
	float x = (d - b1) / (a - c);
	float z = x * a + b1;

	return XYZSpace(x * 100.0f, y * 100.0f, z * 100.0f);
}

RGBSpace LUVSpace::toRGB() {
	XYZSpace tmp = this->toXYZ();

	return tmp.toRGB();
}