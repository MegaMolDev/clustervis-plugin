/*
 * RGBSpace.h
 *
 * Author: Julia B�hnke
 */

#ifndef RGBSPACE_H_INCLUDED
#define RGBSPACE_H_INCLUDED

#pragma once

#include <ostream>
#include <iomanip>

namespace colourspace {

	/**
	  * Class for colour description in RGB space
	  */
	class RGBSpace	{
	public:
		/** 
		  * Ctor. 
		  *
		  * @param r value of red component (0.0...1.0)
		  * @param g value of green component (0.0...1.0)
		  * @param b value of blue component (0.0...1.0)
		  */
		RGBSpace(float r = 1.0f, float g = 1.0f, float b = 1.0f);

		/** Dtor. */
		~RGBSpace(void);

		inline const unsigned char GetR() const {
			return this->R;
		}
		inline const unsigned char GetG() const {
			return this->G;
		}
		inline const unsigned char GetB() const {
			return this->B;
		}

	private:
		/** Value of R component (0...255) - red */
		unsigned char R;

		/** Value of G component (0...255) - green */
		unsigned char G;

		/** Value of B component (0...255) - blue */
		unsigned char B;
	};

	/** Overload stream operator to correctly write rgb colour code */
	inline std::ostream& operator<<(std::ostream& lhs, RGBSpace const& rhs) {
		int R = rhs.GetR();
		int G = rhs.GetG();
		int B = rhs.GetB();

		lhs << std::internal // fill between the prefix and the number
			<< std::setfill('0'); // fill with 0s

		lhs << "#" << std::hex << std::setw(2) << R << std::setw(2) << G << std::setw(2) << B;
		return lhs;
	}
} /* end namespace colourspace */

#endif /* RGBSPACE_H_INCLUDED */