/*
 * CallFeatureParams.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_CALLFEATUREPARAMS_H_INCLUDED
#define MEGAMOLVPC_CALLFEATUREPARAMS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Call.h"
#include "mmcore/factories/CallAutoDescription.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/** Parameter for visualization of a feature */
	struct FeatureParams {
		/** Render tarnsferfunction or "axis-ball" */
		bool transferFunctionNeeded;

		/** center value 0.0 at 0.5 of transferfunction */
		bool centerTransferZero;

		/** Minimum value of feature */
		float minValue;

		/** Maximum value of feature */
		float maxValue;

		/** Ctor */
		FeatureParams(bool transferFunctionNeeded = true, bool centerTransferZero = true, float minValue = 0.0f, float maxValue = 1.0f) : 
			transferFunctionNeeded(transferFunctionNeeded), // rendering of transferfunction only needed for position etc (not rotationaxis etc.)
			centerTransferZero(centerTransferZero),
			minValue(minValue),
			maxValue(maxValue) {
		}
	};

	class CallFeatureParams : public core::Call {
	public:
		/// Index of the 'GetFeatureParams' function
		static const unsigned int CallForFeatureParams;

		/**
		 * Answer the name of the objects of this description.
		 *
		 * @return The name of the objects of this description.
		 */
		static const char *ClassName(void) {
			return "CallFeatureParams";
		}

		/**
		 * Gets a human readable description of the module.
		 *
		 * @return A human readable description of the module.
		 */
		static const char *Description(void) {
			return "Call to transmit feature parameters";
		}

		/**
		 * Get the parameters of a feature (for rendering of transferfunction)
		 *
		 * @return The camera parameters pointer.
		 */
		inline const FeatureParams GetFeatureParams() {
			return this->featureParams;
		}

		/**
		 * Set the parameters of a feature (for rendering of transferfunction)
		 *
		 * @param featureParams The new value for the feature parameters
		 */
		inline void SetFeatureParams(FeatureParams featureParams) {
			this->featureParams = featureParams;
		}
		
		/**
		 * Answer the number of functions used for this call.
		 *
		 * @return The number of functions used for this call.
		 */
		static unsigned int FunctionCount(void) {
			return 1;
		}

		/**
		 * Answer the name of the function used for this call.
		 *
		 * @param idx The index of the function to return it's name.
		 *
		 * @return The name of the requested function.
		 */
		static const char * FunctionName(unsigned int idx) {
			switch(idx) {
			case 0:
				return "featureParams";
			}
			return "";
		}

		/** Ctor. */
		CallFeatureParams(void);

		/** Dtor. */
		virtual ~CallFeatureParams(void);

	private:
		/** The parameter of the feature */
		FeatureParams featureParams;
	};

/// Description class typedef
typedef core::factories::CallAutoDescription<CallFeatureParams> CallFeatureParamsDescription;

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif // MEGAMOLVPC_CALLFEATUREPARAMS_H_INCLUDED