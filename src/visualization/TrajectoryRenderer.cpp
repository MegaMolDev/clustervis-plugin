/*
 * TrajectoryRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "TrajectoryRenderer.h"
#include "vislib/math/ShallowMatrix.h"
#include "vislib/graphics/gl/SimpleFont.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/BoolParam.h"
#include "helper.h"
#include "RenderHelper.h"
#include "CallVisParams.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

visualization::TrajectoryRenderer::TrajectoryRenderer(void) : 
		getDataSlot("getdata", "Connects to the data source"),
		getClusterDataSlot("getclusterdata", "Connects to the cluster data source"),
		getTFSlot("gettransferfunction", "Connects to the transfer function module"),
		sharedVisParamsSlot("sharedvisparams", "Connects to the shared visualization parameters"),
		featureSlot("feature", "The feature used"),
		lineWidthSlot("linewidth", "The width of trajectory-lines"),
		opacitySlot("opacity", "The opacity of trajectory-lines"),
		drawSlot("draw", "Draw only mean, only trajectories or both"),
		scaleSlot("scale text", "Scale factor for text"),
		percentTrajectoriesSlot("percenttrajectories", "Percent of trajectories to draw"),
		greyTF(0), scaling(1.0f), minY(0.0f), maxY(0.0f) {
	this->getDataSlot.SetCompatibleCall<MultiParticleDataCallExtendedDescription>();
	this->MakeSlotAvailable(&this->getDataSlot);
	
	this->getClusterDataSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
	this->MakeSlotAvailable(&this->getClusterDataSlot);

	this->getTFSlot.SetCompatibleCall<core::view::CallGetTransferFunctionDescription>();
    this->MakeSlotAvailable(&this->getTFSlot);

	this->sharedVisParamsSlot.SetCompatibleCall<CallVisParamsDescription>();
    this->MakeSlotAvailable(&this->sharedVisParamsSlot);

	core::param::EnumParam* usedFeature = new core::param::EnumParam(VELOCITYX);
	usedFeature->SetTypePair(POSITIONX, "Position x");
	usedFeature->SetTypePair(POSITIONY, "Position y");
	usedFeature->SetTypePair(POSITIONZ, "Position z");
	usedFeature->SetTypePair(VELOCITYX, "Velocity x");
	usedFeature->SetTypePair(VELOCITYY, "Velocity y");
	usedFeature->SetTypePair(VELOCITYZ, "Velocity z");
	usedFeature->SetTypePair(ROTATIONANGLE, "Rotation angle");
	this->featureSlot.SetParameter(usedFeature);
	this->MakeSlotAvailable(&this->featureSlot);

	core::param::EnumParam* drawing = new core::param::EnumParam(BOTH);
	drawing->SetTypePair(BOTH, "Means and trajectories");
	drawing->SetTypePair(MEAN, "Only means");
	drawing->SetTypePair(TRAJECTORY, "Only trajectories");
	this->drawSlot.SetParameter(drawing);
	this->MakeSlotAvailable(&this->drawSlot);

	this->lineWidthSlot.SetParameter(new core::param::FloatParam(1.0f, 0.1f));
	this->MakeSlotAvailable(&this->lineWidthSlot);

	this->scaleSlot.SetParameter(new core::param::FloatParam(1.0f, 0.01f));
	this->MakeSlotAvailable(&this->scaleSlot);

	this->opacitySlot.SetParameter(new core::param::FloatParam(0.1f, 0.0f, 1.0f));
	this->MakeSlotAvailable(&this->opacitySlot);

	this->percentTrajectoriesSlot.SetParameter(new core::param::FloatParam(0.1f, 0.0f, 1.0f));
	this->MakeSlotAvailable(&this->percentTrajectoriesSlot);
}


visualization::TrajectoryRenderer::~TrajectoryRenderer(void) {
	this->Release();
}

bool visualization::TrajectoryRenderer::create() {
	ASSERT(IsAvailable());
	
	CreateShader(this->instance(), this->trajectoryShader, "trajectory");

    glEnable(GL_TEXTURE_1D);
    glGenTextures(1, &this->greyTF);
    unsigned char tex[6] = {
        0, 0, 0,  255, 255, 255
    };
    glBindTexture(GL_TEXTURE_1D, this->greyTF);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, 2, 0, GL_RGB, GL_UNSIGNED_BYTE, tex);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glBindTexture(GL_TEXTURE_1D, 0);

    glDisable(GL_TEXTURE_1D);

    return true;
}

void visualization::TrajectoryRenderer::release() {
    this->trajectoryShader.Release();
    ::glDeleteTextures(1, &this->greyTF);
}

bool visualization::TrajectoryRenderer::GetExtents(core::view::CallRender2D& call) {
    // set the bounding box to 0..1

	MultiParticleDataCallExtended *mpdcx = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
	if (mpdcx == nullptr) {
		return false;
	}
	mpdcx->SetFrameID(call.Time());
    if (!(*mpdcx)(1)) return false;
	if (!(*mpdcx)(0)) return false;

	vislib::math::Cuboid<float> bbox_temp = mpdcx->AccessBoundingBoxes().ObjectSpaceBBox();
	vislib::math::Point<float,3> minPos = bbox_temp.GetLeftBottomBack();
	vislib::math::Point<float,3> maxPos = bbox_temp.GetRightTopFront();
	vislib::math::Point<float,3> minVel(mpdcx->GetMinVelocity().data());
	vislib::math::Point<float,3> maxVel(mpdcx->GetMaxVelocity().data());
	float minAngle = mpdcx->GetMinRotationAngle();
	float maxAngle = mpdcx->GetMaxRotationAngle();

	MultiParticleClusterDataCall *mpcdc = this->getClusterDataSlot.CallAs<MultiParticleClusterDataCall>();
	mpcdc->SetFrameID(call.Time());

	if (mpcdc == nullptr || !(*mpcdc)(1) || !(*mpcdc)(0)) {
		return false;
	}

	int firstFrame = mpcdc->GetFirstFrame();
	int lastFrame = mpcdc->GetLastFrame();
	int frameStep = mpcdc->GetFrameStep();
	if (firstFrame > mpdcx->FrameCount() || lastFrame > mpdcx->FrameCount() ||
		firstFrame < 0 || lastFrame < 0 || 
		lastFrame < firstFrame || lastFrame - firstFrame < frameStep) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Trajectory invalid\n");
	}

	unsigned int numberTimesteps = (lastFrame - firstFrame) / frameStep + 1;	// floor - lastFrame may not be used

	switch(this->featureSlot.Param<core::param::EnumParam>()->Value()) {
		case POSITIONX:
			minY = minPos.GetX();
			maxY = maxPos.GetX();
			break;
		case POSITIONY:
			minY = minPos.GetY();
			maxY = maxPos.GetY();
			break;
		case POSITIONZ:
			minY = minPos.GetZ();
			maxY = maxPos.GetZ();
			break;
		case VELOCITYX:
			minY = minVel.GetX();
			maxY = maxVel.GetX();
			break;
		case VELOCITYY:
			minY = minVel.GetY();
			maxY = maxVel.GetY();
			break;
		case VELOCITYZ:
			minY = minVel.GetZ();
			maxY = maxVel.GetZ();
			break;
		case ROTATIONANGLE:
			minY = minAngle;
			maxY = maxAngle;
			break;
	}

	scaling = static_cast<float>(numberTimesteps) / (static_cast<float>(call.GetViewport().AspectRatio()) * (maxY - minY));
	call.SetBoundingBox(0.0f, minY*scaling, static_cast<float>(numberTimesteps), maxY*scaling);

    return true;
}

bool visualization::TrajectoryRenderer::MouseEvent(float x, float y, core::view::MouseFlags flags) {
    bool consumeEvent = false;

    return consumeEvent;
}


bool visualization::TrajectoryRenderer::buildTrajectoryVector(MultiParticleDataCallExtended *call, MultiParticleClusterDataCall *mpcdc, const unsigned int numberClusters, const uint8_t * clusterPtr, const unsigned int clusterStride) {
	vislib::math::Cuboid<float> bbox_temp = call->AccessBoundingBoxes().ObjectSpaceBBox();
	vislib::math::Point<float,3> minPos = bbox_temp.GetLeftBottomBack();
	vislib::math::Point<float,3> maxPos = bbox_temp.GetRightTopFront();
	vislib::math::Point<float,3> minVel(call->GetMinVelocity().data());
	vislib::math::Point<float,3> maxVel(call->GetMaxVelocity().data());
	float minAngle = call->GetMinRotationAngle();
	float maxAngle = call->GetMaxRotationAngle();
	float deltaAngle = maxAngle - minAngle;
	
	std::vector<unsigned int> trajectoryIdxInCluster; // tmp to count the trajectories per cluster

	int firstFrame = mpcdc->GetFirstFrame();
	int lastFrame = mpcdc->GetLastFrame();
	int frameStep = mpcdc->GetFrameStep();
	if (firstFrame > call->FrameCount() || lastFrame >= call->FrameCount() ||
		firstFrame < 0 || lastFrame < 0 || 
		lastFrame < firstFrame || lastFrame - firstFrame < frameStep) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Trajectory invalid\n");
	}

	unsigned int numberTimesteps = (lastFrame - firstFrame) / frameStep + 1;	// floor - lastFrame may not be used
	
	trajectoryPositions.clear();
	trajectoryPositions.resize(numberClusters);
	trajectoryVelocities.clear();
	trajectoryVelocities.resize(numberClusters);
	trajectoryRotationAngles.clear();
	trajectoryRotationAngles.resize(numberClusters);

	meanTrajectoryPositions.clear();
	meanTrajectoryPositions.resize(numberClusters, std::vector<float>(3 * numberTimesteps, 0.0f));
	meanTrajectoryVelocities.clear();
	meanTrajectoryVelocities.resize(numberClusters, std::vector<float>(3 * numberTimesteps, 0.0f));
	meanTrajectoryRotationAngles.clear();
	meanTrajectoryRotationAngles.resize(numberClusters, std::vector<float>(numberTimesteps, 0.0f));
	
	//unsigned int frameIdTmp = call->FrameID();

	std::cout << "Building trajectory vector .........." << std::flush;
	std::cout << "\rBuilding trajectory vector " << std::flush;
	
	unsigned int modul = (numberTimesteps - 1) / 10;
	if (modul == 0) {
		modul = 1;
	}

	unsigned int cnt = 0;
	for (unsigned int timestep = firstFrame; timestep <= lastFrame; timestep += frameStep, ++cnt) {
		if((cnt + 1) % modul == 0) {
			std::cout << "x" << std::flush;
		}
		// load data for timestep
		if (!getFrame(call, timestep)) {
			return false;
		}
		
		trajectoryIdxInCluster.clear();	// new timestep -> restart counting of trajectories for right access on vectors
		trajectoryIdxInCluster.resize(numberClusters, 0);
		const uint8_t *cluster = clusterPtr;

		MultiParticleDataCallExtended::Particles pl = call->AccessParticles(0);
		UINT64 particleCount = pl.GetCount();
		
		unsigned int vert_stride = pl.GetVertexDataStride();
		const unsigned char *vert = static_cast<const unsigned char*>(pl.GetVertexData());
		if (vert_stride < 12) vert_stride = 12;

		unsigned int extraMemberStride = pl.GetExtraMemberDataStride();
		const unsigned char *extraMember = static_cast<const unsigned char*>(pl.GetExtraMemberData());

		for (unsigned int particleIndex = 0; particleIndex < particleCount; ++particleIndex) {
			vislib::math::Vector<float,4> currentPosition(	reinterpret_cast<const float*>(vert)[0],
															reinterpret_cast<const float*>(vert)[1],
															reinterpret_cast<const float*>(vert)[2], 1.0f);
			vislib::math::Vector<float,4> currentVelocity(	reinterpret_cast<const float*>(extraMember)[0],
															reinterpret_cast<const float*>(extraMember)[1],
															reinterpret_cast<const float*>(extraMember)[2], 1.0f);
			vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extraMember)[3],
																reinterpret_cast<const float*>(extraMember)[4],
																reinterpret_cast<const float*>(extraMember)[5]);
			float currentRotationAngle = currentRotationAxis.Normalise();
			
			unsigned int currentClusterIdx = 0;
			if (numberClusters > 1) {
				currentClusterIdx = reinterpret_cast<const unsigned int*>(cluster)[0];
				cluster += clusterStride;
			}
			unsigned int currentTrajectoryIdxInCluster = trajectoryIdxInCluster[currentClusterIdx];
			if (trajectoryRotationAngles[currentClusterIdx].size() == currentTrajectoryIdxInCluster) { // trajectoryRotationAngles[currentClusterIdx] == number of current trajectories -> if equal to currentTrajectoryIdxInCluster I have to add a new trajectory (only needed in first timestep)
				trajectoryPositions[currentClusterIdx].push_back(std::vector<float>());
				trajectoryVelocities[currentClusterIdx].push_back(std::vector<float>());
				trajectoryRotationAngles[currentClusterIdx].push_back(std::vector<float>());
			}

			// add to trajectory
			trajectoryPositions[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentPosition.GetX());
			trajectoryPositions[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentPosition.GetY());
			trajectoryPositions[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentPosition.GetZ());

			trajectoryVelocities[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentVelocity.GetX());
			trajectoryVelocities[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentVelocity.GetY());
			trajectoryVelocities[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentVelocity.GetZ());
			
			trajectoryRotationAngles[currentClusterIdx][currentTrajectoryIdxInCluster].push_back(currentRotationAngle);

			// add to mean trajectory
			meanTrajectoryPositions[currentClusterIdx][3 * cnt] += currentPosition.GetX();
			meanTrajectoryPositions[currentClusterIdx][3 * cnt + 1] += currentPosition.GetY();
			meanTrajectoryPositions[currentClusterIdx][3 * cnt + 2] += currentPosition.GetZ();

			meanTrajectoryVelocities[currentClusterIdx][3 * cnt] += currentVelocity.GetX();
			meanTrajectoryVelocities[currentClusterIdx][3 * cnt + 1] += currentVelocity.GetY();
			meanTrajectoryVelocities[currentClusterIdx][3 * cnt + 2] += currentVelocity.GetZ();

			meanTrajectoryRotationAngles[currentClusterIdx][cnt] += currentRotationAngle;

			++trajectoryIdxInCluster[currentClusterIdx];

			vert += vert_stride;
			extraMember += extraMemberStride;
		}
		// compute mean
		for (unsigned int clusterIdx = 0; clusterIdx < numberClusters; ++clusterIdx) {
			float numberParticleInCluster = static_cast<float>(trajectoryRotationAngles[clusterIdx].size());
			meanTrajectoryPositions[clusterIdx][3 * cnt] /= numberParticleInCluster;
			meanTrajectoryPositions[clusterIdx][3 * cnt + 1] /= numberParticleInCluster;
			meanTrajectoryPositions[clusterIdx][3 * cnt + 2] /= numberParticleInCluster;

			meanTrajectoryVelocities[clusterIdx][3 * cnt] /= numberParticleInCluster;
			meanTrajectoryVelocities[clusterIdx][3 * cnt + 1] /= numberParticleInCluster;
			meanTrajectoryVelocities[clusterIdx][3 * cnt + 2] /= numberParticleInCluster;

			meanTrajectoryRotationAngles[clusterIdx][cnt] /= numberParticleInCluster;
		}
	}

	// try "reset" call to frameID it had before - doesn't matter
	//getFrame(call, frameIdTmp); 

	std::cout << " done" << std::endl;

	return true;
}

#ifdef _MSC_VER
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

void visualization::TrajectoryRenderer::renderTrajectory(GLint featurePos, unsigned int currentClusterIdx, unsigned int trajectoryIndex, unsigned int numberTimesteps) {
	switch(this->featureSlot.Param<core::param::EnumParam>()->Value()) {
		case POSITIONX:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryPositions[currentClusterIdx][trajectoryIndex][0]);
			break;
		case POSITIONY:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryPositions[currentClusterIdx][trajectoryIndex][1]);
			break;
		case POSITIONZ:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryPositions[currentClusterIdx][trajectoryIndex][2]);
			break;
		case VELOCITYX:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryVelocities[currentClusterIdx][trajectoryIndex][0]);
			break;
		case VELOCITYY:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryVelocities[currentClusterIdx][trajectoryIndex][1]);
			break;
		case VELOCITYZ:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &trajectoryVelocities[currentClusterIdx][trajectoryIndex][2]);
			break;
		case ROTATIONANGLE:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 0, trajectoryRotationAngles[currentClusterIdx][trajectoryIndex].data());
			break;
	}
	glDrawArrays(GL_LINE_STRIP, 0, static_cast<GLsizei>(numberTimesteps));
}

void visualization::TrajectoryRenderer::renderMeanTrajectory(GLint featurePos, unsigned int currentClusterIdx, unsigned int numberClusters, unsigned int numberTimesteps) {
	switch(this->featureSlot.Param<core::param::EnumParam>()->Value()) {
		case POSITIONX:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryPositions[currentClusterIdx][0]);
			break;
		case POSITIONY:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryPositions[currentClusterIdx][1]);
			break;
		case POSITIONZ:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryPositions[currentClusterIdx][2]);
			break;
		case VELOCITYX:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryVelocities[currentClusterIdx][0]);
			break;
		case VELOCITYY:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryVelocities[currentClusterIdx][1]);
			break;
		case VELOCITYZ:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(float), &meanTrajectoryVelocities[currentClusterIdx][2]);
			break;
		case ROTATIONANGLE:
			glVertexAttribPointerARB(featurePos, 1, GL_FLOAT, GL_FALSE, 0, meanTrajectoryRotationAngles[currentClusterIdx].data());
			break;
	}
	// Halo
	glLineWidth(3.0f * this->lineWidthSlot.Param<core::param::FloatParam>()->Value());
	glUniform1f(this->trajectoryShader.ParameterLocation("colIdx"), static_cast<GLfloat>(numberClusters)); // colIdx == numberClusters -> color = white
	glDrawArrays(GL_LINE_STRIP, 0, static_cast<GLsizei>(numberTimesteps));
	// Trajectory
	glLineWidth(2.0f * this->lineWidthSlot.Param<core::param::FloatParam>()->Value());
	glUniform1f(this->trajectoryShader.ParameterLocation("colIdx"), static_cast<GLfloat>(currentClusterIdx));
	glDrawArrays(GL_LINE_STRIP, 0, static_cast<GLsizei>(numberTimesteps));
}

void visualization::TrajectoryRenderer::renderLabels(unsigned int numberTimesteps, int firstFrameNumber, int lastFrameNumber) {
	glColor3f(0.0f, 0.0f, 0.0f);

	float scale = this->scaleSlot.Param<core::param::FloatParam>()->Value();
	float scaleDiagram = numberTimesteps / 250.0f * scale;

	vislib::graphics::gl::SimpleFont f;
	float fontsize = 3.5f * scaleDiagram;
	float padding = 1.0f * scaleDiagram;
	
	if (f.Initialise()) {
		vislib::StringA tmpStr("time");
		f.DrawString(padding, padding + minY * scaling, static_cast<float>(numberTimesteps - 1), (maxY - minY) * scaling, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_RIGHT_BOTTOM);
		
		tmpStr.Clear();
		tmpStr.Append(this->featureSlot.Param<core::param::EnumParam>()->ValueString());
		f.DrawString(padding, padding + minY * scaling, static_cast<float>(numberTimesteps - 1), (maxY - minY) * scaling, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_LEFT_TOP);

		tmpStr.Clear();
		tmpStr.Format("%.4f", minY);
		f.DrawString(-padding, minY * scaling, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_RIGHT_MIDDLE);

		if (minY < 0.0f) {
			tmpStr.Clear();
			tmpStr.Format("%i", 0);
			f.DrawString(-padding, 0.0f, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_RIGHT_MIDDLE);
		}

		tmpStr.Clear();
		tmpStr.Format("%.4f", maxY);
		f.DrawString(-padding, maxY * scaling, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_RIGHT_MIDDLE);

		tmpStr.Clear();
		tmpStr.Format("%i", firstFrameNumber);
		f.DrawString(0, minY * scaling - padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);
		
		tmpStr.Clear();
		tmpStr.Format("%i", lastFrameNumber);
		f.DrawString(static_cast<float>(numberTimesteps - 1), minY * scaling - padding, fontsize, true, tmpStr.PeekBuffer(), vislib::graphics::AbstractFont::Alignment::ALIGN_CENTER_TOP);
	}

	glLineWidth(1.5f * scale);

	float markerLength = 0.7f * scaleDiagram;
	float extraLength = 1.5f * scaleDiagram;
	glBegin(GL_LINES);
		glVertex2f(0.0f, minY * scaling - markerLength);
		glVertex2f(0.0f, maxY * scaling + extraLength);

		glVertex2f(-markerLength, minY * scaling);
		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1) + extraLength, minY * scaling);

		if (minY < 0.0f) {
			glVertex2f(-markerLength, 0.0f);
			glVertex2f(markerLength, 0.0f);
		}

		glVertex2f(-markerLength, maxY * scaling);
		glVertex2f(markerLength, maxY * scaling);

		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1), minY * scaling - markerLength);
		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1), minY * scaling + markerLength);
	glEnd();

	glBegin(GL_TRIANGLES);
		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1) + extraLength, minY * scaling + 1.5f * markerLength);
		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1) + extraLength, minY * scaling - 1.5f * markerLength);
		glVertex2f(static_cast<GLfloat>(numberTimesteps - 1) + 4.0f * extraLength, minY * scaling);

		glVertex2f(-1.5f * markerLength, maxY * scaling + extraLength);
		glVertex2f(1.5f * markerLength, maxY * scaling + extraLength);
		glVertex2f(0.0f, maxY * scaling + 4.0f * extraLength);
	glEnd();
}

bool visualization::TrajectoryRenderer::Render(core::view::CallRender2D& call) {
	MultiParticleDataCallExtended *mpdcx = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
	if (mpdcx == nullptr) {
		return false;
	}
	mpdcx->SetFrameID(call.Time());
    if (!(*mpdcx)(1)) return false;
	if (!(*mpdcx)(0)) return false;

	MultiParticleClusterDataCall *mpcdc = this->getClusterDataSlot.CallAs<MultiParticleClusterDataCall>();

	unsigned int numberClusters = 1;
	unsigned int clusterStride;
	const uint8_t *cluster;
	if (mpcdc != nullptr) {
		if ((*mpcdc)(1) && (*mpcdc)(0)) {
			MultiParticleClusterDataCall::Particles &parts = mpcdc->AccessParticles(0);
			numberClusters = parts.GetNumberClusters();
			clusterStride = parts.GetClusterDataStride();
			cluster = static_cast<const uint8_t*>(parts.GetClusterData());
		} else {
			return false;
		}
	} else {
		return false;
	}

	int firstFrame = mpcdc->GetFirstFrame();
	int lastFrame = mpcdc->GetLastFrame();
	int frameStep = mpcdc->GetFrameStep();
	if (firstFrame > mpdcx->FrameCount() || lastFrame > mpdcx->FrameCount() ||
		firstFrame < 0 || lastFrame < 0 || 
		lastFrame < firstFrame || lastFrame - firstFrame < frameStep) {
		vislib::sys::Log::DefaultLog.WriteMsg(vislib::sys::Log::LEVEL_ERROR, "Trajectory invalid\n");
		return false;
	}

	unsigned int numberTimesteps = (lastFrame - firstFrame) / frameStep + 1;	// floor - lastFrame may not be used
	lastFrame = firstFrame + (numberTimesteps - 1) * frameStep;

	if ((this->lastHash != mpdcx->DataHash()) || (mpdcx->DataHash() == 0) ||
		(this->lastClusterHash != mpcdc->DataHash()) || (mpcdc->DataHash() == 0)) {
		this->lastHash = mpdcx->DataHash();
		this->lastClusterHash = mpcdc->DataHash();
		if (!buildTrajectoryVector(mpdcx, mpcdc, numberClusters, cluster, clusterStride)) {
			return false;
		}
	}	

	float outScaling = 0.8f;
	glTranslatef(0.0f, minY*scaling, 0.0f);
	glTranslatef(numberTimesteps * (1.0f - outScaling) / 2.0f, (maxY - minY) * scaling * (1.0f - outScaling) / 2.0f, 0.0f);
	glScalef(outScaling, outScaling, 1.0f);
	glTranslatef(0.0f, -minY*scaling, 0.0f);

	glPushMatrix();
	glScalef(1.0f, scaling, 1.0f);

	glEnable(GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	int currentClusterIdx = 0;
	bool cycleTransferFunction = true;

	CallVisParams *vp = this->sharedVisParamsSlot.CallAs<CallVisParams>();
	if (vp != nullptr) {
		vp->SetVisParams(VisualizationParams(numberClusters));
		if (!(*vp)(CallVisParams::CallForGetVisParams)) {
			return false;
		}
		if (!(*vp)(CallVisParams::CallForSetVisParams)) {
			return false;
		}
		currentClusterIdx = vp->GetVisParams().drawOnlyClusterId;
		cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
	}

	this->trajectoryShader.Enable();
	
	// init shader
	core::view::CallGetTransferFunction *cgtf = this->getTFSlot.CallAs<core::view::CallGetTransferFunction>();
	SetMVPMatTextureParams(this->trajectoryShader, mpcdc, cgtf, greyTF);
	glUniform1f(this->trajectoryShader.ParameterLocation("numberCluster"), static_cast<float>(mpcdc->AccessParticles(0).GetNumberClusters()));
    glUniform1f(this->trajectoryShader.ParameterLocation("cycleTransferFunction"), static_cast<float>(cycleTransferFunction));

	GLint featurePos = glGetAttribLocationARB(this->trajectoryShader, "feature"); // y-axis
	GLint timePos = glGetAttribLocationARB(this->trajectoryShader, "time");	// x-axis

	glEnableVertexAttribArrayARB(featurePos);
	glEnableVertexAttribArrayARB(timePos);

	// generate values for x-axis
	std::vector<float> timesteps;
	timesteps.reserve(numberTimesteps);
	for (float ts = 0.0f; ts < numberTimesteps; ++ts) {
		timesteps.push_back(ts);
	}
	glVertexAttribPointerARB(timePos, 1, GL_FLOAT, GL_FALSE, 0, timesteps.data());
	
	float opacity = this->opacitySlot.Param<core::param::FloatParam>()->Value();
	int drawing = this->drawSlot.Param<core::param::EnumParam>()->Value(); // draw only trajectories? or only mean trajectories? or both?
	
	if (drawing != MEAN) { // drawing == TRAJECTORY or BOTH -> draw trajectories
		glUniform1f(this->trajectoryShader.ParameterLocation("opacity"), opacity);
		glLineWidth(this->lineWidthSlot.Param<core::param::FloatParam>()->Value());

		if (currentClusterIdx >= 0) { // draw only trajectories of currentCluster
			glUniform1f(this->trajectoryShader.ParameterLocation("colIdx"), static_cast<GLfloat>(currentClusterIdx));
			
			unsigned int trajectoryCount = trajectoryRotationAngles[currentClusterIdx].size();
			trajectoryCount *= this->percentTrajectoriesSlot.Param<core::param::FloatParam>()->Value();
			
			for (unsigned int trajectoryIndex = 0; trajectoryIndex < trajectoryCount; ++trajectoryIndex) {
				renderTrajectory(featurePos, currentClusterIdx, trajectoryIndex, numberTimesteps);
			}
		} else { // draw trajectories of all clusters
			for (unsigned int clusterIndex = 0; clusterIndex < numberClusters; ++clusterIndex) {
				glUniform1f(this->trajectoryShader.ParameterLocation("colIdx"), static_cast<GLfloat>(clusterIndex));
				
				unsigned int trajectoryCount = trajectoryRotationAngles[clusterIndex].size();
				trajectoryCount *= this->percentTrajectoriesSlot.Param<core::param::FloatParam>()->Value();
				
				for (unsigned int trajectoryIndex = 0; trajectoryIndex < trajectoryCount; ++trajectoryIndex) {
					renderTrajectory(featurePos, clusterIndex, trajectoryIndex, numberTimesteps);
				}
			} 
		}
	}

	if (drawing != TRAJECTORY) { // draw mean trajectories
		glUniform1f(this->trajectoryShader.ParameterLocation("opacity"), 1.0f);

		for (unsigned int clusterIndex = 0; clusterIndex < numberClusters; ++clusterIndex) {
			if (clusterIndex == currentClusterIdx) {
				continue;
			}
			renderMeanTrajectory(featurePos, clusterIndex, numberClusters, numberTimesteps);
		}

		if (currentClusterIdx >= 0) {
			renderMeanTrajectory(featurePos, currentClusterIdx, numberClusters, numberTimesteps);
		}
	}
	
	glDisableVertexAttribArrayARB(timePos);
    glDisableVertexAttribArrayARB(featurePos);
    glDisable(GL_TEXTURE_1D);
    
	this->trajectoryShader.Disable();

	glPopMatrix();

	renderLabels(numberTimesteps, firstFrame, lastFrame);

	glDisable(GL_BLEND);

	mpdcx->Unlock();

	return true;
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#endif /* _MSC_VER */