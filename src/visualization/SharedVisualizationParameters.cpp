/*
 * SharedVisualizationParameters.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "SharedVisualizationParameters.h"
#include "CallVisParams.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/BoolParam.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

visualization::SharedVisualizationParameters::SharedVisualizationParameters(void) : cycleTransferFunctionSlot("cycleTransferFunction", "Is linear transfer function defined as cycle?"),
		drawClusterOfIdxSlot("drawclusterofidx", "All other clusters are not rendered."),
		visParamsSlot("visParams", "Provides read and write access to shared visualization parameters") {
	this->cycleTransferFunctionSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&this->cycleTransferFunctionSlot);

	this->drawClusterOfIdxSlot.SetParameter(new core::param::IntParam(-1,-1));
	this->MakeSlotAvailable(&this->drawClusterOfIdxSlot);

	this->visParamsSlot.SetCallback(CallVisParams::ClassName(),
            CallVisParams::FunctionName(CallVisParams::CallForGetVisParams),
            &SharedVisualizationParameters::getVisParams);
    this->visParamsSlot.SetCallback(CallVisParams::ClassName(),
            CallVisParams::FunctionName(CallVisParams::CallForSetVisParams),
            &SharedVisualizationParameters::setVisParams);
    this->MakeSlotAvailable(&this->visParamsSlot);
}


visualization::SharedVisualizationParameters::~SharedVisualizationParameters(void)	{
	this->Release();
}

bool visualization::SharedVisualizationParameters::create(void) {
    return true;
}

void visualization::SharedVisualizationParameters::release(void) {
}

bool visualization::SharedVisualizationParameters::getVisParams(megamol::core::Call& call) {
	CallVisParams *vp = dynamic_cast<CallVisParams*>(&call);

    if (vp == NULL) {
        return false;
    }

	this->numberClusters = vp->GetVisParams().numberClusters;

    return true;
}

bool visualization::SharedVisualizationParameters::setVisParams(megamol::core::Call& call) {
	CallVisParams *vp = dynamic_cast<CallVisParams*>(&call);

    if (vp == NULL) {
        return false;
    }

	bool cycleTransferFunction = this->cycleTransferFunctionSlot.Param<core::param::BoolParam>()->Value();
	int drawClusterOfIdx = this->drawClusterOfIdxSlot.Param<core::param::IntParam>()->Value();
	if (drawClusterOfIdx > -1 && drawClusterOfIdx >= numberClusters) {
		this->drawClusterOfIdxSlot.Param<core::param::IntParam>()->SetValue(numberClusters - 1);
		drawClusterOfIdx = numberClusters - 1;
	}
	VisualizationParams visParams(numberClusters, cycleTransferFunction, drawClusterOfIdx);

	vp->SetVisParams(visParams);

    return true;
}
