/*
 * HairBallCylinderRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "HairBallCylinderRenderer.h"
#include "RenderHelper.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallClipPlane.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/view/CallRender3D.h"
#include "vislib/assert.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/StringParam.h"
#include <memory>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::HairBallCylinderRenderer::HairBallCylinderRenderer
 */
visualization::HairBallCylinderRenderer::HairBallCylinderRenderer(void) : AbstractHairBallRenderer(),
		cylinderSphereRatioSlot("cylinderSphereRatio", "Ratio of cylinder to sphere") {
	this->cylinderSphereRatioSlot.SetParameter(new core::param::FloatParam(0.35f, 0.01f, 1.0f));
	this->MakeSlotAvailable(&cylinderSphereRatioSlot);
}

/*
 * visualization::HairBallCylinderRenderer::~HairBallCylinderRenderer
 */
visualization::HairBallCylinderRenderer::~HairBallCylinderRenderer(void) {
    this->Release();
}

/*
 * visualization::HairBallCylinderRenderer::createHairballShader
 */
bool visualization::HairBallCylinderRenderer::create(void) {
	ASSERT(IsAvailable());

	CreateShader(this->instance(), this->hairballShader, "hairballcylinder");

    return AbstractHairBallRenderer::create();
}

/*
 * visualization::HairBallCylinderRenderer::release
 */
void visualization::HairBallCylinderRenderer::release(void) {
	this->hairballShader.Release();
    AbstractHairBallRenderer::release();
}

bool visualization::HairBallCylinderRenderer::renderHairballNeighbours(core::view::CallRender3D *cr, MultiParticleClusterDataCall* mpcdc) {
	// already tested that that cr and mpcdc != NULL
	
	bool usedStoredData = true;
	if (dataChanged(mpcdc)) { // nicht ganz sauber, aber da in der Regel alles glatt geht wei� ich so ob gespeicherte Sachen genutzt wurden oder neuberechnet wurde
		findNearestNeighbours(mpcdc, result);
		usedStoredData = false;
	}

	unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	int drawClusterOfIdx = -1;
	bool cycleTransferFunction = true;

	CallVisParams *vp = this->getVisParamsSlot().CallAs<CallVisParams>();
	if (vp != nullptr) {
		vp->SetVisParams(VisualizationParams(numberClusters));
		if (!(*vp)(CallVisParams::CallForGetVisParams)) {
			return false;
		}
		if (!(*vp)(CallVisParams::CallForSetVisParams)) {
			return false;
		}
		drawClusterOfIdx = vp->GetVisParams().drawOnlyClusterId;
		cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
	}

	// init shader
	float sphereRadius = mpcdc->AccessParticles(0).GetGlobalRadius();
	float cylinderRadius = this->cylinderSphereRatioSlot.Param<core::param::FloatParam>()->Value() * sphereRadius;
	vislib::math::Vector<vislib::graphics::SceneSpaceType,3> camIn = cr->GetCameraParameters()->Front();
	camIn.Normalise();

	this->hairballShader.Enable();
	SetViewportLightParams(this->hairballShader);
	core::view::CallGetTransferFunction *cgtf = this->getTFSlot().CallAs<core::view::CallGetTransferFunction>();
	SetMVPMatTextureParams(this->hairballShader, mpcdc, cgtf, this->getGreyTF());
	glUniform1f(this->hairballShader.ParameterLocation("drawClusterOfIdx"), static_cast<float>(drawClusterOfIdx)); // draw only lines of one cluster
	glUniform1f(this->hairballShader.ParameterLocation("numberCluster"), static_cast<float>(numberClusters));
	glUniform1f(this->hairballShader.ParameterLocation("cycleTransferFunction"), static_cast<float>(cycleTransferFunction));
	glUniform1f(this->hairballShader.ParameterLocation("radius"), cylinderRadius);
	glUniform3fv(this->hairballShader.ParameterLocation("camIn"), 1, camIn.PeekComponents());
	
	GLint cylinderVertexPos = glGetAttribLocation(this->hairballShader, "cylinderVertex");
	GLint positionOffset = glGetAttribLocationARB(this->hairballShader, "positionOffset");
	GLint directionPos = glGetAttribLocation(this->hairballShader, "direction");
	GLint clusterIDs = glGetAttribLocationARB(this->hairballShader, "colIdx");
	
    glEnableVertexAttribArray(cylinderVertexPos);
	glEnableVertexAttribArrayARB(positionOffset);
	glEnableVertexAttribArray(directionPos);
	glEnableVertexAttribArrayARB(clusterIDs);
	
	// draw each hairball line
	unsigned int numberLines = result.clusterIds.size()/4;
	for (unsigned int line = 0; line < numberLines; ++line) {
		glVertexAttribPointerARB(clusterIDs, 1, GL_FLOAT, GL_FALSE, 0, &result.clusterIds[4*line]);
		glVertexAttribPointer(cylinderVertexPos, 3, GL_FLOAT, GL_FALSE, 0, &result.cylinderVertex[12*line]);
		glVertexAttribPointerARB(positionOffset, 1, GL_FLOAT, GL_FALSE, 0, &result.positionOffset[4*line]);
		glVertexAttribPointer(directionPos, 3, GL_FLOAT, GL_FALSE, 0, &result.directions[12*line]);
		
		glDrawArrays(GL_TRIANGLE_STRIP, 0, static_cast<GLsizei>(4));
	}

	glDisableVertexAttribArray(cylinderVertexPos);
    glDisableVertexAttribArrayARB(positionOffset);
	glDisableVertexAttribArray(directionPos);
    glDisableVertexAttribArrayARB(clusterIDs);
    glDisable(GL_TEXTURE_1D);
    
	this->hairballShader.Disable();

	return usedStoredData;
}
