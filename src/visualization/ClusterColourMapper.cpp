/*
 * ClusterColourMapper.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "ClusterColourMapper.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/IntParam.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

visualization::ClusterColourMapper::ClusterColourMapper(void) : Module(),
		putDataSlot("putdata", "Connects from the data consumer"),
		getDataSlot("getdata", "Connects to the data source"),
		sharedVisParamsSlot("sharedvisparams", "Connects to the shared visualization parameters"),
		lastFrame(0), lastHash(0), colData(), lastClusterId(-1), lastCycleTF(true) {
	
	this->putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetData", &ClusterColourMapper::getDataCallback);
    this->putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetExtent", &ClusterColourMapper::getExtentCallback);
    this->MakeSlotAvailable(&this->putDataSlot);

    this->getDataSlot.SetCompatibleCall<MultiParticleClusterDataCallDescription>();
    this->MakeSlotAvailable(&this->getDataSlot);

	this->sharedVisParamsSlot.SetCompatibleCall<CallVisParamsDescription>();
    this->MakeSlotAvailable(&this->sharedVisParamsSlot);
}


visualization::ClusterColourMapper::~ClusterColourMapper(void) {
	this->Release();
}

bool visualization::ClusterColourMapper::create(void) {
    // intentionally empty
    return true;
}


void visualization::ClusterColourMapper::release(void) {
    // intentionally empty
}

bool visualization::ClusterColourMapper::getDataCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *inCall = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleClusterDataCall *outCall = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    if (outCall == nullptr) return false;

    outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall

    if ((*outCall)(0)) { // GetDataCallback for outCall
        bool updateData = false;
		
		if ((this->lastFrame != outCall->FrameID()) || (this->lastHash != outCall->DataHash()) || (outCall->DataHash() == 0)) {
            this->lastFrame = outCall->FrameID();
            this->lastHash = outCall->DataHash();
			updateData = true;
        }

		unsigned int numberClusters = outCall->AccessParticles(0).GetNumberClusters();
		CallVisParams *vp = this->sharedVisParamsSlot.CallAs<CallVisParams>();
		if (vp != nullptr) {
			vp->SetVisParams(VisualizationParams(numberClusters));
			if ((*vp)(CallVisParams::CallForGetVisParams) && (*vp)(CallVisParams::CallForSetVisParams)) {
				if (this->lastClusterId != vp->GetVisParams().drawOnlyClusterId || this->lastCycleTF != vp->GetVisParams().cycleTransferFunction) {
					this->lastClusterId = vp->GetVisParams().drawOnlyClusterId;
					this->lastCycleTF = vp->GetVisParams().cycleTransferFunction;
					updateData = true;
				}
			}
		}

		inCall->SetFrameID(outCall->FrameID());
        inCall->SetDataHash(outCall->DataHash());

		if (updateData)	{
			inCall->SetDataHash(0);
			this->colourParticles(outCall);
		}		

        size_t cnt = 0;
        unsigned int plc = outCall->GetParticleListCount();
		inCall->SetParticleListCount(plc);
        for (unsigned int pli = 0; pli < plc; pli++) {
            core::moldyn::SimpleSphericalParticles &p = inCall->AccessParticles(pli);
			ClusteredSphericalParticles &po = outCall->AccessParticles(pli);
            p.SetGlobalRadius(po.GetGlobalRadius());
			p.SetGlobalColour(po.GetGlobalColour()[0],po.GetGlobalColour()[1],po.GetGlobalColour()[2]);
			p.SetCount(numberOfParticlesInCluster);
			p.SetVertexData(core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ, this->posData.At(cnt * sizeof(float)), 3 * sizeof(float));
			//if ((p.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE)
            //    || (p.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_SHORT_XYZ)) continue;
			p.SetColourMapIndexValues(0.0f,1.0f);
			p.SetColourData(core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_I, this->colData.At(cnt * sizeof(float)));
            cnt += static_cast<size_t>(p.GetCount());
        }

		inCall->SetUnlocker(new Unlocker(outCall->GetUnlocker()), false);
        outCall->SetUnlocker(nullptr, false);

        return true;
    }

    return false;
}

bool visualization::ClusterColourMapper::getExtentCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *inCall = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleClusterDataCall *outCall = this->getDataSlot.CallAs<MultiParticleClusterDataCall>();
    if (outCall == nullptr) return false;

	outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall
	
    if ((*outCall)(1)) { // GetExtentCallback for outCall
		outCall->SetUnlocker(nullptr, false);

        inCall->SetFrameCount(outCall->FrameCount());
		inCall->AccessBoundingBoxes().Clear();
		inCall->AccessBoundingBoxes().SetObjectSpaceBBox(outCall->AccessBoundingBoxes().ObjectSpaceBBox());
		inCall->AccessBoundingBoxes().SetObjectSpaceClipBox(outCall->AccessBoundingBoxes().ObjectSpaceClipBox());
		inCall->SetDataHash(outCall->DataHash());

		return true;
	}

    return false;
}

void visualization::ClusterColourMapper::colourParticles(MultiParticleClusterDataCall *dat) {
	size_t all_cnt = 0;
    unsigned int plc = dat->GetParticleListCount();
    for (unsigned int pli = 0; pli < plc; pli++) {
        core::moldyn::SimpleSphericalParticles::VertexDataType vdt = dat->AccessParticles(pli).GetVertexDataType();
        if ((vdt == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE)
            || (vdt == core::moldyn::SimpleSphericalParticles::VERTDATA_SHORT_XYZ)) continue;
        all_cnt += static_cast<size_t>(dat->AccessParticles(pli).GetCount());
    }

	for (unsigned int pli = 0; pli < plc; pli++) {
		MultiParticleClusterDataCall::Particles pl = dat->AccessParticles(pli);
		// TODO: nur VERTDATA_XYZ unterst�tzt
		if ((pl.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE)
            || (pl.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_SHORT_XYZ)) continue;
		
		unsigned int clusterStride = pl.GetClusterDataStride();
		const uint8_t *cluster = static_cast<const uint8_t*>(pl.GetClusterData());
		unsigned int vertexStride = pl.GetVertexDataStride();
		const uint8_t *vert = static_cast<const uint8_t*>(pl.GetVertexData());

		unsigned int numberClusters = pl.GetNumberClusters();
		int drawClusterOfIdx = -1;
		bool cycleTransferFunction = true;

		CallVisParams *vp = this->sharedVisParamsSlot.CallAs<CallVisParams>();
		if (vp != nullptr) {
			vp->SetVisParams(VisualizationParams(numberClusters));
			if (!(*vp)(CallVisParams::CallForGetVisParams)) {
				return;
			}
			if (!(*vp)(CallVisParams::CallForSetVisParams)) {
				return;
			}
			drawClusterOfIdx = vp->GetVisParams().drawOnlyClusterId;
			cycleTransferFunction = vp->GetVisParams().cycleTransferFunction;
		}

		numberOfParticlesInCluster = 0;
		if (drawClusterOfIdx != -1) {
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, cluster += clusterStride) {
				if (reinterpret_cast<const unsigned int*>(cluster)[0] == drawClusterOfIdx) {
					numberOfParticlesInCluster++;
				}
			}
			cluster = static_cast<const uint8_t*>(pl.GetClusterData());
		} else {
			numberOfParticlesInCluster = pl.GetCount();
		}

		this->posData.EnforceSize(numberOfParticlesInCluster * 3 * sizeof(float)); // always store VERTDATA_XYZ
		float *p = this->posData.As<float>();

		this->colData.EnforceSize(numberOfParticlesInCluster * sizeof(float)); // always store COLDATA_FLOAT_I
		float *f = this->colData.As<float>();

		unsigned int currentParticleIdx = 0;
		for (uint64_t pi = 0; pi < pl.GetCount(); pi++, cluster += clusterStride, vert += vertexStride) {
			unsigned int currentClusterId = reinterpret_cast<const unsigned int*>(cluster)[0];
			if (currentClusterId == drawClusterOfIdx || drawClusterOfIdx == -1) {
				if (numberClusters > 1) {
					float denominator =  static_cast<float>(numberClusters - (1 - cycleTransferFunction));
					f[currentParticleIdx] = currentClusterId / denominator;
				} else {
					f[currentParticleIdx] = 0.0f;
				}
				for (unsigned int c = 0; c < 3; ++c) {
					p[3 * currentParticleIdx + c] = reinterpret_cast<const float*>(vert)[c];
				}
				++currentParticleIdx;
			}
        }
	}
}