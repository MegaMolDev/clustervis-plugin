/*
 * HairBallRenderer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_HAIRBALLRENDERER_H_INCLUDED
#define MEGAMOLVPC_HAIRBALLRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "AbstractHairBallRenderer.h"
#include "vislib/graphics/gl/GLSLShader.h"
#include "RenderHelper.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

    /**
     * Renderer for (shaded) hairballs
     */
    class HairBallRenderer : public AbstractHairBallRenderer {
    public:

        /**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void) {
            return "HairBallRenderer";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void) {
            return "Renderer for hairballs.";
        }

        /** Ctor. */
        HairBallRenderer(void);

        /** Dtor. */
        virtual ~HairBallRenderer(void);

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
        static bool IsAvailable(void) {
#ifdef _WIN32
#if defined(DEBUG) || defined(_DEBUG)
            HDC dc = ::wglGetCurrentDC();
            HGLRC rc = ::wglGetCurrentContext();
            ASSERT(dc != NULL);
            ASSERT(rc != NULL);
#endif // DEBUG || _DEBUG
#endif // _WIN32
            return vislib::graphics::gl::GLSLShader::AreExtensionsAvailable();
        }

	protected:
		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);
		
        /**
         * Implementation of 'Release'.
         */
        virtual void release(void);

		/** Nested structure containing all data for rendering a hairball using (shaded) lines */
		struct HairballResult {
			/** Cluster IDs (used for correct colouring) */
			std::vector<float> clusterIds;

			/** Positions of start and end points of the hairball lines */
			std::vector<float> positions;

			/** Directions of lines */
			std::vector<float> directions;

			/** Clear the data */
			void clear() {
				clusterIds.clear();
				positions.clear();
				directions.clear();
			}

			/** Add a line to hairball, compute all necessary data */
			void addLine(const vislib::math::Vector<float,3>& vec1, const vislib::math::Vector<float,3>& vec2, unsigned int currentCluster) {
				vislib::math::Vector<float,3> direction = vec1 - vec2;
				direction.Normalise();
				
				// color + position
				clusterIds.push_back(static_cast<float>(currentCluster));
				stdVectorPushBackVislibVector(positions, vec1);
				stdVectorPushBackVislibVector(directions, direction);
				
				clusterIds.push_back(static_cast<float>(currentCluster));
				stdVectorPushBackVislibVector(positions, vec2);
				stdVectorPushBackVislibVector(directions, direction);
			}
		};

    private:
		/** Rendering of hairball */
		bool renderHairballNeighbours(core::view::CallRender3D *cr, MultiParticleClusterDataCall* mpcdc);

		/** The width of hairball lines */
		core::param::ParamSlot lineWidthSlot;

		/** The hairball shader */
		vislib::graphics::gl::GLSLShader hairballShader;

		/** The hairball */
		HairballResult result;
    };

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_HAIRBALLRENDERER_H_INCLUDED */
