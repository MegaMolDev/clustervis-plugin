/*
 * LinearTransferFunction2.cpp
 *
 * Copyright (C) 2008 by Universitaet Stuttgart (VIS). 
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "visualization/LinearTransferFunction2.h"
#ifdef _WIN32
#include <windows.h>
#endif
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/param/FilePathParam.h"
#include "mmcore/param/ButtonParam.h"
#include "mmcore/utility/ColourParser.h"
#include "vislib/Array.h"
#include "vislib/assert.h"
#include "vislib/math/Vector.h"
#include "vislib/sys/sysfunctions.h"
#include "vislib/sys/Log.h"


namespace {

    static int InterColourComparer(const vislib::math::Vector<float, 5>& lhs, 
            const vislib::math::Vector<float, 5>& rhs) {
        if (lhs[4] >= rhs[4]) {
            if (rhs[4] + vislib::math::FLOAT_EPSILON >= lhs[4]) {
                return 0;
            } else {
                return 1;
            }
        } else {
            if (lhs[4] + vislib::math::FLOAT_EPSILON >= rhs[4]) {
                return 0;
            } else {
                return -1;
            }
        }
    }

}


using namespace megamol;
using namespace megamol::vis_particle_clustering;


/*
 * visualization::LinearTransferFunction::LinearTransferFunction
 */
visualization::LinearTransferFunction::LinearTransferFunction(void) : core::Module(),
        getTFSlot("gettransferfunction", "Provides the transfer function"),
        minColSlot("mincolour", "The colour for the minimum value"),
        maxColSlot("maxcolour", "The colour for the maximum value"),
        texSizeSlot("texsize", "The size of the texture to generate"),
        pathSlot("filepath", "path for serializing the TF"),
        loadTFSlot("loadTF", "trigger loading from file"),
        storeTFSlot("storeTF", "trigger saving to file"),
        texID(0), texSize(1), tex(NULL),
        texFormat(core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGB),
        firstRequest(true),
        outPartData("outPartData", "output slot of MultiParticleData moved through this module to parameterize the transfer function control value range"),
        inPartData("inPartData", "input slot for MultiParticleData moved through this module to parameterize the transfer function control value range"),
        inPartMinVal(0.0f), inPartMaxVal(1.0f) {

    core::view::CallGetTransferFunctionDescription cgtfd;
    this->getTFSlot.SetCallback(cgtfd.ClassName(), cgtfd.FunctionName(0),
        &LinearTransferFunction::requestTF);
    this->MakeSlotAvailable(&this->getTFSlot);

    this->minColSlot << new core::param::StringParam("blue");
    this->MakeSlotAvailable(&this->minColSlot);

    vislib::StringA t1, t2;
    for (SIZE_T i = 0; i < INTER_COLOUR_COUNT; i++) {
        t1.Format("enable%.2d", i + 1);
        t2.Format("Enables the intermediate colour %d", i + 1);
        this->interCols[i].enableSlot = new core::param::ParamSlot(t1, t2);
        this->interCols[i].enableSlot->SetParameter(new core::param::BoolParam(false));
        this->MakeSlotAvailable(this->interCols[i].enableSlot);

        t1.Format("colour%.2d", i + 1);
        t2.Format("The colour for the intermediate value no. %d", i + 1);
        this->interCols[i].colSlot = new core::param::ParamSlot(t1, t2);
        this->interCols[i].colSlot->SetParameter(new core::param::StringParam("Gray"));
        this->MakeSlotAvailable(this->interCols[i].colSlot);

        t1.Format("value%.2d", i + 1);
        t2.Format("The intermediate value no. %d", i + 1);
        this->interCols[i].valSlot = new core::param::ParamSlot(t1, t2);
        this->interCols[i].valSlot->SetParameter(new core::param::FloatParam(
            0.0f)); // static_cast<float>(i + 1) / static_cast<float>(INTER_COLOUR_COUNT + 1),
//            0.0f, 1.0f));
        this->MakeSlotAvailable(this->interCols[i].valSlot);
    }

    this->maxColSlot << new core::param::StringParam("red");
    this->MakeSlotAvailable(&this->maxColSlot);
    this->maxColSlot.ForceSetDirty();

    this->texSizeSlot << new core::param::IntParam(128, 2, 1024);
    this->MakeSlotAvailable(&this->texSizeSlot);

    this->pathSlot << new core::param::FilePathParam("");
    this->MakeSlotAvailable(&this->pathSlot);

    this->loadTFSlot << new core::param::ButtonParam();
    this->loadTFSlot.SetUpdateCallback(&LinearTransferFunction::loadTFPressed);
    this->MakeSlotAvailable(&this->loadTFSlot);

    this->storeTFSlot << new core::param::ButtonParam();
    this->storeTFSlot.SetUpdateCallback(&LinearTransferFunction::storeTFPressed);
    this->MakeSlotAvailable(&this->storeTFSlot);

    outPartData.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), core::moldyn::MultiParticleDataCall::FunctionName(0 /*GetData*/), &LinearTransferFunction::outPartDataGetData);
    outPartData.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), core::moldyn::MultiParticleDataCall::FunctionName(1 /*GetExtent*/), &LinearTransferFunction::outPartDataGetExtent);
    MakeSlotAvailable(&outPartData);

    inPartData.SetCompatibleCall<core::moldyn::MultiParticleDataCallDescription>();
    MakeSlotAvailable(&inPartData);

}


/*
 * visualization::LinearTransferFunction::~LinearTransferFunction
 */
visualization::LinearTransferFunction::~LinearTransferFunction(void) {
    this->Release();
}


/*
 * visualization::LinearTransferFunction::create
 */
bool visualization::LinearTransferFunction::create(void) {
    firstRequest = true;
    return true;
}


/*
 * visualization::LinearTransferFunction::release
 */
void visualization::LinearTransferFunction::release(void) {
    ::glDeleteTextures(1, &this->texID);
    this->texID = 0;
    this->tex = NULL;
}


/*
 * visualization::LinearTransferFunction::loadTFPressed
 */
bool visualization::LinearTransferFunction::loadTFPressed(core::param::ParamSlot& param) {
    try {
        vislib::sys::BufferedFile inFile;
        if (!inFile.Open(pathSlot.Param<core::param::FilePathParam>()->Value(),
            vislib::sys::File::AccessMode::READ_ONLY,
            vislib::sys::File::ShareMode::SHARE_READ,
            vislib::sys::File::CreationMode::OPEN_ONLY)) {
            vislib::sys::Log::DefaultLog.WriteMsg(
                vislib::sys::Log::LEVEL_WARN,
                "Unable to open TF file.");
            // failed, but does not matter here
            return true;
        }

        while (!inFile.IsEOF()) {
            vislib::StringA line = vislib::sys::ReadLineFromFileA(inFile);
            line.TrimSpaces();
            if (line.IsEmpty()) continue;
            if (line[0] == '#') continue;
            vislib::StringA::Size pos = line.Find('=');
            vislib::StringA name = line.Substring(0, pos);
            vislib::StringA value = line.Substring(pos + 1);
            Module::child_list_type::iterator ano_end = this->ChildList_End();
            for (Module::child_list_type::iterator ano_i = this->ChildList_Begin(); ano_i != ano_end; ++ano_i) {
                std::shared_ptr<core::param::ParamSlot> p = std::dynamic_pointer_cast<core::param::ParamSlot>(*ano_i);
                if (p && p->Name().Equals(name)) {
                    p->Parameter()->ParseValue(value);
                }
            }
        }
        inFile.Close();
    } catch (...) {

    }

    return true;
}


/*
 * visualization::LinearTransferFunction::requestTF
 */
bool visualization::LinearTransferFunction::requestTF(core::Call& call) {
    core::view::CallGetTransferFunction *cgtf = dynamic_cast<core::view::CallGetTransferFunction*>(&call);
    if (cgtf == NULL) return false;

    if (firstRequest) {
        loadTFPressed(loadTFSlot);
        firstRequest = false;
    }

    bool dirty = this->minColSlot.IsDirty() || this->maxColSlot.IsDirty() || this->texSizeSlot.IsDirty();
    for (SIZE_T i = 0; !dirty && (i < INTER_COLOUR_COUNT); i++) {
        dirty = this->interCols[i].enableSlot->IsDirty()
            || this->interCols[i].colSlot->IsDirty()
            || this->interCols[i].valSlot->IsDirty();
    }
    if ((this->texID == 0) || dirty) {
        this->minColSlot.ResetDirty();
        this->maxColSlot.ResetDirty();
        this->texSizeSlot.ResetDirty();
        for (SIZE_T i = 0; i < INTER_COLOUR_COUNT; i++) {
            this->interCols[i].enableSlot->ResetDirty();
            this->interCols[i].colSlot->ResetDirty();
            this->interCols[i].valSlot->ResetDirty();
        }

        bool t1de = (glIsEnabled(GL_TEXTURE_1D) == GL_TRUE);
        if (!t1de) glEnable(GL_TEXTURE_1D);
        if (this->texID == 0) glGenTextures(1, &this->texID);

        vislib::Array<vislib::math::Vector<float, 5> > cols;
        vislib::math::Vector<float, 5> cx1, cx2;
        bool validAlpha = false;
        if (megamol::core::utility::ColourParser::FromString(
                this->minColSlot.Param<core::param::StringParam>()->Value(),
                cx1[0], cx1[1], cx1[2], cx1[3])) {
            cx1[4] = inPartMinVal;
        } else {
            cx1[0] = cx1[1] = cx1[2] = cx1[3] = 0.0f;
            cx1[4] = inPartMinVal;
        }
        if (cx1[3] < 0.99f) validAlpha = true;
        cols.Add(cx1);
        if (megamol::core::utility::ColourParser::FromString(
                this->maxColSlot.Param<core::param::StringParam>()->Value(),
                cx1[0], cx1[1], cx1[2], cx1[3])) {
            cx1[4] = inPartMaxVal;
        } else {
            cx1[0] = cx1[1] = cx1[2] = cx1[3] = 0.0f;
            cx1[4] = inPartMaxVal;
        }
        if (cx1[3] < 0.99f) validAlpha = true;
        cols.Add(cx1);
        for (SIZE_T i = 0; i < INTER_COLOUR_COUNT; i++) {
            if (this->interCols[i].enableSlot->Param<core::param::BoolParam>()->Value()) {
                float val = this->interCols[i].valSlot->Param<core::param::FloatParam>()->Value();
                if (megamol::core::utility::ColourParser::FromString(
                        this->interCols[i].colSlot->Param<core::param::StringParam>()->Value(),
                        cx1[0], cx1[1], cx1[2], cx1[3])) {
                    cx1[4] = val;
                } else {
                    cx1[0] = cx1[1] = cx1[2] = cx1[3] = 0.0f;
                    cx1[4] = val;
                }
                if (cx1[3] < 0.99f) validAlpha = true;

                if ((inPartMinVal < cx1[4]) && (cx1[4] < inPartMaxVal)) cols.Add(cx1);
            }
        }

        cols.Sort(&InterColourComparer);
        for (SIZE_T i = 0; i < cols.Count(); ++i) {
            cols[i][4] = (cols[i][4] - inPartMinVal) / (inPartMaxVal - inPartMinVal);
        }

        this->texSize = this->texSizeSlot.Param<core::param::IntParam>()->Value();
        this->tex = new float[4 * this->texSize];
        int p1, p2;

        cx2 = cols[0];
        p2 = 0;
        for (SIZE_T i = 1; i < cols.Count(); i++) {
            cx1 = cx2;
            p1 = p2;
            cx2 = cols[i];
            ASSERT(cx2[4] <= 1.0f + vislib::math::FLOAT_EPSILON);
            p2 = static_cast<int>(cx2[4] * static_cast<float>(this->texSize - 1));
            ASSERT(p2 < static_cast<int>(this->texSize));
            ASSERT(p2 >= p1);

            for (int p = p1; p <= p2; p++) {
                float al = static_cast<float>(p - p1) / static_cast<float>(p2 - p1);
                float be = 1.0f - al;

                this->tex[p * 4] = cx1[0] * be + cx2[0] * al;
                this->tex[p * 4 + 1] = cx1[1] * be + cx2[1] * al;
                this->tex[p * 4 + 2] = cx1[2] * be + cx2[2] * al;
                this->tex[p * 4 + 3] = cx1[3] * be + cx2[3] * al;
            }
        }

        GLint otid = 0;
        glGetIntegerv(GL_TEXTURE_BINDING_1D, &otid);
        glBindTexture(GL_TEXTURE_1D, this->texID);

        glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA, this->texSize, 0, GL_RGBA, GL_FLOAT, tex);

        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);

        glBindTexture(GL_TEXTURE_1D, otid);

        if (!t1de) glDisable(GL_TEXTURE_1D);

        this->texFormat = validAlpha
            ? core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGBA
            : core::view::CallGetTransferFunction::TEXTURE_FORMAT_RGB;

    }

    cgtf->SetTexture(this->texID, this->texSize, this->tex, this->texFormat);

    return true;
}


/*
* visualization::LinearTransferFunction::storeTFPressed
*/
bool visualization::LinearTransferFunction::storeTFPressed(core::param::ParamSlot& param) {

    try {
        vislib::sys::BufferedFile outFile;
        if (!outFile.Open(pathSlot.Param<core::param::FilePathParam>()->Value(),
            vislib::sys::File::AccessMode::WRITE_ONLY,
            vislib::sys::File::ShareMode::SHARE_EXCLUSIVE,
            vislib::sys::File::CreationMode::CREATE_OVERWRITE)) {
            vislib::sys::Log::DefaultLog.WriteMsg(
                vislib::sys::Log::LEVEL_WARN,
                "Unable to create TF file.");
        }

        Module::child_list_type::iterator ano_end = this->ChildList_End();
        for (Module::child_list_type::iterator ano_i = this->ChildList_Begin(); ano_i != ano_end; ++ano_i) {
            std::shared_ptr<core::param::ParamSlot> p = std::dynamic_pointer_cast<core::param::ParamSlot>(*ano_i);
            if (p && !p->Name().Equals("filepath")) {
                writeParameterFileParameter(*p, outFile);
            }
        }
        outFile.Close();
    } catch (...) {

    }

    return true;
}


/*
* visualization::LinearTransferFunction::writeParameterFileParameter
*/
void visualization::LinearTransferFunction::writeParameterFileParameter(
    core::param::ParamSlot& param,
    vislib::sys::BufferedFile &outFile) {

    unsigned int len = 0;
    vislib::RawStorage store;
    param.Parameter()->Definition(store);

    if (::memcmp(store, "MMBUTN", 6) == 0) {
        outFile.Write("# ", 2);
    }

    vislib::sys::WriteFormattedLineToFile(outFile,
        "%s=%s\n", param.Name().PeekBuffer(), vislib::StringA(param.Parameter()->ValueString()).PeekBuffer());
}

bool visualization::LinearTransferFunction::outPartDataGetData(core::Call& call) {
    using core::moldyn::MultiParticleDataCall;
    MultiParticleDataCall *oc = dynamic_cast<MultiParticleDataCall*>(&call);
    if (oc == nullptr) return false;
    MultiParticleDataCall *ic = inPartData.CallAs<MultiParticleDataCall>();
    if (ic == nullptr) return false;

    *ic = *oc;
    if (!(*ic)(0)) return false;
    *oc = *ic;
    ic->SetUnlocker(nullptr, false);

    texSizeSlot.ForceSetDirty();
    if (oc->GetParticleListCount() > 0) {
        const auto& p0 = oc->AccessParticles(0);
        if (p0.GetColourDataType() == core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_I) {
            inPartMinVal = p0.GetMinColourIndexValue();
            inPartMaxVal = p0.GetMaxColourIndexValue();
        }
    }

    return true;
}

bool visualization::LinearTransferFunction::outPartDataGetExtent(core::Call& call) {
    using core::moldyn::MultiParticleDataCall;
    MultiParticleDataCall *oc = dynamic_cast<MultiParticleDataCall*>(&call);
    if (oc == nullptr) return false;
    MultiParticleDataCall *ic = inPartData.CallAs<MultiParticleDataCall>();
    if (ic == nullptr) return false;

    *ic = *oc;
    if (!(*ic)(1)) return false;
    *oc = *ic;
    ic->SetUnlocker(nullptr, false);

    texSizeSlot.ForceSetDirty();

    return true;
}
