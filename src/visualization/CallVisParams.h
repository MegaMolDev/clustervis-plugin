/*
 * CallVisParams.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_CALLVISPARAMS_H_INCLUDED
#define MEGAMOLVPC_CALLVISPARAMS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Call.h"
#include "mmcore/factories/CallAutoDescription.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/** Shared parameters for consistent visualization */
	struct VisualizationParams {
		/** The number of clusters (needed for clipping of drawOnlyClusterId) */
		unsigned int numberClusters;

		/** Is transferfunction normal or cycle? */
		bool cycleTransferFunction;

		/** Draw only one cluster of specified id or all (-1) */
		int drawOnlyClusterId;

		/** Ctor */
		VisualizationParams(unsigned int numberClusters = 0, bool cycleTransferFunction = true, int drawOnlyClusterId = -1) : 
			numberClusters(numberClusters), 
			cycleTransferFunction(cycleTransferFunction), 
			drawOnlyClusterId(drawOnlyClusterId) {
		}
	};

	class CallVisParams : public core::Call {
	public:
		/// Index of the 'GetVisParams' function
		static const unsigned int CallForGetVisParams;

		/// Index of the 'SetVisParams' function
		static const unsigned int CallForSetVisParams;

		/**
		 * Answer the name of the objects of this description.
		 *
		 * @return The name of the objects of this description.
		 */
		static const char *ClassName(void) {
			return "CallVisParams";
		}

		/**
		 * Gets a human readable description of the module.
		 *
		 * @return A human readable description of the module.
		 */
		static const char *Description(void) {
			return "Call to transmit visualization parameters";
		}

		/**
		 * Get the shared parameters of visualization
		 *
		 * @return The shared parameters of visualization
		 */
		inline const VisualizationParams GetVisParams() {
			return this->visParams;
		}

		/**
		 * Set the shared parameters of visualization
		 *
		 * @param The shared parameters of visualization
		 */
		inline void SetVisParams(VisualizationParams visParams) {
			this->visParams = visParams;
		}

		/**
		 * Answer the number of functions used for this call.
		 *
		 * @return The number of functions used for this call.
		 */
		static unsigned int FunctionCount(void) {
			return 2;
		}

		/**
		 * Answer the name of the function used for this call.
		 *
		 * @param idx The index of the function to return it's name.
		 *
		 * @return The name of the requested function.
		 */
		static const char * FunctionName(unsigned int idx) {
			switch(idx) {
			case 0:
				return "getVisParams";
			case 1:
				return "setVisParams";
			}
			return "";
		}

		/** Ctor. */
		CallVisParams(void);
		
		/** Dtor. */
		virtual ~CallVisParams(void);

	private:
		/** The shared paramters for a consistent visualization */
		VisualizationParams visParams;
	};

typedef core::factories::CallAutoDescription<CallVisParams> CallVisParamsDescription;

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif // MEGAMOLVPC_CALLVISPARAMS_H_INCLUDED