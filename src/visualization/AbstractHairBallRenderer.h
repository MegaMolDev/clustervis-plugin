/*
 * AbstractHairBallRenderer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_ABSTRACTHAIRBALLRENDERER_H_INCLUDED
#define MEGAMOLVPC_ABSTRACTHAIRBALLRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/view/Renderer3DModule.h"
#include "mmcore/Call.h"
#include "mmcore/view/CallRender3D.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/param/ParamSlot.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/BoolParam.h"
#include "MultiParticleClusterDataCall.h"
#include "CallVisParams.h"
#include "RenderHelper.h"
#include "ANN/ANN.h"
#include <boost/pending/disjoint_sets.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/incremental_components.hpp>
#include <boost/foreach.hpp>
#include <queue>

//#define CONNECT_TO_MEAN
//#define TIME_MEASUREMENT

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	typedef boost::adjacency_list <boost::vecS, boost::vecS, boost::undirectedS> Graph;
	typedef boost::graph_traits<Graph>::vertex_descriptor Cluster;
	typedef boost::graph_traits<Graph>::vertices_size_type ClusterIndex;
	typedef boost::component_index<ClusterIndex> Components;

    /**
     * Renderer for hairballs
     */
    class AbstractHairBallRenderer : public core::view::Renderer3DModule {
    public:
        /** Ctor. */
        AbstractHairBallRenderer(void);

        /** Dtor. */
        virtual ~AbstractHairBallRenderer(void);

    protected:
		/**
         * The get capabilities callback. The module should set the members
         * of 'call' to tell the caller its capabilities.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetCapabilities(core::Call& call);
		
		/**
         * The get extents callback. The module should set the members of
         * 'call' to tell the caller the extents of its data (bounding boxes
         * and times).
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetExtents(core::Call& call);


		MultiParticleClusterDataCall *getData(unsigned int t, float& outScaling);

		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);
		
        /**
         * Implementation of 'Release'.
         */
        virtual void release(void);

        /**
         * The render callback.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool Render(core::Call& call);
		
		/**
         * Search of the nearest Neighbours to create the hairball (SimpleHairball, Hairball or HairballCylinder)
         *
         * @param mpcdc The clusterdata call.
		 * @param result The hairball
         */
		template <typename Result>
		void findNearestNeighbours(MultiParticleClusterDataCall* mpcdc, Result& result);
		
		/** Check whether relevant data or parameter changed (Need to recreate hairball?) */
		bool dataChanged(MultiParticleClusterDataCall* mpcdc);

		inline const core::param::ParamSlot& getNumberNeighboursSlot() const {
			return this->numberNeighboursSlot;
		}
		inline core::CallerSlot& getTFSlot() {
			return this->tfSlot;
		}
		inline const unsigned int getGreyTF() const {
			return this->greyTF;
		}
		inline core::CallerSlot& getVisParamsSlot() {
			return this->sharedVisParamsSlot;
		}

		/** Nested class representing an edge in the hairball */
		class ClusterComponentEdge {
		public:
			/** 
			  * Ctor.
			  *
			  * @param startId	ID of component containing startVertex
			  * @param endId	ID of component containing endVertex
			  * @param startVertex	Position of start
			  * @param endVertex	Position of end
			  */
			ClusterComponentEdge(unsigned int startId, unsigned int endId, vislib::math::Vector<float,3> startVertex, vislib::math::Vector<float,3> endVertex) : 
				startId(startId), endId(endId), startVertex(startVertex), endVertex(endVertex) {};
		
			/** Answer length of edge */
			const float Length() const {
				return (this->startVertex - this->endVertex).Length();
			}

			/**
			  * Get ID of component containing startVertex
			  *
			  * @return	ID of component containing startVertex
			  */
			const unsigned int GetStartId() const {
				return this->startId;
			}

			/**
			  * Get ID of component containing endVertex
			  *
			  * @return	ID of component containing endVertex
			  */
			const unsigned int GetEndId() const {
				return this->endId;
			}

			/**
			  * Get position of startVertex
			  *
			  * @return	position of startVertex
			  */
			const vislib::math::Vector<float,3>& GetStartVertex() const {
				return this->startVertex;
			}

			/**
			  * Get position of endVertex
			  *
			  * @return	position of endVertex
			  */
			const vislib::math::Vector<float,3>& GetEndVertex() const {
				return this->endVertex;
			}

		private:
			/** ID of component containing startVertex */
			unsigned int startId;

			/** ID of component containing endVertex */
			unsigned int endId;

			/** Position of startVertex */
			vislib::math::Vector<float,3> startVertex;

			/** Position of endVertex */
			vislib::math::Vector<float,3> endVertex;
		};

		/** Nested class for comparison of edges */
		class ComparecomponentEdges {
		public:
			/** Check whether lhs is longer than rhs */
			bool operator() (const ClusterComponentEdge& lhs, const ClusterComponentEdge& rhs) {
				return lhs.Length() > rhs.Length();
			}
		};

	private:
		/** Rendering of thinned out hairball */
		virtual bool renderHairballNeighbours(core::view::CallRender3D *cr, MultiParticleClusterDataCall* mpcdc) = 0;
	
	#ifdef CONNECT_TO_MEAN
		/** Rendering of hairball - all particles connected to mean of cluster */
		void renderHairballMeans(MultiParticleClusterDataCall* mpcdc);
	#endif

		/** 
		  * Connect hairball components of one cluster
		  *
		  * @param components Components of hairball for the cluster
		  * @param dataPts All ANNpoints for all clusters
		  * @param clusterIdx Index of cluster
		  * @param result Hairball
		  */
		template <typename Result>
		void connectComponents(Components &components, std::vector<std::vector<ANNpoint> > &dataPts, unsigned int clusterIdx, Result& result);
		
		//ClusterComponentEdge findNearestNeighboursBetweenComponents(const std::vector<std::vector<ClusterIndex> >& component, std::vector<ClusterIndex>& representatives, unsigned int component1Id, unsigned int component2Id, std::vector<ANNpoint>& dataPts, std::unique_ptr<ANNkd_tree>& kdTree); // too slow
		
		/** 
		  * Search for approximate nearest neighbours of components
		  *
		  * @param componentDataPts All ANNpoints of all components of the cluster
		  * @param componentKdTree Kd-trees of all components
		  * @param component1Id ID of component 1
		  * @param component2Id ID of component 2
		  *
		  * @return Shortest edge between component 1 and 2
		  */
		ClusterComponentEdge findNearestNeighboursBetweenComponents(std::vector<std::vector<ANNpoint> >& componentDataPts, 
														std::vector<std::unique_ptr<ANNkd_tree> >& componentKdTree, 
														unsigned int component1Id, unsigned int component2Id);
		/** The call for data */
        core::CallerSlot getDataSlot;

		/** The call for Transfer function */
        core::CallerSlot tfSlot;

		/** The call for shared visualization parameters */
		core::CallerSlot sharedVisParamsSlot;

		/** The number of nearest neighbours to connect while computation of hairball */
		core::param::ParamSlot numberNeighboursSlot;

		/** Checkbox for connection of hairball components */
		core::param::ParamSlot connectAllComponentsSlot;
	
	#ifdef CONNECT_TO_MEAN
		/** Checkbox for drawing normal hairball or hairball where particles are connected to mean */
		core::param::ParamSlot drawHairballMeansSlot;

		/** Offset to mean */
		core::param::ParamSlot drawHairballMeansHeightSlot;
	#endif

        /** A simple black-to-white transfer function texture as fallback */
        unsigned int greyTF;

	#ifdef CONNECT_TO_MEAN
		/** Mean positions */
		std::vector<std::vector<float> > meanPositions;

		/** Count of particles corresponding to the means */
		std::vector<float> meanCount;
	#endif

		/** The last frame */
		unsigned int lastFrame;

		/** The last hash */
        size_t lastHash;
    };

	template <typename Result>
	void visualization::AbstractHairBallRenderer::findNearestNeighbours(MultiParticleClusterDataCall* mpcdc, Result& result) {
		MultiParticleClusterDataCall::Particles &pl = mpcdc->AccessParticles(0);

		UINT64 particleCount = pl.GetCount();
		unsigned int numberClusters = pl.GetNumberClusters();

		// init ANN
		std::vector<std::vector<ANNcoord> > dataPtsData(numberClusters);
		std::vector<std::vector<ANNpoint> > dataPts(numberClusters);
		std::vector<std::unique_ptr<ANNkd_tree> > kdTree(numberClusters);
	
		size_t vert_stride = pl.GetVertexDataStride();
		const uint8_t *vert = static_cast<const uint8_t*>(pl.GetVertexData());
		if (vert_stride < 12) vert_stride = 12;
		unsigned int clusterStride = pl.GetClusterDataStride();
		const uint8_t * cluster = static_cast<const uint8_t*>(pl.GetClusterData());

		for (uint64_t pi = 0; pi < pl.GetCount(); pi++, vert += vert_stride, cluster += clusterStride) {
			unsigned int currentCluster = reinterpret_cast<const unsigned int*>(cluster)[0];
			dataPtsData[currentCluster].push_back(static_cast<ANNcoord>(reinterpret_cast<const float*>(vert)[0]));
			dataPtsData[currentCluster].push_back(static_cast<ANNcoord>(reinterpret_cast<const float*>(vert)[1]));
			dataPtsData[currentCluster].push_back(static_cast<ANNcoord>(reinterpret_cast<const float*>(vert)[2]));
		}

		// Problem: Adressen k�nnen sich bei resize des vector �ndern! -> danach in extra schleife
		for (unsigned int cl = 0; cl < numberClusters; ++cl) {
			unsigned int numberParticles = dataPtsData[cl].size()/3;
			for (unsigned int pi = 0; pi < numberParticles; pi++) {
				dataPts[cl].push_back(&dataPtsData[cl][3*pi]); 
			}
		}

		for (unsigned int cl = 0; cl < numberClusters; ++cl) {
			kdTree[cl].reset(new ANNkd_tree(dataPts[cl].data(), static_cast<int>(dataPts[cl].size()), 3));
		}

		int k = this->getNumberNeighboursSlot().Param<core::param::IntParam>()->Value() + 1;
		ANNidxArray indices = new ANNidx[k];
		ANNdistArray distances = new ANNdist[k];

		result.clear();

		// prepare union-find-structure
		bool useUnionFind = this->connectAllComponentsSlot.Param<core::param::BoolParam>()->Value(); // connect all components?

		std::vector<Graph> clusterGraph(numberClusters);

		std::vector<std::vector<ClusterIndex> > rank(numberClusters);
		std::vector<std::vector<Cluster> > parent(numberClusters);
	
		std::vector<boost::disjoint_sets<ClusterIndex*, Cluster*> > clusterComponents;
		clusterComponents.reserve(numberClusters);

		if (useUnionFind) {
			for (unsigned int cl = 0; cl < numberClusters; ++cl) {
				clusterGraph[cl] = Graph(dataPts[cl].size());
		
				rank[cl].resize(boost::num_vertices(clusterGraph[cl]));
				parent[cl].resize(boost::num_vertices(clusterGraph[cl]));

				unsigned int numberParticles = dataPts[cl].size();
		
				clusterComponents.push_back(boost::disjoint_sets<ClusterIndex*, Cluster*>(&rank[cl][0], &parent[cl][0]));
				boost::initialize_incremental_components(clusterGraph[cl], clusterComponents[cl]);
				boost::incremental_components(clusterGraph[cl], clusterComponents[cl]);
			}
		}

		boost::graph_traits<Graph>::edge_descriptor Edge;
		bool flag;

		// compute hairball
		for (unsigned int cl = 0; cl < numberClusters; ++cl) {
			unsigned int numberParticles = dataPts[cl].size();
			int usedK = std::min<int>(k, numberParticles); // if k is used and k > numberParticles not enough neighbours -> problems with access to vector (index out of range) 
			for (unsigned int pi = 0; pi < numberParticles; pi++) {
				kdTree[cl]->annkPriSearch(dataPts[cl][pi], usedK, indices, distances);

				for(int j=1; j < usedK; ++j) { // j=0 -> particle itself -> has min dist to itself 
					ANNidx idx = indices[j];
					
					vislib::math::Vector<float,3> vec1(	static_cast<float>(dataPts[cl][pi][0]),
														static_cast<float>(dataPts[cl][pi][1]),
														static_cast<float>(dataPts[cl][pi][2]));
					vislib::math::Vector<float,3> vec2(	static_cast<float>(dataPts[cl][idx][0]),
														static_cast<float>(dataPts[cl][idx][1]),
														static_cast<float>(dataPts[cl][idx][2]));

					result.addLine(vec1, vec2, cl);

					if (useUnionFind) {
						boost::tie(Edge, flag) = boost::add_edge(pi, static_cast<unsigned int>(idx), clusterGraph[cl]);
						clusterComponents[cl].union_set(pi, static_cast<unsigned int>(idx));
					}
				}
			}

			// Connect all components
			if (useUnionFind) {
				Components components(parent[cl].begin(), parent[cl].end());
				connectComponents(components, dataPts, cl, result);
			}
		}

		delete[] indices;
		delete[] distances;

		annClose();
	}

	template <typename Result>
	void visualization::AbstractHairBallRenderer::connectComponents(Components &components, std::vector<std::vector<ANNpoint> > &dataPts, unsigned int clusterIdx, Result& result) {
		unsigned int numberComponents = components.size();
		//std::cout << "Cluster:" << clusterIdx << ", Components: " << numberComponents << std::endl;
		if (numberComponents > 1) {
			// init ann - kd-tree containing all elements of one component and the representatives of all components
			std::vector<ClusterIndex> representatives;
			representatives.reserve(numberComponents);
			std::vector<std::vector<ANNpoint> > componentDataPts(numberComponents, std::vector<ANNpoint>(numberComponents));
			std::vector<std::unique_ptr<ANNkd_tree> > componentKdTree(numberComponents);
					
			BOOST_FOREACH(ClusterIndex currentIndex, components) {
				representatives.push_back(*components[currentIndex].first);
						
				// prepend all representatives
				BOOST_FOREACH(ClusterIndex currentIndex2, components) {
					componentDataPts[currentIndex2][currentIndex] = dataPts[clusterIdx][representatives[currentIndex]];
				}

				BOOST_FOREACH(ClusterIndex childIndex, components[currentIndex]) {
					componentDataPts[currentIndex].push_back(dataPts[clusterIdx][childIndex]);
				}
			}

			BOOST_FOREACH(ClusterIndex currentIndex, components) {
				componentKdTree[currentIndex].reset(new ANNkd_tree(componentDataPts[currentIndex].data(), static_cast<int>(componentDataPts[currentIndex].size()), 3));
			}

			// init union-find-structure - components should be connected only once
			Graph componentGraph(numberComponents);
			boost::graph_traits<Graph>::edge_descriptor Edge;
			bool flag;

			std::vector<ClusterIndex> componentRank(boost::num_vertices(componentGraph));
			std::vector<Cluster> componentParent(boost::num_vertices(componentGraph));
	
			boost::disjoint_sets<ClusterIndex*, Cluster*> currentComponents(&componentRank[0], &componentParent[0]);

			boost::initialize_incremental_components(componentGraph, currentComponents);
			boost::incremental_components(componentGraph, currentComponents);

			std::priority_queue<ClusterComponentEdge, std::vector<ClusterComponentEdge>, ComparecomponentEdges> Q;

			// compute possible edges
			for (unsigned int comp0 = 0; comp0 < numberComponents - 1; ++comp0) {
				unsigned int v0 = representatives[comp0];
				vislib::math::Vector<float,3> start(static_cast<float>(dataPts[clusterIdx][v0][0]),
													static_cast<float>(dataPts[clusterIdx][v0][1]),
													static_cast<float>(dataPts[clusterIdx][v0][2]));
				for (unsigned int comp1 = comp0 + 1; comp1 < numberComponents; ++comp1) {
					unsigned int v1 = representatives[comp1];
					vislib::math::Vector<float,3> end(	static_cast<float>(dataPts[clusterIdx][v1][0]),
														static_cast<float>(dataPts[clusterIdx][v1][1]),
														static_cast<float>(dataPts[clusterIdx][v1][2]));
					ClusterComponentEdge tmp(findNearestNeighboursBetweenComponents(componentDataPts, componentKdTree, comp0, comp1));
					Q.push(tmp);
				}
			}

			// add shortest edges to connect all components
			while (!Q.empty()) {
				ClusterComponentEdge cce = Q.top();
				Q.pop();
				Cluster u = currentComponents.find_set(cce.GetStartId());
				Cluster v = currentComponents.find_set(cce.GetEndId());
				if ( u != v ) {
					boost::tie(Edge, flag) = add_edge(cce.GetStartId(), cce.GetEndId(), componentGraph);	// does this have any effect?
					result.addLine(cce.GetStartVertex(), cce.GetEndVertex(), clusterIdx);
					currentComponents.link(u, v);
				}
			}
		}
	}

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_ABSTRACTHAIRBALLRENDERER_H_INCLUDED */
