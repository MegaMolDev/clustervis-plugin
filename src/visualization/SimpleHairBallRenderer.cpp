/*
 * SimpleHairBallRenderer.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "SimpleHairBallRenderer.h"
#include "mmcore/CoreInstance.h"
#include "mmcore/view/CallGetTransferFunction.h"
#include "mmcore/param/IntParam.h"
#include "mmcore/param/FloatParam.h"
#include "mmcore/param/BoolParam.h"
#include "mmcore/param/StringParam.h"
#include "mmcore/utility/ColourParser.h"
#include "vislib/graphics/gl/IncludeAllGL.h"
#include "vislib/assert.h"
#include "vislib/math/Matrix.h"
#include "vislib/math/ShallowMatrix.h"
#include <memory>

using namespace megamol;
using namespace megamol::vis_particle_clustering;

/*
 * visualization::SimpleHairBallRenderer::SimpleHairBallRenderer
 */
visualization::SimpleHairBallRenderer::SimpleHairBallRenderer(void) : AbstractHairBallRenderer(),
		lineWidthSlot("linewidth", "The width of the hairball-lines"),
		useHaloSlot("useHalo", "Determines whether to draw halos or not"),
		haloLineWidthRatioSlot("halolinewidthratio", "The ration between linewidth and the width of the halo effect"),
		haloColourSlot("halocolour", "The colour of the halo effect") {
    this->lineWidthSlot.SetParameter(new core::param::FloatParam(1.0f, 0.1f));
	this->MakeSlotAvailable(&this->lineWidthSlot);
	
	this->useHaloSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&useHaloSlot);

	this->haloLineWidthRatioSlot.SetParameter(new core::param::FloatParam(3.0f,0.1f));
	this->MakeSlotAvailable(&this->haloLineWidthRatioSlot);

	this->haloColourSlot.SetParameter(new core::param::StringParam("#000000"));
	this->MakeSlotAvailable(&this->haloColourSlot);
}

/*
 * visualization::SimpleHairBallRenderer::~SimpleHairBallRenderer
 */
visualization::SimpleHairBallRenderer::~SimpleHairBallRenderer(void) {
    this->Release();
}

/*
 * visualization::SimpleHairBallRenderer::createHairballShader
 */
bool visualization::SimpleHairBallRenderer::create(void) {
	ASSERT(IsAvailable());

    CreateShader(this->instance(), this->hairballShader, "simplehairball");

    return AbstractHairBallRenderer::create();
}

/*
 * visualization::SimpleHairBallRenderer::release
 */
void visualization::SimpleHairBallRenderer::release(void) {
	this->hairballShader.Release();
    AbstractHairBallRenderer::release();
}

bool visualization::SimpleHairBallRenderer::renderHairballNeighbours(core::view::CallRender3D *cr, MultiParticleClusterDataCall*mpcdc) {
	// already tested that cr and mpcdc != NULL

	bool usedStoredData = true;
	if (dataChanged(mpcdc)) { // nicht ganz sauber, aber da in der Regel alles glatt geht wei� ich so ob gespeicherte Sachen genutzt wurden oder neuberechnet wurde
		findNearestNeighbours(mpcdc, result);
		usedStoredData = false;
	}

	unsigned int numberClusters = mpcdc->AccessParticles(0).GetNumberClusters();
	CallVisParams *vp = this->getVisParamsSlot().CallAs<CallVisParams>();
	int drawClusterOfIdx = GetDrawClusterOfIdx(vp, numberClusters);
	bool cycleTransferFunction = GetCycleTransferFunction(vp, numberClusters);

	glDisable(GL_BLEND);

	// init shader
	this->hairballShader.Enable();
	core::view::CallGetTransferFunction *cgtf = this->getTFSlot().CallAs<core::view::CallGetTransferFunction>();
	SetMVPMatTextureParams(this->hairballShader, mpcdc, cgtf, this->getGreyTF());
	glUniform1f(this->hairballShader.ParameterLocation("drawClusterOfIdx"), static_cast<float>(drawClusterOfIdx)); // draw only lines of one cluster
	glUniform1f(this->hairballShader.ParameterLocation("numberCluster"), static_cast<float>(numberClusters));
	glUniform1f(this->hairballShader.ParameterLocation("cycleTransferFunction"), static_cast<float>(cycleTransferFunction));

	GLint vertexPos = glGetAttribLocation(this->hairballShader, "vertex"); // position
    GLint clusterIDs = glGetAttribLocationARB(this->hairballShader, "colIdx"); // used to get the right colour
	
	glEnableVertexAttribArray(vertexPos);
	glEnableVertexAttribArrayARB(clusterIDs);
    
	glVertexAttribPointer(vertexPos, 3, GL_FLOAT, GL_FALSE, 0, result.positions.data());
	glVertexAttribPointerARB(clusterIDs, 1, GL_FLOAT, GL_FALSE, 0, result.clusterIds.data());
	
	// draw halos
	if (this->useHaloSlot.Param<core::param::BoolParam>()->Value()) {
		float haloDistance = 0.0001f;
		vislib::math::Vector<vislib::graphics::SceneSpaceType,3> camIn = cr->GetCameraParameters()->Front();
		camIn.Normalise();
		camIn *= haloDistance;
		glUniform3fv(this->hairballShader.ParameterLocation("camIn"), 1, camIn.PeekComponents());
		glUniform1i(this->hairballShader.ParameterLocation("isHalo"), 1);
		float r, g, b;
		core::utility::ColourParser::FromString(this->haloColourSlot.Param<core::param::StringParam>()->Value(), r, g, b);
		glUniform3f(this->hairballShader.ParameterLocation("haloColour"), r, g, b);
		glLineWidth(this->haloLineWidthRatioSlot.Param<core::param::FloatParam>()->Value() * this->lineWidthSlot.Param<core::param::FloatParam>()->Value());
		glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(result.positions.size()/3));
	}

	// draw hairball
	glUniform3f(this->hairballShader.ParameterLocation("camIn"), 0.0f, 0.0f, 0.0f);
	glUniform1i(this->hairballShader.ParameterLocation("isHalo"), 0);
	glLineWidth(this->lineWidthSlot.Param<core::param::FloatParam>()->Value());
	glDrawArrays(GL_LINES, 0, static_cast<GLsizei>(result.positions.size()/3));

    glDisableVertexAttribArray(vertexPos);
    glDisableVertexAttribArrayARB(clusterIDs);
    glDisable(GL_TEXTURE_1D);
    
	this->hairballShader.Disable();

	return usedStoredData;
}
