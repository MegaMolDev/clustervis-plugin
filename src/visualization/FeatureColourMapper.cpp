/*
 * FeatureColourMapper.cpp
 *
 * Author: Julia B�hnke
 */

#include "stdafx.h"
#include "FeatureColourMapper.h"
#include "CallFeatureParams.h"
#include "RenderHelper.h"
#include "mmcore/param/EnumParam.h"
#include "mmcore/param/BoolParam.h"

using namespace megamol;
using namespace megamol::vis_particle_clustering;

visualization::FeatureColourMapper::FeatureColourMapper(void) : Module(),
		putDataSlot("putdata", "Connects from the data consumer"),
		getDataSlot("getdata", "Connects to the data source"),
		featureParamsSlot("visParams", "Provides access to feature parameters"),
		useGlobalMaximaSlot("useglobalMaxima", "use global (or local) maxima?"),
		centerTransferZeroSlot("centertransferzero", "zero at value 0.5 of transferfunction"),
		whichFeatureSlot("which feature", "Which feature should be mapped."),
		lastFrame(0), lastHash(0), colData(), minValue(0.0f), maxValue(0.0f) {
	
	this->putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetData", &FeatureColourMapper::getDataCallback);
    this->putDataSlot.SetCallback(core::moldyn::MultiParticleDataCall::ClassName(), "GetExtent", &FeatureColourMapper::getExtentCallback);
    this->MakeSlotAvailable(&this->putDataSlot);

    this->getDataSlot.SetCompatibleCall<MultiParticleDataCallExtendedDescription>();
    this->MakeSlotAvailable(&this->getDataSlot);

	this->useGlobalMaximaSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&useGlobalMaximaSlot);

	this->centerTransferZeroSlot.SetParameter(new core::param::BoolParam(true));
	this->MakeSlotAvailable(&centerTransferZeroSlot);

	core::param::EnumParam *whichFeature = new core::param::EnumParam(VELOCITY);
	//whichFeature->SetTypePair(POSITION, "Position");
	whichFeature->SetTypePair(VELOCITY, "Velocity");
	whichFeature->SetTypePair(VELOCITYDIR, "Velocity direction");
	whichFeature->SetTypePair(VELOCITYX, "Velocity x");
	whichFeature->SetTypePair(VELOCITYY, "Velocity y");
	whichFeature->SetTypePair(VELOCITYZ, "Velocity z");
	whichFeature->SetTypePair(ROTATIONANGLE, "Rotationangle");
	whichFeature->SetTypePair(ROTATIONAXIS, "Rotationaxis");
	whichFeature->SetTypePair(ROTATIONAXISX, "Rotationaxis x");
	whichFeature->SetTypePair(ROTATIONAXISY, "Rotationaxis y");
	whichFeature->SetTypePair(ROTATIONAXISZ, "Rotationaxis z");
	this->whichFeatureSlot.SetParameter(whichFeature);
    this->MakeSlotAvailable(&this->whichFeatureSlot);

	this->featureParamsSlot.SetCallback(CallFeatureParams::ClassName(),
            CallFeatureParams::FunctionName(CallFeatureParams::CallForFeatureParams),
            &FeatureColourMapper::setFeatureParams);
    this->MakeSlotAvailable(&this->featureParamsSlot);
}


visualization::FeatureColourMapper::~FeatureColourMapper(void) {
	this->Release();
}

bool visualization::FeatureColourMapper::create(void) {
    return true;
}


void visualization::FeatureColourMapper::release(void) {
    // intentionally empty
}

bool visualization::FeatureColourMapper::getDataCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *inCall = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleDataCallExtended *outCall = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
    if (outCall == nullptr) return false;

    outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall

    if ((*outCall)(0)) { // GetDataCallback for outCall
        bool updataData = false;
		
		if ((this->lastFrame != outCall->FrameID()) || (this->lastHash != outCall->DataHash()) || (outCall->DataHash() == 0)) {
            this->lastFrame = outCall->FrameID();
            this->lastHash = outCall->DataHash();
			updataData = true;
        }

		if (this->whichFeatureSlot.IsDirty() || this->useGlobalMaximaSlot.IsDirty() || this->centerTransferZeroSlot.IsDirty()) {
			this->whichFeatureSlot.ResetDirty();
			this->useGlobalMaximaSlot.ResetDirty();
			this->centerTransferZeroSlot.ResetDirty();
			updataData = true;
		}

		if (updataData)	{
			this->colourParticles(outCall);
		}

		inCall->SetFrameID(outCall->FrameID());
        inCall->SetDataHash(outCall->DataHash());

        size_t cnt = 0;
        unsigned int plc = outCall->GetParticleListCount();
		inCall->SetParticleListCount(plc);
        for (unsigned int pli = 0; pli < plc; pli++) {
            core::moldyn::SimpleSphericalParticles &p = inCall->AccessParticles(pli);
			SphericalParticles &po = outCall->AccessParticles(pli);
            p.SetGlobalRadius(po.GetGlobalRadius());
			p.SetGlobalColour(po.GetGlobalColour()[0],po.GetGlobalColour()[1],po.GetGlobalColour()[2]);
            p.SetCount(po.GetCount());
			p.SetVertexData(po.GetVertexDataType(), po.GetVertexData(), po.GetVertexDataStride());
			if ((p.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_NONE)
                || (p.GetVertexDataType() == core::moldyn::SimpleSphericalParticles::VERTDATA_SHORT_XYZ)) continue;
			p.SetColourMapIndexValues(0.0f,1.0f);
			int currentFeature = this->whichFeatureSlot.Param<core::param::EnumParam>()->Value();
			if (currentFeature == POSITION || currentFeature == VELOCITYDIR || currentFeature == ROTATIONAXIS) {
				p.SetColourData(core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_RGB, this->colData.At(cnt * 3*sizeof(float)));
			} else { // use transferfunction
				p.SetColourData(core::moldyn::SimpleSphericalParticles::COLDATA_FLOAT_I, this->colData.At(cnt * sizeof(float)));
			}
            cnt += static_cast<size_t>(p.GetCount());
        }

		inCall->SetUnlocker(new Unlocker(outCall->GetUnlocker()), false);
        outCall->SetUnlocker(nullptr, false);

        return true;
    }

    return false;
}

bool visualization::FeatureColourMapper::getExtentCallback(core::Call& caller) {
    core::moldyn::MultiParticleDataCall *inCall = dynamic_cast<core::moldyn::MultiParticleDataCall*>(&caller);
    if (inCall == nullptr) return false;

    MultiParticleDataCallExtended *outCall = this->getDataSlot.CallAs<MultiParticleDataCallExtended>();
    if (outCall == nullptr) return false;

	outCall->SetFrameID(inCall->FrameID());	// to get next frame for outcall
	
    if ((*outCall)(1)) { // GetExtentCallback for outCall
		outCall->SetUnlocker(nullptr, false);

        inCall->SetFrameCount(outCall->FrameCount());
		inCall->AccessBoundingBoxes().Clear();
		inCall->AccessBoundingBoxes().SetObjectSpaceBBox(outCall->AccessBoundingBoxes().ObjectSpaceBBox());
		inCall->AccessBoundingBoxes().SetObjectSpaceClipBox(outCall->AccessBoundingBoxes().ObjectSpaceClipBox());
		inCall->SetDataHash(outCall->DataHash());

		return true;
	}

    return false;
}

#ifdef _MSC_VER
#pragma push_macro("max")
#undef max
#pragma push_macro("min")
#undef min
#endif /* _MSC_VER */

// Assumptions: only one particle list with core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ particles
void visualization::FeatureColourMapper::colourParticles(MultiParticleDataCallExtended *dat) {
	UINT64 all_cnt = dat->AccessParticles(0).GetCount();
    if (dat->GetParticleListCount() != 1 || dat->AccessParticles(0).GetVertexDataType() != core::moldyn::SimpleSphericalParticles::VERTDATA_FLOAT_XYZ) {
		return;
	}
    
	int currentFeature = this->whichFeatureSlot.Param<core::param::EnumParam>()->Value();
	
	if (currentFeature == POSITION || currentFeature == VELOCITYDIR || currentFeature == ROTATIONAXIS) {
		this->colData.EnforceSize(all_cnt * 3*sizeof(float)); // store COLDATA_FLOAT_RGB
	} else {
		this->colData.EnforceSize(all_cnt * sizeof(float)); // store COLDATA_FLOAT_I
	}
	float *f = this->colData.As<float>();
	
	MultiParticleDataCallExtended::Particles pl = dat->AccessParticles(0);

	unsigned int vert_stride = pl.GetVertexDataStride();
	const uint8_t *vert = static_cast<const uint8_t*>(pl.GetVertexData());
	if (vert_stride < 12) vert_stride = 12;

	unsigned int extraStride = pl.GetExtraMemberDataStride();
	const uint8_t *extra = static_cast<const uint8_t*>(pl.GetExtraMemberData());

	bool useGlobalMaxima = this->useGlobalMaximaSlot.Param<core::param::BoolParam>()->Value();
	vislib::math::Cuboid<float> bbox_temp = dat->AccessBoundingBoxes().ObjectSpaceBBox();
	vislib::math::Point<float,3> minPos = bbox_temp.GetLeftBottomBack();
	vislib::math::Point<float,3> maxPos = bbox_temp.GetRightTopFront();
	std::vector<float> minVel;
	std::vector<float> maxVel;
	float minVelLength;
	float maxVelLength;
	float minAngle;
	float maxAngle;
	if (useGlobalMaxima) {
		minVel = dat->GetMinVelocity();
		maxVel = dat->GetMaxVelocity();
		minVelLength = dat->GetMinVelocityLength();
		maxVelLength = dat->GetMaxVelocityLength();
		minAngle = dat->GetMinRotationAngle();
		maxAngle = dat->GetMaxRotationAngle();
	} else {
		minVel = pl.GetMinVelocity();
		maxVel = pl.GetMaxVelocity();
		minVelLength = pl.GetMinVelocityLength();
		maxVelLength = pl.GetMaxVelocityLength();
		minAngle = pl.GetMinRotationAngle();
		maxAngle = pl.GetMaxRotationAngle();
	}
	std::vector<float> delta(3);
	bool centerTransferZero = this->centerTransferZeroSlot.Param<core::param::BoolParam>()->Value();
	
	// TODO: cases in eigene Methoden auslagern (?)
	switch (currentFeature) {
		case POSITION: // not very useful ;)
			for (int c = 0; c<3; ++c) {
				delta[c] = maxPos[c] - minPos[c];
			}
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, vert += vert_stride) {
				for (int c = 0; c<3; ++c) {
					float position = reinterpret_cast<const float*>(vert)[c];
					f[3*pi + c] = (position - minPos[c]) / delta[c];
				}
			}
			break;
		case VELOCITY:
			delta[0] = maxVelLength - minVelLength;	// velLength always greater 0
			minValue = minVelLength;
			maxValue = maxVelLength;
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentVelocity(	reinterpret_cast<const float*>(extra)[0],
																reinterpret_cast<const float*>(extra)[1],
																reinterpret_cast<const float*>(extra)[2]);
				float velLength = currentVelocity.Length();
				float normalizedVelLength = (velLength - minVelLength) / delta[0];
				if (centerTransferZero) {
					normalizedVelLength *= 0.5f;
					normalizedVelLength += 0.5f;
				}
				f[pi] = normalizedVelLength;
			}
			break;
		case VELOCITYDIR:
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentVelocity(	reinterpret_cast<const float*>(extra)[0],
																reinterpret_cast<const float*>(extra)[1],
																reinterpret_cast<const float*>(extra)[2]);
				currentVelocity.Normalise();
				vislib::math::Vector<float,3> colour;
				getBaryCentricInterpolatedColour(currentVelocity.GetX(), currentVelocity.GetY(), currentVelocity.GetZ(), colour);
				
				for (int c = 0; c<3; ++c) {
					f[3*pi + c] = colour.PeekComponents()[c];
				}
			}
			break;
		case VELOCITYX:
			delta[0] = maxVel[0] - minVel[0];
			minValue = minVel[0];
			maxValue = maxVel[0];

			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				float velocity = reinterpret_cast<const float*>(extra)[0];
				if (centerTransferZero) {
					if (velocity >= 0.0f) {
						f[pi] = 0.5f + 0.5f * ((velocity) / maxVel[0]);
					} else {
						f[pi] = 0.5f * (velocity) / minVel[0];
					}
				} else {
					f[pi] = (velocity - minVel[0]) / delta[0];
				}
				
			}
			break;
		case VELOCITYY:
			delta[1] = maxVel[1] - minVel[1];
			minValue = minVel[1];
			maxValue = maxVel[1];

			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				float velocity = reinterpret_cast<const float*>(extra)[1];
				if (centerTransferZero) {
					if (velocity >= 0.0f) {
						f[pi] = 0.5f + 0.5f * ((velocity) / maxVel[1]);
					} else {
						f[pi] = 0.5f * (velocity) / minVel[1];
					}
				} else {
					f[pi] = (velocity - minVel[1]) / delta[1];
				}
				
			}
			break;
		case VELOCITYZ:
			delta[2] = maxVel[2] - minVel[2];
			minValue = minVel[2];
			maxValue = maxVel[2];

			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				float velocity = reinterpret_cast<const float*>(extra)[2];
				if (centerTransferZero) {
					if (velocity >= 0.0f) {
						f[pi] = 0.5f + 0.5f * ((velocity) / maxVel[2]);
					} else {
						f[pi] = 0.5f * (velocity) / minVel[2];
					}
				} else {
					f[pi] = (velocity - minVel[2]) / delta[2];
				}
				
			}
			break;
		case ROTATIONANGLE:
			delta[0] = maxAngle - minAngle;
			minValue = minAngle;
			maxValue = maxAngle;
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extra)[3],
																reinterpret_cast<const float*>(extra)[4],
																reinterpret_cast<const float*>(extra)[5]);
				float rotationAngle = currentRotationAxis.Normalise();
				float normalizedRotationAngle = (rotationAngle - minAngle) / delta[0];
				if (centerTransferZero) {
					normalizedRotationAngle *= 0.5f;
					normalizedRotationAngle += 0.5f;
				}
				f[pi] = normalizedRotationAngle;
			}
			break;
		case ROTATIONAXIS:
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extra)[3],
																reinterpret_cast<const float*>(extra)[4],
																reinterpret_cast<const float*>(extra)[5]);
				currentRotationAxis.Normalise();
				vislib::math::Vector<float,3> colour;
				getBaryCentricInterpolatedColour(currentRotationAxis.GetX(), currentRotationAxis.GetY(), currentRotationAxis.GetZ(), colour);
				
				for (int c = 0; c<3; ++c) {
					f[3*pi + c] = colour.PeekComponents()[c];
				}
			}
			break;
		case ROTATIONAXISX:
			minValue = -1.0f;
			maxValue = 1.0f;
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extra)[3],
																reinterpret_cast<const float*>(extra)[4],
																reinterpret_cast<const float*>(extra)[5]);
				currentRotationAxis.Normalise();
				f[pi] = (currentRotationAxis.PeekComponents()[0] + 1.0f) / 2.0f;
			}
			break;
		case ROTATIONAXISY:
			minValue = -1.0f;
			maxValue = 1.0f;
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extra)[3],
																reinterpret_cast<const float*>(extra)[4],
																reinterpret_cast<const float*>(extra)[5]);
				currentRotationAxis.Normalise();
				f[pi] = (currentRotationAxis.PeekComponents()[1] + 1.0f) / 2.0f;
			}
			break;
		case ROTATIONAXISZ:
			minValue = -1.0f;
			maxValue = 1.0f;
			for (uint64_t pi = 0; pi < pl.GetCount(); pi++, extra += extraStride) {
				vislib::math::Vector<float,3> currentRotationAxis(	reinterpret_cast<const float*>(extra)[3],
																reinterpret_cast<const float*>(extra)[4],
																reinterpret_cast<const float*>(extra)[5]);
				currentRotationAxis.Normalise();
				f[pi] = (currentRotationAxis.PeekComponents()[2] + 1.0f) / 2.0f;
			}
			break;
	}
}

#ifdef _MSC_VER
#pragma pop_macro("min")
#pragma pop_macro("max")
#endif /* _MSC_VER */

bool visualization::FeatureColourMapper::setFeatureParams(megamol::core::Call& call) {
	CallFeatureParams *fp = dynamic_cast<CallFeatureParams*>(&call);

    if (fp == NULL) {
        return false;
    }

	int currentFeature = this->whichFeatureSlot.Param<core::param::EnumParam>()->Value();
	bool transferFunctionNeeded = !(currentFeature == POSITION || currentFeature == VELOCITYDIR || currentFeature == ROTATIONAXIS);
	
	bool centerTransferZero = this->centerTransferZeroSlot.Param<core::param::BoolParam>()->Value();

	FeatureParams featureParams(transferFunctionNeeded, centerTransferZero, minValue, maxValue);

	fp->SetFeatureParams(featureParams);

    return true;
}