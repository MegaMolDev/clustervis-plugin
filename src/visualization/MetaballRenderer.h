/*
 * MetaballRenderer.h
 *
 * Copyright (C) 2011 by Universitaet Stuttgart (VISUS).
 * Addition and Deletion of parameters by Julia B�hnke
 * All rights reserved.
 */

#ifdef WITH_CUDA

#ifndef MMPROTEINPLUGIN_QSR2_H_INCLUDED
#define MMPROTEINPLUGIN_QSR2_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "MultiParticleClusterDataCall.h"
#include "mmcore/param/ParamSlot.h"
#include "mmcore/CallerSlot.h"
#include "mmcore/view/Renderer3DModuleDS.h"
#include "mmcore/view/AbstractCallRender3D.h"
#include "vislib/graphics/gl/GLSLShader.h"
#include "mmcore/view/CallClipPlane.h"
#include "extern/CUDAQuickSurf.h"

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "vislib/graphics/gl/IncludeAllGL.h"
#define WGL_NV_gpu_affinity
#include <cuda_gl_interop.h>

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

    /*
     * Metaball Renderer class
     */

    class MetaballRenderer : public megamol::core::view::Renderer3DModuleDS {
    public:

        /**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void)
        {
            return "MetaballRenderer";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void)
        {
            return "Offers particle-metaball renderings.";
        }

        /**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
        static bool IsAvailable(void)
        {
            return true;
        }

        /** Ctor. */
        MetaballRenderer(void);

        /** Dtor. */
        virtual ~MetaballRenderer(void);

    protected:

        /**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);

        /**
         * Implementation of 'release'.
         */
        virtual void release(void);
        
        /**
         * Calculate the density map and surface.
         * TODO
         *
         * @return 
         */
        int calcSurf(MultiParticleClusterDataCall *mpcdc, float globalRadius, float clusterIdx, bool useCycleTransferFunction,
                         int quality, float radscale, float gridspacing,
                         float isoval);

    private:

       /**********************************************************************
        * 'render'-functions
        **********************************************************************/
        
        // This function returns the best GPU (with maximum GFLOPS)
        VISLIB_FORCEINLINE int cudaUtilGetMaxGflopsDeviceId() const {
            int device_count = 0;
            cudaGetDeviceCount( &device_count );

            cudaDeviceProp device_properties;
            int max_gflops_device = 0;
            int max_gflops = 0;
    
            int current_device = 0;
            cudaGetDeviceProperties( &device_properties, current_device );
            max_gflops = device_properties.multiProcessorCount * device_properties.clockRate;
            ++current_device;

            while( current_device < device_count ) {
                cudaGetDeviceProperties( &device_properties, current_device );
                int gflops = device_properties.multiProcessorCount * device_properties.clockRate;
                if( gflops > max_gflops ) {
                    max_gflops        = gflops;
                    max_gflops_device = current_device;
                }
                ++current_device;
            }

            return max_gflops_device;
        }

        /**
         * The get capabilities callback. The module should set the members
         * of 'call' to tell the caller its capabilities.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetCapabilities( megamol::core::Call& call);

        /**
         * The get extents callback. The module should set the members of
         * 'call' to tell the caller the extents of its data (bounding boxes
         * and times).
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetExtents( megamol::core::Call& call);

        /**
         * The Open GL Render callback.
         *
         * @param call The calling call.
         * @return The return value of the function.
         */
        virtual bool Render( megamol::core::Call& call);

		/**
         * Cache the particle positions for each cluster sperately
         *
         * @param mpcdc The cluster data call
         * @return Did it work?
         */
		bool updateParticlePositions(MultiParticleClusterDataCall* mpcdc);

        void getClipData(float *clipDat, float *clipCol);

        /**********************************************************************
         * variables
         **********************************************************************/

        /** caller slot */
        megamol::core::CallerSlot dataCallerSlot;

		/** The call for Transfer function */
        core::CallerSlot tfSlot;

		/** The call for shared visualization parameters */
		core::CallerSlot sharedVisParamsSlot;
        
        /** camera information */
        vislib::SmartPtr<vislib::graphics::CameraParameters> cameraInfo;

        // QuickSurf parameters
        megamol::core::param::ParamSlot qualityParam;
        megamol::core::param::ParamSlot radscaleParam;
        megamol::core::param::ParamSlot gridspacingParam;
        megamol::core::param::ParamSlot isovalParam;
        // paramater to turn two sided lighting on and off
        megamol::core::param::ParamSlot twoSidedLightParam;

        /** shader for the spheres (raycasting view) */
        vislib::graphics::gl::GLSLShader sphereShader;
        // shader for per pixel lighting (polygonal view)
        vislib::graphics::gl::GLSLShader lightShader;
        vislib::graphics::gl::GLSLShader lightShaderOR;
        
       float isovalue;          ///< Isovalue of the surface to extract
        int numvoxels[3];        ///< Number of voxels in each dimension
        float origin[3];         ///< Origin of the volumetric map
        float xaxis[3];          ///< X-axis of the volumetric map
        float yaxis[3];          ///< Y-axis of the volumetric map
        float zaxis[3];          ///< Z-axis of the volumetric map

        void *cudaqsurf;         ///< Pointer to CUDAQuickSurf object if it exists
        int gpuvertexarray;      ///< Flag indicating if we're getting mesh from GPU 
        int gpunumverts;         ///< GPU vertex count
        float *gv;               ///< GPU vertex coordinates
        float *gn;               ///< GPU vertex normals
        float *gc;               ///< GPU vertex colors
        int gpunumfacets;        ///< GPU face count
        int   *gf;               ///< GPU facet index list

        GLuint v3f_vbo;
        GLuint n3f_vbo;
        GLuint c3f_vbo;

        cudaGraphicsResource *v3f_vbo_res;
        cudaGraphicsResource *n3f_vbo_res;
        cudaGraphicsResource *c3f_vbo_res;

        
        // CPU data
		std::vector<std::vector<float> > clusterPositions;
		std::vector<std::vector<float> > clusterBoundingBoxes; // minx, miny, minz, maxx, maxy, maxz (for each cluster)
        UINT64 numParticles;
        float currentSurfaceArea;
        float callTime;
        
        bool setCUDAGLDevice;

        /** The call for clipping plane */
        core::CallerSlot getClipPlaneSlot;

		unsigned int lastFrame;
        size_t lastHash;
    };

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif // MMPROTEINPLUGIN_QSR2_H_INCLUDED

#endif // WITH_CUDA
