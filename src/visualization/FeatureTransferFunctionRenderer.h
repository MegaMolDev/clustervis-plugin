/*
 * FeatureTransferFunctionRenderer.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_FEATURETRANSFERFUNCTIONRENDERER_H_INCLUDED
#define MEGAMOLVPC_FEATURETRANSFERFUNCTIONRENDERER_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/view/Renderer3DModule.h"
#include "mmcore/Call.h"
#include "mmcore/CallerSlot.h"
#include "CallFeatureParams.h"
#include "mmcore/param/ParamSlot.h"
#include "vislib/graphics/gl/GLSLShader.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

    /**
     * Renderer for TransferFunctions
     */
    class FeatureTransferFunctionRenderer : public core::view::Renderer3DModule {
    public:
        /**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
        static const char *ClassName(void) {
            return "FeatureTransferFunctionRenderer";
        }

        /**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
        static const char *Description(void) {
            return "Renderer for Transferfunctions.";
        }

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
        static bool IsAvailable(void) {
#ifdef _WIN32
#if defined(DEBUG) || defined(_DEBUG)
            HDC dc = ::wglGetCurrentDC();
            HGLRC rc = ::wglGetCurrentContext();
            ASSERT(dc != NULL);
            ASSERT(rc != NULL);
#endif // DEBUG || _DEBUG
#endif // _WIN32
            return vislib::graphics::gl::GLSLShader::AreExtensionsAvailable();
		}

		/** Ctor. */
        FeatureTransferFunctionRenderer(void);

        /** Dtor. */
        virtual ~FeatureTransferFunctionRenderer(void);

    protected:

		/**
         * The get capabilities callback. The module should set the members
         * of 'call' to tell the caller its capabilities.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetCapabilities(core::Call& call);
		
		/**
         * The get extents callback. The module should set the members of
         * 'call' to tell the caller the extents of its data (bounding boxes
         * and times).
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool GetExtents(core::Call& call);

		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
        virtual bool create(void);
		
        /**
         * Implementation of 'Release'.
         */
        virtual void release(void);

        /**
         * The render callback.
         *
         * @param call The calling call.
         *
         * @return The return value of the function.
         */
        virtual bool Render(core::Call& call);

	private:
		/** Rendering of transferfunction */
		void renderTransferfunction(FeatureParams params);

		/** Rendering of "axis-ball" for rotation and direction features */
		void renderAxisBall();

		/** The call for data */
        core::CallerSlot getFeatureParamsSlot;

		/** The call for Transfer function */
        core::CallerSlot tfSlot;

		/** The font colour of labels */
		core::param::ParamSlot fontColourSlot;
		
        /** A simple black-to-white transfer function texture as fallback */
        unsigned int greyTF;

		/** The transferFunction shader */
		vislib::graphics::gl::GLSLShader transferFunctionShader;
    };

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif /* MEGAMOLVPC_FEATURETRANSFERFUNCTIONRENDERER_H_INCLUDED */
