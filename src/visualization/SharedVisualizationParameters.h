/*
 * SharedVisualizationParameters.h
 *
 * Author: Julia B�hnke
 */

#ifndef MEGAMOLVPC_SHAREDVISUALIZATIONPARAMETERS_H_INCLUDED
#define MEGAMOLVPC_SHAREDVISUALIZATIONPARAMETERS_H_INCLUDED
#if (defined(_MSC_VER) && (_MSC_VER > 1000))
#pragma once
#endif /* (defined(_MSC_VER) && (_MSC_VER > 1000)) */

#include "mmcore/Module.h"
#include "mmcore/CalleeSlot.h"
#include "mmcore/param/ParamSlot.h"

namespace megamol {
namespace vis_particle_clustering {
namespace visualization {

	/**
	  * Contains all parameter for consistent visualization of particle clusters
	  */
	class SharedVisualizationParameters : public core::Module {
	public:
		/**
         * Answer the name of this module.
         *
         * @return The name of this module.
         */
		static const char *ClassName(void) {
			return "SharedVisualizationParameters";
		}

		/**
         * Answer a human readable description of this module.
         *
         * @return A human readable description of this module.
         */
		static const char *Description(void) {
			return "Module that syncs visualization parameters amongst several renderer modules.";
		}

		/**
         * Answers whether this module is available on the current system.
         *
         * @return 'true' if the module is available, 'false' otherwise.
         */
		static bool IsAvailable(void) {
			return true;
		}

		/** Ctor */
		SharedVisualizationParameters(void);

		/** Dtor */
		virtual ~SharedVisualizationParameters(void);

	protected:
		/**
         * Implementation of 'Create'.
         *
         * @return 'true' on success, 'false' otherwise.
         */
		virtual bool create (void);

		/**
         * Implementation of 'Release'.
         */
		virtual void release (void);

		bool getVisParams(megamol::core::Call& call);
		bool setVisParams(megamol::core::Call& call);

	private:
		/** Checkbox: use cycle transferfunction? */
		core::param::ParamSlot cycleTransferFunctionSlot;

		/** Index of cluster user wants to see or '-1' for all clusters */
		core::param::ParamSlot drawClusterOfIdxSlot;

		/** number of clusters */
		unsigned int numberClusters;

		/** The call for the parameters */
		core::CalleeSlot visParamsSlot;
	};

} /* end namespace visualization */
} /* end namespace vis_particle_clustering */
} /* end namespace megamol */

#endif // MEGAMOLVPC_SHAREDVISUALIZATIONPARAMETERS_H_INCLUDED