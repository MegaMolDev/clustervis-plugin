/*
 * MultiParticleDataCallExtended.cpp
 *
 * Modification of MultiParticleDataCall.cpp by Julia B�hnke
 * Copyright (C) 2009 by Universitaet Stuttgart (VISUS). 
 * Alle Rechte vorbehalten.
 */

#include "stdafx.h"
#include "MultiParticleDataCallExtended.h"
//#include "vislib/memutils.h"

using namespace megamol;

/****************************************************************************/


/*
 * vis_particle_clustering::SphericalParticles::SphericalParticles
 */
vis_particle_clustering::SphericalParticles::SphericalParticles(void)
        : colDataType(core::moldyn::SimpleSphericalParticles::COLDATA_NONE), colPtr(NULL), colStride(0), count(0),
		maxColI(1.0f), minColI(0.0f), radius(0.5f), rho(0.0f), time(0.0f), 
		particleType(0),
		vertDataType(core::moldyn::SimpleSphericalParticles::VERTDATA_NONE), vertPtr(NULL), vertStride(0), 
		extraMemberPtr(NULL), extraMemberStride(0),
		disabledNullChecks(false) {
    this->col[0] = 255;
    this->col[1] = 0;
    this->col[2] = 0;
    this->col[3] = 255;
}


/*
 * vis_particle_custering::SphericalParticles::SphericalParticles
 */
vis_particle_clustering::SphericalParticles::SphericalParticles(
        const vis_particle_clustering::SphericalParticles& src) {
    *this = src;
}


/*
 * vis_particle_custering::SphericalParticles::~SphericalParticles
 */
vis_particle_clustering::SphericalParticles::~SphericalParticles(void) {
    this->colDataType = core::moldyn::SimpleSphericalParticles::COLDATA_NONE;
    this->colPtr = NULL; // DO NOT DELETE
    this->count = 0;
    this->vertDataType = core::moldyn::SimpleSphericalParticles::VERTDATA_NONE;
    this->vertPtr = NULL; // DO NOT DELETE
	this->extraMemberPtr = NULL; // DO NOT DELETE
}


/*
 * vis_particle_custering::SphericalParticles::operator=
 */
vis_particle_clustering::SphericalParticles&
vis_particle_clustering::SphericalParticles::operator=(
        const vis_particle_clustering::SphericalParticles& rhs) {
    this->col[0] = rhs.col[0];
    this->col[1] = rhs.col[1];
    this->col[2] = rhs.col[2];
    this->col[3] = rhs.col[3];
    this->colDataType = rhs.colDataType;
    this->colPtr = rhs.colPtr;
    this->colStride = rhs.colStride;
    this->count = rhs.count;
    this->maxColI = rhs.maxColI;
    this->minColI = rhs.minColI;
    this->radius = rhs.radius;
	this->rho = rhs.rho;
	this->time = rhs.time;
    this->particleType = rhs.particleType;
    this->vertDataType = rhs.vertDataType;
    this->vertPtr = rhs.vertPtr;
    this->vertStride = rhs.vertStride;
	this->extraMemberPtr = rhs.extraMemberPtr;
	this->extraMemberStride = rhs.extraMemberStride;
	this->minVelocity = rhs.minVelocity;
	this->maxVelocity = rhs.maxVelocity;
	this->minVelocityLength = rhs.minVelocityLength;
	this->maxVelocityLength = rhs.maxVelocityLength;
	this->minRotationAngle = rhs.minRotationAngle;
	this->maxRotationAngle = rhs.maxRotationAngle;
	this->disabledNullChecks = rhs.disabledNullChecks;
    return *this;
}


/*
 * vis_particle_custering::SphericalParticles::operator==
 */
bool vis_particle_clustering::SphericalParticles::operator==(
        const vis_particle_clustering::SphericalParticles& rhs) const {
    return ((this->col[0] == rhs.col[0])
        && (this->col[1] == rhs.col[1])
        && (this->col[2] == rhs.col[2])
        && (this->col[3] == rhs.col[3])
        && (this->colDataType == rhs.colDataType)
        && (this->colPtr == rhs.colPtr)
        && (this->colStride == rhs.colStride)
        && (this->count == rhs.count)
        && (this->maxColI == rhs.maxColI)
        && (this->minColI == rhs.minColI)
        && (this->radius == rhs.radius)
        && (this->rho == rhs.rho)
        && (this->time == rhs.time)
        && (this->vertDataType == rhs.vertDataType)
        && (this->vertPtr == rhs.vertPtr)
        && (this->vertStride == rhs.vertStride)
		&& (this->extraMemberPtr == rhs.extraMemberPtr)
		&& (this->extraMemberStride == rhs.extraMemberStride))
		&& (this->minVelocity == rhs.minVelocity)
		&& (this->maxVelocity == rhs.maxVelocity)
		&& (this->minVelocityLength == rhs.minVelocityLength)
		&& (this->maxVelocityLength == rhs.maxVelocityLength)
		&& (this->minRotationAngle == rhs.minRotationAngle)
		&& (this->maxRotationAngle == rhs.maxRotationAngle);
}

/****************************************************************************/


/*
 * vis_particle_clustering::MultiParticleDataCallExtended::MultiParticleDataCallExtended
 */
vis_particle_clustering::MultiParticleDataCallExtended::MultiParticleDataCallExtended(void)
        : core::moldyn::AbstractParticleDataCall<SphericalParticles>(),
		minVelocity(3), maxVelocity(3) {
    // Intentionally empty
}


/*
 * vis_particle_clustering::MultiParticleDataCallExtended::~MultiParticleDataCallExtended
 */
vis_particle_clustering::MultiParticleDataCallExtended::~MultiParticleDataCallExtended(void) {
    // Intentionally empty
}


/*
 * vis_particle_clustering::MultiParticleDataCallExtended::operator=
 */
vis_particle_clustering::MultiParticleDataCallExtended& vis_particle_clustering::MultiParticleDataCallExtended::operator=(
        const vis_particle_clustering::MultiParticleDataCallExtended& rhs) {
			core::moldyn::AbstractParticleDataCall<SphericalParticles>::operator =(rhs);
			return *this;
}
