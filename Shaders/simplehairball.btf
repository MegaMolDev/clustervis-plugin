<?xml version="1.0" encoding="utf-8"?>
<btf type="MegaMolGLSLShader" version="1.0" namespace="simplehairball">
    <include file="common"/>

    <shader name="vertex">
        <snippet type="version">150</snippet>
        <snippet name="common::defines"/>
        <snippet name="body" type="string">
            <![CDATA[
            uniform mat4 modelview;
            uniform mat4 proj;
            
            in vec4 vertex;
            
            mat4 modelviewproj = proj*modelview; // TODO Move this to the CPU?
            mat4 modelviewInv = inverse(modelview);
            
            uniform float numberCluster;

            uniform vec3 inConsts1;
            attribute float colIdx;
            uniform sampler1D colTab;
            uniform float cycleTransferFunction;
            uniform float drawClusterOfIdx;
            
            #define MIN_COLV inConsts1.x
            #define MAX_COLV inConsts1.y
            #define COLTAB_SIZE inConsts1.z
            
            uniform bool isHalo;
            uniform vec3 camIn;
            uniform vec3 haloColour;
            
            out VertexData {
                vec4 color;
            } VertexOut;
            
            void main()
            {
                if(isHalo) {
                  VertexOut.color = vec4(haloColour, 1.0);
                } else {
                  float cid = (MAX_COLV - MIN_COLV);
                  if (cid < 0.000001) {
                     VertexOut.color = vec4(0.0);
                  } else {
                    cid = (colIdx / (numberCluster - (1.0 - cycleTransferFunction)) - MIN_COLV) / cid;
                    cid = clamp(cid, 0.0, 1.0);

                    cid *= (1.0 - 1.0 / COLTAB_SIZE);
                    cid += 0.5 / COLTAB_SIZE;

                    VertexOut.color = texture(colTab, cid);
                  }
                }
                
                if (drawClusterOfIdx != -1.0 && drawClusterOfIdx != colIdx) {
                  VertexOut.color.a = 0.0;
                }
                
                vec4 objPos = vertex;
                
                vec4 projObjPos = modelviewproj * objPos;
                projObjPos /= projObjPos.w;
                
                projObjPos -= modelview * vec4(camIn, 0.0);
                
                gl_Position = projObjPos;
            }
]]>
        </snippet>
    </shader>

    <shader name="fragment">
        <snippet type="version">150</snippet>
        <snippet name="common::defines"/>
        <snippet name="common::lighting::simple"/>
        <snippet name="body" type="string">
            <![CDATA[
            in VertexData {
              vec4 color;
            } VertexIn;
            
            void main(void) {
              if(VertexIn.color.a < 0.01) {
                discard;
              }
              gl_FragColor = VertexIn.color;
            }
]]>
        </snippet>
    </shader>

</btf>